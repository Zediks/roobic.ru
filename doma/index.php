<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Заказать загородный дом");
use Bitrix\Main\Loader;
	Loader::includeModule('iblock');

// сортировка
$houseSort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : $_COOKIE['house_sort'];
if(isset($houseSort)){ // если есть сортировка
	switch($houseSort){
		case 'sort':
			$sortBy = 'SORT';
			$sortOrder = 'ASC';
			break;
		case 'rating':
			$sortBy = 'PROPERTY_RATING';
			$sortOrder = 'DESC';
			break;
		case 'price_ask':
			$sortBy = 'PROPERTY_PRICE';
			$sortOrder = 'ASC';
			break;
		case 'price_desc':
			$sortBy = 'PROPERTY_PRICE';
			$sortOrder = 'DESC';
			break;
		default:
			$sortBy = 'SORT';
			$sortOrder = 'ASC';
			break;
	}
}else{
	$sortBy = 'SORT';
	$sortOrder = 'ASC';
}

// статичные страницы
$status404 = 'Y';
$tagApply = false;
$arPageAll = ['doma-iz-dereva','kamennye-doma','bani']; // общие разделы

if ($_REQUEST['PAGE_HOUSE_TYPE']) // материал
{
	// отображение тегов
	switch ($_REQUEST['PAGE_HOUSE_TYPE']) {
		case 'doma-iz-dereva':
			$houseTypeName = 'деревянный дом';
			$arTagShow = ['1etazh','2etazh','8x8','9x9','12x12','100m','120m','150m','modul','loft','budjet'];
			// ,'3x4','3x6','4x4','5x4','5x5','6x6','6x8','7x6','15x15','20x20'
			break;
		case 'doma-iz-brusa':
			$houseTypeName = 'дом из бруса';
			$arTagShow = ['1etazh','2etazh','6x8','50m','100m','120m','150m','modul','svainye','komb'];
			break;
		case 'dom-iz-kleennogo-brusa':
			$houseTypeName = 'дом из клееного бруса';
			$arTagShow = ['1etazh','2etazh','100m','150m','8x8','6x6'];
			break;
		case 'dom-iz-profilirovannnogo-brusa':
			$houseTypeName = 'дом из профилированного бруса';
			$arTagShow = ['1etazh','2etazh','brusa-100x100','brusa-150x150','9x9'];
			break;
		case 'dom-iz-brevna':
			$houseTypeName = 'дом из бревна';
			$arTagShow = ['1etazh','2etazh','6x6','6x8','6x9','7x7','8x8','9x9','10x10','12x12','small'];
			break;
		case 'karkasnye-doma':
			$houseTypeName = 'каркасный дом';
			$arTagShow = ['1etazh','2etazh','s-kommun','6x9','7x7','8x8','10x10','11x11','50m','60m','70m','100m','120m','modul','barnhouse','budjet'];
			// ,'3x3','4x4','5x5','5x6','6x4','6x6','6x8','8x9','12x12','svayno','mansardnye','odnoskatnye','karkasno-panelnye'
			break;
		case 'karkasno-shchitovye-doma':
			$houseTypeName = 'каркасно-щитовой дом';
			$arTagShow = ['1etazh','6x6'];
			break;
		case 'doma-iz-sip-paneley':
			$houseTypeName = 'СИП дом';
			$arTagShow = ['1etazh','8x8','9x9','10x10','100m','120m','150m','barnhouse','s-kommun'];
			// ,'4x4','5x5','6x6','6x8','7x7'
			break;
	}

	if (in_array($_REQUEST['PAGE_HOUSE_TYPE'],$arPageAll)) // общая страница раздела
	{
		switch ($_REQUEST['PAGE_HOUSE_TYPE']) {
			case 'doma-iz-dereva':
				$mainSection = 'Дома из дерева';
				break;
			case 'kamennye-doma':
				$mainSection = 'Дома из камня';
				break;

			default:
				$mainSection = 'Бани';
				break;
		}

		$arType = getElHL(11,[],['UF_ACTIVE'=>true,'UF_MAIN_SECTION'=>$mainSection],['*']);
		foreach ($arType as $key => $value)
			$_REQUEST['type'][] = $value['UF_XML_ID'];

		$newH1 = $mainSection;
		$chainURL = '/'.$_REQUEST['PAGE_HOUSE_TYPE'].'/';

		$status404 = 'N';
	}
	else
	{
	  $arType = getElHL(11,[],['UF_ACTIVE'=>true,'UF_XML_ID'=>$_REQUEST['PAGE_HOUSE_TYPE']],['*']);
		if ($arType) {

			$_REQUEST['type'][] = $_REQUEST['PAGE_HOUSE_TYPE'];

			// UF_H1 UF_TITLE UF_DESCRIPTION
			$houseType = array_values($arType)[0];
			// $houseName = mb_strtolower($houseType['UF_NAME']);
			// $houseNameOne = str_replace('дома','дом',$houseName);

			$newH1 = $houseType['UF_NAME'];
			$chainURL = '/'.$_REQUEST['PAGE_HOUSE_TYPE'].'/';

			$status404 = 'N';

			// $newH1 = ($houseType['UF_H1']) ? $houseType['UF_H1'] : 'Заказать '.$houseNameOne;
	    // $newTitle = ($houseType['UF_TITLE']) ? $houseType['UF_TITLE'] : 'Построить '.$houseNameOne;
	    // $newDesc = ($houseType['UF_DESCRIPTION']) ? $houseType['UF_DESCRIPTION'] : 'Каталог '.$houseName.'. Проверенные подрядчики с отзывами!';

			// $APPLICATION->AddChainItem($houseName,"/".$_REQUEST['PAGE_HOUSE_TYPE']."/",true);

			// if ($_REQUEST['HOUSE_SQUARE']) {
			// 	$houseSquare = $_REQUEST['HOUSE_SQUARE'].' кв.м';
			// 	$newH1 .= ' '.$houseSquare;
			// 	$newTitle .= ' '.$houseSquare;
			// 	$newDesc .= ' '.$houseSquare;
			// 	$APPLICATION->AddChainItem($houseSquare,"/".$_REQUEST['HOUSE_SQUARE']."m/",true);
			// }
			//
			// if ($_REQUEST['HOUSE_PRICE']) {
			// 	$housePrice = $_REQUEST['HOUSE_PRICE'].' млн рублей';
			// 	$newH1 .= ' за '.$housePrice;
			// 	$newTitle .= ' за '.$housePrice;
			// 	$newDesc .= ' за '.$housePrice;
			// 	$APPLICATION->AddChainItem($housePrice,"/".$_REQUEST['HOUSE_PRICE']."mln/",true);
			// }

	  } else
	    $status404 = 'Y';
	}
} else {
	global $arrFilter;
	$arrFilter['!PROPERTY']['BATH'] = 107;
}

if ($_REQUEST['HOUSE_FLOORS']) // этажность
{
	switch ($_REQUEST['HOUSE_FLOORS']) {
		case 1:
			$houseFloorName = 'одноэтажный';
			break;
		case 2:
			$houseFloorName = 'двухэтажный';
			break;

		default:
			$houseFloorName = 'многоэтажный';
			break;
	}

	$newH1 = $houseFloorName.' дома';
	$chainURL = '/doma-'.$_REQUEST['HOUSE_FLOORS'].'-etazh/';

	$status404 = 'N';

	// вложенные теги
	if ($_REQUEST['PAGE_HOUSE_TYPE']) { // тип дома

		$newH1 = 'Заказать '.$houseFloorName.' '.$houseTypeName;
		$chainURL = '/'.$_REQUEST['PAGE_HOUSE_TYPE'].'-'.$_REQUEST['HOUSE_FLOORS'].'etazh/';
		$tagApply = true;

		if (!$houseTypeName) $status404 = 'Y';
		else $status404 = 'N';
	}
}

if ($_REQUEST['HOUSE_SIZE']) // размер
{
	// вложенные теги
	if ($_REQUEST['PAGE_HOUSE_TYPE']) { // тип дома

		$newH1 = 'Заказать '.$houseTypeName.' '.$_REQUEST['HOUSE_SIZE']. ' м';
		$chainURL = '/'.$_REQUEST['PAGE_HOUSE_TYPE'].'-'.$_REQUEST['HOUSE_SIZE'].'/';
		$tagApply = true;

		if (!$houseTypeName) $status404 = 'Y';
		else $status404 = 'N';
	}
}

if ($_REQUEST['HOUSE_SQUARE']) // площадь
{
	$_REQUEST['area'] = $_REQUEST['HOUSE_SQUARE'];

	$newH1 = 'Дома '.$_REQUEST['HOUSE_SQUARE'].' м2';
	$chainURL = '/doma-'.$_REQUEST['HOUSE_SQUARE'].'m/';

	$status404 = 'N';

	// вложенные теги
	if ($_REQUEST['PAGE_HOUSE_TYPE']) { // тип дома

		if ($_REQUEST['HOUSE_SQUARE'] == 'small') {
			$_REQUEST['area'] = 'do-100';
			$chainURL = '/'.$_REQUEST['PAGE_HOUSE_TYPE'].'-small/';
		}
		// if ($_REQUEST['HOUSE_SQUARE'] == 'do-100') {
		// 	$chainURL = '/'.$_REQUEST['PAGE_HOUSE_TYPE'].'-do100m/';
		// }

		$newH1 = 'Заказать '.$houseTypeName.' '.$_REQUEST['HOUSE_SQUARE']. ' кв.м';
		$tagApply = true;

		if (!$houseTypeName) $status404 = 'Y';
		else $status404 = 'N';
	}
}

if ($_REQUEST['HOUSE_PRICE']) // площадь
{
	$_REQUEST['price'] = $_REQUEST['HOUSE_PRICE'];

	$newH1 = 'Дома по цене '.$_REQUEST['HOUSE_PRICE'].' млн. рублей';
	$chainURL = '/doma-'.$_REQUEST['HOUSE_PRICE'].'mln/';

	$status404 = 'N';
}

if ($_REQUEST['HOUSE_BEDROOMS']) // спальни
{
	$arrFilter['PROPERTY']['CNT_BEDROOM'] = $_REQUEST['HOUSE_BEDROOMS'];
	$h1TXT = ($_REQUEST['HOUSE_BEDROOMS']==1) ? 'спальней' : 'спальнями';
	$txtURL = ($_REQUEST['HOUSE_BEDROOMS']<=4) ? 'spalni' : 'spalen';

	$newH1 = 'Дома с '.$_REQUEST['HOUSE_BEDROOMS'].' '.$h1TXT;
	$chainURL = '/dom-'.$_REQUEST['HOUSE_BEDROOMS'].'-'.$txtURL.'/';

	$status404 = 'N';
}

if ($_REQUEST['HOUSE_BATHROOM']) // спальни
{
	$arrFilter['PROPERTY']['CNT_BATHROOM'] = $_REQUEST['HOUSE_BATHROOM'];
	$txtURL = ($_REQUEST['HOUSE_BATHROOM'] == 1) ? 'sanuzel' : 'sanuzla';

	$bathroomText = ($_REQUEST['HOUSE_BATHROOM'] == 1) ? 'санузлом' : 'санузлами';
	$newH1 = 'Дома с '.$_REQUEST['HOUSE_BATHROOM'].' '.$bathroomText;
	$chainURL = '/doma-'.$_REQUEST['HOUSE_BATHROOM'].$txtURL.'/';

	$status404 = 'N';
}

if ($_REQUEST['HOUSE_FINISHING']) // отделка
{
	switch ($_REQUEST['HOUSE_FINISHING']) {
		case 'bez-otdelki':
			$houseFinishingID = 15;
			$finishingText = 'Дома без отделки (коробка, теплый контур)';
			break;
		case 's-inzhener-kom':
			$houseFinishingID = 16;
			$finishingText = 'Дома с инженерными коммуникациями';
			break;
		case 's-chernovoy-otdelkoy':
			$houseFinishingID = 23;
			$finishingText = 'Дома с черновой отделкой';
			break;
		case 's-chistivoi-otdelkoy':
			$houseFinishingID = 17;
			$finishingText = 'Дома с чистовой отделкой (White box)';
			break;
		case 'pod-kluch':
			$houseFinishingID = 18;
			$finishingText = 'Дома с отделкой под ключ';
			break;

		default:
			$houseFinishingID = 'all';
			break;
	}
	$_REQUEST['finishing'] = $houseFinishingID;
	$newH1 = $finishingText;
	$chainURL = '/doma-'.$_REQUEST['HOUSE_FINISHING'].'/';

	$status404 = 'N';

	// вложенные теги
	if ($_REQUEST['PAGE_HOUSE_TYPE']) { // тип дома
		switch ($_REQUEST['HOUSE_FINISHING']) {
			case 's-inzhener-kom':
				$chainURL= '/'.$_REQUEST['PAGE_HOUSE_TYPE'].'-s-kommun/';
				break;

			default:
				$houseTypeName = false;
				break;
		}
		$tagApply = true;

		if (!$houseTypeName) $status404 = 'Y';
		else $status404 = 'N';
	}
}

if ($_REQUEST['MATERIAL_ROOF']) // Материал кровли
{
	switch ($_REQUEST['MATERIAL_ROOF']) {
		case 'doma-s-kryshei-iz-metallocherepici':
			$propCode = 'metallocherepitsa';
			$newH1 = 'Дома с крышей из металлочерепицы';
			break;
		case 'doma-s-kryshei-iz-profnastila':
			$propCode = 'profnastil';
			$newH1 = 'Дома с крышей из профнастила';
			break;
		case 'doma-s-kryshei-iz-ondulina':
			$propCode = 'ondulin';
			$newH1 = 'Дома с крышей из ондулина';
			break;
		case 'doma-s-kryshei-iz-shifera':
			$propCode = 'shifer';
			$newH1 = 'Дома с крышей из шифера';
			break;
		case 'doma-s-falcevoi-kryshei':
			$propCode = 'faltsevaya-krovlya';
			$newH1 = 'Дома с фальцевой крышей';
			break;
		case 'doma-s-kryshei-iz-cherepici':
			$propCode = ['keramicheskaya-cherepitsa','tsementno-peschanaya-cherepitsa','bitumnaya-cherepitsa'];
			$newH1 = 'Дома с крышей из черепицы';
			break;
		case 'doma-s-kryshei-iz-keramicheskoy-cherepici':
			$propCode = 'keramicheskaya-cherepitsa';
			$newH1 = 'Дома с крышей из керамической черепицы';
			break;
		case 'doma-s-kryshei-iz-cem-pesch-cherepici':
			$propCode = 'tsementno-peschanaya-cherepitsa';
			$newH1 = 'Дома с крышей из цементно-песчаной черепицы';
			break;
		case 'doma-s-kryshei-iz-gibkoy-cherepici':
			$propCode = 'bitumnaya-cherepitsa';
			$newH1 = 'Дома с крышей из гибкой черепицы';
			break;

		default:
			break;
	}
	$arrFilter['PROPERTY']['MATERIAL_ROOF'] = $propCode;
	$chainURL = '/doma-'.$_REQUEST['MATERIAL_ROOF'].'/';

	$status404 = 'N';
}

$arPropNew = ['STYLE','FORM','TYPE_ROOF','ANNEX','FINISHING_FACADE','INSULATION','FOUNDATION_LIST','CLASS','SIZE_BEAM'];
foreach ($arPropNew as $propCode)
{
	if ($_REQUEST[$propCode])
	{
		$rsPropertyEnum = \CIBlockPropertyEnum::GetList([],["IBLOCK_ID"=>3, "CODE"=>$propCode]);
		while($arPropertyEnum = $rsPropertyEnum->Fetch())
			$arProp[$arPropertyEnum['XML_ID']] = [
				'ID' => $arPropertyEnum['ID'],
				'NAME' => $arPropertyEnum['VALUE']
			];

		$arrFilter['PROPERTY'][$propCode] = $arProp[$_REQUEST[$propCode]]['ID'];

		$newH1 = $arProp[$_REQUEST[$propCode]]['NAME'];
		$chainURL = '/'.$_REQUEST[$propCode].'/';

		$status404 = 'N';

		// вложенные теги
		if ($_REQUEST['PAGE_HOUSE_TYPE']) { // тип дома
			switch ($_REQUEST[$propCode]) {
				case 'modulnye-doma':
					$chainURL= '/'.$_REQUEST['PAGE_HOUSE_TYPE'].'-modul/';
					break;
				case 'dom-loft':
					$chainURL= '/'.$_REQUEST['PAGE_HOUSE_TYPE'].'-loft/';
					break;
				case 'doma-econom':
					$chainURL= '/'.$_REQUEST['PAGE_HOUSE_TYPE'].'-budjet/';
					break;
				case 'doma-s-svainym-fundamentom':
					$chainURL= '/'.$_REQUEST['PAGE_HOUSE_TYPE'].'-svainye/';
					break;
				case 'doma-komb':
					$chainURL= '/'.$_REQUEST['PAGE_HOUSE_TYPE'].'-komb/';
				case '100x100':
					$chainURL= '/'.$_REQUEST['PAGE_HOUSE_TYPE'].'-100x100/';
					break;
				case '150x150':
					$chainURL= '/'.$_REQUEST['PAGE_HOUSE_TYPE'].'-150x150/';
					break;
				case 'doma-so-svayno-karkasnym-fundamentom':
					$chainURL= '/'.$_REQUEST['PAGE_HOUSE_TYPE'].'-svayno/';
					break;
				case 'doma-s-mansardoy':
					$chainURL= '/'.$_REQUEST['PAGE_HOUSE_TYPE'].'-mansardnye/';;
					break;
				case 'doma-s-odnoskatnoi-kryshei':
					$chainURL= '/'.$_REQUEST['PAGE_HOUSE_TYPE'].'-odnoskatnye/';;
					break;
				case 'dom-barnhouse':
					$chainURL= '/'.$_REQUEST['PAGE_HOUSE_TYPE'].'-barnhouse/';;
					break;

				default:
					$houseTypeName = false;
					break;
			}
			$tagApply = true;

			if (!$houseTypeName) $status404 = 'Y';
			else $status404 = 'N';
		}
	}
}

// if ($_REQUEST['STYLE']) // Стиль, разновидность
// {
// 	$rsPropertyEnum = \CIBlockPropertyEnum::GetList([],["IBLOCK_ID"=>3, "CODE"=>"STYLE"]);
// 	while($arPropertyEnum = $rsPropertyEnum->Fetch())
// 		$arProp[$arPropertyEnum['XML_ID']] = [
// 			'ID' => $arPropertyEnum['ID'],
// 			'NAME' => $arPropertyEnum['VALUE']
// 		];
//
// 	$arrFilter['PROPERTY']['STYLE'] = $arProp[$_REQUEST['STYLE']]['ID'];
//
// 	$newH1 = $arProp[$_REQUEST['STYLE']]['NAME'];
// 	$chainURL = '/'.$_REQUEST['STYLE'].'/';
//
// 	$status404 = 'N';
// }
//
// if ($_REQUEST['FORM']) // Форма
// {
// 	$rsPropertyEnum = \CIBlockPropertyEnum::GetList([],["IBLOCK_ID"=>3, "CODE"=>"FORM"]);
// 	while($arPropertyEnum = $rsPropertyEnum->Fetch())
// 		$arProp[$arPropertyEnum['XML_ID']] = [
// 			'ID' => $arPropertyEnum['ID'],
// 			'NAME' => $arPropertyEnum['VALUE']
// 		];
//
// 	$arrFilter['PROPERTY']['FORM'] = $arProp[$_REQUEST['FORM']]['ID'];
//
// 	$newH1 = $arProp[$_REQUEST['FORM']]['NAME'];
// 	$chainURL = '/'.$_REQUEST['FORM'].'/';
//
// 	$status404 = 'N';
// }

if ($status404 == 'N')
{
	if (!$tagApply) {
		$newH1 = str_replace(['Дома','дома','Дом','дом'],'',$newH1);
		$newH1 = 'Заказать дом '.trim($newH1);
	}

	$titleEnd = ' - строительство под ключ проверенными застройщиками: проекты и цены | Москва и Московская область';
	$descEnd = ' - строительство под ключ по проектам лучших строительных компаний в Москве и Московской области ★ Только проверенные застройщики ★ Описание, отзывы, цены ★★★ Stroiman.ru';
	$newTitle = $newH1.$titleEnd;
	$newDesc = $newH1.$descEnd;

	$arElHL = array_values(getElHL(18,[],['UF_XML_ID'=>$chainURL],['*'])); // dump($arElHL);

	if ($arElHL[0]['UF_H1']) $newH1 = $arElHL[0]['UF_H1'];

	if ($arElHL[0]['UF_TITLE']) $newTitle = $arElHL[0]['UF_TITLE'];
	elseif($arElHL[0]['UF_H1']) $newTitle = $newH1.$titleEnd;

	if ($arElHL[0]['UF_DESCRIPTION']) $newDesc = $arElHL[0]['UF_DESCRIPTION'];
	elseif($arElHL[0]['UF_H1']) $newDesc = $newH1.$descEnd;

	if ($arElHL[0]['UF_TEXT']) $textSEO = $arElHL[0]['UF_TEXT'];

	// $metaInfo = getMetaInfo($arrFilter); dump($metaInfo);

	$APPLICATION->SetTitle($newH1);
	$APPLICATION->SetPageProperty("title", $newTitle);
	$APPLICATION->SetPageProperty("description", $newDesc);
	$APPLICATION->AddChainItem($newH1,$chainURL,true);
}
?>
<main class="main--catalog">
	<div class="container-fluid">
		<?$APPLICATION->IncludeComponent(
      "bitrix:breadcrumb",
      "breadcrumb",
      array(
        "PATH" => "",
        "SITE_ID" => "s1",
        "START_FROM" => "0",
        "COMPONENT_TEMPLATE" => "breadcrumb"
      ),
      false
    );?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:news",
			"doma",
			array(
				"ADD_ELEMENT_CHAIN" => "Y",
				"ADD_SECTIONS_CHAIN" => "Y",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_ADDITIONAL" => "",
				"AJAX_OPTION_HISTORY" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"BROWSER_TITLE" => "-",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "Y",
				"CACHE_TIME" => "36000000",
				"CACHE_TYPE" => "A",
				"CHECK_DATES" => "Y",
				"DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
				"DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
				"DETAIL_DISPLAY_TOP_PAGER" => "N",
				"DETAIL_FIELD_CODE" => array(
					0 => "",
					1 => "",
				),
				"DETAIL_PAGER_SHOW_ALL" => "Y",
				"DETAIL_PAGER_TEMPLATE" => "",
				"DETAIL_PAGER_TITLE" => "Страница",
				"DETAIL_PROPERTY_CODE" => array(
					0 => "MATERIAL_ROOF",
					1 => "MATERIAL_WALL",
					2 => "COMPANY",
					3 => "",
				),
				"DETAIL_SET_CANONICAL_URL" => "Y",
				"DISPLAY_BOTTOM_PAGER" => "Y",
				"DISPLAY_DATE" => "N",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "N",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"DISPLAY_TOP_PAGER" => "N",
				"FILE_404" => "",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"IBLOCK_ID" => "3",
				"IBLOCK_TYPE" => "content",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
				"LIST_FIELD_CODE" => array(
					0 => "",
					1 => "",
				),
				"LIST_PROPERTY_CODE" => array(
					0 => "CNT_BATHROOM",
					1 => "CNT_BEDROOM",
					2 => "COMPANY",
					3 => "TYPE",
					4 => "",
				),
				"MESSAGE_404" => "",
				"META_DESCRIPTION" => "-",
				"META_KEYWORDS" => "-",
				"NEWS_COUNT" => "20",
				"PAGER_BASE_LINK_ENABLE" => "N",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_TEMPLATE" => "roobic",
				"PAGER_TITLE" => "Новости",
				"PREVIEW_TRUNCATE_LEN" => "",
				"SEF_FOLDER" => "/doma/",
				"SEF_MODE" => "Y",
				"SET_LAST_MODIFIED" => "Y",
				"SET_STATUS_404" => $status404,
				"SET_TITLE" => "N",
				"SHOW_404" => $status404,
				"SORT_BY1" => $sortBy,
				"SORT_BY2" => "SHOW_COUNTER",
				"SORT_ORDER1" => $sortOrder,
				"SORT_ORDER2" => "DESC",
				"HOUSE_SORT" => $houseSort,
				"STRICT_SECTION_CHECK" => "N",
				"USE_CATEGORIES" => "N",
				"USE_FILTER" => "Y",
				"USE_PERMISSIONS" => "N",
				"USE_RATING" => "N",
				"USE_RSS" => "N",
				"USE_SEARCH" => "N",
				"USE_SHARE" => "N",
				"COMPONENT_TEMPLATE" => "doma",
				"FILTER_NAME" => "arrFilter",
				"FILTER_FIELD_CODE" => array(
					0 => "",
					1 => "",
				),
				"FILTER_PROPERTY_CODE" => array(
					0 => "AREA",
					1 => "TYPE",
					2 => "PRICE",
					3 => "",
				),
				"SEF_URL_TEMPLATES" => array(
					"news" => "",
					"section" => "",
					"detail" => "#ELEMENT_CODE#/",
				),
				"TEXT_SEO" => $textSEO,
				"TAG_SHOW" => $arTagShow
			),
			false
		);?>
</main>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
