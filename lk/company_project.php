<?define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Карточка проекта");
$APPLICATION->AddChainItem('Карточка проекта');?>
<main class="main--catalog">
  <div class="container-fluid">
    <?$APPLICATION->IncludeComponent(
			"bitrix:breadcrumb",
			"breadcrumb",
			array(
				"PATH" => "",
				"SITE_ID" => "s1",
				"START_FROM" => "0",
				"COMPONENT_TEMPLATE" => "breadcrumb"
			),
			false
		);?>
    <div class="row">
      <div class="col-md-12">
        <h1><?$APPLICATION->ShowTitle(false)?></h1>
      </div>
    </div>
    <?$APPLICATION->IncludeComponent(
    	"roobic:lk.companyProject",
    	"",
    	Array(
    		"CACHE_TIME" => "36000000",
    		"CACHE_TYPE" => "A"
    	)
    );?>
  </div>
</main>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
