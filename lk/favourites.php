<?define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Избранное");
$APPLICATION->AddChainItem('Избранное');
$templateFavourites = (CSite::InGroup([5])) ? 'companyFavourites' : 'clientFavourites';?>
<main class="main--catalog">
  <div class="container-fluid">
    <?$APPLICATION->IncludeComponent(
			"bitrix:breadcrumb",
			"breadcrumb",
			array(
				"PATH" => "",
				"SITE_ID" => "s1",
				"START_FROM" => "0",
				"COMPONENT_TEMPLATE" => "breadcrumb"
			),
			false
		);?>
    <h1 class="d-none"><?$APPLICATION->ShowTitle(false)?></h1>
    <div class="row">
      <?$APPLICATION->IncludeComponent(
      	"roobic:lk.leftBlock",
      	"",
      	Array(
      		"CACHE_TIME" => "36000000",
      		"CACHE_TYPE" => "A"
      	)
      );?>
      <?$APPLICATION->IncludeComponent(
      	"roobic:lk.".$templateFavourites,
      	"",
      	Array(
      		"CACHE_TIME" => "36000000",
      		"CACHE_TYPE" => "A"
      	)
      );?>
    </div>
  </div>
</main>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
