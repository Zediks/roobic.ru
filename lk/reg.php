<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Регистрация");?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.register",
	"roobic",
	array(
		"AUTH" => "N",
		"REQUIRED_FIELDS" => array(
			0 => "NAME",
		),
		"SET_TITLE" => "N",
		"SHOW_FIELDS" => array(
			0 => "NAME",
			1 => "PERSONAL_ICQ",
			2 => "PERSONAL_PHONE",
		),
		"SUCCESS_PAGE" => "/lk/?from=reg",
		"USER_PROPERTY" => array(
			0 => "UF_PASSWORD",
		),
		"USER_PROPERTY_NAME" => "",
		"USE_BACKURL" => "Y",
		"COMPONENT_TEMPLATE" => "roobic"
	),
	false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
