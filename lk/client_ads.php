<?define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Мои объявления");
$APPLICATION->AddChainItem('Объявления');?>
<main class="main--catalog">
    <div class="container-fluid">
        <?$APPLICATION->IncludeComponent(
                "bitrix:breadcrumb",
                "breadcrumb",
                array(
                    "PATH" => "",
                    "SITE_ID" => "s1",
                    "START_FROM" => "0",
                    "COMPONENT_TEMPLATE" => "breadcrumb"
                ),
                false
            );?>
        <div class="row">
            <div class="col-md-12 col-lg-4">
                <?$APPLICATION->IncludeComponent(
                    "roobic:lk.leftBlock",
                    "",
                    Array(
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A"
                    )
                );?>
            </div>
            <div class="col-md-12 col-lg-8 d-lg-block">
                <?$APPLICATION->IncludeComponent(
                "roobic:lk.clientAds",
                "",
                Array(
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A"
                )
            );?>
            </div>
        </div>
    </div>
</main>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
