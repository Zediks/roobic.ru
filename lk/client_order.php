<?define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Разместить заказ");
$APPLICATION->AddChainItem('Заказ');?>
<main class="main--catalog">
  <div class="container-fluid">
    <?$APPLICATION->IncludeComponent(
			"bitrix:breadcrumb",
			"breadcrumb",
			array(
				"PATH" => "",
				"SITE_ID" => "s1",
				"START_FROM" => "0",
				"COMPONENT_TEMPLATE" => "breadcrumb"
			),
			false
		);?>
    <div class="row">
      <div class="col-md-12">
        <h1>Разместить заказ</h1>
      </div>
    </div>
    <?$APPLICATION->IncludeComponent(
    	"roobic:lk.clientOrder",
    	"",
    	Array(
    		"CACHE_TIME" => "36000000",
    		"CACHE_TYPE" => "A"
    	)
    );?>
  </div>
</main>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
