<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Заказать баню");

// сортировка
$houseSort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : $_COOKIE['house_sort'];
if(isset($houseSort)){ // если есть сортировка
	switch($houseSort){
		case 'sort':
			$sortBy = 'SORT';
			$sortOrder = 'ASC';
			break;
		case 'rating':
			$sortBy = 'PROPERTY_RATING';
			$sortOrder = 'DESC';
			break;
		case 'price_ask':
			$sortBy = 'PROPERTY_PRICE';
			$sortOrder = 'ASC';
			break;
		case 'price_desc':
			$sortBy = 'PROPERTY_PRICE';
			$sortOrder = 'DESC';
			break;
		default:
			$sortBy = 'SORT';
			$sortOrder = 'ASC';
			break;
	}
}else{
	$sortBy = 'SORT';
	$sortOrder = 'ASC';
}

// статичные страницы
$status404 = 'Y';
$arPageAll = ['doma-iz-dereva','kamennye-doma','bani']; // общие разделы

if ($_REQUEST['PAGE_HOUSE_TYPE']) // материал
{
	if (in_array($_REQUEST['PAGE_HOUSE_TYPE'],$arPageAll)) // общая страница раздела
	{
		switch ($_REQUEST['PAGE_HOUSE_TYPE']) {
			case 'doma-iz-dereva':
				$mainSection = 'Дома из дерева';
				break;
			case 'kamennye-doma':
				$mainSection = 'Дома из камня';
				break;

			default:
				$mainSection = 'Бани';
				break;
		}

		$arType = getElHL(11,[],['UF_ACTIVE'=>true,'UF_MAIN_SECTION'=>$mainSection],['*']);
		foreach ($arType as $key => $value)
			$_REQUEST['type'][] = $value['UF_XML_ID'];

		$newH1 = $mainSection;
		$chainURL = '/'.$_REQUEST['PAGE_HOUSE_TYPE'].'/';

		$status404 = 'N';
	}
	else
	{
	  $arType = getElHL(11,[],['UF_ACTIVE'=>true,'UF_XML_ID'=>$_REQUEST['PAGE_HOUSE_TYPE']],['*']);
		if ($arType) {

			$_REQUEST['type'][] = $_REQUEST['PAGE_HOUSE_TYPE'];

			// UF_H1 UF_TITLE UF_DESCRIPTION
			$houseType = array_values($arType)[0];
			// $houseName = mb_strtolower($houseType['UF_NAME']);
			// $houseNameOne = str_replace('бани','баня',$houseName);

			$newH1 = $houseType['UF_NAME'];
			$chainURL = '/'.$_REQUEST['PAGE_HOUSE_TYPE'].'/';

			$status404 = 'N';

			// $newH1 = ($houseType['UF_H1']) ? $houseType['UF_H1'] : 'Заказать '.$houseNameOne;
	    // $newTitle = ($houseType['UF_TITLE']) ? $houseType['UF_TITLE'] : 'Построить '.$houseNameOne;
	    // $newDesc = ($houseType['UF_DESCRIPTION']) ? $houseType['UF_DESCRIPTION'] : 'Каталог '.$houseName.'. Проверенные подрядчики с отзывами!';

			// $APPLICATION->AddChainItem($houseName,"/".$_REQUEST['PAGE_HOUSE_TYPE']."/",true);

			// if ($_REQUEST['HOUSE_SQUARE']) {
			// 	$houseSquare = $_REQUEST['HOUSE_SQUARE'].' кв.м';
			// 	$newH1 .= ' '.$houseSquare;
			// 	$newTitle .= ' '.$houseSquare;
			// 	$newDesc .= ' '.$houseSquare;
			// 	$APPLICATION->AddChainItem($houseSquare,"/".$_REQUEST['HOUSE_SQUARE']."m/",true);
			// }
			//
			// if ($_REQUEST['HOUSE_PRICE']) {
			// 	$housePrice = $_REQUEST['HOUSE_PRICE'].' млн рублей';
			// 	$newH1 .= ' за '.$housePrice;
			// 	$newTitle .= ' за '.$housePrice;
			// 	$newDesc .= ' за '.$housePrice;
			// 	$APPLICATION->AddChainItem($housePrice,"/".$_REQUEST['HOUSE_PRICE']."mln/",true);
			// }

	  } else
	    $status404 = 'Y';
	}
} else {
	global $arrFilter;
	$arrFilter['PROPERTY']['BATH'] = 107;
}

if ($_REQUEST['HOUSE_FLOORS']) // этажность
{
	switch ($_REQUEST['HOUSE_FLOORS']) {
		case 1:
			$houseFloorName = 'Одноэтажные';
			break;
		case 2:
			$houseFloorName = 'Двухэтажные';
			break;

		default:
			$houseFloorName = 'Многоэтажные';
			break;
	}

	$newH1 = $houseFloorName.' бани';
	$chainURL = '/bani-'.$_REQUEST['HOUSE_FLOORS'].'-etazh/';

	$status404 = 'N';
}

if ($_REQUEST['HOUSE_SQUARE']) // площадь
{
	$_REQUEST['area'] = $_REQUEST['HOUSE_SQUARE'];

	$newH1 = 'Бани '.$_REQUEST['HOUSE_SQUARE'].' м2';
	$chainURL = '/bani-'.$_REQUEST['HOUSE_SQUARE'].'m/';

	$status404 = 'N';
}

if ($_REQUEST['HOUSE_PRICE']) // площадь
{
	$_REQUEST['price'] = $_REQUEST['HOUSE_PRICE'];

	$newH1 = 'Бани стоимостью '.$_REQUEST['HOUSE_PRICE'].' млн. рублей';
	$chainURL = '/bani-'.$_REQUEST['HOUSE_PRICE'].'mln/';

	$status404 = 'N';
}

if ($_REQUEST['HOUSE_BEDROOMS']) // спальни
{
	$arrFilter['PROPERTY']['CNT_BEDROOM'] = $_REQUEST['HOUSE_BEDROOMS'];
	$h1TXT = ($_REQUEST['HOUSE_BEDROOMS']==1) ? 'спальней' : 'спальнями';
	$txtURL = ($_REQUEST['HOUSE_BEDROOMS']<=4) ? 'spalni' : 'spalen';

	$newH1 = 'Бани с '.$_REQUEST['HOUSE_BEDROOMS'].' '.$h1TXT;
	$chainURL = '/banya-'.$_REQUEST['HOUSE_BEDROOMS'].'-'.$txtURL.'/';

	$status404 = 'N';
}

if ($_REQUEST['HOUSE_BATHROOM']) // спальни
{
	$arrFilter['PROPERTY']['CNT_BATHROOM'] = $_REQUEST['HOUSE_BATHROOM'];
	$txtURL = ($_REQUEST['HOUSE_BATHROOM'] == 1) ? 'sanuzel' : 'sanuzla';

	$bathroomText = ($_REQUEST['HOUSE_BATHROOM'] == 1) ? 'санузлом' : 'санузлами';
	$newH1 = 'Бани с '.$_REQUEST['HOUSE_BATHROOM'].' '.$bathroomText;
	$chainURL = '/bani-'.$_REQUEST['HOUSE_BATHROOM'].$txtURL.'/';

	$status404 = 'N';
}

if ($_REQUEST['HOUSE_FINISHING']) // отделка
{
	switch ($_REQUEST['HOUSE_FINISHING']) {
		case 'bez-otdelki':
			$houseFinishingID = 15;
			$finishingText = 'Бани без отделки (коробка, теплый контур)';
			break;
		case 's-inzhener-kom':
			$houseFinishingID = 16;
			$finishingText = 'Бани с инженерными коммуникациями';
			break;
		case 's-chernovoy-otdelkoy':
			$houseFinishingID = 23;
			$finishingText = 'Бани с черновой отделкой';
			break;
		case 's-chistivoi-otdelkoy':
			$houseFinishingID = 17;
			$finishingText = 'Бани с чистовой отделкой (White box)';
			break;
		case 'pod-kluch':
			$houseFinishingID = 18;
			$finishingText = 'Бани с отделкой под ключ';
			break;

		default:
			$houseFinishingID = 'all';
			break;
	}
	$_REQUEST['finishing'] = $houseFinishingID;
	$newH1 = $finishingText;
	$chainURL = '/bani-'.$_REQUEST['HOUSE_FINISHING'].'/';

	$status404 = 'N';
}

if ($status404 == 'N')
{
	$newH1 = str_replace(['Бани','бани','Баня','баня'],'',$newH1);
	$newH1 = 'Заказать баню '.trim($newH1);
	$titleEnd = ' - строительство под ключ проверенными застройщиками: проекты и цены | Москва и Московская область';
	$descEnd = ' - строительство под ключ по проектам лучших строительных компаний в Москве и Московской области ★ Только проверенные застройщики ★ Описание, отзывы, цены ★★★ Stroiman.ru';
	$newTitle = $newH1.$titleEnd;
	$newDesc = $newH1.$descEnd;

	$arElHL = array_values(getElHL(18,[],['UF_XML_ID'=>$chainURL],['*'])); // dump($arElHL);
	if ($arElHL[0]['UF_H1']) $newH1 = $arElHL[0]['UF_H1'];

	if ($arElHL[0]['UF_TITLE']) $newTitle = $arElHL[0]['UF_TITLE'];
	elseif($arElHL[0]['UF_H1']) $newTitle = $newH1.$titleEnd;

	if ($arElHL[0]['UF_DESCRIPTION']) $newDesc = $arElHL[0]['UF_DESCRIPTION'];
	elseif($arElHL[0]['UF_H1']) $newDesc = $newH1.$descEnd;

	if ($arElHL[0]['UF_TEXT']) $textSEO = $arElHL[0]['UF_TEXT'];

	$APPLICATION->SetTitle($newH1);
	$APPLICATION->SetPageProperty("title", $newTitle);
	$APPLICATION->SetPageProperty("description", $newDesc);
	$APPLICATION->AddChainItem($newH1,$chainURL,true);
}
?>
<main class="main--catalog">
	<div class="container-fluid">
		<?$APPLICATION->IncludeComponent(
      "bitrix:breadcrumb",
      "breadcrumb",
      array(
        "PATH" => "",
        "SITE_ID" => "s1",
        "START_FROM" => "0",
        "COMPONENT_TEMPLATE" => "breadcrumb"
      ),
      false
    );?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:news",
			"doma",
			array(
				"ADD_ELEMENT_CHAIN" => "Y",
				"ADD_SECTIONS_CHAIN" => "Y",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_ADDITIONAL" => "",
				"AJAX_OPTION_HISTORY" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"BROWSER_TITLE" => "-",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "Y",
				"CACHE_TIME" => "36000000",
				"CACHE_TYPE" => "A",
				"CHECK_DATES" => "Y",
				"DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
				"DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
				"DETAIL_DISPLAY_TOP_PAGER" => "N",
				"DETAIL_FIELD_CODE" => array(
					0 => "",
					1 => "",
				),
				"DETAIL_PAGER_SHOW_ALL" => "Y",
				"DETAIL_PAGER_TEMPLATE" => "",
				"DETAIL_PAGER_TITLE" => "Страница",
				"DETAIL_PROPERTY_CODE" => array(
					0 => "MATERIAL_ROOF",
					1 => "MATERIAL_WALL",
					2 => "COMPANY",
					3 => "",
				),
				"DETAIL_SET_CANONICAL_URL" => "Y",
				"DISPLAY_BOTTOM_PAGER" => "Y",
				"DISPLAY_DATE" => "N",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "N",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"DISPLAY_TOP_PAGER" => "N",
				"FILE_404" => "",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"IBLOCK_ID" => "3",
				"IBLOCK_TYPE" => "content",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
				"LIST_FIELD_CODE" => array(
					0 => "",
					1 => "",
				),
				"LIST_PROPERTY_CODE" => array(
					0 => "CNT_BATHROOM",
					1 => "CNT_BEDROOM",
					2 => "COMPANY",
					3 => "TYPE",
					4 => "",
				),
				"MESSAGE_404" => "",
				"META_DESCRIPTION" => "-",
				"META_KEYWORDS" => "-",
				"NEWS_COUNT" => "20",
				"PAGER_BASE_LINK_ENABLE" => "N",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_TEMPLATE" => "roobic",
				"PAGER_TITLE" => "Новости",
				"PREVIEW_TRUNCATE_LEN" => "",
				"SEF_FOLDER" => "/bani/",
				"SEF_MODE" => "Y",
				"SET_LAST_MODIFIED" => "Y",
				"SET_STATUS_404" => $status404,
				"SET_TITLE" => "N",
				"SHOW_404" => $status404,
				"SORT_BY1" => $sortBy,
				"SORT_BY2" => "SHOW_COUNTER",
				"SORT_ORDER1" => $sortOrder,
				"SORT_ORDER2" => "DESC",
				"HOUSE_SORT" => $houseSort,
				"STRICT_SECTION_CHECK" => "N",
				"USE_CATEGORIES" => "N",
				"USE_FILTER" => "Y",
				"USE_PERMISSIONS" => "N",
				"USE_RATING" => "N",
				"USE_RSS" => "N",
				"USE_SEARCH" => "N",
				"USE_SHARE" => "N",
				"COMPONENT_TEMPLATE" => "doma",
				"FILTER_NAME" => "arrFilter",
				"FILTER_FIELD_CODE" => array(
					0 => "",
					1 => "",
				),
				"FILTER_PROPERTY_CODE" => array(
					0 => "AREA",
					1 => "TYPE",
					2 => "PRICE",
					3 => "",
				),
				"SEF_URL_TEMPLATES" => array(
					"news" => "",
					"section" => "",
					"detail" => "#ELEMENT_CODE#/",
				),
				"TEXT_SEO" => $textSEO
			),
			false
		);?>
</main>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
