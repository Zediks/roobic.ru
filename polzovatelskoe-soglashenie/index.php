<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Пользовательское соглашение");
?>

<main class="main--catalog">
  <div class="container-fluid">
    <?$APPLICATION->IncludeComponent(
      "bitrix:breadcrumb",
      "breadcrumb",
      array(
        "PATH" => "",
        "SITE_ID" => "s1",
        "START_FROM" => "0",
        "COMPONENT_TEMPLATE" => "breadcrumb"
      ),
      false
    );?>
    <h1><?$APPLICATION->ShowTitle(false)?></h1>
  </div>
</main>
<div class="container my-5">
	<div class="row">
		<div class="col-12">
				<div class="textPage">
					<p>Отношения между физическим лицом (далее Пользователь) и маркетплейсом строительных услуг  https://stroiman.ru (далее – Компания) по использованию сайта <a target="_blank" href="https://stroiman.ru">https://stroiman.ru</a> (далее – Сайт) регулируются данным Пользовательским соглашением (далее – Соглашение) и Согласием на обработку персональных данных, которые являются Публичной офертой. Используя Сайт Пользователь подтверждает, что ознакомился с условиями Соглашения и Согласия на обработку персональных данных и безоговорочно принимает их.</p>
					<p>Условия соглашения:</p>
					<p>Пользователь:</p>
					<ul>
						<li>Осознает, что вся информация, опубликованная на Сайте, не является публичной офертой и носит информационный характер. Мнение редакции портала может не совпадать с мнением авторов и интервьюируемых.</li>
						<li>Согласен с использованием на сайте cookie для хранения персональных предпочтений и настроек пользователя, отслеживания состояния сеанса доступа пользователя и ведения статистики, в том числе для всех сервисов третьих лиц, используемых на Сайте.</li>
						<li>Обязуется не публиковать любые персональные данные, в том числе адреса электронной почты, номера телефонов, почтовые адреса, ссылки и т.д.</li>
						<li>Обязуется соблюдать законодательство РФ, в том числе не публиковать оскорбления, клевету; материалы, демонстрирующие или пропагандирующие жестокость (в том числе террор); материалы, оскорбляющие человеческое достоинство. Кроме того Пользователь обязуется не вводить других пользователей в заблуждение.</li>
						<li>Обязуется не использовать Сайт для распространения рекламных материалов или материалов незаконной пропаганды.</li>
						<li>Отчуждает все исключительные права на материалы, добавленные на Сайт или переданные Компании любым другим способом, в том числе по электронной почте в пользу Компании. В том числе дает согласие на использование его изображения в соответствии со ст.152.1 ГК РФ без какого-либо вознаграждения на неограниченный срок.</li>
					</ul>
					<p>Компания:</p>
					<ul>
						<li>Может вносить изменения в Соглашение по своему смотрению. Все изменения вступают в силу с момента публикации их на Сайте.</li>
						<li>Оставляет за собой право редактировать или удалять материалы, опубликованные на Сайте, в т.ч. Пользователем, если они не соответствуют условиям Соглашения.</li>
						<li>По своему усмотрению может отказать Пользователю в доступе к Сайту.</li>
						<li>Имеет право использовать материалы, переданные Пользователем в целях разработки рекламных материалов, размещения материалов на сайтах партнеров Компании, в том числе передавать их третьим лицам без получения дополнительного согласия Пользователя.</li>
					</ul>
					<p>Отказ от ответственности:</p>
					<p>Компания не несет ответственность за содержание, соответствие действительности материалов, опубликованных Пользователями; за причинение вреда, любых убытков любым лицам, которые возникли при пользовании сервисом Сайта; за нарушение Пользователем авторских и иных прав третьих лиц путем опубликования материалов, несоответствующих действующему законодательству (в том числе авторскому), добавленных Пользователем на Сайт или переданных им Компании иным способом.</p>
				</div>
		</div>
	</div>	
</div>					
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
