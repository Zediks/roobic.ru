<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");?>

<main class="main--catalog">
  <div class="container-fluid">
    <?$APPLICATION->IncludeComponent(
      "bitrix:breadcrumb",
      "breadcrumb",
      array(
        "PATH" => "",
        "SITE_ID" => "s1",
        "START_FROM" => "0",
        "COMPONENT_TEMPLATE" => "breadcrumb"
      ),
      false
    );?>
    <h1><?$APPLICATION->ShowTitle(false)?></h1>
    <p>Сервис по поиску строительных компаний и домов</p>
    <p>Адрес: г. Москва, м. Калужская, ул. Бутлерова 17, БЦ Neo Geo</p>
    <p>E-mail: welcome@stroiman.ru</p>
  </div>
</main>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
