<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('highloadblock');
use Bitrix\Highloadblock as HL, Bitrix\Main\Entity;

$params = [
  "max_len" => "100", // обрезает символьный код до 100 символов
  "change_case" => "L", // буквы преобразуются к нижнему регистру
  "replace_space" => "-", // меняем пробелы на тире
  "replace_other" => "-", // меняем левые символы на тире
  "delete_repeat_replace" => "true", // удаляем повторяющиеся символы
  "use_google" => "false", // отключаем использование google
];

// получим категории
$arElHL = getElHL(17,[],[],['*']);
foreach ($arElHL as $value)
  $arCategory[$value['UF_NAME']] = $value['UF_XML_ID'];
// dump($arCategory);

// получим регионы
$arElHL = getElHL(16,[],[],['*']);
foreach ($arElHL as $value)
  $arRegion[$value['UF_NAME']] = [
    'ID' => $value['UF_ID'],
    'XML_ID' => $value['UF_XML_ID'],
  ];

// $str = file_get_contents("construction_2.csv");
$str = file_get_contents("Kwork_work.csv");
$arr = explode("\n",$str); // dump($arr);

function trim_and_del ($val){
  $val = trim($val); // убираем пробелы
  $val = str_replace('"','',$val); // убираем кавычки
  return $val;
}

$params = [
  "max_len" => "100", // обрезает символьный код до 100 символов
  "change_case" => "L", // буквы преобразуются к нижнему регистру
  "replace_space" => "-", // меняем пробелы на тире
  "replace_other" => "-", // меняем левые символы на тире
  "delete_repeat_replace" => "true", // удаляем повторяющиеся символы
  "use_google" => "false", // отключаем использование google
];

$iblockID = 4;

$arOrder = ['SORT'=>'ASC'];
$arFilter = ['IBLOCK_ID'=>$iblockID,'ACTIVE'=>'Y'];
$arSelect = ['ID','NAME','CODE'];
$rsSections = CIBlockSection::GetList($arOrder,$arFilter,false,$arSelect,false);
while ($arSections = $rsSections->Fetch())
	$arSection[$arSections['CODE']] = $arSections['ID'];

$arRegionsOnly = ['Москва','Московская обл','Санкт-Петербург','Ленинградская обл'];

$i=0; $j=0;
foreach($arr as $item){
  $i++;

  $arItem = array_map("trim_and_del",explode(";",$item)); // dump($arItem);
  if (is_numeric($arItem[0]))
  {
    // if ($i < 113) {
      // dump($arItem);
      if ($arItem[0] == 'constructionId') continue;
      if (!in_array($arItem[3],$arRegionsOnly)) continue;

      $elementName = $arItem[1];

      $elementText = $arItem[2];
      $arElementText = explode('<div>',$elementText);
      $elementText = $arElementText[0];

      $PROP['STROIMAN_ID'] = $arItem[0];
      $PROP['ADDRESS'] = ($arItem[4]) ? $arItem[3].', '.$arItem[4] : $arItem[3];
      $PROP['NAME'] = $arItem[7];
      $PROP['PHONE'] = ($arItem[9]) ? $arItem[8].', '.$arItem[9] : $arItem[8];
      $PROP['REGION'] = $arRegion[$arItem[3]]['XML_ID'];
      $PROP['CATEGORY'] = $arCategory[$arItem[10]];

      $el = new CIBlockElement;
      $arLoadProductArray = Array(
        "IBLOCK_ID"      => $iblockID,
        "IBLOCK_SECTION_ID" => $arSection[$PROP['REGION']],
        "PROPERTY_VALUES" => $PROP,
        "NAME"           => $elementName,
        "CODE"           => $arCategory[$arItem[10]].'-'.$arItem[0],
        "DETAIL_TEXT"   => $elementText,
      );

    	// dump($arLoadProductArray);
      // if (!$el->Add($arLoadProductArray))
      //   echo "Error: ".$i." - ".$el->LAST_ERROR.'<br>';
      // else
        $j++;
    }
  // }
}
echo 'Всего: '.$j;
