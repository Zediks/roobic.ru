<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('highloadblock');
use Bitrix\Highloadblock as HL, Bitrix\Main\Entity;

// echo 'import<br>';

$params = [
  "max_len" => "100", // обрезает символьный код до 100 символов
  "change_case" => "L", // буквы преобразуются к нижнему регистру
  "replace_space" => "-", // меняем пробелы на тире
  "replace_other" => "-", // меняем левые символы на тире
  "delete_repeat_replace" => "true", // удаляем повторяющиеся символы
  "use_google" => "false", // отключаем использование google
];

$str = file_get_contents("import2.csv");
$arr = explode("\n",$str); // dump($arr);

function trim_and_del ($val){
  $val = trim($val); // убираем пробелы
  $val = str_replace('"','',$val); // убираем кавычки
  return $val;
}

// св-во фундамент
// $rsPropertyEnum = \CIBlockPropertyEnum::GetList([],["IBLOCK_ID"=>3, "CODE"=>"FOUNDATION_LIST"]);
// while($arPropertyEnum = $rsPropertyEnum->Fetch())
//   $arSizeNew[$arPropertyEnum['VALUE']] = $arPropertyEnum['ID'];
// dump($arSizeNew);
//
// $arOrder = ['SORT'=>'ASC'];
// $arFilter = ['IBLOCK_ID'=>3,'ACTIVE'=>'Y'];
// $arSelect = ['ID','NAME','PROPERTY_FOUNDATION'];
// $rsElements = CIBlockElement::GetList($arOrder,$arFilter,false,false,$arSelect);
// while ($arElement = $rsElements->Fetch()) {
//   // dump($arElement);
//   $foundation = trim($arElement['PROPERTY_FOUNDATION_VALUE']);
//   if ($foundation) {
//     switch ($foundation) {
//       case 'Свайный':
//         $propSizeID = 132;
//         break;
//       case 'Свайно-винтовой':
//       case 'Свайно-винтовой фундамент':
//       case 'Свайно-винтовые':
//         $propSizeID = 133;
//         break;
//       case 'Свайно-ростверковый':
//       case 'Свайный с ростверком':
//       case 'Свайный-ростверковый':
//         $propSizeID = 136;
//         break;
//       case 'Железобетонная плита':
//       case 'Железобетонный':
//       case 'Монолитная железо-бетонная плита':
//       case 'Монолитная железобетонная плита':
//       case 'Монолит':
//       case 'Монолитная ж/б плита':
//         $propSizeID = 134;
//         break;
//       case 'Ростверковый':
//       case 'Монолитный ж/б ростверк':
//         $propSizeID = 135;
//         break;
//       case 'Плитный':
//         $propSizeID = 131;
//         break;
//       case 'Ленточный':
//         $propSizeID = 130;
//         break;
//
//       default:
//         $propSizeID = 0;
//         break;
//     }
//
//     // if ($arElement['ID'] == 77901) {
//     //   dump($propSizeID);
//     //   dump($arElement);
//       // CIBlockElement::SetPropertyValues($arElement['ID'], 3, $propSizeID, "FOUNDATION_LIST");
//     // }
//   }
// }

// св-во размер
// $rsPropertyEnum = \CIBlockPropertyEnum::GetList([],["IBLOCK_ID"=>3, "CODE"=>"SIZE_LIST"]);
// while($arPropertyEnum = $rsPropertyEnum->Fetch())
//   $arSizeNew[$arPropertyEnum['XML_ID']] = $arPropertyEnum['ID'];
// // dump($arSizeNew);
//
// $arOrder = ['SORT'=>'ASC'];
// $arFilter = ['IBLOCK_ID'=>3,'ACTIVE'=>'Y'];
// $arSelect = ['ID','NAME','PROPERTY_SIZE'];
// $rsElements = CIBlockElement::GetList($arOrder,$arFilter,false,false,$arSelect);
// while ($arElement = $rsElements->Fetch()) {
//   if ($arElement['PROPERTY_SIZE_VALUE']) {
//     $sizeOld = str_replace(['X','х','Х','*','×'],'x',$arElement['PROPERTY_SIZE_VALUE']);
//     $sizeOld = str_replace(',','.',$sizeOld);
//     $arSizeOld = explode('x',$sizeOld);
//     $sizeClear = round(trim($arSizeOld[0])).'x'.round(trim($arSizeOld[1]));
//     // echo $arElement['PROPERTY_SIZE_VALUE'].' - '.$sizeClear.'<br>';
//     $propSizeID = $arSizeNew[$sizeClear];
//     if ($propSizeID) {
//       // if ($arElement['ID'] == 41)
//         // CIBlockElement::SetPropertyValues($arElement['ID'], 3, $propSizeID, "SIZE_LIST");
//     } else
//       echo $arElement['ID'].';'.$arElement['NAME'].';'.$sizeClear.'<br>';
//   }
// }

// заполнить св-ва типа список
// $PROPERTY_ID = 96;
// foreach ($arr as $value) {
//   $PROPERTY_XML_ID = $value;
//   $PROPERTY_VALUE = $value;
//   $ibpenum = new CIBlockPropertyEnum;
//   if($PropID = $ibpenum->Add(Array('PROPERTY_ID'=>$PROPERTY_ID, 'XML_ID'=>$PROPERTY_XML_ID, 'VALUE'=>$PROPERTY_VALUE)))
//   	echo 'New ID:'.$PropID;
// }

// $i = 2960;
// foreach ($arr as $value) {
//   $i += 10;
//   if ($value) {
//     echo $i." => "."[<br>".
//       "qqqq'CONDITION' => '#^/".$value."/(\\?(.*))?#',<br>".
//       "qqqq'RULE' => 'MATERIAL_ROOF=".$value."',<br>".
//       "qqqq'ID' => '',<br>".
//       "qqqq'PATH' => '/doma/index.php',<br>".
//       "qqqq'SORT' => 100,<br>"
//     ."],<br>";
//   }
// }

// foreach($arr as $item){
//
//   $arItem = array_map("trim_and_del",explode(";",$item));  dump($arItem);
//
//   if($item)
//   {
//     $hlblock_id = 17; // id HL
//   	$hlblock = HL\HighloadBlockTable::getById($hlblock_id)->fetch();
//   	$entity = HL\HighloadBlockTable::compileEntity($hlblock);
//   	$entity_data_class = $entity->getDataClass();
//   	$entity_table_name = $hlblock['TABLE_NAME'];
//   	$sTableID = 'tbl_'.$entity_table_name;
//     $data = [
//       "UF_NAME" => $arItem[0],
//       // "UF_ID" => $arItem[0],
//       "UF_XML_ID" => CUtil::translit($arItem[0], "ru", $params),
//       "UF_SORT" => 100,
//       // "UF_MAIN_SECTION" => 55
//     ];  dump($data);
//
//     // $result = $entity_data_class::add($data);
//   }
//
// }
