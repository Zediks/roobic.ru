<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('highloadblock');
use Bitrix\Highloadblock as HL, Bitrix\Main\Entity;

$params = [
  "max_len" => "100", // обрезает символьный код до 100 символов
  "change_case" => "L", // буквы преобразуются к нижнему регистру
  "replace_space" => "-", // меняем пробелы на тире
  "replace_other" => "-", // меняем левые символы на тире
  "delete_repeat_replace" => "true", // удаляем повторяющиеся символы
  "use_google" => "false", // отключаем использование google
];

$str = file_get_contents("announcement_1.csv");
$arr = explode("\n",$str); // dump($arr);

function trim_and_del ($val){
  $val = trim($val); // убираем пробелы
  $val = str_replace('"','',$val); // убираем кавычки
  return $val;
}

$params = [
  "max_len" => "100", // обрезает символьный код до 100 символов
  "change_case" => "L", // буквы преобразуются к нижнему регистру
  "replace_space" => "-", // меняем пробелы на тире
  "replace_other" => "-", // меняем левые символы на тире
  "delete_repeat_replace" => "true", // удаляем повторяющиеся символы
  "use_google" => "false", // отключаем использование google
];

// получим категории
$arElHL = getElHL(15,[],[],['*']);
foreach ($arElHL as $value)
  $arCategory[$value['UF_NAME'].$value['UF_NAME']] = $value['UF_XML_ID'];

// получим регионы
$arElHL = getElHL(16,[],[],['*']);
foreach ($arElHL as $value)
  $arRegion[$value['UF_NAME']] = [
    'ID' => $value['UF_ID'],
    'XML_ID' => $value['UF_XML_ID'],
  ];

$iblockID = 5;

$arOrder = ['SORT'=>'ASC'];
$arFilter = ['IBLOCK_ID'=>$iblockID,'ACTIVE'=>'Y'];
$arSelect = ['ID','NAME','CODE'];
$rsSections = CIBlockSection::GetList($arOrder,$arFilter,false,$arSelect,false);
while ($arSections = $rsSections->Fetch())
	$arSection[$arSections['CODE']] = $arSections['ID'];

$arRegionsOnly = ['Москва','Московская обл','Санкт-Петербург','Ленинградская обл'];

$i=0; $j=0;
foreach($arr as $item){
  $i++;

  $arItem = array_map("trim_and_del",explode(";",$item)); // dump($arItem);

  // if ($i < 10)
  // {
    // dump($arItem);
    if ($arItem[0] == 'announcementHref') continue;

    $arUrl = explode('/view/',$arItem[0]);
    $elementName = $arItem[1];
    $arPhoto = explode(',',$arItem[5]);
    $arRegionEl = explode(',',$arItem[8]);
    if (!in_array($arRegionEl[0],$arRegionsOnly)) continue;

    $PROP['STROIMAN_ID'] = $arUrl[1];
    $PROP['ADDRESS'] = $arItem[8];
    $PROP['NAME'] = $arItem[9];
    $PROP['PHONE'] = $arItem[10];
    $PROP['CATEGORY'] = $arCategory[$arItem[3]];
    $PROP['REGION'] = $arRegion[$arRegionEl[0]]['XML_ID'];
    // foreach ($arPhoto as $value) {
    //   $value = trim($value);
    //   if($value != 'https://stroiman.ru/img/dummy_86.png')
    //     $PROP['PHOTOS'][] = \CFile::MakeFileArray($value);
    // }


    $el = new CIBlockElement;
    $arLoadProductArray = Array(
      "IBLOCK_ID"      => $iblockID,
      "IBLOCK_SECTION_ID" => $arSection[$PROP['REGION']],
      "PROPERTY_VALUES" => $PROP,
      "NAME"           => $elementName,
      "CODE"           => $arCategory[$arItem[3]].'-'.$arUrl[1],
      "DETAIL_TEXT"   => strip_tags(preg_replace('@((https?://)?([-\w]+\.[-\w\.]+)+\w(:\d+)?(/([-\w/_\.]*(\?\S+)?)?)*)@', '',$arItem[2])),
    );

    unset($arPhoto); unset($PROP);

  	// dump($arLoadProductArray);
    // if (!$el->Add($arLoadProductArray))
    //   echo "Error: ".$i." - ".$el->LAST_ERROR.'<br>';
    // else
      $j++;
  // }
}
echo 'Всего: '.$j;
