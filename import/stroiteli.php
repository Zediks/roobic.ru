<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('highloadblock');
use Bitrix\Highloadblock as HL, Bitrix\Main\Entity;

$params = [
  "max_len" => "100", // обрезает символьный код до 100 символов
  "change_case" => "L", // буквы преобразуются к нижнему регистру
  "replace_space" => "-", // меняем пробелы на тире
  "replace_other" => "-", // меняем левые символы на тире
  "delete_repeat_replace" => "true", // удаляем повторяющиеся символы
  "use_google" => "false", // отключаем использование google
];

$str = file_get_contents("master_2.csv");
$arr = explode("\n",$str); // dump($arr);

function trim_and_del ($val){
  $val = trim($val); // убираем пробелы
  $val = str_replace('"','',$val); // убираем кавычки
  return $val;
}

$params = [
  "max_len" => "100", // обрезает символьный код до 100 символов
  "change_case" => "L", // буквы преобразуются к нижнему регистру
  "replace_space" => "-", // меняем пробелы на тире
  "replace_other" => "-", // меняем левые символы на тире
  "delete_repeat_replace" => "true", // удаляем повторяющиеся символы
  "use_google" => "false", // отключаем использование google
];

// получим категории
$arElHL = getElHL(15,[],[],['*']);
foreach ($arElHL as $value)
  $arCategory[$value['UF_NAME']] = $value['UF_XML_ID'];
// dump($arCategory);

$i=0;
foreach($arr as $item){
  $i++;

  $arItem = array_map("trim_and_del",explode(";",$item)); // dump($arItem);

  if ($i < 13)
  {
    dump($arItem);
    if ($arItem[0] == 'masterHref') continue;

    $iblockID = 2;
    $arUrl = explode('/card/',$arItem[0]);
    $elementName = $arItem[1];
    $arPhoto = explode(',',$arItem[8]);

    $arCategoryF = explode(',',$arItem[3]); // dump($arCategoryF);

    foreach ($arCategoryF as $key => $value) {
      if ($value == 'Монтаж окон') {
        $arCategoryF[$key] = 'Монтаж окон, дверей, жалюзи';
        unset($arCategoryF[$key+1]); unset($arCategoryF[$key+2]);
      } elseif ($value == 'Вода') {
        $arCategoryF[$key] = 'Вода, тепло, сантехника, канализация';
        unset($arCategoryF[$key+1]); unset($arCategoryF[$key+2]); unset($arCategoryF[$key+3]);
      } elseif ($value == 'Заборы') {
        $arCategoryF[$key] = 'Заборы, ограждения, ворота';
        unset($arCategoryF[$key+1]); unset($arCategoryF[$key+2]);
      } elseif ($value == 'Окна') {
        $arCategoryF[$key] = 'Окна, двери, жалюзи';
        unset($arCategoryF[$key+1]); unset($arCategoryF[$key+2]);
      }
    }

    foreach ($arCategoryF as $key => $value) {
      if (mb_substr($value, 0, 1) == ' ') {
        $arCategoryF[$key-1] .= ','.$value;
        unset($arCategoryF[$key]);
      }
    }

    foreach ($arCategoryF as $key => $value)
      $arCategoryF[$key] = $arCategory[$value];

    $PROP['STROIMAN_ID'] = $arUrl[1];
    $PROP['ADDRESS'] = ($arItem[5]) ? $arItem[5] : $arItem[4];
    $PROP['NAME'] = $arItem[9];
    $PROP['PHONE'] = $arItem[10];
    $PROP['CATEGORY'] = $arCategoryF;
    if ($arPhoto[0])
      foreach ($arPhoto as $value)
        $PROP['PHOTO'][] = \CFile::MakeFileArray(trim($value));

    $el = new CIBlockElement;
    $arLoadProductArray = Array(
      "IBLOCK_ID"       => $iblockID,
      "PROPERTY_VALUES" => $PROP,
      "NAME"            => $elementName,
      "CODE"            => CUtil::translit($elementName, "ru", $params),
      "DETAIL_TEXT"     => $arItem[2],
      "DETAIL_PICTURE"  => \CFile::MakeFileArray(trim($arPhoto[0]))
    );

    unset($arPhoto); unset($arCategoryF); unset($PROP);

  	dump($arLoadProductArray);
    // $el->Add($arLoadProductArray);
  }
}
