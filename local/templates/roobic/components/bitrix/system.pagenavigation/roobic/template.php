<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

$this->setFrameMode(true);

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
?>

<div class="pagintaion">
	<?if($arResult["bDescPageNumbering"] === true):?>

		<?if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
			<?if($arResult["bSavePage"]):?>
				<a class="pagintaion__link pagintaion__link--prev" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>">#</a>
				<a class="pagintaion__link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>">1</a>
			<?else:?>
				<?if (($arResult["NavPageNomer"]+1) == $arResult["NavPageCount"]):?>
					<a class="pagintaion__link pagintaion__link--prev" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">#</a>
				<?else:?>
					<a class="pagintaion__link pagintaion__link--prev" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>">#</a>
				<?endif?>
				<a class="pagintaion__link" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">1</a>
			<?endif?>
		<?else:?>
				<a class="pagintaion__link pagintaion__link--disabled pagintaion__link--prev" href="">#</a>
				<a class="pagintaion__link pagintaion__link--disabled pagintaion__link--active" href="">1</a>
		<?endif?>

		<?
		$arResult["nStartPage"]--;
		while($arResult["nStartPage"] >= $arResult["nEndPage"]+1):
		?>
			<?$NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1;?>

			<?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
				<a class="pagintaion__link pagintaion__link--disabled pagintaion__link--active" href=""><?=$NavRecordGroupPrint?></a>
			<?else:?>
				<a class="pagintaion__link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><?=$NavRecordGroupPrint?></a>
			<?endif?>

			<?$arResult["nStartPage"]--?>
		<?endwhile?>

		<?if ($arResult["NavPageNomer"] > 1):?>
			<?if($arResult["NavPageCount"] > 1):?>
				<a class="pagintaion__link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1"><?=$arResult["NavPageCount"]?></a>
			<?endif?>
				<a class="pagintaion__link pagintaion__link--next" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>">$</a>
		<?else:?>
			<?if($arResult["NavPageCount"] > 1):?>
				<a class="pagintaion__link pagintaion__link--disabled pagintaion__link--active" href=""><?=$arResult["NavPageCount"]?></a>
			<?endif?>
				<a class="pagintaion__link pagintaion__link--disabled pagintaion__link--next" href="">$</a>
		<?endif?>

	<?else:?>

		<?if ($arResult["NavPageNomer"] > 1):?>
			<?if($arResult["bSavePage"]):?>
				<a class="pagintaion__link pagintaion__link--prev" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>">#</a>
				<a class="pagintaion__link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1">1</a>
			<?else:?>
				<?if ($arResult["NavPageNomer"] > 2):?>
					<a class="pagintaion__link pagintaion__link--prev" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>">#</a>
				<?else:?>
					<a class="pagintaion__link pagintaion__link--prev" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">#</a>
				<?endif?>
				<a class="pagintaion__link" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">1</a>
			<?endif?>
		<?else:?>
				<a class="pagintaion__link pagintaion__link--disabled pagintaion__link--prev" href="">#</a>
				<a class="pagintaion__link pagintaion__link--disabled pagintaion__link--active" href="">1</a>
		<?endif?>

		<?
		$arResult["nStartPage"]++;
		while($arResult["nStartPage"] <= $arResult["nEndPage"]-1):
		?>
			<?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
				<a class="pagintaion__link pagintaion__link--disabled pagintaion__link--active" href=""><?=$arResult["nStartPage"]?></a>
			<?else:?>
				<a class="pagintaion__link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><?=$arResult["nStartPage"]?></a>
			<?endif?>
			<?$arResult["nStartPage"]++?>
		<?endwhile?>

		<?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
			<?if($arResult["NavPageCount"] > 1):?>
				<a class="pagintaion__link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>"><?=$arResult["NavPageCount"]?></a>
			<?endif?>
				<a class="pagintaion__link pagintaion__link--next" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>">$</a>
		<?else:?>
			<?if($arResult["NavPageCount"] > 1):?>
				<a class="pagintaion__link pagintaion__link--disabled pagintaion__link--active" href=""><?=$arResult["NavPageCount"]?></a>
			<?endif?>
				<a class="pagintaion__link pagintaion__link--disabled pagintaion__link--next" href="">$</a>
		<?endif?>
	<?endif?>

	<?if ($arResult["bShowAll"]):?>
		<?if ($arResult["NavShowAll"]):?>
				<a class="pagintaion__link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>SHOWALL_<?=$arResult["NavNum"]?>=0" rel="nofollow">. . .</a>
		<?else:?>
				<a class="pagintaion__link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>SHOWALL_<?=$arResult["NavNum"]?>=1" rel="nofollow">. . .</a>
		<?endif?>
	<?endif?>
</div>
