<?
// dump($arResult['PROPERTIES']);

global $USER;
if ($USER->IsAuthorized())
{
  if($arResult['PROPERTIES']['USER']['VALUE'])
  {
    $by = 'id'; $sort = 'asc'; // поле сортировки
    $filter = ['ID' => $arResult['PROPERTIES']['USER']['VALUE']];
    $arParams = [ // наши поля
      'FIELDS' => ['ID','NAME','EMAIL','PERSONAL_PHONE'],
    ];
    $rsUsers = CUser::GetList($by, $sort, $filter, $arParams);
    $arResult['USER'] = $rsUsers->Fetch();
  }
  else
  {
    $arResult['USER']['NAME'] = $arResult['PROPERTIES']['NAME']['VALUE'];
    $arResult['USER']['EMAIL'] = $arResult['PROPERTIES']['EMAIL']['VALUE'];
    $arResult['USER']['PERSONAL_PHONE'] = $arResult['PROPERTIES']['PHONE']['VALUE'];
  }
}
else
{
  $arResult['USER']['NAME'] = 'Имя заказчика скрыто';
  $arResult['USER']['EMAIL'] = 'ХХХ@ХХХХ.XXX';
  $arResult['USER']['PERSONAL_PHONE'] = '+7 9ХХ-ХХХ-ХХ-ХХ';
  $arResult['USER']['CLASS'] = 'not-auth';
}

// // получим Услуги и Цены
// $priceList = getElHL(4,[],['UF_AD'=>$arResult['ID']],['ID','UF_SERVICE','UF_PRICE']);
// foreach ($priceList as $value)
//   $arIdsService[] = $value['UF_SERVICE'];
//
// // получим услуги
// $arService = getElHL(3,[],['ID'=>$arIdsService],['ID','UF_NAME','UF_MAIN_SECTION']);
//
// foreach ($priceList as $value)
//   $arResult['PRICE_LIST'][] = $arService[$value['UF_SERVICE']]['UF_NAME'].' от '.$value['UF_PRICE'].' ₽/м2';
//
// $arService = array_values($arService);
//
// $arResult['ICON'] = getIconMainSection($arService[0]['UF_MAIN_SECTION']);

// Категории стройман
$arCategory = getElHL(15,[],['UF_XML_ID'=>$arResult['PROPERTIES']['CATEGORY']['VALUE']],['ID','UF_TYPE','UF_NAME']);
$arCategory = array_values($arCategory);

$arResult['ICON'] = getIconMainSection($arCategory[0]['UF_TYPE']);
$arResult['PRICE_LIST'][] = $arCategory[0]['UF_NAME'];
