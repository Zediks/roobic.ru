<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

// dump($_COOKIE); // разбираем куки
if(isset($_COOKIE['comparison_ads'])){
	$arComparison = explode('-',$_COOKIE['comparison_ads']);
}
if(isset($_COOKIE['favorites_ads'])){
	$arFavorites = explode('-',$_COOKIE['favorites_ads']);
}
$comparison = (in_array($arResult['ID'],$arComparison)) ? 'Y' : 'N';
$favorites = (in_array($arResult['ID'],$arFavorites)) ? 'Y' : 'N';
$comp_active = ($comparison == 'Y') ? 'control--favorites--active' : '';
$fav_active = ($favorites == 'Y') ? 'control--favorites--active' : '';
$comp_text = ($comparison != 'Y') ? 'Добавить к сравнению' : 'Удалить из сравнения';
$fav_text = ($favorites != 'Y') ? 'Добавить в избранное' : 'Удалить из избранного';
// dump($arResult);
?>
<div class="order">
	<div class="order__top">
		<div class="orders__item-place"><?=$arResult['PROPERTIES']['ADDRESS']['VALUE']?></div>
		<div class="orders__item-favourites control control--favorites <?=$fav_active?>">
			<div class="control__icon js-add-favorites" data-id="<?=$arResult['ID']?>" data-cookie="favorites_ads">
				<svg class="icon icon-favorites" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 46.9" style="enable-background:new 0 0 50 46.9;" xml:space="preserve">
					<path d="M46.4,4.4l-0.3-0.3C43.8,1.3,40.4,0,36.7,0s-6.8,1.6-9.4,4.2L25,6.8l-2.3-2.6C20.3,1.3,16.9,0,13.3,0S6.5,1.6,3.9,4.2L3.6,4.4C1.3,7,0,10.7,0,14.6s1.3,7.3,3.6,10.2l20.1,21.6c0.3,0.3,1.3,0.5,1.3,0.5s1-0.3,1.3-0.5l20.1-21.6c2.3-2.6,3.6-6.3,3.6-10.2C50,10.7,48.7,7,46.4,4.4z" />
				</svg>
			</div>
			<div class="control__container">
				<div class="control__main"><?=$fav_text?></div>
			</div>
		</div>
		<div class="control control--share js-control-share orders__item-share">
			<div class="control__icon">
				<svg id="icon-share" class="icon icon-favorites" viewBox="0 0 50 50">
					<path d="M34.3,22.1l-5.4-5.4c-0.1-0.1-0.3-0.2-0.5-0.2s-0.3,0.1-0.5,0.2c-0.1,0.1-0.2,0.3-0.2,0.5v2.7h-2.4
																	c-5,0-8.1,1.4-9.3,4.3c-0.4,0.9-0.6,2.1-0.6,3.5c0,1.2,0.4,2.8,1.3,4.8c0,0,0.1,0.1,0.1,0.3c0.1,0.1,0.1,0.2,0.1,0.3
																	c0,0.1,0.1,0.2,0.1,0.2c0.1,0.1,0.2,0.2,0.3,0.2c0.1,0,0.2,0,0.2-0.1c0.1-0.1,0.1-0.2,0.1-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.2
																	c0-0.5-0.1-0.9-0.1-1.3c0-0.7,0.1-1.4,0.2-1.9c0.1-0.6,0.3-1.1,0.5-1.5c0.2-0.4,0.5-0.8,0.8-1.1c0.3-0.3,0.7-0.5,1.1-0.7
																	c0.4-0.2,0.9-0.3,1.4-0.5c0.5-0.1,1.1-0.2,1.6-0.2c0.5,0,1.2-0.1,1.9-0.1h2.4v2.7c0,0.2,0.1,0.3,0.2,0.5c0.1,0.1,0.3,0.2,0.5,0.2
																	c0.2,0,0.3-0.1,0.5-0.2l5.4-5.4c0.1-0.1,0.2-0.3,0.2-0.5C34.5,22.4,34.4,22.3,34.3,22.1z"></path>
				</svg>
			</div>
			<div class="control__container">
				<div class="control__main">
					<span>Поделиться</span>
					<div class="ya-share2 social-list" data-curtain data-size="l" data-shape="round" data-services="vkontakte,odnoklassniki,telegram,twitter,viber,whatsapp"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="order__head">
		<div class="order__head-ico d-none d-md-block d-lg-block">
			<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/<?=$arResult['ICON']?>">
		</div>
		<div class="order__head-title">
			<div class="order__name"><?=$arResult['NAME']?></div>
			<div class="order__head-ico d-block d-md-none">
				<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/<?=$arResult['ICON']?>">
			</div>
			<div class="order__cat"><?=implode(' | ',$arResult['PRICE_LIST'])?></div>
		</div>
	</div>
	<div class="order__description">
		<p><?=$arResult["DETAIL_TEXT"]?></p>
		<div class="order-slider swiper-container">
			<div class="swiper-wrapper">
				<?foreach ($arResult['PROPERTIES']['PHOTOS']['VALUE'] as $photo) {
					$photoRes = CFile::ResizeImageGet($photo, array('width' => 288, 'height' => 216), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);?>
						<div class="swiper-slide"><a data-fancybox="gallery" href="<?=CFile::GetPath($photo)?>"><img class="lazyload" src="" data-src="<?=$photoRes['src']?>" alt="Изображение"></a></div>
				<?}?>
			</div>
			<div class="js-slider-img swiper-button-next" data-icon="5"></div>
			<div class="js-slider-img swiper-button-prev" data-icon="4"></div>
		</div>
	</div>
	<div class="order__contacts <?=$arResult['USER']['CLASS']?>">
		<div class="order__contacts-title">
			Контакты:
		</div>
		<div class="order__contacts-block">
			<p><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/lock.svg" alt=""><?=$arResult['USER']['NAME']?></p>
			<p>
				<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/lock.svg" alt="">
				<a href="tel:<?=str_replace(['(',')',' ','-'],'',$arResult['USER']['PERSONAL_PHONE'])?>"><?=$arResult['USER']['PERSONAL_PHONE']?></a>
			</p>
			<p><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/lock.svg" alt=""><?=$arResult['USER']['EMAIL']?></p>
		</div>
	</div>
	<div class="order__contacts-auth <?=$arResult['USER']['CLASS']?>">
		<a href="/lk/" class="a_grey">Авторизуйтесь</a>, чтобы увидеть контакты заказчика <a href="/lk/" class="btn btn--red">Войти</a>
	</div>
</div>
