<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Grid\Declension;

// что делает компания
$priceList = getElHL(4,[],['UF_COMPANY'=>$arResult['ID']],['ID','UF_SERVICE','UF_PRICE']);
foreach ($priceList as $value)
  $arIdsService[] = $value['UF_SERVICE'];

// получим выполненые работы
$completedWork = getElHL(8,[],['UF_COMPANY' => $arResult['ID'],'UF_ACTIVE'=>true],['*']);
foreach ($completedWork as $id => $value)
  $arIdsService[] = $value['UF_SERVICE'];

$bedroomDeclension = new Declension('спальня', 'спальни', 'спален');
$bathroomDeclension = new Declension('санузел', 'санузла', 'санузлов');

// получим проекты
$arOrder = Array('SORT'=>'ASC');
$arFilter = Array('IBLOCK_ID'=>3,'PROPERTY_COMPANY'=>$arResult['ID'],'ACTIVE'=>'Y');
$arSelect = Array('ID','NAME','DETAIL_PAGE_URL','PROPERTY_TYPE','PROPERTY_PHOTO','PROPERTY_AREA','PROPERTY_SIZE','PROPERTY_CNT_BEDROOM','PROPERTY_CNT_BATHROOM','PROPERTY_PURPOSE','PROPERTY_OLD_PRICE','PROPERTY_PRICE'); // ,'PROPERTY_'
$rsElements = CIBlockElement::GetList($arOrder,$arFilter,false,false,$arSelect);
while ($arElement = $rsElements->GetNext())
{ // dump($arElement);
  // выводим правильные окончания
  $cntBedroom = $arElement['PROPERTY_CNT_BEDROOM_VALUE'];
  if (!$cntBedroom) $cntBedroom = 0;
  $bedroomText = $bedroomDeclension->get($cntBedroom);
  $bedroomText = ($cntBedroom > 0) ? $cntBedroom.' '.$bedroomText : 'Нет спален';
  $arElement['BEDROOM_TEXT'] = $bedroomText;

  // санузлы
  $cntBathroom = $arElement['PROPERTY_CNT_BATHROOM_VALUE'];
  if (!$cntBathroom) $cntBathroom = 0;
  $bathroomText = $bathroomDeclension->get($cntBathroom);
  $bathroomText = ($cntBathroom > 0) ? $cntBathroom.' '.$bathroomText : 'Нет санузлов';
  $arElement['BATHROOM_TEXT'] = $bathroomText;

  $xmlIdTypes[] = $arElement['PROPERTY_TYPE_VALUE'];

  $arResult['PROJECTS'][] = $arElement;
}

if ($xmlIdTypes) {
  $typesHouse = getElHL(11,[],['UF_XML_ID'=>$xmlIdTypes],['ID','UF_NAME','UF_XML_ID']);
  foreach ($typesHouse as $key => $value) {
    $arResult['TYPE_HOUSE'][$value['UF_XML_ID']] = $value['UF_NAME'];
  }
}

$arResult['PURPOSE'] = getPropertyEnum([],["CODE"=>"PURPOSE"]); // получим Назначение дома

// получим отзывы
$arReviews = getElHL(9,[],['UF_COMPANY' => $arResult['ID']],['*']);
foreach ($arReviews as $value) {
  // подсчитаем рейтинг
  $sumScore = (int)$value['UF_EXPERTISE'] + (int)$value['UF_DEADLINE'] + (int)$value['UF_SERVICE'] + (int)$value['UF_PRICE'] + (int)$value['UF_QUALITY'];
  $rating = $sumScore / 5;
  $value['RATING'] = round($rating,2);

  $sumExpertise += (int)$value['UF_EXPERTISE'];
  $sumDeadline += (int)$value['UF_DEADLINE'];
  $sumService += (int)$value['UF_SERVICE'];
  $sumPrice += (int)$value['UF_PRICE'];
  $sumQuality += (int)$value['UF_QUALITY'];

  if ($value['UF_DATE_TIME']) $value['DATE'] = $value['UF_DATE_TIME'] -> format('d.m.Y, H:i');

  $arResult['REVIEWS'][] = $value;
}

$cntReviews = count($arResult['REVIEWS']);

if($cntReviews)
{
  $arResult['TOTAL'] = [
    'EXPERTISE' => round($sumExpertise / $cntReviews,2),
    'DEADLINE' => round($sumDeadline / $cntReviews,2),
    'SERVICE' => round($sumService / $cntReviews,2),
    'PRICE' => round($sumPrice / $cntReviews,2),
    'QUALITY' => round($sumQuality / $cntReviews,2),
  ];
  $arResult['TOTAL']['SUM'] = round(array_sum($arResult['TOTAL']) / 5, 2);
} else
  $arResult['TOTAL']['SUM'] = 0;

foreach ($arResult['PROPERTIES']['TECH']['VALUE'] as $value2)
  $arIdsTech[] = $value2;

// сформируем услуги
$arService = getElHL(3,[],['ID'=>$arIdsService],['*']);
$arServicesMain = getListProperty([],["USER_FIELD_ID" => 72]); // Главный раздел
$arServicesSub = getListProperty([],["USER_FIELD_ID" => 73]); // Подраздел

foreach ($arService as $value)
{
  // dump($value);
  $mainSectionID = $value['UF_MAIN_SECTION'];
  $subSectionID = $value['UF_SUBSECTION'];
  $mainSection = $arServicesMain[$mainSectionID];
  $subSection = $arServicesSub[$subSectionID];
  $arResult['SERVICES'][$mainSectionID]['NAME'] = $mainSection;
  $arResult['SERVICES'][$mainSectionID]['ITEMS'][$subSectionID]['NAME'] = $subSection;
  $arResult['SERVICES'][$mainSectionID]['ITEMS'][$subSectionID]['ITEMS'][$value['ID']] = [
    'NAME' => $value['UF_NAME'],
    'XML_ID' => $value['UF_XML_ID']
  ];
  $arSectionShow[$value['ID']] = [
    'MAIN_SECTION' => $mainSectionID,
    'SUBSECTION' => $subSectionID,
    'NAME' => $value['UF_NAME']
  ];
} // dump($arSectionShow);

$arTech = getElHL(5,[],['UF_XML_ID'=>$arIdsTech],['*']);
$arStaff = getElHL(6,[],['UF_COMPANY'=>$arResult['ID']],['*']);

foreach ($priceList as $value)
{
  $mainSectionID = $arSectionShow[$value['UF_SERVICE']]['MAIN_SECTION'];
  $subSectionID = $arSectionShow[$value['UF_SERVICE']]['SUBSECTION'];
  $value['NAME'] = $arSectionShow[$value['UF_SERVICE']]['NAME'];

  switch ($mainSectionID) {
    case 4: $icon = 's_01.svg'; break; // Строительство дома
    case 5: $icon = 's_02.svg'; break; // Строительные работы
    case 6: $icon = 's_05.svg'; break; // Отделочные работы
    case 7: $icon = 's_03.svg'; break; // Коммуникации
    case 8: $icon = 's_04.svg'; break; // Проектные работы
    case 19: $icon = 's_01.svg'; break; // Жилые дома
    case 23: $icon = 's_06.svg'; break; // Бани
    case 24: $icon = 's_07.svg'; break; // Беседки
    case 25: $icon = 's_08.svg'; break; // Бытовки
    case 26: $icon = 's_09.svg'; break; // Прочие строения
  }
  // $arResult['PRICE_LIST'][$value['UF_SERVICE']]['NAME'] = $arService[$value['UF_SERVICE']]['UF_NAME'];
  // $arResult['PRICE_LIST'][$value['UF_SERVICE']]['ICON'] = $icon;
  // $arResult['PRICE_LIST'][$value['UF_SERVICE']]['SUMM'] += $value['UF_PRICE'];
  // $arResult['PRICE_LIST'][$value['UF_SERVICE']]['MATERIALS'][] = [
  //   'NAME' => $arMaterial[$value['UF_MATERIAL']]['UF_NAME'],
  //   'PRICE' => $value['UF_PRICE']
  // ];

  if (!$minSum[$mainSectionID] || $minSum[$mainSectionID] > $value['UF_PRICE']) $minSum[$mainSectionID] = $value['UF_PRICE'];

  $arResult['PRICE_LIST_SHOW'][$mainSectionID]['NAME'] = $arServicesMain[$mainSectionID];
  $arResult['PRICE_LIST_SHOW'][$mainSectionID]['ICON'] = $icon;
  $arResult['PRICE_LIST_SHOW'][$mainSectionID]['SUMM'] = $minSum[$mainSectionID];
  $arResult['PRICE_LIST_SHOW'][$mainSectionID]['ITEMS'][] = $value;
}
// dump($arResult['PRICE_LIST_SHOW']);

// обрежем детальный текст
$detailText = strip_tags($arResult['~DETAIL_TEXT']);
// $detailText = substr($detailText,0,200);
// $detailText = rtrim($detailText, "!,.-");
// $detailText = substr($detailText, 0, strrpos($detailText, ' '));
// $arResult['SMALL_TEXT'] = $detailText.' ... <strong><a class="js-alltext" href="">Читать далее</a></strong>';
$arResult['SMALL_TEXT'] = $detailText;

$arResult['SERVICES'] = $arService;
$arResult['COMPLETED_WORK'] = $completedWork;
$arResult['TECH'] = $arTech;
$arResult['STAFF'] = $arStaff;

// dump($arResult['STAFF']);
