<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use Bitrix\Main\Grid\Declension;
// выводим правильное окончание
$reviewsDeclension = new Declension('отзыв', 'отзыва', 'отзывов');

// $rating = round($arResult['PROPERTIES']['RATING']['VALUE']); // рейтинг
$rating = $arResult['TOTAL']['SUM'];
$services = implode(', ',$arResult['DISPLAY_PROPERTIES']['SERVICES']['DISPLAY_VALUE']); // услуги

// отзывы
$cntReviews = ($arResult['REVIEWS']) ? count($arResult['REVIEWS']) : 0;
$reviewsText = $reviewsDeclension->get($cntReviews);
$reviewsText = ($cntReviews > 0) ? $cntReviews.' '.$reviewsText : 'Нет отзывов';

// dump($_COOKIE); // разбираем куки
if(isset($_COOKIE['comparison_bild'])){
	$arComparison = explode('-',$_COOKIE['comparison_bild']);
}
if(isset($_COOKIE['favorites_bild'])){
	$arFavorites = explode('-',$_COOKIE['favorites_bild']);
}
$comparison = (in_array($arResult['ID'],$arComparison)) ? 'Y' : 'N';
$favorites = (in_array($arResult['ID'],$arFavorites)) ? 'Y' : 'N';
$comp_active = ($comparison == 'Y') ? 'control--favorites--active' : '';
$fav_active = ($favorites == 'Y') ? 'control--favorites--active' : '';
$comp_text = ($comparison != 'Y') ? 'Добавить к сравнению' : 'Удалить из сравнения';
$fav_text = ($favorites != 'Y') ? 'Добавить в избранное' : 'Удалить из избранного';

if ($arResult['PROPERTIES']['PHONE']['VALUE'])
	$arResult['PROPERTIES']['PHONE']['VALUE'] = ': '.$arResult['PROPERTIES']['PHONE']['VALUE']

// dump($arResult['PROPERTIES']);
// unset($_COOKIE['favorites_bild']); setcookie('favorites_bild', null, -1, '/');
?>
<?$this->SetViewTarget('cart_screen');?>
	<div class="cart_screen">
<?$this->EndViewTarget();?>
	<?$this->SetViewTarget('detail_picture');?>
		style="background-image: url(<?=$arResult["DETAIL_PICTURE"]["SRC"]?>)"
	<?$this->EndViewTarget();?>
			<div class="right-control">
				<div class="control control--favorites <?=$fav_active?>">
					<div class="control__icon js-add-favorites" data-id="<?=$arResult['ID']?>" data-cookie="favorites_bild">
						<svg class="icon icon-favorites">
							<use xlink:href="#icon-favorites"></use>
						</svg>
					</div>
					<div class="control__container">
						<div class="control__main"><?=$fav_text?></div>
					</div>
				</div>
				<div class="control control--share js-control-share">
					<div class="control__icon">
						<svg class="icon icon-favorites">
							<use xlink:href="#icon-share"></use>
						</svg>
					</div>
					<div class="control__container">
						<div class="control__main">
							<span>Поделиться</span>
							<div class="ya-share2 social-list" data-curtain data-shape="round" data-services="vkontakte,odnoklassniki,telegram,twitter,viber,whatsapp"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="cart_screen__content row">
				<div class="col-xl-4 col-lg-6 cart_screen__main">
					<h1 class="title">Строительная компания <?=$arResult["NAME"]?></h1>
					<div class="title-desc"><?=$services?></div>
					<div class="reviews">
						<div class="reviews-stars">
							<?for ($x=1; $x<6; $x++) {?>
								<div class="reviews__star <?if ($rating >= $x) echo 'reviews__star--active'?>">
									<svg class="icon">
										<use xlink:href="#icon_star"></use>
									</svg>
								</div>
							<?}?>
							<div class="reviews__count"><?=$rating?></div>
						</div>
						<a class="reviews__link js-scroll" href="#reviews-block"><?=$reviewsText?></a>
					</div>
					<a class="btn btn--red" href="" data-toggle="modal" data-target="#modal-callback">Оставить заявку</a>
				</div>
			</div>
		</div>
	</div>
	<div class="cart_screen__bottom">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xl-8 col-lg-10 align-self-end offset-xl-4 offset-lg-2">
					<div class="cart-box">
						<div class="box-video"><img class="lazyload" src="" data-src="<?=SITE_TEMPLATE_PATH?>/images/video-bg.jpg" alt="Изображение"/>
							<div class="box-video__content">
								<div class="js-open-video play" data-toggle="modal" data-target="#modalVideo" data-src="<?=$arResult['PROPERTIES']['VIDEO']['VALUE']?>">
									<svg class="icon">
										<use xlink:href="<?=SITE_TEMPLATE_PATH?>/images/svg/sprite.svg#icon-play"></use>
									</svg>
								</div>
							</div>
							<div class="box-video__footer"><span>Видео о компании</span>
								<div class="full-screen js-open-video" data-toggle="modal" data-target="#modalVideo" data-src="<?=$arResult['PROPERTIES']['VIDEO']['VALUE']?>">
									<svg class="icon">
										<use xlink:href="#icon-resize"></use>
									</svg>
								</div>
							</div>
						</div>
						<div class="box-cart">
							<svg class="icon icon--handshake">
								<use xlink:href="#icon-handshake"></use>
							</svg>
							<p>
								<?if($arResult['PROPERTIES']['YEARS']['VALUE']):?>
									<?=round($arResult['PROPERTIES']['YEARS']['VALUE'])?> лет на рынке <br>
								<?endif;?>
								<?if($arResult['PROPERTIES']['CNT_STAFF']['VALUE']):?>
									<strong><a class="js-scroll" href="#team-block">бригада <?=round($arResult['PROPERTIES']['CNT_STAFF']['VALUE'])?> человек</a></strong>
								<?endif;?>
							</p>
						</div>
						<div class="box-cart">
							<svg class="icon icon--handshake">
								<use xlink:href="#icon-location"></use>
							</svg>
							<p><?=$arResult['PROPERTIES']['ADDRESS']['VALUE']?></p>
							<p><?=$arResult['PROPERTIES']['NAME']['VALUE']?><?=$arResult['PROPERTIES']['PHONE']['VALUE']?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<main>
	<div class="container-fluid">
		<section class="about_company">
			<h5 class="h-title">Что делает компания?</h5>
			<div class="row">
				<div class="col-xl-8">
					<div class="accordion-list" id="accordion">
						<?$cntPriceListShow = ($arResult['PRICE_LIST_SHOW'])?count($arResult['PRICE_LIST_SHOW']):0;
						foreach ($arResult['PRICE_LIST_SHOW'] as $key => $value) {?>
							<div class="card services-ls">
								<div id="heading_<?=$key?>">
									<div class="card__header <?=($cntPriceListShow == 1)?'':'collapsed'?>" role="button" data-toggle="collapse" data-target="#collapse_<?=$key?>" aria-expanded="true" aria-controls="collapse_<?=$key?>">
										<div class="card__images">
											<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/<?=$value['ICON']?>" class="icon">
										</div>
										<div class="card__desc">
											<div class="card__name"><?=$value['NAME']?></div>
											<strong>от <?=formatPrice($value['SUMM'])?> ₽ за 1 м<sup>2</sup></strong>
										</div>
										<div class="arrow">
											<svg class="icon">
												<use xlink:href="#icon-arrow_up"></use>
											</svg>
										</div>
									</div>
								</div>
								<div class="collapse <?=($cntPriceListShow == 1)?'show':''?>" id="collapse_<?=$key?>" aria-labelledby="heading_<?=$key?>" data-parent="#accordion">
									<div class="card-body">
										<div class="card-list">
											<?foreach ($value['ITEMS'] as $item) {?>
												<div class="card__item">
													<div class="card__name"><?=$item['NAME']?></div>
													<strong id="price-<?=$item['ID']?>" data-price="<?=$item['UF_PRICE']?>">от <?=formatPrice($item['UF_PRICE'])?> ₽ за 1 м<sup>2</sup></strong>
												</div>
											<?}?>
										</div>
									</div>
								</div>
							</div>
						<?}?>
					</div>
					<?if(count($arResult['PRICE_LIST_SHOW']) > 3){?>
						<a class="show-all js-services-show-all" href=""><span data-icon="3">Все услуги</span></a>
					<?}?>
				</div>
				<div class="col-xl-4">
					<div class="calculate">
						<h5 class="h-title">Калькулятор</h5>
						<?//dump($arResult['PRICE_LIST_SHOW']);?>
						<form class="form-calculate place_show" name="js-calculate" method="post">
							<input type="hidden" name="act" value="sendCalculate">
							<label class="select" for="option-1">
								<select class="js-select__letter select__select select__select-calc select__styler" id="option-1" name="option-1" title="Выберите наименование услуг" required>
									<option value="Выберите наименование услуг" disabled="disabled" selected="selected">Выберите наименование услуг</option>
									<?foreach ($arResult['PRICE_LIST_SHOW'] as $mainSectionID => $mainSection) {?>
										<option data-main="<?=$mainSectionID?>" value="<?=$mainSectionID?>"><?=$mainSection['NAME']?></option>
									<?}?>
								</select>
							</label>
							<label class="select disabled" for="option-2">
								<select class="js-select__letter select__select select__styler select__select-calc" name="option-2" id="option-2" required data-hide-disabled='true' title="Выберите материал, тип услуг" required>
									<!-- <option value="Выберите материал, тип услуг" disabled="disabled" selected="selected">Выберите материал, тип услуг</option> -->
									<?foreach ($arResult['PRICE_LIST_SHOW'] as $mainSectionID => $mainSection) {
											foreach ($mainSection['ITEMS'] as $items){?>
												<option data-main="<?=$mainSectionID?>" value="<?=$items['ID']?>"><?=$items['NAME']?></option>
											<?}
									}?>
								</select>
							</label>
							<label for="number">
								<input class="form-input js-number" type="text" id="number" name="number" placeholder="Введите объем работы" disabled="disabled" required />
							</label>
							<button class="btn btn--red form-calculate__button">Расчет моей сметы</button>
						</form>
					</div>
				</div>
			</div>
		</section>

		<?if($arResult['PROJECTS']):?>
		<section class="index-block">
			<h5 class="h-title"> <span>Проекты домов <span class="count"><?=count($arResult['PROJECTS'])?></span></span></h5>
			<div class="object-list object-list--catalog">
				<?foreach ($arResult['PROJECTS'] as $arItem) {?>
					<div class="object">
						<div class="row">
							<div class="col-md-5">
								<div class="object__images">
									<div class="object-slider swiper-container">
										<div class="right-control">
											<div class="control control--favorites <?=$comp_active?>">
												<div class="control__icon js-add-favorites" data-id="<?=$arItem['ID']?>" data-cookie="comparison_house">
													<svg class="icon-compare" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14 14" style="enable-background:new 0 0 14 14;"
														xml:space="preserve">
														<path d="M2,14H0V1.2C0,0.5,0.4,0,1,0l0,0c0.6,0,1,0.5,1,1.2V14z" />
														<path d="M6,14H4V7.2C4,6.5,4.4,6,5,6l0,0c0.6,0,1,0.5,1,1.2V14z" />
														<path d="M10,14H8V1.2C8,0.5,8.4,0,9,0l0,0c0.6,0,1,0.5,1,1.2V14z" />
														<path d="M14,14h-2V7.2C12,6.5,12.4,6,13,6l0,0c0.6,0,1,0.5,1,1.2V14z" />
													</svg>
												</div>
												<div class="control__container">
													<div class="control__main"><?=$comp_text?></div>
												</div>
											</div>
											<div class="control control--favorites <?=$fav_active?>">
												<div class="control__icon js-add-favorites" data-id="<?=$arItem['ID']?>" data-cookie="favorites_house">
													<svg viewBox="0 0 50 50" width="100%" height="100%">
														<path d="M33.2,17.8l-0.1-0.1c-0.9-1.1-2.2-1.6-3.6-1.6s-2.6,0.6-3.6,1.6l-0.9,1l-0.9-1c-0.9-1.1-2.2-1.6-3.6-1.6s-2.6,0.6-3.6,1.6l-0.1,0.1c-0.9,1-1.4,2.4-1.4,3.9c0,1.5,0.5,2.8,1.4,3.9l0,0l7.7,8.3c0.1,0.1,0.3,0.2,0.5,0.2s0.4-0.1,0.5-0.2l7.7-8.3l0,0c0.9-1,1.4-2.4,1.4-3.9C34.6,20.2,34.1,18.8,33.2,17.8z"></path>
													</svg>
												</div>
												<div class="control__container">
													<div class="control__main"><?=$fav_text?></div>
												</div>
											</div>
										</div>
										<div class="swiper-wrapper">
											<?foreach ($arItem['PROPERTY_PHOTO_VALUE'] as $photo) {
												$photoRes = CFile::ResizeImageGet($photo, array('width' => 880, 'height' => 548), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);?>
												<div class="swiper-slide"><img class="lazyload" src="" data-src="<?=$photoRes['src']?>" alt="Изображение"></div>
											<?}?>
										</div>
										<div class="swiper-pagination"></div>
										<div class="js-slider-img swiper-button-next" data-icon="5"></div>
										<div class="js-slider-img swiper-button-prev" data-icon="4"></div>
									</div>
								</div>
							</div>
							<div class="col-md-7">
								<div class="object-body">
									<a class="object__name" href="<?=$arItem["DETAIL_PAGE_URL"]?>">Проект <?=getBath($arItem['PROPERTY_TYPE_VALUE'])?> <?=$arItem["NAME"]?></a>
									<div class="object__desc">
										<a href="/<?=$arItem['PROPERTY_TYPE_VALUE']?>/"><?=$arResult['TYPE_HOUSE'][$arItem['PROPERTY_TYPE_VALUE']]?></a>
									</div>
									<div class="object__developer">
										<div class="proect__description">
											<a href="" target="_blank" class="proect__cat">СК <?=$arResult["NAME"]?></a>
											<span class="proect__raiting"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/sorting_02.svg" alt=""><?=$rating?></span>
											<a href="#reviews-block" class="proect__rew"><?=$reviewsText?></a>
										</div>
									</div>
									<div class="object__comfort">
										<div class="row">
											<div class="col-xl-8 col-md-12">
												<div class="row object__comfort-list">
													<div class="col-md-6 col-sm-6 proect__comfort-property"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/area_02.svg" alt=""><span>Площадь: <span><?=$arItem['PROPERTY_AREA_VALUE']?> м<sup>2</sup></span></span></div>
													<div class="col-md-6 col-sm-6 proect__comfort-property"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/area.svg" alt=""><span>Размер: <span><?=$arItem['PROPERTY_SIZE_VALUE']?></span></span></div>
													<div class="col-md-6 col-sm-6 proect__comfort-property"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/bed.svg" alt=""><span><?=$arItem['BEDROOM_TEXT']?></span></div>
													<div class="col-md-6 col-sm-6  proect__comfort-property"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/bath.svg" alt=""><span><?=$arItem['BATHROOM_TEXT']?></span></div>
												</div>
											</div>
										</div>
									</div>
									<div class="object__bottom">
										<div class="object__bottom-left">
											<?foreach ($arItem['PROPERTY_PURPOSE_VALUE'] as $key => $value) {?>
												<div class="foryou">
													<div class="for"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/<?=$arResult['PURPOSE'][$key]['XML_ID']?>.svg" alt=""></div>
													<div class="control__container">
														<div class="control__main">
															<strong>Дом подходит для:</strong>
															<?=$value?>
														</div>
													</div>
												</div>
											<?}?>
										</div>
										<div class="object__bottom-right">
											<div class="proect__specifications-item proect__specifications-price">
												<div class="proect__specifications-value">
													<?if($arItem['PROPERTY_OLD_PRICE_VALUE']){?>
														<span class="old-price"><s><?=formatPrice($arItem['PROPERTY_OLD_PRICE_VALUE'])?> ₽</s></span>
													<?}?>
													<span class="new-price"><?=formatPrice($arItem['PROPERTY_PRICE_VALUE'])?> ₽</span>
												</div>
											</div>
											<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="btn btn--red">Подробнее</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="object__bottom mobile">
							<div class="object__bottom-left">
								<?foreach ($arItem['PROPERTY_PURPOSE_VALUE'] as $key => $value) {?>
									<div class="foryou">
										<div class="for"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/<?=$arResult['PURPOSE'][$key]['XML_ID']?>.svg" alt=""></div>
										<div class="control__container">
											<div class="control__main">
												<strong>Дом подходит для:</strong>
												<?=$value?>
											</div>
										</div>
									</div>
								<?}?>
							</div>
							<div class="object__bottom-right">
								<div class="proect__specifications-item proect__specifications-price">
									<div class="proect__specifications-value">
										<?foreach ($arItem['PROPERTY_PURPOSE_VALUE'] as $key => $value) {?>
											<div class="for">
												<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/<?=$arResult['PURPOSE'][$key]['XML_ID']?>.svg" alt="">
												<div class="control__container">
													<div class="control__main">
														<strong>Дом подходит для:</strong>
														<?=$value?>
													</div>
												</div>
											</div>
										<?}?>
										<span class="new-price"><?=formatPrice($arItem['PROPERTY_PRICE_VALUE'])?> ₽</span>
										<?if($arItem['PROPERTY_OLD_PRICE_VALUE']){?>
											<span class="old-price"><s><?=formatPrice($arItem['PROPERTY_OLD_PRICE_VALUE'])?> ₽</s></span>
										<?}?>
									</div>
								</div>
								<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="btn btn--red">Подробнее</a>
							</div>
						</div>
					</div>
				<?}?>
			</div>
			<?if(count($arResult['PROJECTS']) > 5){?>
				<a class="show-all show-all-completed-work" href=""><span data-icon="3">Показать еще</span></a>
			<?}?>
		</section>
		<?endif;?>

		<?if($arResult['COMPLETED_WORK']):?>
		<section class="index-block <?if($arResult['PROJECTS'])echo'index-block__2'?>">
			<h5 class="h-title"> <span>ВЫПОЛНЕННЫЕ ОБЪЕКТЫ: ЗА ВСЕ ВРЕМЯ 50, ПРЕДСТАВЛЕНО НА САЙТЕ<span class="count"><?=count($arResult['COMPLETED_WORK'])?></span></span></h5>
			<div class="object-list">
				<?foreach ($arResult['COMPLETED_WORK'] as $key => $work) {?>
					<div class="object">
						<div class="row">
							<div class="col-md-5">
								<div class="object__images">
									<div class="object__area"><?=$work['UF_AREA']?> м<sup>2</sup></div>
									<div class="object-slider swiper-container">
										<div class="swiper-wrapper">
											<?foreach ($work['UF_IMAGE'] as $image) {
												$photoRes = CFile::ResizeImageGet($image, array('width' => 692, 'height' => 484), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);?>
												<div class="swiper-slide"><img class="lazyload" src="" data-src="<?=$photoRes['src']?>" alt="Изображение объекта"/></div>
											<?}?>
										</div>
										<div class="swiper-pagination"></div>
										<div class="js-slider-img swiper-button-next" data-icon="5"></div>
										<div class="js-slider-img swiper-button-prev" data-icon="4"></div>
									</div>
								</div>
							</div>
							<div class="col-md-7">
								<div class="object__info">
									<div class="object__head">
										<div class="object__name"><?=$work['UF_NAME']?></div>
									</div>
									<div class="object__strong">
										<svg class="icon">
											<use xlink:href="#icon-address"></use>
										</svg><span><?=$work['UF_ADDRESS']?></span>
									</div>
									<div class="object__strong">
										<svg class="icon">
											<use xlink:href="#icon-home-2"></use>
										</svg>
										<span class="object-characteristic__name">Вид услуг:</span>
										<span><?=$arResult['SERVICES'][$work['UF_SERVICE']]['UF_NAME']?></span>
									</div>
									<div class="row align-items-end">
										<div class="col-12 col-lg-7 col-xl-8">
											<div class="object__characteristics">
												<div class="object-characteristic">
													<div class="object-characteristic-lft">
														<div class="object-characteristic__icon">
															<svg class="icon">
																<use xlink:href="#icon-price"></use>
															</svg>
														</div>
														<div class="object-characteristic__name">Стоимость:</div>
													</div>
													<div class="object-characteristic__value"><strong><?=formatPrice($work['UF_PRICE'])?> ₽</strong></div>
												</div>
												<div class="object-characteristic">
													<div class="object-characteristic-lft">
														<div class="object-characteristic__icon">
															<svg class="icon">
																<use xlink:href="#icon-area"></use>
															</svg>
														</div>
														<div class="object-characteristic__name">Площадь:</div>
													</div>
													<div class="object-characteristic__value"><strong><?=$work['UF_AREA']?> м<sup>2</sup></strong></div>
												</div>
												<div class="object-characteristic">
													<div class="object-characteristic-lft">
														<div class="object-characteristic__icon">
															<svg class="icon">
																<use xlink:href="#icon-deadlines"></use>
															</svg>
														</div>
														<div class="object-characteristic__name">Сроки:</div>
													</div>
													<div class="object-characteristic__value"><strong><?=$work['UF_DEADLINE']?></strong></div>
												</div>
											</div>
										</div>
										<?if($work['UF_SHOW']):?>
											<div class="col-12 col-lg-5 col-xl-4"><a class="btn btn--red" href="" data-toggle="modal" data-target="#modal-callback">Посмотреть вживую</a></div>
										<?endif;?>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?}?>
			</div>
			<?if(count($arResult['COMPLETED_WORK']) > 5){?>
				<a class="show-all show-all-completed-work" href=""><span data-icon="3">Показать еще</span></a>
			<?}?>
		</section>
		<?endif;?>

	</div>
	<section class="technology-block">
		<div class="container-fluid">
			<h5 class="h-title">Технологии и материалы, используемые в работе</h5>
			<div class="technology-list">
				<div class="row">
					<?foreach ($arResult['TECH'] as $key => $tech) {
						switch ($key) {
							case 2: $classColor = 'technology--green'; break;
							case 3: $classColor = 'technology--purple'; break;
							case 4: $classColor = 'technology--purple'; break;
							case 6: $classColor = 'technology--green'; break;
							default: $classColor = ''; break;
						}?>
						<div class="col-md-4">
							<div class="technology <?=$classColor?>">
								<svg class="icon">
									<use xlink:href="<?=$tech['UF_ICON']?>"></use>
								</svg>
								<strong><span><?=$tech['UF_NAME']?></span></strong>
							</div>
						</div>
					<?}?>
				</div>
			</div>
			<div class="form-index">
				<h5 class="h-title">Отправьте свои контакты и мы расчитаем вам смету</h5>
				<form class="form-contancts send-form place_show" method="post">
					<input type="hidden" name="act" value="sendEstimate">
					<div class="form-contancts--column">
						<label for="name">
							<input class="form-input" type="text" id="name" name="name" placeholder="Ваше имя" required/>
						</label>
						<label for="phone">
							<input class="form-input" type="tel" id="phone" name="phone" placeholder="Номер телефона" required/>
						</label>
					</div>
					<div class="form-contancts--column-2">
						<label for="comment">
							<textarea name="comment" id="comment" placeholder="Ваши пожелания"></textarea>
						</label>
					</div>
					<div class="form-contancts--column form-contancts--column-md-last">
						<div class="politic">
							<label class="fake-check" for="politic">
								<input type="checkbox" id="politic" checked required/>
								<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 22 22" style="enable-background:new 0 0 22 22;" xml:space="preserve">
									<g>
										<path class="checks" d="M9.6,15.6l-3.2-3.1c-0.1-0.1-0.2-0.3-0.2-0.4c0-0.2,0-0.3,0.2-0.5c0.1-0.1,0.3-0.2,0.4-0.2s0.3,0.1,0.4,0.2l2.7,2.6l4.8-7.7c0.1-0.1,0.2-0.2,0.4-0.2c0.2,0,0.3,0,0.5,0.1c0.1,0.1,0.2,0.2,0.3,0.4c0,0.2,0,0.3-0.1,0.5l-5.2,8.4c0,0,0,0.1,0,0.1c0,0,0,0-0.1,0.1l0,0c-0.1,0-0.1,0.1-0.2,0.1c0,0,0,0-0.1,0c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1-0.1C9.7,15.7,9.7,15.7,9.6,15.6C9.6,15.7,9.6,15.7,9.6,15.6C9.6,15.6,9.6,15.6,9.6,15.6z"/>
									</g>
								</svg>
								<span>Нажимая на кнопку, вы даете согласие на обработку персональных данных и соглашаетесь с <a href="/politika-konfidentsialnosti/"> Политикой Конфиденциальности</a></span>
							</label>
						</div>
						<button class="btn btn--red">Отправить</button>
					</div>
					<div class="form-contancts--column-3">
						<!-- <div class="form-contancts--column">
							<label class="fake-check align-items-center konkurs" for="konkurs">
								<input type="checkbox" id="konkurs" name="konkurs" value="Да"/>
								<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 22 22" style="enable-background:new 0 0 22 22;" xml:space="preserve">
									<g>
										<path class="checks" d="M9.6,15.6l-3.2-3.1c-0.1-0.1-0.2-0.3-0.2-0.4c0-0.2,0-0.3,0.2-0.5c0.1-0.1,0.3-0.2,0.4-0.2s0.3,0.1,0.4,0.2
											l2.7,2.6l4.8-7.7c0.1-0.1,0.2-0.2,0.4-0.2c0.2,0,0.3,0,0.5,0.1c0.1,0.1,0.2,0.2,0.3,0.4c0,0.2,0,0.3-0.1,0.5l-5.2,8.4
											c0,0,0,0.1,0,0.1c0,0,0,0-0.1,0.1l0,0c-0.1,0-0.1,0.1-0.2,0.1c0,0,0,0-0.1,0c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1-0.1
											C9.7,15.7,9.7,15.7,9.6,15.6C9.6,15.7,9.6,15.7,9.6,15.6C9.6,15.6,9.6,15.6,9.6,15.6z"/>
									</g>
								</svg>
								<span>Включить компанию в конкурс</span>
							</label>
						</div> -->
						<div class="form-contancts--column">
							<div class="file-input">
								<input type="file" name="files"/>
								<svg class="icon">
									<use xlink:href="#icon-clip"></use>
								</svg>
								<span class="file-name__show">Прикрепить документ</span>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
	<section class="team-block" id="team-block">
		<div class="container-fluid">
			<?/*if($arResult['STAFF']):?>
			<h5 class="h-title">Бригада: Всего <?=$arResult['PROPERTIES']['CNT_STAFF']['VALUE']?> человек, на сайте <span class="count"><?=count($arResult['STAFF'])?></span></h5>
			<div class="team-slider swiper-container">
				<div class="swiper-wrapper">
					<?foreach ($arResult['STAFF'] as $key => $staff) {
						$photoRes = CFile::ResizeImageGet($staff['UF_FILE'], array('width' => 540, 'height' => 442), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);?>
						<div class="swiper-slide">
							<div class="team">
								<div class="team__avatar"><img class="lazyload" alt="" src="" data-src="<?=$photoRes['src']?>"/></div>
								<div class="team__name"><?=$staff['UF_NAME']?></div>
								<div class="team__position"><?=$staff['UF_POSITION']?></div>
								<div class="team__desc">Стаж: <strong><?=$staff['UF_EXP']?></strong></div>
							</div>
						</div>
					<?}?>
				</div>
			</div>
			<?endif;*/?>
			<div class="team-bottom">
				<?if($arResult['PROPERTIES']['SOCIAL']['VALUE']):?>
					<div class="socials">
						<span>Компания &laquo;<?=$arResult['NAME']?>&raquo; в&nbsp;соц.сетях:</span>
						<ul class="social-list">
							<li class="social-item"><a class="social social--link js-copylink" href="" aria-label="Копировать ссылку">
									<svg class="icon icon--copy">
										<use xlink:href="#icon-copy"></use>
									</svg></a></li>
							<?foreach ($arResult['PROPERTIES']['SOCIAL']['VALUE'] as $key => $value)
							{
								$nameSocial = strtolower($arResult['PROPERTIES']['SOCIAL']['DESCRIPTION'][$key]);

								if (strpos($nameSocial,'vk') !== false)
									$socilaIcon = 'icon-vk';
								elseif (strpos($nameSocial,'facebook') !== false)
									$socilaIcon = 'icon-fb';
								elseif (strpos($nameSocial,'ok') !== false)
									$socilaIcon = 'icon-ok';
								elseif (strpos($nameSocial,'telegram') !== false)
									$socilaIcon = 'icon-tg';
								elseif (strpos($nameSocial,'twitter') !== false)
									$socilaIcon = 'icon-tw';
								elseif (strpos($nameSocial,'instagram') !== false)
									$socilaIcon = 'icon-ig';
							?>
								<li class="social-item"><a class="social" href="<?=$value?>" target='_blank'>
									<?if($socilaIcon == 'icon-ig'):?>
										<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/instagram_color.svg" alt="icon-ig">
									<?else:?>
										<svg class="icon"><use xlink:href="#<?=$socilaIcon?>"></use></svg>
									<?endif;?>
								</a></li>
							<?}?>
						</ul>
					</div>
				<?endif;?>
				<?if($arResult['STAFF']):?>
					<div class="swiper-btn">
						<div class="swiper-button-prev">
							<svg class="icon">
								<use xlink:href="#icon-arrow_work"></use>
							</svg>
						</div>
						<div class="swiper-button-next">
							<svg class="icon">
								<use xlink:href="#icon-arrow_work"></use>
							</svg>
						</div>
					</div>
				<?endif;?>
			</div>
		</div>
	</section>
	<section class="contacts-index">
		<div class="container-fluid">
			<div class="contacts-block">
				<div class="contacts-item">
					<h6>Юридическая информация</h6>
					<div class="contacts-address">
						<p>
							Полное наименование: <?=$arResult['PROPERTIES']['FULL_NAME']['VALUE']?><br>
							Директор: <?=$arResult['PROPERTIES']['DIRECTOR']['VALUE']?><br>
							ИНН: <?=$arResult['PROPERTIES']['INN']['VALUE']?><br>
							<!-- Юр. лицо: <?=$arResult['PROPERTIES']['LEGAL_ENTITY']['VALUE']?><br> -->
							<!-- Кол-во афиллированных юр. лиц: <?=$arResult['PROPERTIES']['NUMBER_AFFILIATED']['VALUE']?><br> -->
							Судебные дела: <?=$arResult['PROPERTIES']['COURT_CASE']['VALUE']?><strong><a href="<?=$arResult['PROPERTIES']['COURT_CASE_URL']['VALUE']?>">Подробнее</a></strong><br>
							<!-- Кол-во соучредителей: <?=$arResult['PROPERTIES']['NUMBER_COFOUNDE']['VALUE']?> -->
						</p>
						<noindex><a href="https://egrul.nalog.ru/index.html" class="btn btn--default" rel="nofollow" target="_blank">Заказать выписку из ФНС</a></noindex>
					</div>
				</div>
				<div class="contacts-item body-text">
					<h6>Описание</h6>
					<?if($arResult['SMALL_TEXT']):?>
						<div class="small-text">
							<?=$arResult['SMALL_TEXT']?>
						</div>
						<div class="all-text">
							<?=$arResult['DETAIL_TEXT']?>
						</div>
					<?else:?>
						<div class="small-text">
							<?=$arResult['DETAIL_TEXT']?>
						</div>
					<?endif;?>
				</div>
			</div>
		</div>
	</section>
	<?if($arResult['PROPERTIES']['SERTIFICATES']['VALUE']):?>
	<section class="sertificate-block">
		<div class="container-fluid">
			<h5 class="h-title">Сертификаты компании</h5>
			<div class="sertificate-slider swiper-container">
				<div class="swiper-wrapper">
					<?foreach ($arResult['PROPERTIES']['SERTIFICATES']['VALUE'] as $sertif) {
						$photoRes = CFile::ResizeImageGet($sertif, array('width' => 480, 'height' => 680), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);?>
						<div class="swiper-slide">
							<div class="sertificate"><a class="loupe" data-fancybox="gallery" href="<?=CFile::GetPath($sertif)?>">T</a><img class="lazyload" data-src="<?=$photoRes['src']?>" alt=""/></div>
						</div>
					<?}?>
				</div>
			</div>
			<div class="team-bottom justify-content-end">
				<div class="swiper-btn">
					<div class="swiper-button-prev">
						<svg class="icon">
							<use xlink:href="#icon-arrow_work"></use>
						</svg>
					</div>
					<div class="swiper-button-next">
						<svg class="icon">
							<use xlink:href="#icon-arrow_work"></use>
						</svg>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?endif;?>
	<div class="reviews-block" id="reviews-block">
		<div class="container-fluid">
			<div class="reviews-header">
				<h6 class="h-title">Отзывы:<span class="rating"><?=count($arResult['REVIEWS'])?></span></h6>
        <a class="add-reviews btn btn--reviews" href="" data-toggle="modal" data-target="#modalFeedback">Оставить отзыв</a>
			</div>
			<div class="reviews-rating">
				<div class="rating-item">
					<div class="rating-item__name">Экспертность</div>
					<div class="rating-item__stars">
						<div class="reviews-stars">
              <?for ($i=0; $i < 5; $i++) {
                $active = ($i < $arResult['TOTAL']['EXPERTISE']) ? 'reviews__star--active' : '';?>
                <div class="reviews__star <?=$active?>">
  								<svg class="icon">
  									<use xlink:href="#icon_star"></use>
  								</svg>
  							</div>
              <?}?>
						</div>
					</div>
				</div>
				<div class="rating-item">
					<div class="rating-item__name">Выполнение сроков</div>
					<div class="rating-item__stars">
						<div class="reviews-stars">
              <?for ($i=0; $i < 5; $i++) {
                $active = ($i < $arResult['TOTAL']['DEADLINE']) ? 'reviews__star--active' : '';?>
                <div class="reviews__star <?=$active?>">
  								<svg class="icon">
  									<use xlink:href="#icon_star"></use>
  								</svg>
  							</div>
              <?}?>
						</div>
					</div>
				</div>
				<div class="rating-item">
					<div class="rating-item__name">Сервис</div>
					<div class="rating-item__stars">
						<div class="reviews-stars">
              <?for ($i=0; $i < 5; $i++) {
                $active = ($i < $arResult['TOTAL']['SERVICE']) ? 'reviews__star--active' : '';?>
                <div class="reviews__star <?=$active?>">
  								<svg class="icon">
  									<use xlink:href="#icon_star"></use>
  								</svg>
  							</div>
              <?}?>
						</div>
					</div>
				</div>
				<div class="rating-item">
					<div class="rating-item__name">Цена</div>
					<div class="rating-item__stars">
						<div class="reviews-stars">
              <?for ($i=0; $i < 5; $i++) {
                $active = ($i < $arResult['TOTAL']['PRICE']) ? 'reviews__star--active' : '';?>
                <div class="reviews__star <?=$active?>">
  								<svg class="icon">
  									<use xlink:href="#icon_star"></use>
  								</svg>
  							</div>
              <?}?>
						</div>
					</div>
				</div>
				<div class="rating-item">
					<div class="rating-item__name">Качество работ</div>
					<div class="rating-item__stars">
						<div class="reviews-stars">
              <?for ($i=0; $i < 5; $i++) {
                $active = ($i < $arResult['TOTAL']['QUALITY']) ? 'reviews__star--active' : '';?>
                <div class="reviews__star <?=$active?>">
  								<svg class="icon">
  									<use xlink:href="#icon_star"></use>
  								</svg>
  							</div>
              <?}?>
						</div>
					</div>
				</div>
			</div>
			<div class="review-list">
        <?foreach ($arResult['REVIEWS'] as $value) {?>
          <div class="review">
  					<div class="review__header">
  						<div class="user">
  							<div class="user__avatar"><img class="lazyload" data-src="<?=SITE_TEMPLATE_PATH?>/images/avatar.jpg" alt=""/></div>
  							<div class="user__info">
  								<h6 class="user__name"><?=$value['UF_NAME']?> <?=$value['UF_LAST_NAME']?><span class="rating"><?=$value['RATING']?></span></h6>
  								<div class="user__date"><?=$value['DATE']?></div>
  							</div>
  						</div>
  					</div>
  					<div class="review-body">
  						<p><?=$value['UF_DESCRIPTION']?></p>
  					</div>
  				</div>
        <?}?>
			</div>
      <?if(count($arResult['REVIEWS']) > 4){?>
			   <a class="show-all" href=""><span data-icon="3">Все отзывы</span></a>
      <?}?>
		</div>
	</div>
	<input type="hidden" id="company" value="<?=$arResult['ID']?>">
</main>

<div class="modal fade" id="modalFeedback" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog--reply" role="document">
		<div class="modal-content">
			<div class="modal-close js-video-close" data-dismiss="modal" aria-label="Close">
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20px" height="20px">
					<path fill-rule="evenodd" fill="rgb(149, 149, 149)" d="M1.515,0.100 L19.899,18.485 L18.485,19.899 L0.100,1.515 L1.515,0.100 Z" />
					<path fill-rule="evenodd" fill="rgb(149, 149, 149)" d="M18.485,0.100 L19.899,1.515 L12.828,8.586 L11.414,7.171 L18.485,0.100 Z" />
					<path fill-rule="evenodd" fill="rgb(149, 149, 149)" d="M7.171,11.414 L8.586,12.828 L1.515,19.899 L0.100,18.485 L7.171,11.414 Z" />
				</svg>
			</div>
			<div class="modal-body">
				<div class="modal-header">
					<h4 class="modal-header__title">
						Оставить Отзыв
					</h4>
				</div>
				<div class="modal-container">
					<form action="" method="post" id="formSendReview">
						<div class="row">
							<div class="col-md-6">
								<div class="form-block">
									<label>
										<input class="form-input" type="text" name="review-name" placeholder="Имя*" required>
										<span class="placeholder">Имя</span>
										<a href="#" class="edit-input">
											<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
										</a>
									</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-block">
									<label>
										<input class="form-input" type="text" name="review-surname" placeholder="Фамилия*" required>
										<span class="placeholder">Фамилия</span>
										<a href="#" class="edit-input">
											<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
										</a>
									</label>
								</div>
							</div>
						</div>
						<p style="margin-top: 15px; text-align: center;">Оцените застройщика по следующим критериям:</p>
						<div class="row">
							<div class="col-md-4">
								<div class="form-block">
									<label class="select">
										<select class="select__select select__styler" name="review-expertise" title="Экспертность*" required>
											<option value="">Экспертность</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
										</select>
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-block">
									<label class="select">
										<select class="select__select select__styler" name="review-deadline" title="Выполнение сроков*" required>
											<option value="">Выполнение сроков</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
										</select>
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-block">
									<label class="select">
										<select class="select__select select__styler" name="review-service" title="Сервис*" required>
											<option value="">Сервис</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
										</select>
									</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-block">
									<label class="select">
										<select class="select__select select__styler" name="review-price" title="Цена*" required>
											<option value="">Цена</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
										</select>
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-block">
									<label class="select">
										<select class="select__select select__styler" name="review-quality" title="Качество работ*" required>
											<option value="">Качество работ</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
										</select>
									</label>
								</div>
							</div>
							<div class="col-md-4"></div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-block">
									<label>
										<textarea class="form-textarea" name="review-text" cols="30" rows="10" placeholder="Введите текст отзыва"></textarea required>
									</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-block">
									<label>
										<input class="form-input" type="email" name="review-email" placeholder="E-mail*" required>
										<span class="placeholder">E-mail</span>
										<a href="#" class="edit-input">
											<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
										</a>
									</label>
								</div>
							</div>
							<div class="col-md-6">
								<!-- <a href="#" class="btn btn--red">Отправить</a> -->
								<input type="submit" class="btn btn--red" value="Отправить">
							</div>
						</div>
						<div class="politic">
							<label class="fake-check" for="politic">
								<input type="checkbox" id="politic" checked="checked">
								<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 22 22" style="enable-background:new 0 0 22 22;" xml:space="preserve">
									<g>
										<path class="checks" d="M9.6,15.6l-3.2-3.1c-0.1-0.1-0.2-0.3-0.2-0.4c0-0.2,0-0.3,0.2-0.5c0.1-0.1,0.3-0.2,0.4-0.2s0.3,0.1,0.4,0.2l2.7,2.6l4.8-7.7c0.1-0.1,0.2-0.2,0.4-0.2c0.2,0,0.3,0,0.5,0.1c0.1,0.1,0.2,0.2,0.3,0.4c0,0.2,0,0.3-0.1,0.5l-5.2,8.4c0,0,0,0.1,0,0.1c0,0,0,0-0.1,0.1l0,0c-0.1,0-0.1,0.1-0.2,0.1c0,0,0,0-0.1,0c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1-0.1C9.7,15.7,9.7,15.7,9.6,15.6C9.6,15.7,9.6,15.7,9.6,15.6C9.6,15.6,9.6,15.6,9.6,15.6z"></path>
									</g>
								</svg>
								<span>Нажимая на кнопку, вы даете согласие на обработку персональных данных и соглашаетесь с <a href="/politika-konfidentsialnosti/"> Политикой Конфиденциальности</a></span>
							</label>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
