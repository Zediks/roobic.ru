<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$arSort = [
	[
		'NAME' => 'По релевантности',
		'CODE' => 'sort',
		'ICON' => 'type-relevance',
	],[
		'NAME' => 'По рейтингу',
		'CODE' => 'rating',
		'ICON' => 'type-rating',
	],[
		'NAME' => 'Сначала дешевле',
		'CODE' => 'price_ask',
		'ICON' => 'type-pricesmall',
	],[
		'NAME' => 'Сначала дороже',
		'CODE' => 'price_desc',
		'ICON' => 'type-pricelarge',
	]
];

if ($_REQUEST['sort'])
	setcookie("build_sort", $_REQUEST['sort'], time()+3600);
?>
<main class="main--catalog">
  <div class="container-fluid">
    <?$APPLICATION->IncludeComponent(
      "bitrix:breadcrumb",
      "breadcrumb",
      array(
        "PATH" => "",
        "SITE_ID" => "s1",
        "START_FROM" => "0",
        "COMPONENT_TEMPLATE" => "breadcrumb"
      ),
      false
    );?>
    <h1><?$APPLICATION->ShowTitle(false)?></h1>
    <?require_once $_SERVER["DOCUMENT_ROOT"].'/local/inc/filterBuilders.php';?>
    <div class="sorting">
      <div class="row">
        <div class="col-md-7 col-xl-8">
					<?/*?><div class="filter"><a class="filter-item filter-item--active" href="">Все</a><a class="filter-item" href="">Компании</a><a class="filter-item" href="">Мастера</a></div><?*/?>
        </div>
        <div class="col-md-5 col-xl-4">
          <div class="sort">
            <label class="select select--sorting" for="sorting">
              <select class="select__select js-select__letter" name="sort" id="sorting">
								<?foreach ($arSort as $value) { // data-icon="<?=$value['ICON']?>"?>
									<option value="<?=$value['CODE']?>" <?if($arParams["BUILD_SORT"] == $value['CODE'])echo'selected'?>><?=$value['NAME']?></option>
								<?}?>
              </select>
            </label>
          </div>
        </div>
      </div>
    </div>
		<?if($arParams["USE_FILTER"]=="Y"):?>
			<?
				// $APPLICATION->IncludeComponent(
				// 	"bitrix:catalog.filter",
				// 	"",
				// 	Array(
				// 		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
				// 		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				// 		"FILTER_NAME" => $arParams["FILTER_NAME"],
				// 		"FIELD_CODE" => $arParams["FILTER_FIELD_CODE"],
				// 		"PROPERTY_CODE" => $arParams["FILTER_PROPERTY_CODE"],
				// 		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
				// 		"CACHE_TIME" => $arParams["CACHE_TIME"],
				// 		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
				// 		"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
				// 	),
				// 	$component
				// );
				global $arrFilter;
				// if ($_REQUEST['region']) $arrFilter['PROPERTY']['REGION'] = $_REQUEST['region'];
				if ($buildServices) $arrFilter['PROPERTY']['SERVICES'] = $buildServices;
				if ($buildPrice) $arrFilter['PROPERTY']['PRICE'] = $buildPrice;
				// dump($arrFilter);
			?>
		<?endif?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:news.list",
			"stroiteli",
			Array(
				"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"NEWS_COUNT" => $arParams["NEWS_COUNT"],
				"SORT_BY1" => $arParams["SORT_BY1"],
				"SORT_ORDER1" => $arParams["SORT_ORDER1"],
				"SORT_BY2" => $arParams["SORT_BY2"],
				"SORT_ORDER2" => $arParams["SORT_ORDER2"],
				"FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
				"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
				"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
				"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
				"IBLOCK_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
				"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
				"SET_TITLE" => $arParams["SET_TITLE"],
				"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
				"MESSAGE_404" => $arParams["MESSAGE_404"],
				"SET_STATUS_404" => $arParams["SET_STATUS_404"],
				"SHOW_404" => $arParams["SHOW_404"],
				"FILE_404" => $arParams["FILE_404"],
				"INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
				"CACHE_TYPE" => $arParams["CACHE_TYPE"],
				"CACHE_TIME" => $arParams["CACHE_TIME"],
				"CACHE_FILTER" => $arParams["CACHE_FILTER"],
				"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
				"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
				"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
				"PAGER_TITLE" => $arParams["PAGER_TITLE"],
				"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
				"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
				"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
				"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
				"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
				"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
				"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
				"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
				"DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
				"DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
				"PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
				"ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
				"USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
				"GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
				"FILTER_NAME" => $arParams["FILTER_NAME"],
				"HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
				"CHECK_DATES" => $arParams["CHECK_DATES"],
			),
			$component
		);?>
	</div>
</main>
<div class="services-block">
  <div class="container-fluid">
    <div class="services-ls row">
      <div class="col-xl-4 col-md-6">
        <div class="services">
          <div class="services__name">Строительство домов</div>
          <div class="services-lists">
            <ul class="services-list">
              <li class="services__item"><a class="services__link" href="">Деревянные дома</a></li>
              <li class="services__item"><a class="services__link" href="">Кирпичные дома</a></li>
              <li class="services__item"><a class="services__link" href="">Дома из бруса</a></li>
              <li class="services__item"><a class="services__link" href="">Каркасные дома</a></li>
            </ul>
            <ul class="services-list">
              <li class="services__item"><a class="services__link" href="">Каркасные дома</a></li>
              <li class="services__item"><a class="services__link" href="">Дома тз бруса</a></li>
              <li class="services__item"><a class="services__link" href="">Деревянные дома</a></li>
              <li class="services__item"><a class="services__link" href="">Кирпичные дома</a></li>
            </ul>
          </div>
          <div class="services-arrow" data-icon="3"></div>
        </div>
      </div>
      <div class="col-xl-4 col-md-6">
        <div class="services services--bg--green">
          <div class="services__name">Подряды</div>
          <div class="services-lists">
            <ul class="services-list">
              <li class="services__item"><a class="services__link" href="">Кровельные работы</a></li>
              <li class="services__item"><a class="services__link" href="">Фундамент</a></li>
              <li class="services__item"><a class="services__link" href="">Кирпичная кладка</a></li>
              <li class="services__item"><a class="services__link" href="">Внутренняя отделка</a></li>
            </ul>
            <ul class="services-list">
              <li class="services__item"><a class="services__link" href="">Внутренняя отделка</a></li>
              <li class="services__item"><a class="services__link" href="">Кирпичная кладка</a></li>
              <li class="services__item"><a class="services__link" href="">Фундамент</a></li>
              <li class="services__item"><a class="services__link" href="">Кровельные работы</a></li>
            </ul>
          </div>
          <div class="services-arrow" data-icon="3"></div>
        </div>
      </div>
      <div class="col-xl-4 col-md-6">
        <div class="services services--bg--purple">
          <div class="services__name">Обустройство</div>
          <div class="services-lists">
            <ul class="services-list">
              <li class="services__item"><a class="services__link" href="">Заборы</a></li>
              <li class="services__item"><a class="services__link" href="">Ворота</a></li>
              <li class="services__item"><a class="services__link" href="">Дороги</a></li>
              <li class="services__item"><a class="services__link" href="">Отопление</a></li>
            </ul>
            <ul class="services-list">
              <li class="services__item"><a class="services__link" href="">Отопление</a></li>
              <li class="services__item"><a class="services__link" href="">Дороги</a></li>
              <li class="services__item"><a class="services__link" href="">Ворота</a></li>
              <li class="services__item"><a class="services__link" href="">Заборы</a></li>
            </ul>
          </div>
          <div class="services-arrow" data-icon="3"></div>
        </div>
      </div>
      <div class="col-xl-4 col-md-6">
        <div class="services">
          <div class="services__name">Строительство домов</div>
          <div class="services-lists">
            <ul class="services-list">
              <li class="services__item"><a class="services__link" href="">Деревянные дома</a></li>
              <li class="services__item"><a class="services__link" href="">Кирпичные дома</a></li>
              <li class="services__item"><a class="services__link" href="">Дома из бруса</a></li>
              <li class="services__item"><a class="services__link" href="">Каркасные дома</a></li>
            </ul>
            <ul class="services-list">
              <li class="services__item"><a class="services__link" href="">Каркасные дома</a></li>
              <li class="services__item"><a class="services__link" href="">Дома тз бруса</a></li>
              <li class="services__item"><a class="services__link" href="">Деревянные дома</a></li>
              <li class="services__item"><a class="services__link" href="">Кирпичные дома</a></li>
            </ul>
          </div>
          <div class="services-arrow" data-icon="3"> </div>
        </div>
      </div>
      <div class="col-xl-4 col-md-6">
        <div class="services services--bg--green">
          <div class="services__name">Подряды</div>
          <div class="services-lists">
            <ul class="services-list">
              <li class="services__item"><a class="services__link" href="">Кровельные работы</a></li>
              <li class="services__item"><a class="services__link" href="">Фундамент</a></li>
              <li class="services__item"><a class="services__link" href="">Кирпичная кладка</a></li>
              <li class="services__item"><a class="services__link" href="">Внутренняя отделка</a></li>
            </ul>
            <ul class="services-list">
              <li class="services__item"><a class="services__link" href="">Внутренняя отделка</a></li>
              <li class="services__item"><a class="services__link" href="">Кирпичная кладка</a></li>
              <li class="services__item"><a class="services__link" href="">Фундамент</a></li>
              <li class="services__item"><a class="services__link" href="">Кровельные работы</a></li>
            </ul>
          </div>
          <div class="services-arrow" data-icon="3"></div>
        </div>
      </div>
      <div class="col-xl-4 col-md-6">
        <div class="services services--bg--purple">
          <div class="services__name">Обустройство</div>
          <div class="services-lists">
            <ul class="services-list">
              <li class="services__item"><a class="services__link" href="">Заборы</a></li>
              <li class="services__item"><a class="services__link" href="">Ворота</a></li>
              <li class="services__item"><a class="services__link" href="">Дороги</a></li>
              <li class="services__item"><a class="services__link" href="">Отопление</a></li>
            </ul>
            <ul class="services-list">
              <li class="services__item"><a class="services__link" href="">Отопление</a></li>
              <li class="services__item"><a class="services__link" href="">Дороги</a></li>
              <li class="services__item"><a class="services__link" href="">Ворота</a></li>
              <li class="services__item"><a class="services__link" href="">Заборы</a></li>
            </ul>
          </div>
          <div class="services-arrow" data-icon="3"> </div>
        </div>
      </div>
    </div>
    <a class="show-all js-services-show-all" href=""><span data-icon="3">Показать еще</span></a>
    <div class="heading-text">
      <div class="row">
        <div class="col-xl-8">
          <h6>Как выбрать строительную компанию?</h6>
          <p>Выбор подрядчика - важнейший этап строительства дома, ремонта или отделочных работ. На нашем сайте собраны надежные подрядчики по строительству, ремонту и отделке домов. Каждая компания прошла проверку на юридическую чистоту и качеству выполненных заказов.</p>
          <p>Для поиска нужного подрядчика используйте фильтр в каталоге, ознакомьтесь с отзывами и портфолио компании, отправьте строительной компании заявку на расчет сметы.</p>
        </div>
      </div>
    </div>
  </div>
</div>
