<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<h1><?=$arResult["NAME"]?></h1>
<div class="art__date"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></div>
<div class="art-body">
	<div class="art-img">
		<img class="lazyload" data-src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>" title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"/>
	</div>
	<?=$arResult["DETAIL_TEXT"]?>
	<div class="share-articles">
		<div class="icon-share">
			<svg class="icon">
				<use xlink:href="#icon-share"></use>
			</svg>
		</div>
		<span>Поделиться статьей:</span>
		<div class="ya-share2" data-services="vkontakte,twitter,odnoklassniki,telegram"></div>
		<!-- <ul class="social-list">
			<li class="social-item"><a class="social social--link js-copylink" href="" aria-label="Копировать ссылку">
					<svg class="icon icon--copy">
						<use xlink:href="#icon-copy"></use>
					</svg></a></li>
			<li class="social-item"><a class="social" href="" aria-label="Вконтакте">
					<svg class="icon">
						<use xlink:href="#icon-vk"> </use>
					</svg></a></li>
			<li class="social-item"><a class="social" href="" aria-label="Twitter">
					<svg class="icon">
						<use xlink:href="#icon-tw"> </use>
					</svg></a></li>
			<li class="social-item"><a class="social" href="" aria-label="Одноклассники">
					<svg class="icon">
						<use xlink:href="#icon-ok"></use>
					</svg></a></li>
			<li class="social-item"><a class="social" href="" aria-label="Телеграмм">
					<svg class="icon">
						<use xlink:href="#icon-tg"></use>
					</svg></a></li>
		</ul> -->
	</div>
</div>
