<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
// dump($arResult["ITEMS"]);
?>
<div class="filter">
		<a class="filter-item <?=(CSite::InDir('/blog/index.php')) ? 'filter-item--active' : '';?>" href="/blog/">Все</a>
	<?foreach($arResult["LABELS"] as $label){
		$active = (CSite::InDir($label["SECTION_PAGE_URL"])) ? 'filter-item--active' : '';?>
		<a class="filter-item <?=$active?>" href="<?=$label["SECTION_PAGE_URL"]?>"><?=$label["NAME"]?></a>
	<?}?>
</div>
<div class="articles-blog">
	<div class="row">
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<div class="col-lg-4 col-sm-6">
				<div class="articles" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
					<a class="articles__img" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
						<img class="lazyload" data-src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>" title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>" /></a>
					<div class="articles-body">
						<div class="tag-list">
							<div class="tag"><?=$arResult["LABELS"][$arItem['IBLOCK_SECTION_ID']]["NAME"]?></div>
						</div>
						<a class="articles__title" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
						<p><?=$arItem["PREVIEW_TEXT"]?>...</p>
					</div>
					<div class="articles-footer">
						<div class="articles__date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></div>
						<div class="count-share">
							<svg class="icon">
								<use xlink:href="#icon-share"></use>
							</svg><span>146</span>
						</div>
					</div>
				</div>
			</div>
		<?endforeach;?>
	</div>
	<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
		<?=$arResult["NAV_STRING"]?>
	<?endif;?>
</div>
