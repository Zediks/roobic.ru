<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

global $typeURL;

$arSort = [
	[
		'NAME' => 'По релевантности',
		'CODE' => 'sort',
		'ICON' => 'type-relevance',
	// ],[
	// 	'NAME' => 'По рейтингу',
	// 	'CODE' => 'rating',
	// 	'ICON' => 'type-rating',
	],[
		'NAME' => 'Сначала дешевле',
		'CODE' => 'price_ask',
		'ICON' => 'type-pricesmall',
	],[
		'NAME' => 'Сначала дороже',
		'CODE' => 'price_desc',
		'ICON' => 'type-pricelarge',
	]
];

// получим св-во Отделка
$rsPropertyEnum = \CIBlockPropertyEnum::GetList([],["IBLOCK_ID"=>3, "CODE"=>"FINISHING"]);
while($arPropertyEnum = $rsPropertyEnum->Fetch()){
	if ($typeURL == 'bani') $arPropertyEnum['XML_ID'] = str_replace('doma','bani',$arPropertyEnum['XML_ID']);
	$arFinishing[$arPropertyEnum['ID']] = [
		'XML_ID' => $arPropertyEnum['XML_ID'],
		'NAME' => $arPropertyEnum['VALUE'],
	];
}

// запишем в куки
// if ($_REQUEST['finishing'])
// 	setcookie("house_finishing", $_REQUEST['finishing'], time()+3600, '/');

if ($_REQUEST['sort'])
	setcookie("house_sort", $_REQUEST['sort'], time()+3600, '/');

$houseFinishing = ($_REQUEST['finishing']) ? $_REQUEST['finishing'] : $_COOKIE['house_finishing'];

// этажность
// if ($_REQUEST['HOUSE_FLOORS'])
// 	setcookie("house_floors", $_REQUEST['HOUSE_FLOORS'], time()+3600, '/');

$houseFloors = ($_REQUEST['HOUSE_FLOORS']) ? $_REQUEST['HOUSE_FLOORS'] : $_COOKIE['house_floors'];

$houseSize = $_REQUEST['HOUSE_SIZE'];
// if ($houseSize) {
	// получим св-во Размер (список)
	$rsPropertyEnum = \CIBlockPropertyEnum::GetList([],["IBLOCK_ID"=>3, "CODE"=>"SIZE_LIST"]);
	while($arPropertyEnum = $rsPropertyEnum->Fetch()){
		$arSize[$arPropertyEnum['XML_ID']] = [
			'ID' => $arPropertyEnum['ID'],
			'NAME' => $arPropertyEnum['VALUE'],
		];
	}
// }

// Наличие коммуникаций
// if ($_REQUEST['communications'])
// 	setcookie("house_communications", $_REQUEST['communications'], time()+3600, '/');

$houseCommunications = ($_REQUEST['communications']) ? $_REQUEST['communications'] : $_COOKIE['house_communications'];

if ($_REQUEST['finishing'] == 'all') { // сбросим
	setcookie('house_finishing', '', time() - 3600, '/');
	setcookie("house_floors", '', time()+3600, '/');
	setcookie("house_communications", '', time()+3600, '/');
	unset($houseFinishing);
	unset($houseFloors);
	unset($houseCommunications);
}

$arTag = [
	'1etazh' => [
		'CODE' => $typeURL.'-1-etazh',
		'NAME' => 'Одноэтажные'
	],
	'2etazh' => [
		'CODE' => $typeURL.'-2-etazh',
		'NAME' => 'Двухэтажные'
	],
];

foreach ($arSize as $key => $value) { // тег Размера
	$arTag[$key] = [
		'CODE' => $typeURL.'-'.$key,
		'NAME' => $value['NAME'].'м'
	];
}

$arSquare = [
	'50m' => '50 м2',
	'60m' => '60 м2',
	'70m' => '70 м2',
	'100m' => '100 м2',
	'120m' => '120 м2',
	'150m' => '150 м2',
];
foreach ($arSquare as $key => $value) { // тег Площадь
	$arTag[$key] = [
		'CODE' => $typeURL.'-'.$key,
		'NAME' => $value
	];
}

$arTagOther = [
	'modul' => 'Модульные/сборные',
	'loft' => 'Лофт',
	'budjet' => 'Бюджетные',
	'svainye' => 'Свайные',
	'komb' => 'Комбинированные',
	'brusa-100x100' => 'Брус 100х100',
	'brusa-150x150' => 'Брус 150х150',
	'small' => 'Небольшие',
	'svayno' => 'Свайно-каркасные',
	'mansardnye' => 'Мансардные',
	'odnoskatnye' => 'Односкатные',
	'karkasno-panelnye' => 'Каркасно-панельные',
	'barnhouse' => 'Барнхаус',
	's-kommun' => 'С коммуникациями',
];
foreach ($arTagOther as $key => $value) { // тег Разное
	$arTag[$key] = [
		'CODE' => $typeURL.'-'.$key,
		'NAME' => $value
	];
}

foreach ($arParams['TAG_SHOW'] as $value)
{
	if ($_REQUEST['PAGE_HOUSE_TYPE']) // тип дома и этаж
		$arTag[$value]['CODE'] = $_REQUEST['PAGE_HOUSE_TYPE'].'-'.$value;
}

foreach ($arFinishing as $value) // теги по умолчанию
{
	$arTagDefault[] = [
		'CODE' => $value['XML_ID'],
		'NAME' => $value['NAME']
	];
}
array_push($arTagDefault,$arTag['1etazh'],$arTag['2etazh']);

$ourDir = $APPLICATION->GetCurDir();
?>
<h1><?$APPLICATION->ShowTitle(false)?></h1>
<?require_once $_SERVER["DOCUMENT_ROOT"].'/local/inc/filterHouses.php';?>
<div class="sorting">
	<div class="row">
		<div class="col-md-10 col-xl-10 hidden-xs sorting-block">
			<div class="filter">
				<a class="filter-item <?if(!$houseFinishing)echo'filter-item--active'?>" href="/<?=$typeURL?>/?finishing=all">Все</a>
				<?if($arParams['TAG_SHOW']):?>
					<?foreach ($arParams['TAG_SHOW'] as $value) {
						$tagUrl = '/'.$arTag[$value]['CODE'].'/';
						$active = ($tagUrl == $ourDir) ? 'filter-item--active' : '';?>
						<a class="filter-item <?=$active?>" href="<?=$tagUrl?>"><?=$arTag[$value]['NAME']?></a>
					<?}?>
				<?else:?>
					<?foreach ($arTagDefault as $value) {
						$tagUrl = '/'.$value['CODE'].'/';
						$active = ($tagUrl == $ourDir) ? 'filter-item--active' : '';?>
						<a class="filter-item <?=$active?>" href="<?=$tagUrl?>"><?=$value['NAME']?></a>
					<?}?>
				<?endif;?>
			</div>
		</div>
		<div class="col-md-2 col-xl-2">
			<div class="sort">
				<a href="#" class="visible-xs sorting-btn"></a>
				<label class="select select--sorting" for="sorting">
					<select class="select__select js-select__letter" name="sort" id="sorting">
						<?foreach ($arSort as $value) { // data-icon="<?=$value['ICON']?>"?>
							<option value="<?=$value['CODE']?>" <?if($arParams["HOUSE_SORT"] == $value['CODE'])echo'selected'?>><?=$value['NAME']?></option>
						<?}?>
					</select>
				</label>
			</div>
		</div>
	</div>
</div>
<?if($arParams["USE_FILTER"]=="Y"):?>
	<?
	// $APPLICATION->IncludeComponent(
	// 	"bitrix:catalog.filter",
	// 	"",
	// 	Array(
	// 		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
	// 		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
	// 		"FILTER_NAME" => $arParams["FILTER_NAME"],
	// 		"FIELD_CODE" => $arParams["FILTER_FIELD_CODE"],
	// 		"PROPERTY_CODE" => $arParams["FILTER_PROPERTY_CODE"],
	// 		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
	// 		"CACHE_TIME" => $arParams["CACHE_TIME"],
	// 		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
	// 		"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
	// 	),
	// 	$component
	// );

	global $arrFilter;

	if ($houseFinishing) $arrFilter['PROPERTY']['FINISHING'] = $houseFinishing; // Отделка

	// этажность
	if ($houseFloors) {
		// if ( == 'single')
		// 	$arrFilter['PROPERTY']['CNT_FLOORS'] = 1;
		// if ($houseFloors == 'multi')
		// 	$arrFilter['PROPERTY']['CNT_FLOORS'] = 2;
		$arrFilter['PROPERTY']['CNT_FLOORS'] = $houseFloors;
	}

	if ($houseSize)
		$arrFilter['PROPERTY']['SIZE_LIST'] = $arSize[$houseSize]['ID'];

	if ($houseCommunications == 'y')
		$arrFilter['PROPERTY']['COMMUNICATIONS'] = 22; // Наличие коммуникаций

	if ($houseType) $arrFilter['PROPERTY']['TYPE'] = $houseType; // тип дома

	if ($houseArea) { // площадь
		$arArea = explode('-',$houseArea); // dump($arArea);
		if ($arArea[0] == 'do')
			$arrFilter['PROPERTY']['<=AREA'] = $arArea[1];
		elseif ($arArea[0] == 'ot')
			$arrFilter['PROPERTY']['>=AREA'] = $arArea[1]-5;
		elseif ($arArea[0] && $arArea[1]) {
			$arrFilter['PROPERTY']['><AREA'] = [$arArea[0]-5,$arArea[1]+5];
		} else {
			$arrFilter['PROPERTY']['>=AREA'] = $arArea[0]-5;
			$arrFilter['PROPERTY']['<AREA'] = $arArea[0]+5;
		}
	}

	if ($housePrice) { // стоимость
		$arPrice = explode('-',$housePrice); // dump($arPrice);
		if ($arPrice[0] == 'do')
			$arrFilter['PROPERTY']['<=PRICE'] = $arPrice[1];
		elseif ($arPrice[0] == 'ot')
			$arrFilter['PROPERTY']['>=PRICE'] = $arPrice[1];
		elseif ($arPrice[0] && $arPrice[1]) {
			$arrFilter['PROPERTY']['><PRICE'] = [$arPrice[0],$arPrice[1]];
		} else {
			// $arrFilter['PROPERTY']['>=PRICE'] = $arPrice[0];
			// $arrFilter['PROPERTY']['<=PRICE'] = $arPrice[1];
			if ($arPrice[0] == 1)
				$arrFilter['PROPERTY']['<=PRICE'] = 1000000;
			elseif ($arPrice[0] < 100) {
				$minusSum = ($arPrice[0] == 15) ? 5000000 : 1000000;
				$arPrice[0] .= '000000';
				$arrFilter['PROPERTY']['>PRICE'] = $arPrice[0]-$minusSum;
				$arrFilter['PROPERTY']['<=PRICE'] = $arPrice[0];
			}
		}
	}
	// dump($arrFilter);
	$metaInfo = getMetaInfo($arrFilter); // dump($metaInfo);
	$cntPos = $metaInfo['cntPos'];
	if ($cntPos > 0) {
		$title = $APPLICATION->GetProperty("title");
		$newTitle = str_replace('проекты и цены','проекты и цены, кол-во домов '.$metaInfo['cntPos'],$title);
		$APPLICATION->SetPageProperty("title", $newTitle);
	}
	?>
<?endif?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"doma",
	Array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"NEWS_COUNT" => $arParams["NEWS_COUNT"],
		"SORT_BY1" => $arParams["SORT_BY1"],
		"SORT_ORDER1" => $arParams["SORT_ORDER1"],
		"SORT_BY2" => $arParams["SORT_BY2"],
		"SORT_ORDER2" => $arParams["SORT_ORDER2"],
		"FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
		"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
		"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"IBLOCK_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
		"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
		"SET_TITLE" => $arParams["SET_TITLE"],
		"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
		"MESSAGE_404" => $arParams["MESSAGE_404"],
		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
		"SHOW_404" => $arParams["SHOW_404"],
		"FILE_404" => $arParams["FILE_404"],
		"INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_FILTER" => $arParams["CACHE_FILTER"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
		"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
		"PAGER_TITLE" => $arParams["PAGER_TITLE"],
		"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
		"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
		"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
		"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
		"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
		"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
		"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
		"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
		"DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
		"DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
		"PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
		"ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
		"USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
		"GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
		"FILTER_NAME" => $arParams["FILTER_NAME"],
		"HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
		"CHECK_DATES" => $arParams["CHECK_DATES"],
	),
	$component
);?>
</div> <!-- container-fluid -->
<?
// получим материалы
$arMaterials = getElHL(11,[],['UF_ACTIVE'=>true],['*']);
foreach ($arMaterials as $key => $value) // распределим для формирования
  $materials[$value['UF_MAIN_SECTION']][$value['ID']] = $value;

$arMainSection = [
	'Дома из дерева' => 'doma-iz-dereva',
	'Дома из камня' => 'kamennye-doma',
	'Бани' => 'bani',
];
?>
<div class="services-block block-fast-url">
	<div class="container-fluid">
		<div class="services-ls row">
			<?foreach ($materials as $idMain => $mainSection) {?>
				<div class="col-xl-4 col-md-6">
					<div class="services">
						<div class="services__name"><a href="/<?=$arMainSection[$idMain]?>/"><?=$idMain?></a></div>
						<div class="services-lists">
							<ul class="services-list">
								<?foreach ($mainSection as $section) {?>
									<li class="services__item"><a class="services__link" href="/<?=$section['UF_XML_ID']?>/"><?=$section['UF_SHORT']?></a></li>
								<?}?>
							</ul>
						</div>
						<div class="services-arrow" data-icon="3"></div>
					</div>
				</div>
			<?}?>
			<?if ($typeURL == 'bani'):?>
				<div class="col-xl-4 col-md-6">
					<div class="services">
						<div class="services__name">Кол-во спален</div>
						<div class="services-lists">
							<ul class="services-list">
								<li class="services__item">
									<a class="services__link" href="/banya-1-spalnya/">1 спальня</a>
								</li>
								<?for ($i=2; $i <= 4; $i++) {?>
									<li class="services__item">
										<a class="services__link" href="/banya-<?=$i?>-spalni/"><?=$i?> спальни</a>
									</li>
								<?}?>
								<?for ($i=5; $i <= 10; $i++) {?>
									<li class="services__item">
										<a class="services__link" href="/banya-<?=$i?>-spalen/"><?=$i?> спален</a>
									</li>
								<?}?>
							</ul>
						</div>
						<div class="services__name">Кол-во санузлов</div>
						<div class="services-lists">
							<ul class="services-list">
									<li class="services__item">
										<a class="services__link" href="/bani-1sanuzel/">1 санузлел</a>
									</li>
								<?for ($i=2; $i <= 4; $i++) {?>
									<li class="services__item">
										<a class="services__link" href="/bani-<?=$i?>sanuzla/"><?=$i?> санузла</a>
									</li>
								<?}?>
							</ul>
						</div>
						<div class="services-arrow" data-icon="3"></div>
					</div>
				</div>
				<div class="col-xl-4 col-md-6">
					<div class="services services--bg--green">
						<div class="services__name">Площадь</div>
						<div class="services-lists">
							<ul class="services-list">
								<?for ($i=70; $i < 200; $i+=10) {?>
									<li class="services__item">
										<a class="services__link" href="/bani-<?=$i?>m/"><?=$i?> м2</a>
									</li>
								<?}?>
								<?for ($i=200; $i <= 500; $i+=50) {?>
									<li class="services__item">
										<a class="services__link" href="/bani-<?=$i?>m/"><?=$i?> м2</a>
									</li>
								<?}?>
							</ul>
						</div>
						<div class="services-arrow" data-icon="3"></div>
					</div>
				</div>
				<div class="col-xl-4 col-md-6">
					<div class="services services--bg--purple">
						<div class="services__name">Этажность</div>
						<div class="services-lists">
							<ul class="services-list">
								<li class="services__item"><a class="services__link" href="/bani-1-etazh/">Одноэтажные</a></li>
								<li class="services__item"><a class="services__link" href="/bani-2-etazh/">Двухэтажные</a></li>
							</ul>
						</div>
						<div class="services__name">Стоимость</div>
						<div class="services-lists">
							<ul class="services-list">
									<li class="services__item">
										<a class="services__link" href="/bani-do1mln/">До 1 млн. рублей</a>
									</li>
								<?for ($i=2; $i <= 10; $i++) {?>
									<li class="services__item">
										<a class="services__link" href="/bani-<?=$i?>mln/">До <?=$i?> млн. рублей</a>
									</li>
								<?}?>
									<li class="services__item">
										<a class="services__link" href="/bani-15mln/">До 15 млн. рублей</a>
									</li>
							</ul>
						</div>
						<div class="services-arrow" data-icon="3"></div>
					</div>
				</div>
			<?else:?>
				<div class="col-xl-4 col-md-6">
					<div class="services">
						<div class="services__name">Кол-во спален</div>
						<div class="services-lists">
							<ul class="services-list">
								<li class="services__item">
									<a class="services__link" href="/dom-1-spalnya/">1 спальня</a>
								</li>
								<?for ($i=2; $i <= 4; $i++) {?>
									<li class="services__item">
										<a class="services__link" href="/dom-<?=$i?>-spalni/"><?=$i?> спальни</a>
									</li>
								<?}?>
								<?for ($i=5; $i <= 10; $i++) {?>
									<li class="services__item">
										<a class="services__link" href="/dom-<?=$i?>-spalen/"><?=$i?> спален</a>
									</li>
								<?}?>
							</ul>
						</div>
						<div class="services__name">Кол-во санузлов</div>
						<div class="services-lists">
							<ul class="services-list">
									<li class="services__item">
										<a class="services__link" href="/doma-1sanuzel/">1 санузлел</a>
									</li>
								<?for ($i=2; $i <= 4; $i++) {?>
									<li class="services__item">
										<a class="services__link" href="/doma-<?=$i?>sanuzla/"><?=$i?> санузла</a>
									</li>
								<?}?>
							</ul>
						</div>
						<div class="services-arrow" data-icon="3"></div>
					</div>
				</div>
				<div class="col-xl-4 col-md-6">
					<div class="services services--bg--green">
						<div class="services__name">Площадь</div>
						<div class="services-lists">
							<ul class="services-list">
								<?for ($i=70; $i < 200; $i+=10) {?>
									<li class="services__item">
										<a class="services__link" href="/doma-<?=$i?>m/"><?=$i?> м2</a>
									</li>
								<?}?>
								<?for ($i=200; $i <= 500; $i+=50) {?>
									<li class="services__item">
										<a class="services__link" href="/doma-<?=$i?>m/"><?=$i?> м2</a>
									</li>
								<?}?>
							</ul>
						</div>
						<div class="services-arrow" data-icon="3"></div>
					</div>
				</div>
				<div class="col-xl-4 col-md-6">
					<div class="services services--bg--purple">
						<div class="services__name">Этажность</div>
						<div class="services-lists">
							<ul class="services-list">
								<li class="services__item"><a class="services__link" href="/doma-1-etazh/">Одноэтажные</a></li>
								<li class="services__item"><a class="services__link" href="/doma-2-etazh/">Двухэтажные</a></li>
							</ul>
						</div>
						<div class="services__name">Стоимость</div>
						<div class="services-lists">
							<ul class="services-list">
									<li class="services__item">
										<a class="services__link" href="/doma-do1mln/">До 1 млн. рублей</a>
									</li>
								<?for ($i=2; $i <= 10; $i++) {?>
									<li class="services__item">
										<a class="services__link" href="/doma-<?=$i?>mln/">До <?=$i?> млн. рублей</a>
									</li>
								<?}?>
									<li class="services__item">
										<a class="services__link" href="/doma-15mln/">До 15 млн. рублей</a>
									</li>
							</ul>
						</div>
						<div class="services-arrow" data-icon="3"></div>
					</div>
				</div>
			<?endif;?>
		</div>
		<a class="show-all js-services-show-all" href=""><span data-icon="3">Показать еще</span></a>
		<div class="heading-text">
			<div class="row">
				<div class="col-xl-8">
					<?if($arParams['TEXT_SEO']):?>
						<?=$arParams['~TEXT_SEO']?>
					<?else:?>
						<h6>О проектах домов</h6>
						<p>Примеры готовых проектов помогут в выборе строительной компании. Внимательно изучите проект понравившегося дома, ознакомьтесь с отзывами о компании, отправьте заявку на расчет сметы.</p>
						<p>На сайте <a target="_blank" href="/">stroiman.ru</a> собраны только надежные подрядчики по строительству и отделке домов. Каждая компания прошла проверку на юридическую чистоту и качество выполненных заказов.</p>
					<?endif;?>
				</div>
			</div>
		</div>
	</div>
</div>
