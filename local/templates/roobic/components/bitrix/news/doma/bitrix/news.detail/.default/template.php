<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use Bitrix\Main\Grid\Declension;

global $typeURL, $typeURLOne;

// выводим правильные окончания
$cntBedroom = $arResult['PROPERTIES']['CNT_BEDROOM']['VALUE'];
if (!$cntBedroom) $cntBedroom = 0;
$bedroomDeclension = new Declension('спальня', 'спальни', 'спален');
$bedroomDeclensionURL = new Declension('spalnya', 'spalni', 'spalen');
$bedroomText = $bedroomDeclension->get($cntBedroom);
$bedroomText = ($cntBedroom > 0) ? $cntBedroom.' '.$bedroomText : 'Нет спален';
$bedroomURL = '/'.$typeURLOne.'-'.$cntBedroom.'-'.$bedroomDeclensionURL->get($cntBedroom).'/';
// санузлы
$cntBathroom = $arResult['PROPERTIES']['CNT_BATHROOM']['VALUE'];
if (!$cntBathroom) $cntBathroom = 0;
$bathroomDeclension = new Declension('санузел', 'санузла', 'санузлов');
$bathroomDeclensionURL = new Declension('sanuzel', 'sanuzla', 'sanuzla');
$bathroomText = $bathroomDeclension->get($cntBathroom);
$bathroomText = ($cntBathroom > 0) ? $cntBathroom.' '.$bathroomText : 'Нет санузлов';
$bathroomURL = '/'.$typeURL.'-'.$cntBathroom.$bathroomDeclensionURL->get($cntBathroom).'/';

// dump($_COOKIE); // разбираем куки
if(isset($_COOKIE['comparison_house'])){
	$arComparison = explode('-',$_COOKIE['comparison_house']);
}
if(isset($_COOKIE['favorites_house'])){
	$arFavorites = explode('-',$_COOKIE['favorites_house']);
}
$comparison = (in_array($arResult['ID'],$arComparison)) ? 'Y' : 'N';
$favorites = (in_array($arResult['ID'],$arFavorites)) ? 'Y' : 'N';
$comp_active = ($comparison == 'Y') ? 'control--favorites--active' : '';
$fav_active = ($favorites == 'Y') ? 'control--favorites--active' : '';
$comp_text = ($comparison != 'Y') ? 'Добавить к сравнению' : 'Удалить из сравнения';
$fav_text = ($favorites != 'Y') ? 'Добавить в избранное' : 'Удалить из избранного';

// ссылка площади
if ($arResult['PROPERTIES']['AREA']['VALUE'] < 200)
	$areaURL = round($arResult['PROPERTIES']['AREA']['VALUE'],'-1');
elseif ($arResult['PROPERTIES']['AREA']['VALUE'] < 500)
	$areaURL = round($arResult['PROPERTIES']['AREA']['VALUE'],'-1.5');
$areaURL = '/'.$typeURL.'-'.$areaURL.'m/';

$floorURL = ($arResult['PROPERTIES']['CNT_FLOORS']['VALUE'] == 1) ? '/'.$typeURL.'-1-etazh/' : '/'.$typeURL.'-2-etazh/';
$materialURL = '/'.$arResult['PROPERTIES']['TYPE']['VALUE'].'/';

foreach ($arResult['LAYOUT'] as $value)
	if ($value['UF_FILE']) $arLayoutImg[] = $value['UF_FILE'];

$photoAll = [];
if ($arResult['PROPERTIES']['PHOTO']['VALUE'])
	$photoAll = array_merge($photoAll,$arResult['PROPERTIES']['PHOTO']['VALUE']);
if ($arResult['PROPERTIES']['FACADES']['VALUE'])
	$photoAll = array_merge($photoAll,$arResult['PROPERTIES']['FACADES']['VALUE']);
if ($arLayoutImg)
	$photoAll = array_merge($photoAll,$arLayoutImg);

if (getBath($arResult['PROPERTIES']['TYPE']['VALUE']) == 'бани')
	$floorText = ($arResult['PROPERTIES']['CNT_FLOORS']['VALUE'] == 1)?'одноэтажной':'двухэтажной';
else
	$floorText = ($arResult['PROPERTIES']['CNT_FLOORS']['VALUE'] == 1)?'одноэтажного':'двухэтажного';

$typeName = ($arItem['PROPERTIES']['BATH']['VALUE']) ? 'Баня' : 'Дом';
$typeNameM = ($arItem['PROPERTIES']['BATH']['VALUE']) ? 'бани' : 'дома';

$areaVal = $arResult['PROPERTIES']['AREA']['VALUE'];
$priceVal = $arResult['PROPERTIES']['PRICE']['VALUE'];

// ссылка на стоимость дома
if ($priceVal <= 1000000)
	$priceUrl = '/doma-do1mln/';

$j=1;
for ($i=2000000; $i <= 10000000; $i+=1000000) {
	$j++;
	if (!$priceUrl && $priceVal <= $i)
		$priceUrl = '/doma-'.$j.'mln/';
}

if(!$priceUrl) $priceUrl = '/doma-15mln/';

$size = ($arResult['PROPERTIES']['SIZE_LIST']['VALUE'])?$arResult['PROPERTIES']['SIZE_LIST']['VALUE']:$arResult['PROPERTIES']['SIZE']['VALUE'];

$arPurpose = [
	'fam_01' => 'Семья с одним ребенком',
	'fam_02' => 'Семья с двумя детьми',
	'fam_03' => 'Для большой семьи',
];

if ($cntBedroom > 3)
	$purpose = 'fam_03';
elseif($cntBedroom > 2)
	$purpose = 'fam_02';
else
	$purpose = 'fam_01';
?>
	<h1>Проект <?=$floorText?> <?=getBath($arResult['PROPERTIES']['TYPE']['VALUE'])?> <?=$arResult['NAME']?></h1>
	<div class="proect__description">
		<?if($arResult['COMPANY_URL']):?>
			<a href="<?=$arResult['COMPANY_URL']?>" class="proect__cat"><?=$arResult['COMPANY_NAME']?></a>
			<span class="proect__raiting"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/sorting_02.svg" alt=""><?=$arResult['COMPANY_RATING']?></span>
			<a href="<?=$arResult['COMPANY_URL']?>#reviews-block" class="proect__rew"><?=$arResult['COMPANY_REVIEWS']?></a>
		<?else:?>
			<span class="proect__cat"><?=$arResult['PROPERTIES']['COMPANY_TEXT']['VALUE']?></span>
		<?endif;?>
	</div>
	<div class="row">
		<div class="col-xl-8 col-lg-7">
			<div class="proect__slider">
				<div class="swiper-container gallery-top">
					<div class="right-control">
						<div class="control control--favorites <?=$comp_active?>">
							<div class="control__icon js-add-favorites" data-id="<?=$arResult['ID']?>" data-cookie="comparison_house">
								<svg id="icon-favorites" class="icon-compare" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14 14" style="enable-background:new 0 0 14 14;"
									xml:space="preserve">
									<path d="M2,14H0V1.2C0,0.5,0.4,0,1,0l0,0c0.6,0,1,0.5,1,1.2V14z" />
									<path d="M6,14H4V7.2C4,6.5,4.4,6,5,6l0,0c0.6,0,1,0.5,1,1.2V14z" />
									<path d="M10,14H8V1.2C8,0.5,8.4,0,9,0l0,0c0.6,0,1,0.5,1,1.2V14z" />
									<path d="M14,14h-2V7.2C12,6.5,12.4,6,13,6l0,0c0.6,0,1,0.5,1,1.2V14z" />
								</svg>
							</div>
							<div class="control__container">
								<div class="control__main"><?=$comp_text?></div>
							</div>
						</div>
						<div class="control control--favorites <?=$fav_active?>">
							<div class="control__icon js-add-favorites" data-id="<?=$arResult['ID']?>" data-cookie="favorites_house">
								<svg id="icon-favorites" viewBox="0 0 50 50" width="100%" height="100%">
									<path d="M33.2,17.8l-0.1-0.1c-0.9-1.1-2.2-1.6-3.6-1.6s-2.6,0.6-3.6,1.6l-0.9,1l-0.9-1c-0.9-1.1-2.2-1.6-3.6-1.6s-2.6,0.6-3.6,1.6l-0.1,0.1c-0.9,1-1.4,2.4-1.4,3.9c0,1.5,0.5,2.8,1.4,3.9l0,0l7.7,8.3c0.1,0.1,0.3,0.2,0.5,0.2s0.4-0.1,0.5-0.2l7.7-8.3l0,0c0.9-1,1.4-2.4,1.4-3.9C34.6,20.2,34.1,18.8,33.2,17.8z"></path>
								</svg>
							</div>
							<div class="control__container">
								<div class="control__main"><?=$fav_text?></div>
							</div>
						</div>
					</div>
					<div class="swiper-wrapper">
						<?foreach ($photoAll as $photo) {
							$photoRes = CFile::ResizeImageGet($photo, array('width' => 880, 'height' => 548), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);?>
							<a href="<?=CFile::GetPath($photo)?>" data-fancybox="gallery" class="swiper-slide" style="background-image:url(<?=$photoRes['src']?>)"></a>
						<?}?>
					</div>
					<div class="swiper-pagination"></div>
					<div data-icon="5" class="swiper-button-next swiper-button-white"></div>
					<div data-icon="4" class="swiper-button-prev swiper-button-white"></div>
				</div>
				<div class="thumbs">
					<div class="swiper-container gallery-thumbs">
						<div class="swiper-wrapper">
							<?foreach ($photoAll as $photo) {
								$photoRes = CFile::ResizeImageGet($photo, array('width' => 390, 'height' => 278), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);?>
								<div class="swiper-slide" style="background-image:url(<?=$photoRes['src']?>)"></div>
							<?}?>
						</div>
						<!-- Add Arrows -->
						<div data-icon="5" class="swiper-button-next swiper-button-white"></div>
						<div data-icon="4" class="swiper-button-prev swiper-button-white"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xl-4 col-lg-5">
			<div class="proect__desc">
				<div class="proect__block">
					<div class="proect__top">
						<div class="proect__top-property">Площадь: <span><a href="<?=$areaURL?>"><?=round($arResult['PROPERTIES']['AREA']['VALUE'])?> м<sup>2</sup></a></span></div>
						<div class="proect__top-property">Размер: <span><?=$size?></span></div>
					</div>
					<div class="proect__comfort">
						<div class="proect__comfort-property"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/bed.svg" alt=""><span><a href="<?=$bedroomURL?>"><?=$bedroomText?></a></span></div>
						<div class="proect__comfort-property"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/bath.svg" alt=""><span><a href="<?=$bathroomURL?>"><?=$bathroomText?></a></span></div>
					</div>
				</div>
				<div class="proect__suitable">
					<div class="proect__suitable-title"><?=$typeName?> подходит для:</div>
					<div class="proect__suitable-property">
						<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/<?=$purpose?>.svg" alt="">
						<span><?=$arPurpose[$purpose]?></span>
					</div>
				</div>
				<div class="proect__specifications">
					<div class="proect__specifications-item">
						<div class="proect__specifications-name">Этажей:</div>
						<div class="proect__specifications-value">
							<a href="<?=$floorURL?>"><?=$arResult['PROPERTIES']['CNT_FLOORS']['VALUE']?></a>
						</div>
					</div>
					<div class="proect__specifications-item">
						<div class="proect__specifications-name">Стены:</div>
						<div class="proect__specifications-value">
							<a href="<?=$materialURL?>"><?=$arResult['DISPLAY_PROPERTIES']['MATERIAL_WALL']['DISPLAY_VALUE']?></a>
						</div>
					</div>
					<div class="proect__specifications-item">
						<div class="proect__specifications-name">Кровля:</div>
						<div class="proect__specifications-value"><?=$arResult['DISPLAY_PROPERTIES']['MATERIAL_ROOF']['DISPLAY_VALUE']?></div>
					</div>
					<div class="proect__specifications-item proect__specifications-price">
						<div class="proect__specifications-name">Цена:</div>
						<div class="proect__specifications-value">
							<?if($arResult['PROPERTIES']['OLD_PRICE']['VALUE']){?>
								<span class="old-price"><s><?=formatPrice($arResult['PROPERTIES']['OLD_PRICE']['VALUE'])?> ₽</s></span>
							<?}?>
							<span class="new-price"><a href="<?=$priceUrl?>"><?=formatPrice($priceVal)?> ₽</a></span>
						</div>
					</div>
				</div>
				<a href="#" class="btn btn--red" data-toggle="modal" data-target="#modal-callback">Оставить заявку</a>
			</div>
		</div>
	</div>
</div> <!-- container-fluid -->
<?if($arResult['LAYOUT']):?>
	<section class="plan">
		<div class="container-fluid">
			<h5 class="h-title">Планировка</h5>
			<div class="plan__slider">
				<div class="swiper-container plan-sliders">
					<div class="swiper-wrapper">
						<?foreach ($arResult['LAYOUT'] as $layout) { // dump($layout);
							$photoRes = CFile::ResizeImageGet($layout['UF_FILE'], array('width' => 445, 'height' => 470), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);?>
							<div class="swiper-slide">
								<div class="plan-sliders-container">
									<a data-fancybox="gallery_2" href="<?=CFile::GetPath($layout['UF_FILE'])?>">
										<div class="plan-sliders-title"><?=$layout['UF_NAME']?>:</div>
										<div class="plan-sliders-plottage"><?=$layout['UF_PLOTTAGE']?> м<sup>2</sup></div>
										<div class="plan-sliders-img">
											<!-- <a data-fancybox="gallery_2" href="<?=CFile::GetPath($layout['UF_FILE'])?>" class="plan-zoom"></a> -->
											<span class="plan-zoom"></span>
											<img src="<?=$photoRes['src']?>" alt="">
										</div>
									</a>
								</div>
							</div>
						<?}?>
					</div>
					<!-- Add Arrows -->
					<div data-icon="5" class="swiper-button-next swiper-button-white"></div>
					<div data-icon="4" class="swiper-button-prev swiper-button-white"></div>
				</div>
			</div>
		</div>
	</section>
<?endif;?>
<section class="facades-block">
	<div class="container-fluid">
		<?if($arResult['PROPERTIES']['FACADES']['VALUE']):?>
			<h5 class="h-title">Фасады</h5>
			<div class="facades__items row">
				<?foreach ($arResult['PROPERTIES']['FACADES']['VALUE'] as $key => $value) {
					$photoRes = CFile::ResizeImageGet($value, array('width' => 620, 'height' => 400), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);?>
					<div class="facades__item col-xl-3 col-lg-3 col-sm-6">
						<a data-fancybox="gallery_3" href="<?=CFile::GetPath($value)?>">
							<img src="<?=$photoRes['src']?>" alt="">
						</a>
					</div>
				<?}?>
			</div>
			<div class="facades__slider">
				<div class="swiper-container plan-sliders">
					<div class="swiper-wrapper">
						<?foreach ($arResult['PROPERTIES']['FACADES']['VALUE'] as $key => $value) {
							$photoRes = CFile::ResizeImageGet($value, array('width' => 620, 'height' => 400), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);?>
							<div class="swiper-slide">
								<a href="<?=CFile::GetPath($value)?>">
									<img src="<?=$photoRes['src']?>" alt="">
								</a>
							</div>
						<?}?>
					</div>
					<!-- Add Arrows -->
					<div data-icon="5" class="swiper-button-next swiper-button-white"></div>
					<div data-icon="4" class="swiper-button-prev swiper-button-white"></div>
				</div>
			</div>
		<?endif;?>
		<?if($arResult['DETAIL_TEXT']):?>
			<div class="facades__about">
				<h5 class="h-title">О проекте</h5>
				<?=$arResult['DETAIL_TEXT']?>
				<p><?=($arResult['PROPERTIES']['GUARANTEES']['VALUE'])?'Гарантии: '.$arResult['PROPERTIES']['GUARANTEES']['VALUE']:''?></p>
			</div>
		<?endif;?>
	</div>
</section>
<div class="specifications-block">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-6 col-lg-6 col-sm-6">
				<div class="specifications__container">
					<h5 class="h-title">Характеристики</h5>
					<ul>
						<?if($arResult['PROPERTIES']['AREA']['VALUE']):?>
							<li><span>Общая площадь:</span> <span class="specifications__value"><?=$arResult['PROPERTIES']['AREA']['VALUE']?> м<sup>2</sup></span></li>
						<?endif;?>
						<?if($arResult['PROPERTIES']['AREA_LIVING']['VALUE']):?>
							<li><span>Жилая площадь:</span> <span class="specifications__value"><?=$arResult['PROPERTIES']['AREA_LIVING']['VALUE']?> м<sup>2</sup></span></li>
						<?endif;?>
						<?if($arResult['PROPERTIES']['CNT_FLOORS']['VALUE']):?>
							<li><span>Количество этажей:</span> <span class="specifications__value"><?=$arResult['PROPERTIES']['CNT_FLOORS']['VALUE']?></span></li>
						<?endif;?>
						<?if($arResult['PROPERTIES']['GARAGE']['VALUE']):?>
							<li><span>Гараж:</span> <span class="specifications__value"><?=$arResult['PROPERTIES']['GARAGE']['VALUE']?> <?if(is_int($arResult['PROPERTIES']['GARAGE']['VALUE'])){?>м<sup>2</sup><?}?></span></li>
						<?endif;?>
						<?if($arResult['PROPERTIES']['HEIGHT']['VALUE']):?>
							<li><span>Высота <?=$typeNameM?>:</span> <span class="specifications__value"><?=$arResult['PROPERTIES']['HEIGHT']['VALUE']?> м</span></li>
						<?endif;?>
						<?if($arResult['PROPERTIES']['AREA_ROOF']['VALUE']):?>
							<li><span>Площадь кровли:</span> <span class="specifications__value"><?=$arResult['PROPERTIES']['AREA_ROOF']['VALUE']?> м<sup>2</sup></span></li>
						<?endif;?>
						<!-- <li><span>Фундамент:</span> <span class="specifications__value"><?=$arResult['PROPERTIES']['FOUNDATION']['VALUE']?></span></li> -->
						<?foreach ($arResult['PROP_ADDITIONAL'] as $value) {
							if ($value['NAME'] == 'Размер (список)') $value['NAME'] = 'Размер';
							if ($arResult['PROPERTIES'][$value['CODE']]['VALUE']){?>
								<li><span><?=$value['NAME']?>:</span> <span class="specifications__value"><?=$arResult['PROPERTIES'][$value['CODE']]['VALUE']?></span></li>
							<?}
						}?>
					</ul>
					<!-- <a href="#" class="specifications__more"><span data-icon="3">Показать все</span></a> -->
				</div>
			</div>
			<div class="col-xl-6 col-lg-6 col-sm-6">
				<div class="specifications__video">
					<a data-src="<?=$arResult['PROPERTIES']['VIDEO']['VALUE']?>" class="play js-open-video" data-toggle="modal" data-target="#modalVideo"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/play.svg" alt=""></a>
					<div class="specifications__video-container">
						<img class="specifications__video-cover" src="<?=SITE_TEMPLATE_PATH?>/images/video-cover.png" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?if($arResult['LIST_WORKS']):?>
<section class="materials-block">
	<div class="container-fluid">
		<h5 class="h-title">Материалы и перечень работ</h5>
		<div class="tabs">
			<input id="tab1" type="radio" name="tabs" checked>
			<label for="tab1" class="project_01 col-xl-2 col-lg-2 col-sm-2">
				<span class="tabs__ico"></span>
				<span class="tabs__name">Фундамент</span>
			</label>

			<input id="tab2" type="radio" name="tabs">
			<label for="tab2" class="project_02 col-xl-2 col-lg-2 col-sm-2">
				<span class="tabs__ico"></span>
				<span class="tabs__name">Коробка <?=$typeNameM?></span>
			</label>

			<input id="tab3" type="radio" name="tabs">
			<label for="tab3" class="project_03 col-xl-2 col-lg-2 col-sm-2">
				<span class="tabs__ico"></span>
				<span class="tabs__name">Кровля</span>
			</label>

			<input id="tab4" type="radio" name="tabs">
			<label for="tab4" class="project_04 col-xl-2 col-lg-2 col-sm-2">
				<span class="tabs__ico"></span>
				<span class="tabs__name">Окна и двери</span>
			</label>

			<input id="tab5" type="radio" name="tabs">
			<label for="tab5" class="project_05 col-xl-2 col-lg-2 col-sm-2">
				<span class="tabs__ico"></span>
				<span class="tabs__name">Инженерия</span>
			</label>

			<input id="tab6" type="radio" name="tabs">
			<label for="tab6" class="project_06 col-xl-2 col-lg-2 col-sm-2">
				<span class="tabs__ico"></span>
				<span class="tabs__name">Фасад и отделка</span>
			</label>
			<div class="clr"></div>
			<section id="content-tab1" class="materials-content">
				<div class="row">
					<div class="col-xl-4 col-lg-5 col-sm-5">
						<img src="<?=SITE_TEMPLATE_PATH?>/images/list_works_01.jpeg" alt="">
					</div>
					<div class="col-xl-8 col-lg-7 col-sm-7">
						<ul>
							<?foreach ($arResult['LIST_WORKS'][13]['STEPS'] as $stepText) {?>
								<li><?=$stepText?></li>
							<?}?>
						</ul>
					</div>
				</div>
			</section>
			<section id="content-tab2" class="materials-content">
				<div class="row">
					<div class="col-xl-4 col-lg-5 col-sm-5">
						<img src="<?=SITE_TEMPLATE_PATH?>/images/list_works_02.jpeg" alt="">
					</div>
					<div class="col-xl-8 col-lg-7 col-sm-7">
						<ul>
							<?foreach ($arResult['LIST_WORKS'][14]['STEPS'] as $stepText) {?>
								<li><?=$stepText?></li>
							<?}?>
						</ul>
					</div>
				</div>
			</section>
			<section id="content-tab3" class="materials-content">
				<div class="row">
					<div class="col-xl-4 col-lg-5 col-sm-5">
						<img src="<?=SITE_TEMPLATE_PATH?>/images/list_works_03.jpeg" alt="">
					</div>
					<div class="col-xl-8 col-lg-7 col-sm-7">
						<ul>
							<?foreach ($arResult['LIST_WORKS'][15]['STEPS'] as $stepText) {?>
								<li><?=$stepText?></li>
							<?}?>
						</ul>
					</div>
				</div>
			</section>
			<section id="content-tab4" class="materials-content">
				<div class="row">
					<div class="col-xl-4 col-lg-5 col-sm-5">
						<img src="<?=SITE_TEMPLATE_PATH?>/images/list_works_04.jpeg" alt="">
					</div>
					<div class="col-xl-8 col-lg-7 col-sm-7">
						<ul>
							<?foreach ($arResult['LIST_WORKS'][16]['STEPS'] as $stepText) {?>
								<li><?=$stepText?></li>
							<?}?>
						</ul>
					</div>
				</div>
			</section>
			<section id="content-tab5" class="materials-content">
				<div class="row">
					<div class="col-xl-4 col-lg-5 col-sm-5">
						<img src="<?=SITE_TEMPLATE_PATH?>/images/list_works_05.jpeg" alt="">
					</div>
					<div class="col-xl-8 col-lg-7 col-sm-7">
						<ul>
							<?foreach ($arResult['LIST_WORKS'][17]['STEPS'] as $stepText) {?>
								<li><?=$stepText?></li>
							<?}?>
						</ul>
					</div>
				</div>
			</section>
			<section id="content-tab6" class="materials-content">
				<div class="row">
					<div class="col-xl-4 col-lg-5 col-sm-5">
						<img src="<?=SITE_TEMPLATE_PATH?>/images/list_works_06.jpeg" alt="">
					</div>
					<div class="col-xl-8 col-lg-7 col-sm-7">
						<ul>
							<?foreach ($arResult['LIST_WORKS'][18]['STEPS'] as $stepText) {?>
								<li><?=$stepText?></li>
							<?}?>
						</ul>
					</div>
				</div>
			</section>
		</div>
		<div class="mobile-tabs">
			<label class="select" for="option-1">
				<select class="select__select select__select-calc select__styler" id="select" name="option-1">
					<option value="tab1" data-img="<?=SITE_TEMPLATE_PATH?>/images/svg/project_01_activ.svg">Фундамент</option>
					<option value="tab2" data-img="<?=SITE_TEMPLATE_PATH?>/images/svg/project_02_activ.svg">Коробка <?=$typeNameM?></option>
					<option value="tab3" data-img="<?=SITE_TEMPLATE_PATH?>/images/svg/project_03_activ.svg">Кровля</option>
					<option value="tab4" data-img="<?=SITE_TEMPLATE_PATH?>/images/svg/project_04_activ.svg">Окна и двери</option>
					<option value="tab5" data-img="<?=SITE_TEMPLATE_PATH?>/images/svg/project_05_activ.svg">Инженерия</option>
					<option value="tab6" data-img="<?=SITE_TEMPLATE_PATH?>/images/svg/project_06_activ.svg">Фасад и отделка</option>
				</select>
			</label>
			<div id="tabs">
				<div id="tab1" class="active">
					<section id="content-tab1" class="materials-content">
						<div class="row">
							<div class="col-xl-4 col-lg-5 col-sm-5">
								<img src="<?=$arResult['LIST_WORKS'][13]['FILE']?>" alt="">
							</div>
							<div class="col-xl-8 col-lg-7 col-sm-7">
								<ul>
									<?foreach ($arResult['LIST_WORKS'][13]['STEPS'] as $stepText) {?>
										<li><?=$stepText?></li>
									<?}?>
								</ul>
							</div>
						</div>
					</section>
				</div>
				<div id="tab2">
					<section id="content-tab2" class="materials-content">
						<div class="row">
							<div class="col-xl-4 col-lg-5 col-sm-5">
								<img src="<?=$arResult['LIST_WORKS'][14]['FILE']?>" alt="">
							</div>
							<div class="col-xl-8 col-lg-7 col-sm-7">
								<ul>
									<?foreach ($arResult['LIST_WORKS'][14]['STEPS'] as $stepText) {?>
										<li><?=$stepText?></li>
									<?}?>
								</ul>
							</div>
						</div>
					</section>
				</div>
				<div id="tab3">
					<section id="content-tab3" class="materials-content">
						<div class="row">
							<div class="col-xl-4 col-lg-5 col-sm-5">
								<img src="<?=$arResult['LIST_WORKS'][15]['FILE']?>" alt="">
							</div>
							<div class="col-xl-8 col-lg-7 col-sm-7">
								<ul>
									<?foreach ($arResult['LIST_WORKS'][15]['STEPS'] as $stepText) {?>
										<li><?=$stepText?></li>
									<?}?>
								</ul>
							</div>
						</div>
					</section>
				</div>
				<div id="tab4">
					<section id="content-tab4" class="materials-content">
						<div class="row">
							<div class="col-xl-4 col-lg-5 col-sm-5">
								<img src="<?=$arResult['LIST_WORKS'][16]['FILE']?>" alt="">
							</div>
							<div class="col-xl-8 col-lg-7 col-sm-7">
								<ul>
									<?foreach ($arResult['LIST_WORKS'][16]['STEPS'] as $stepText) {?>
										<li><?=$stepText?></li>
									<?}?>
								</ul>
							</div>
						</div>
					</section>
				</div>
				<div id="tab5">
					<section id="content-tab5" class="materials-content">
						<div class="row">
							<div class="col-xl-4 col-lg-5 col-sm-5">
								<img src="<?=$arResult['LIST_WORKS'][17]['FILE']?>" alt="">
							</div>
							<div class="col-xl-8 col-lg-7 col-sm-7">
								<ul>
									<?foreach ($arResult['LIST_WORKS'][17]['STEPS'] as $stepText) {?>
										<li><?=$stepText?></li>
									<?}?>
								</ul>
							</div>
						</div>
					</section>
				</div>
				<div id="tab6">
					<section id="content-tab6" class="materials-content">
						<div class="row">
							<div class="col-xl-4 col-lg-5 col-sm-5">
								<img src="<?=$arResult['LIST_WORKS'][18]['FILE']?>" alt="">
							</div>
							<div class="col-xl-8 col-lg-7 col-sm-7">
								<ul>
									<?foreach ($arResult['LIST_WORKS'][18]['STEPS'] as $stepText) {?>
										<li><?=$stepText?></li>
									<?}?>
								</ul>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	</div>
</section>
<?endif;?>
<section class="else-block">
	<div class="container-fluid">
		<?if($arResult['PROPERTIES']['INCLUDED_PRICE']['VALUE']):?>
			<h5 class="h-title">Что еще входит в стоимость:</h5>
			<div class="else-list">
				<div class="row">
					<?foreach ($arResult['PROPERTIES']['INCLUDED_PRICE']['VALUE'] as $key => $value) {
						switch ($arResult['PROPERTIES']['INCLUDED_PRICE']['VALUE_ENUM_ID'][$key]) {
							case 8: $class = ''; $img = 'advan_01'; break;
							case 9: $class = 'else--green'; $img = 'advan_02'; break;
							case 10: $class = 'else--purple'; $img = 'advan_03'; break;
							case 11: $class = 'else--green'; $img = 'advan_04'; break;
							default: $class = ''; $img = 'advan_01'; break;
						}?>
						<div class="col-xl-3 col-lg-3 col-sm-6">
							<div class="else <?=$class?>">
								<div>
									<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/<?=$img?>.svg" alt="">
								</div>
								<strong> <span><?=$value?></span></strong>
							</div>
						</div>
					<?}?>
				</div>
			</div>
		<?endif;?>
		<div class="form-index">
			<h5 class="h-title">Отправьте свои контакты и мы расчитаем вам смету</h5>
			<form class="form-contancts send-form place_show" method="post" enctype="multipart/form-data">
				<input type="hidden" name="act" value="sendEstimate">
				<div class="form-contancts--column">
					<label for="name">
						<input class="form-input" type="text" id="name" name="name" placeholder="Ваше имя" required>
					</label>
					<label for="phone">
						<input class="form-input" type="tel" id="phone" name="phone" placeholder="Номер телефона" inputmode="text" required>
					</label>
				</div>
				<div class="form-contancts--column-2">
					<label for="comment">
						<textarea name="comment" id="comment" placeholder="Ваши пожелания"></textarea>
					</label>
				</div>
				<div class="form-contancts--column form-contancts--column-md-last">
					<div class="politic">
						<label class="fake-check" for="politic">
							<input type="checkbox" id="politic" checked="checked">
							<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 22 22" style="enable-background:new 0 0 22 22;" xml:space="preserve">
								<g>
									<path class="checks" d="M9.6,15.6l-3.2-3.1c-0.1-0.1-0.2-0.3-0.2-0.4c0-0.2,0-0.3,0.2-0.5c0.1-0.1,0.3-0.2,0.4-0.2s0.3,0.1,0.4,0.2l2.7,2.6l4.8-7.7c0.1-0.1,0.2-0.2,0.4-0.2c0.2,0,0.3,0,0.5,0.1c0.1,0.1,0.2,0.2,0.3,0.4c0,0.2,0,0.3-0.1,0.5l-5.2,8.4c0,0,0,0.1,0,0.1c0,0,0,0-0.1,0.1l0,0c-0.1,0-0.1,0.1-0.2,0.1c0,0,0,0-0.1,0c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1-0.1C9.7,15.7,9.7,15.7,9.6,15.6C9.6,15.7,9.6,15.7,9.6,15.6C9.6,15.6,9.6,15.6,9.6,15.6z"></path>
								</g>
							</svg>
							<span>Нажимая на кнопку, вы даете согласие на обработку персональных данных и соглашаетесь с <a href="/politika-konfidentsialnosti/"> Политикой Конфиденциальности</a></span>
						</label>
					</div>
					<button class="btn btn--red">Отправить</button>
				</div>
				<div class="form-contancts--column-3">
					<!-- <div class="form-contancts--column">
						<label class="fake-check align-items-center konkurs" for="konkurs">
							<input type="checkbox" id="konkurs" name="konkurs" value="Да" checked />
							<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 22 22" style="enable-background:new 0 0 22 22;" xml:space="preserve">
								<g>
									<path class="checks" d="M9.6,15.6l-3.2-3.1c-0.1-0.1-0.2-0.3-0.2-0.4c0-0.2,0-0.3,0.2-0.5c0.1-0.1,0.3-0.2,0.4-0.2s0.3,0.1,0.4,0.2
										l2.7,2.6l4.8-7.7c0.1-0.1,0.2-0.2,0.4-0.2c0.2,0,0.3,0,0.5,0.1c0.1,0.1,0.2,0.2,0.3,0.4c0,0.2,0,0.3-0.1,0.5l-5.2,8.4
										c0,0,0,0.1,0,0.1c0,0,0,0-0.1,0.1l0,0c-0.1,0-0.1,0.1-0.2,0.1c0,0,0,0-0.1,0c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1-0.1
										C9.7,15.7,9.7,15.7,9.6,15.6C9.6,15.7,9.6,15.7,9.6,15.6C9.6,15.6,9.6,15.6,9.6,15.6z"></path>
								</g>
							</svg>
							<span>Включить компанию в конкурс </span>
						</label>
					</div> -->
					<div class="form-contancts--column">
						<div class="file-input">
							<input type="file" name="files">
							<svg class="icon">
								<svg id="icon-clip" viewBox="0 0 22 22">
									<path fill="#FFFFFF" d="M18,22H4c-2.2,0-4-1.8-4-4V4c0-2.2,1.8-4,4-4h14c2.2,0,4,1.8,4,4v14C22,20.2,20.2,22,18,22z"></path>
									<g>
										<g>
											<path fill="#959595" d="M15.5,5.9c-1.2-1.2-3.1-1.2-4.2,0l-4.5,4.5c-0.1,0.1-0.1,0.4,0,0.5c0.1,0.1,0.4,0.1,0.5,0l4.5-4.5c0.9-0.9,2.4-0.9,3.2,0c0.9,0.9,0.9,2.3,0,3.2l-6.2,6.3c-0.5,0.5-1.5,0.5-2,0c-0.5-0.5-0.5-1.4,0-2l6-6c0.2-0.2,0.5-0.2,0.7,0c0.2,0.2,0.2,0.5,0,0.7l-5.7,5.8c-0.1,0.1-0.1,0.4,0,0.5c0.1,0.1,0.4,0.1,0.5,0L14,9.1c0.5-0.5,0.5-1.3,0-1.7c-0.5-0.5-1.3-0.5-1.7,0l-6,6c-0.8,0.8-0.8,2.2,0,3C6.7,16.8,7.2,17,7.8,17s1.1-0.2,1.5-0.6l6.2-6.3C16.6,8.9,16.6,7,15.5,5.9z"></path>
										</g>
									</g>
								</svg>
							</svg>
							<span class="file-name__show">Прикрепить документ</span>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>
<?
	global $arrFilterElse;
	$areaFrom = $areaVal - ($areaVal * (20 / 100)); // -20%
	$areaTo = $areaVal + ($areaVal * (20 / 100)); // +20%
	$priceFrom = $priceVal - ($priceVal * (10 / 100)); // -10%
	$priceTo = $priceVal + ($priceVal * (10 / 100)); // +10%
	$arrFilterElse = [
		'!ID' => $arResult['ID'],
		'PROPERTY_MATERIAL_WALL' => $arResult['PROPERTIES']['MATERIAL_WALL']['VALUE'],
		'PROPERTY_CNT_FLOORS' => $arResult['PROPERTIES']['CNT_FLOORS']['VALUE'],
		'>=PROPERTY_AREA' => $areaFrom,
		'<=PROPERTY_AREA' => $areaTo,
		'>=PROPERTY_PRICE' => $priceFrom,
		'<=PROPERTY_PRICE' => $priceTo,
	];
	// dump($arrFilterElse);
?>
<section class="company-list-block">
	<div class="container-fluid">
		<h5 class="h-title">Скорее всего вам подойдут следующие проекты домов:</h5>
		<?$APPLICATION->IncludeComponent(
			"bitrix:news.list",
			"doma",
			Array(
				"ACTIVE_DATE_FORMAT" => "d.m.Y",
				"ADD_SECTIONS_CHAIN" => "N",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_ADDITIONAL" => "",
				"AJAX_OPTION_HISTORY" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "Y",
				"CACHE_TIME" => "36000000",
				"CACHE_TYPE" => "A",
				"CHECK_DATES" => "Y",
				"DETAIL_URL" => "",
				"DISPLAY_BOTTOM_PAGER" => "N",
				"DISPLAY_DATE" => "N",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"DISPLAY_TOP_PAGER" => "N",
				"FIELD_CODE" => array("",""),
				"FILTER_NAME" => "arrFilterElse",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"IBLOCK_ID" => "3",
				"IBLOCK_TYPE" => "content",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"INCLUDE_SUBSECTIONS" => "Y",
				"MESSAGE_404" => "",
				"NEWS_COUNT" => "3",
				"PAGER_BASE_LINK_ENABLE" => "N",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_TEMPLATE" => "roobic",
				"PAGER_TITLE" => "Новости",
				"PARENT_SECTION" => "",
				"PARENT_SECTION_CODE" => "",
				"PREVIEW_TRUNCATE_LEN" => "",
				"PROPERTY_CODE" => array("TYPE","COMPANY",""),
				"SET_BROWSER_TITLE" => "N",
				"SET_LAST_MODIFIED" => "N",
				"SET_META_DESCRIPTION" => "N",
				"SET_META_KEYWORDS" => "N",
				"SET_STATUS_404" => "N",
				"SET_TITLE" => "N",
				"SHOW_404" => "N",
				"SORT_BY1" => "ACTIVE_FROM",
				"SORT_BY2" => "SORT",
				"SORT_ORDER1" => "DESC",
				"SORT_ORDER2" => "ASC",
				"STRICT_SECTION_CHECK" => "N"
			)
		 );?>
	</div>
</section>
