<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

// dump($arResult);

use Bitrix\Main\Grid\Declension;
// выводим правильное окончание
$reviewsDeclension = new Declension('отзыв', 'отзыва', 'отзывов');

$companyID = $arResult['PROPERTIES']['COMPANY']['VALUE'];
$arCompany = $arResult['DISPLAY_PROPERTIES']['COMPANY']['LINK_ELEMENT_VALUE'][$companyID];
$arResult['COMPANY_NAME'] = $arCompany['NAME'];
$arResult['COMPANY_URL'] = $arCompany['DETAIL_PAGE_URL'];

// получим отзывы
$arElHL = getElHL(9,[],['UF_COMPANY' => $companyID],['ID','UF_COMPANY','UF_EXPERTISE','UF_DEADLINE','UF_SERVICE','UF_PRICE','UF_QUALITY']);
foreach ($arElHL as $key => $value)
{
	$sumScore = (int)$value['UF_EXPERTISE'] + (int)$value['UF_DEADLINE'] + (int)$value['UF_SERVICE'] + (int)$value['UF_PRICE'] + (int)$value['UF_QUALITY'];
  $rating = $sumScore / 5;
  $value['RATING'] = round($rating,2);

	$arReviews[$value['UF_COMPANY']][] = $value;
}

// переберем компанию
$cntReviews = count($arReviews[$companyID]);
if ($cntReviews) // рейтинг
{
	$ratingSum = 0;
	foreach ($arReviews[$companyID] as $value)
		$ratingSum += $value['RATING'];

	$rating = $ratingSum / $cntReviews;
}
else $rating = 0;

$arResult['COMPANY_RATING'] = $rating;

// отзывы
$reviewsText = $reviewsDeclension->get($cntReviews);
$reviewsText = ($cntReviews > 0) ? $cntReviews.' '.$reviewsText : 'Нет отзывов';

$arResult['COMPANY_REVIEWS'] = $reviewsText;

$arResult['LAYOUT'] = getElHL(10,[],['UF_HOUSE'=>$arResult['ID']],['*']);

$arListWorks = getElHL(14,[],['UF_HOUSE'=>$arResult['ID']],['*']);
foreach ($arListWorks as $value) {
 $photoRes = CFile::ResizeImageGet($value['UF_FILE'], array('width' => 848, 'height' => 690), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
 $arResult['LIST_WORKS'][$value['UF_TYPE']] = [
   'STEPS' => $value['UF_STEPS'],
   'FILE' => $photoRes['src'],
 ];
}

// новые свойства
$arResult['PROP_ADDITIONAL'] = [
	[
		'CODE' => 'STYLE',
		'NAME' => 'Стиль, разновидность',
		'REQUIRED' => false
	],
	[
		'CODE' => 'FORM',
		'NAME' => 'Форма',
		'REQUIRED' => false
	],
	[
		'CODE' => 'TYPE_ROOF',
		'NAME' => 'Тип кровли',
		'REQUIRED' => false
	],
	[
		'CODE' => 'ANNEX',
		'NAME' => 'Пристрой',
		'REQUIRED' => false
	],
	[
		'CODE' => 'FINISHING',
		'NAME' => 'Отделка',
		'REQUIRED' => false
	],
	[
		'CODE' => 'FINISHING_FACADE',
		'NAME' => 'Отделка фасада',
		'REQUIRED' => false
	],
	[
		'CODE' => 'INSULATION',
		'NAME' => 'Утеплитель',
		'REQUIRED' => false
	],
	[
		'CODE' => 'SIZE_LIST',
		'NAME' => 'Размер (список)',
		'REQUIRED' => false
	],
	[
		'CODE' => 'FOUNDATION_LIST',
		'NAME' => 'Фундамент',
		'REQUIRED' => false
	],
	[
		'CODE' => 'CLASS',
		'NAME' => 'Класс дома',
		'REQUIRED' => false
	],
	[
		'CODE' => 'SIZE_BEAM',
		'NAME' => 'Размер бруса',
		'REQUIRED' => false
	],
	[
		'CODE' => 'THICKNESS_BEAM',
		'NAME' => 'Толщина бруса',
		'REQUIRED' => false
	],
];

if (getBath($arResult['PROPERTIES']['TYPE']['VALUE']) == 'бани') {
	$floorText = ($arResult['PROPERTIES']['CNT_FLOORS']['VALUE'] == 1) ? 'одноэтажной' : 'двухэтажной';
	// $seoTitle = 'Загородной '.$floorText.' бани '.$arResult['NAME'].': цена и описание проекта, заказать строительство бани в Москве и Московской области';
	// $setDescription = 'Загородной '.$floorText.' бани '.$arResult['NAME'].': цена и описание проекта. Заказать строительство бани на нашем портале у проверенных застройщиков в Москве и Московской области.';
	$seoTitle = 'Заказать одноэтажную баню '.$arResult['NAME'].': цена и описание проекта, заказать строительство бани в Москве и Московской области';
	$setDescription = 'Одноэтажная баня '.$arResult['NAME'].': цена и описание проекта. Заказать строительство бани на нашем портале у проверенных застройщиков в Москве и Московской области ★★★ Stroiman.ru';
} else {
	$floorText = ($arResult['PROPERTIES']['CNT_FLOORS']['VALUE'] == 1) ? 'одноэтажный' : 'двухэтажный';
	$seoTitle = 'Загородный '.$floorText.' дом '.$arResult['NAME'].': цена и описание проекта, заказать строительство коттеджа в Москве и Московской области';
	$setDescription = 'Загородный '.$floorText.' дом '.$arResult['NAME'].': цена и описание проекта. Заказать строительство коттеджа на нашем портале у проверенных застройщиков в Москве и Московской области.';
}

$cp = $this->__component;
if (is_object($cp))
{
  $cp->arResult["SEO_TITLE"] = $seoTitle;
  $cp->arResult["SEO_DESCRIPTION"] = $setDescription;
  $cp->SetResultCacheKeys(array("SEO_TITLE","SEO_DESCRIPTION")); //cache keys in $arResult array
}

// dump($arResult['PROPERTIES']['INCLUDED_PRICE']);
