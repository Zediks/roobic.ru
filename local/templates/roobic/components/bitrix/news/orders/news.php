<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
global $USER;
// dump($_REQUEST);
// получим значения для фильтра
// $arElHL = getElHL(3,[],[],['ID','UF_MAIN_SECTION','UF_SUBSECTION','UF_NAME','UF_XML_ID']);
// foreach ($arElHL as $key => $value) {
// 	$arService[$value['UF_MAIN_SECTION']][$value['UF_XML_ID']] = $value['UF_NAME'];
// 	$arServiceIDs[$value['UF_MAIN_SECTION']][] = $value['UF_XML_ID'];
// }
// $arServicesMain = getListProperty([],["USER_FIELD_ID" => 72]); // Главный раздел
// // $arServicesSub = getListProperty([],["USER_FIELD_ID" => 73]); // Подраздел

$regionURL = getRegionURL();

$arElHL = getElHL(17,[],[],['ID','UF_NAME','UF_XML_ID']);
foreach ($arElHL as $key => $value)
	$arCategory[$value['UF_XML_ID']] = $value['UF_NAME'];
?>
<?$this->SetViewTarget('body-class');?>
	class="orders-page"
<?$this->EndViewTarget();?>
	<h1 class="d-none d-sm-block"><?$APPLICATION->ShowTitle(false)?></h1>
	<div class="row">
		<div class="col-md-12 col-lg-8">
			<div class="order__new">
				<h1 class="d-block d-sm-none"><?$APPLICATION->ShowTitle(false)?></h1>
				<div class="order__new-title">
					<span class="order__new-title-ico"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="60px" height="54px">
							<image x="0px" y="0px" width="60px" height="54px" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA2CAYAAACbZ/oUAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH5AcDDg8tKU3tegAADUFJREFUaN7NmntwXOV1wH/n3qvdlVYrqQHZaWy5CbYckE1rO+CpKYmdhGkcx3HGCS1tRCHO0GKGx5hSKDYTpsVJaKN0II4nhIbB7QyPSQQYnITaY8Akxo+WUEwA48ag2sFgx7FkvbWr3b2nf9x793737q60SpziM7Ozu9/zvM/5zvfJzJltqAIoIoJ6fwCC/5cB17guHeDWE/b63+H4yUGM8VLDXHOPyvtZloWqRvAOBopYgyKyW4TNqvoWCGISXGGjr7iue9fIyAjpdJpEIokIqIIIUwYTp+j8gHjvu9b1VZVCoYCIhePYZXuNjo4wPp6nsbHx16B/ATwnM2bM9CVpEi2ArnJd9ymAG2+8kc98ZgUNDelJpPb/CwcPHqSzs5OlS5dy7733xtnBiRMneOCBB9i27Yc0NKR/BSxwAk6FRAOoBdw6NjbGxo0bueGG698TgiaDsbEx8vk86XSaOXNml/XPmTOHJUuW8MUvdrJz587pDQ0NX7JKJEaF1FIsFudPnz6dK6748/earqowPj6OZVm4rlt1jG3bdHZ2+qovl/gEl6llXbFYtBsbG0mn05zNUMFZlUFr67k4joOqJnyCyz2c93lvbHMqICLIJB7O0AC1Qo8sBL+DBaq4+7MOJsPRZIih0lApFp/tUAuu5hjHi6sa6Qw4MpGqHDp0iEOHDrFixQoSiQQAxWKRbdu20dfXh22HcdFkYvDb1KJ4m9mey42zbNlS5s6dW4ZDqM61JwWOMT3Yqiau3XrrbfzoRz9kz569XHLJEgCGh0e46aZ1HDv2SyzLiq0b4TnRZCNoI5YTCK5b5J577qlIcIhn7SrtxGNwKI2JufblL6+hvb2d88//cKktlUqxZs2XOH78XRzHiUgxLt2JmBr0q0Iul2PRokVVx07V/JxwkpTSucm8HsDq1atZvXp1pC2ZTHDXXf84JQTODNSi0t4YJ+Q6FXLqsxtCXzPxOE+gHl1OoBJBwu59Ty7t4eFh+vr6mDVrVmTho0ePMjo6WpOWTEZMEBbb2trIZDITjquFMSA4ofNQVAObC2NwtcXWrbuZ7u7H+MlPdrFgwQIARkZGufzyP6On539JJOpiXI5KIu6Vq7UNDAzy3e/ex9VXX1UzswqFAo7jVOjRICx51JvqHLj8apKaNauNuXPbI5y3LIvp06eTzWapq0sQxnVKvyci0GRK0NfU1ERDQ0NV4kyBnD59mq6ub7J79wucc87vsXbtWpYvX15ygOA7rdCbloemahK+8847Wb9+PXV1oSQbGup5/PHHyOfzFefEvXStSU59fX3F9mAN27ZxXZd1627moYceIpPJkM1m2b37BXbs2E59fUNJeI5xJDSQQkEqqpgJJrEBpFIpUqnUpEScKVCFRCJBT08PTz75JAsXLuR73/tXdu7cyfr1d/Doo49y5ZVXlhjrSxiirl1ylVSwdiSiUvtt0tWJcPD6FMuyGB8fJ5/Pk8lk6Ojo4M0330LE8ytmyHWCjMa3n3NU9a9EZCXQOBEimzZtYvv27dx///20tbUBkM3muO666zh27FjEacRTxgABMyGJSy1g+OjoKLfffjvLl3+qImNFhPHxcc477zwWL17Mvn37WLLkEk6dOoVlWaxY8emIs3SMDT4L8m3gD2qRxr59+9mxYwcnTvyqRHA+n2fPnj309PSUMq1qtho4s4DwSgVEj4ljdHZ2VsVD1ds3lUqxefNmbrnlFl555RUaGxv56lc3smrVKvbv318a6/jc/hzI46pqAy8APwD5mqpmqhG/adO32LBhPRdeeGGpLZ1u4JFHHmZoaAjbtickJH6AiDIi7CsWXTo6LphArcPf8+fP4+mnf8yRI0fIZDJMmzbNX6NY0hoH9AOqPADYwLeBm0EcYONEYam1tZXW1tZIm2VZXHTRRZNqx5mEuGO1bZvZs8vrW8EQC1gLcq6IPA9yE1AEmsGLUWf/2bg2h1iKwyKyygvM0vWe5NNDA/DTHZDLQulIGWDpQl0CPrmSQqIex7Z/oy1MTXWADwI5EV6O2tzEIUlVOXz4MMWiy+zZ55WKAFOGo2/C1i1g13l7Cj7XBWxhrABdjzzBMydP01xfz5o1a/j858NTWu2RMzw82CKSBUaD8DRZcf3kyZNcf/0N7N27F1Vl3rx53Hffd5gzZ87UCT7/j2Dd12BsBGwrum2xyDV//xW+/+LLtCQTuaLrJp999lm2bNkypfJxqPKC5f0vUYlI8KmeS9999z+xbds2VPUI8MaePXvYsOEOisXi1Al2HJi/CC7+KCz6E/hI+PnF+z/EM28eYVpT04lUff2CdDq9LpFIsHXrk1OUbgiWNzGseIS5gDj5fL6syD00NMhzzz1HS0tL3rbt5ZZlfbS5uXng5ZcPcOrUqaljMAHkBvrRXBYRjgKHROR5y7LI5bKxkbVRLgJWKEHviOg7riHHcY4dPfpLuru7I5Nef/0g77zzDo7jvKGq/wO8z3Xd+qamTNUk/zcFEQlqY5bvW5KAUS8LwlJNq6GqWMGZN1Bff4FREbkjkUjwjW90cfz48dK0ffv2k81mEbF2+02fzGZziYULF9DU1HRGCTahejW1Vr325lvleW5w+Oexurq67W+/fYyurm9GCE4kEojwjN90mesWufTSS39nxMbxC8ArBU8eQo2MTqzquS6IyN81NzflHn74EXbt2sVrr73GSy/9jFQqNQbsFZGUKn+cyWS4+OKLf0dERpEG7+pkcHCQkZERLMua9FQ3NDSE67qISKF0eKiU16rq6yLWv1iWu+Gqq66mrq6OsbExbNt+XFVPisiS8fHcjHnz5nHBBRdwpsE7gEAYLr1iwKuvvsqyZR/HdV2SySSu6zIwMEAymax4Fu/u7kbEQlV/5piJRpUbgX+wbTtRLBb/slh0Hdu2nxaRv/XqXiwbHy/Q0dFBf38/rutWLRpM1B6AeXAQEU6d6g1GYd57jYyMMDg4mAVJ1dfXc+DAAT72saWkUimuvfZvWLnys1iW0Nvby4MPPsjWrU+RTjcMAv8mM2e2DariAh8E7a92FQI0qaoF9Bt9O1X1soaGdFnRLq6W5bSGyU15f3BSKjA8PALoi8BiYLGI/KeqvgvycWAV0OW6RVzXzRaLxRR4B5uAMf39/WQymdOqXAP6hBNyMMrxKNEAOhgb0wLyhyLoyMjw8NCQF7Crvc+IloGj7Z7kSpRm/O+ciCQ9x1RWMDiiyi+AXaDYtn3MsqxPOI7zBVW9u7e3FxHJWpaVymQyqGoX8AQYdenKnDfvbsJN/UQlA3quiPTatr3Etu3h6Jxokby8eOetG2NqO7BLRPpUWQp8AnSzd3IT07kGCVPSN60eETnsMUCoq6s7AvKnoFeo6kbgIwH+TpxYsxhfLmmNDkYsIKeqPYBbfr9sSrfyMS5mw+f4RYi3RXjDNyFzrPjrBL4nOG0k/GUCj3VEhMOqPO/j0hLU2q0I/qXybFj1N8+bwdWkL41WX+KjfhAzpFlCMXLNEaxRKTsKCPGZZvl7JmOJRR7o8zRA/f/aB/T5F/p5EekDBnxBJX063IDG2N2SiXR4IxF1ZMF/+bTfdkCVYvC+Km5v5tsrU6LhETT+PkyJKoJZZJSfq2o7SMHv/G+gHSj4eP2XKu0+Y0K7IjQfJ3r2rfTirWJ9ulmEtT5iD8dvGDzGRS/p4qXg6AVetFJZKVT5eBQ8aZbG+f9LaxZ8+4+tEzLWiSJQ7l7DwB9xMH8NOkNEXlLVH5fbZ1RKldLXaGnG1JwI0cFEx2ROtEgRveqNm2FEV1SDXDrqmEypV/DiLSLc7BP+dZBCOCR0WqG9l4c8U5uC/cv7BSCvGtqsecCJaqFWlGbwUCfkt5gvAOJlnXIb88d2quoHQF4S4Skzvob2pgYh4ZoTveUwCTac5EF8Gy3XovKHOFGzCGve4Z2ZfyFezploUcBUS1Uu9zfYrEqxkjrH55qqVu4Tyh+8gKiPrGGjEltXI1EjWsQoMVqjOKkZh6PXHmb10iCoGWQ+UAB+ap5PKyBdQqJSXDc1IiQc/92Ypsodj8ZsuJrEIxoavEMrOWerklqZsTPmyBpFaFHV0yC/Nl8PhJIML7nCeKsllYtWWEytUkBPijAIfBj4QrV6sxnHo3athv8AVf2UX6M7GuxhxeOjCVXsRjwJa8G4hItxNo5ctSdRoXPxEX1HVb8OYonIP4NkTKaHb7IkYu/leT8ATSCd/v9/DxrNTCsfXTRqd740Sjfd8Qcl8ZAWrGMiG3VOYX/g9HwivgV6UFVnA7eFxJlSjOwUM6nSOitB3w/6c2C3UYhXRMRSZSbIQGU1KrX9frBJ+alHKzgOqKKVlHvw0tgscAvwH6D+N2+JIJVz8rhteyVXVb3WD0lbgkwQQGbMaBsDTYHkgqgRjWMldQly7wTQC9oGMhaP4ea8KMRDXtzBxRmn3SJyub/mmLlApWeLsb0ENAmMAu0i8m4w3gF9RkQ6VNWJciqa+hnIFoAXRWR84gSlqpbEUAs9eSy1vM1Ta5kWHA8rJSjVXhuISF5VHwV51zSh/wM4nMoREFm5NQAAAABJRU5ErkJggg==" />
						</svg></span>
					Разместите заказ и получайте предложения от частных мастеров и компаний, вам останется только сравнить и выбрать подходящего исполнителя.
				</div>
				<?if($USER->IsAuthorized()):?>
					<a href="/lk/client_order" class="order__new-btn btn">Разместить заказ</a>
				<?else:?>
					<a href="#" data-toggle="modal" data-target="#modalLogin" class="order__new-btn btn defGroup" data-group="6">Разместить заказ</a>
				<?endif;?>
			</div>
			<form id="order__filter" class="order__filter d-block d-sm-none" method="get">
				<div class="row">
					<div class="col-sm-6">
						<div class="order__filter-title">Фильтр по заказам</div>
						<div class="order__filter-block">
							<label class="select">
								<select class="select__select select__styler changeUrl" name="service" data-url="/zakazy/<?=$regionURL?>">
										<option value="">--- Выберите ---</option>
									<?foreach ($arCategory as $key => $value) {?>
										<option value="<?=$key?>" <?if ($key == $_REQUEST['service']) echo 'selected'?>><?=$value?></option>
									<?}?>
								</select>
							</label>
						</div>
					</div>
					<?/*if($_REQUEST['servicesMain']):?>
						<div class="col-sm-6">
							<div class="order__filter-title">Теги</div>
							<div class="order__filter-block">
								<label class="select">
									<select class="select__select select__styler" name="service">
											<option value="">--- Выберите ---</option>
										<?foreach ($_REQUEST['servicesMain'] as $serviceID) {?>
											<optgroup label="<?=$arServicesMain[$serviceID]?>">
												<?foreach ($arService[$serviceID] as $key => $value) {?>
													<option value="<?=$key?>" <?if ($key == $_REQUEST['service']) echo 'selected'?>><?=$value?></option>
												<?}?>
											</optgroup>
										<?}?>
										</option>
									</select>
								</label>
							</div>
						</div>
					<?endif;*/?>
				</div>
			</form>
			<?if($arParams["USE_FILTER"] == "Y"):

				global $arrFilterOrders;
				if ($_REQUEST['service'])
					$arrFilterOrders['PROPERTY']['CATEGORY'] = $_REQUEST['service'];
				// elseif ($_REQUEST['servicesMain'])
				// 	foreach ($_REQUEST['servicesMain'] as $serviceID)
				// 		foreach ($arServiceIDs[$serviceID] as $service_XML_ID)
				// 			$arrFilterOrders['PROPERTY']['SERVICE'][] = $service_XML_ID;
				?>
				<?/*$APPLICATION->IncludeComponent(
					"bitrix:catalog.filter",
					"",
					Array(
						"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
						"IBLOCK_ID" => $arParams["IBLOCK_ID"],
						"FILTER_NAME" => $arParams["FILTER_NAME"],
						"FIELD_CODE" => $arParams["FILTER_FIELD_CODE"],
						"PROPERTY_CODE" => $arParams["FILTER_PROPERTY_CODE"],
						"CACHE_TYPE" => $arParams["CACHE_TYPE"],
						"CACHE_TIME" => $arParams["CACHE_TIME"],
						"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
						"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
					),
					$component
				);*///dump($arrFilterOrders);?>
			<?endif?>
			<?$APPLICATION->IncludeComponent(
				"bitrix:news.list",
				"orders",
				Array(
					"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					"NEWS_COUNT" => $arParams["NEWS_COUNT"],
					"SORT_BY1" => $arParams["SORT_BY1"],
					"SORT_ORDER1" => $arParams["SORT_ORDER1"],
					"SORT_BY2" => $arParams["SORT_BY2"],
					"SORT_ORDER2" => $arParams["SORT_ORDER2"],
					"FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
					"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
					"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
					"SET_TITLE" => $arParams["SET_TITLE"],
					"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
					"MESSAGE_404" => $arParams["MESSAGE_404"],
					"SET_STATUS_404" => $arParams["SET_STATUS_404"],
					"SHOW_404" => $arParams["SHOW_404"],
					"FILE_404" => $arParams["FILE_404"],
					"INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
					"ADD_SECTIONS_CHAIN" => $arParams["ADD_SECTIONS_CHAIN"],
					"CACHE_TYPE" => $arParams["CACHE_TYPE"],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
					"CACHE_FILTER" => $arParams["CACHE_FILTER"],
					"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
					"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
					"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
					"PAGER_TITLE" => $arParams["PAGER_TITLE"],
					"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
					"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
					"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
					"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
					"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
					"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
					"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
					"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
					"DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
					"DISPLAY_NAME" => "Y",
					"DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
					"DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
					"PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
					"ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
					"USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
					"GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
					"FILTER_NAME" => $arParams["FILTER_NAME"],
					"HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
					"CHECK_DATES" => $arParams["CHECK_DATES"],
					"STRICT_SECTION_CHECK" => $arParams["STRICT_SECTION_CHECK"],
					"PARENT_SECTION" => $arResult["VARIABLES"]["SECTION_ID"],
					"PARENT_SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
					"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
					"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
					"IBLOCK_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
				),
				$component
			);?>
		</div>
		<div class="col-md-12 col-lg-4 d-none d-lg-block">
			<?require_once $_SERVER["DOCUMENT_ROOT"].'/local/inc/textOrders.php';?>
			<form class="" action="" method="get">
				<div class="order__filter">
					<div class="order__filter-title">Фильтр по заказам:</div>
					<div class="order__filter-items">
						<?foreach ($arCategory as $key => $value) {?>
							<div class="order__filter-item">
								<input id="order__filter-<?=$key?>" class="checkbox" type="checkbox" name="service" value="<?=$key?>" <?if ($key == $_REQUEST['service']) echo 'checked'?> data-url="/zakazy/<?=$regionURL?><?=$key?>/"/>
								<label for="order__filter-<?=$key?>"><?=$value?></label>
							</div>
						<?}?>
					</div>
				</div>
				<?/*if($_REQUEST['servicesMain']):?>
					<div class="order__tags">
						<div class="order__tags-title">Теги:</div>
						<div class="order__tags-items">
							<!-- <a href="#" class="order__tags-item">Дома</a> -->
							<?foreach ($_REQUEST['servicesMain'] as $serviceID) {
								foreach ($arService[$serviceID] as $key => $value) {?>
									<input id="<?=$key?>" class="hide" type="checkbox" name="service" value="<?=$key?>" <?if ($key == $_REQUEST['service']) echo 'checked'?> />
									<label for="<?=$key?>" class="order__tags-item"><?=$value?></label>
								<?}
							}?>
						</div>
					</div>
				<?endif;*/?>
			</form>
		</div>
	</div>
</div>
<div class="heading-text">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8">
				<h2>О Заказах</h2>
				<p>На нашем сайте в разделе Заказы, любой посетитель может разместить заказ на выполнение строительных работ. Опишите подробно что необходимо выполнить, указав бюджет, объем и сроки работ, приложите фото.</p>
			</div>
		</div>
	</div>
</div>

<?// установим метатеги
if ($_REQUEST['service']) {
	$serviceName = ($arCategory[$_REQUEST['service']]) ? $arCategory[$_REQUEST['service']] : $arCategory[$_REQUEST['service'][0]];
	$APPLICATION->SetTitle('Строительные заказы - '.$serviceName);
	$APPLICATION->SetPageProperty("title", 'Строительные заказы - '.$serviceName);
	$APPLICATION->SetPageProperty("description", 'Строительные заказы, '.$serviceName.'. Более 100 тысяч профессиональных строителей уже с нами!');
}
?>
