<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
// dump($arResult['PROPERTIES']['PHOTOS']);
$dateShow = ($arResult["DISPLAY_ACTIVE_FROM"] == date('d.m.Y')) ? 'Сегодня' : $arResult["DISPLAY_ACTIVE_FROM"];

// dump($_COOKIE); // разбираем куки
if(isset($_COOKIE['comparison_orders'])){
	$arComparison = explode('-',$_COOKIE['comparison_orders']);
}
if(isset($_COOKIE['favorites_orders'])){
	$arFavorites = explode('-',$_COOKIE['favorites_orders']);
}
$comparison = (in_array($arResult['ID'],$arComparison)) ? 'Y' : 'N';
$favorites = (in_array($arResult['ID'],$arFavorites)) ? 'Y' : 'N';
$comp_active = ($comparison == 'Y') ? 'control--favorites--active' : '';
$fav_active = ($favorites == 'Y') ? 'control--favorites--active' : '';
$comp_text = ($comparison != 'Y') ? 'Добавить к сравнению' : 'Удалить из сравнения';
$fav_text = ($favorites != 'Y') ? 'Добавить в избранное' : 'Удалить из избранного';

?>
<div class="order">
	<div class="order__top">
		<div class="orders__item-status"><?=($arResult['PROPERTIES']['STATUS']['VALUE'])?$arResult['PROPERTIES']['STATUS']['VALUE']:'Открыт'?></div>
		<div class="orders__item-time"><?=$dateShow?></div>
		<div class="orders__item-place"><?=$arResult['PROPERTIES']['ADDRESS']['VALUE']?></div>
		<div class="orders__item-favourites control control--favorites <?=$fav_active?>">
			<div class="control__icon js-add-favorites" data-id="<?=$arResult['ID']?>" data-cookie="favorites_orders">
				<svg class="icon icon-favorites" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 46.9" style="enable-background:new 0 0 50 46.9;" xml:space="preserve">
					<path d="M46.4,4.4l-0.3-0.3C43.8,1.3,40.4,0,36.7,0s-6.8,1.6-9.4,4.2L25,6.8l-2.3-2.6C20.3,1.3,16.9,0,13.3,0S6.5,1.6,3.9,4.2L3.6,4.4C1.3,7,0,10.7,0,14.6s1.3,7.3,3.6,10.2l20.1,21.6c0.3,0.3,1.3,0.5,1.3,0.5s1-0.3,1.3-0.5l20.1-21.6c2.3-2.6,3.6-6.3,3.6-10.2C50,10.7,48.7,7,46.4,4.4z" />
				</svg>
			</div>
			<div class="control__container">
				<div class="control__main"><?=$fav_text?></div>
			</div>
		</div>
		<div class="control control--share js-control-share orders__item-share">
			<div class="control__icon">
				<svg id="icon-share" class="icon icon-favorites" viewBox="0 0 50 50">

					<path d="M34.3,22.1l-5.4-5.4c-0.1-0.1-0.3-0.2-0.5-0.2s-0.3,0.1-0.5,0.2c-0.1,0.1-0.2,0.3-0.2,0.5v2.7h-2.4
																	c-5,0-8.1,1.4-9.3,4.3c-0.4,0.9-0.6,2.1-0.6,3.5c0,1.2,0.4,2.8,1.3,4.8c0,0,0.1,0.1,0.1,0.3c0.1,0.1,0.1,0.2,0.1,0.3
																	c0,0.1,0.1,0.2,0.1,0.2c0.1,0.1,0.2,0.2,0.3,0.2c0.1,0,0.2,0,0.2-0.1c0.1-0.1,0.1-0.2,0.1-0.3c0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0-0.2
																	c0-0.5-0.1-0.9-0.1-1.3c0-0.7,0.1-1.4,0.2-1.9c0.1-0.6,0.3-1.1,0.5-1.5c0.2-0.4,0.5-0.8,0.8-1.1c0.3-0.3,0.7-0.5,1.1-0.7
																	c0.4-0.2,0.9-0.3,1.4-0.5c0.5-0.1,1.1-0.2,1.6-0.2c0.5,0,1.2-0.1,1.9-0.1h2.4v2.7c0,0.2,0.1,0.3,0.2,0.5c0.1,0.1,0.3,0.2,0.5,0.2
																	c0.2,0,0.3-0.1,0.5-0.2l5.4-5.4c0.1-0.1,0.2-0.3,0.2-0.5C34.5,22.4,34.4,22.3,34.3,22.1z"></path>
				</svg>
			</div>
			<div class="control__container">
				<div class="control__main">
					<span>Поделиться</span>
					<div class="ya-share2 social-list" data-curtain data-size="l" data-shape="round" data-services="vkontakte,odnoklassniki,telegram,twitter,viber,whatsapp"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="order__head">
		<div class="order__head-ico d-none d-md-block d-lg-block">
			<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/<?=$arResult['ICON']?>">
		</div>
		<div class="order__head-title">
			<div class="order__name"><?=$arResult['NAME']?></div>
			<div class="order__head-ico d-block d-md-none">
				<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/<?=$arResult['ICON']?>">
			</div>
			<div class="order__cat"><?=$arResult['DISPLAY_PROPERTIES']['CATEGORY']['DISPLAY_VALUE']?></div>
		</div>
	</div>
	<div class="order__description">
		<p><?=$arResult["DETAIL_TEXT"]?></p>
		<!-- <div class="order__properties">
			<div class="order__properties-item">
				<span class="order__properties-ico">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="22px" height="22px">
						<image x="0px" y="0px" width="22px" height="22px" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAWCAMAAADzapwJAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABiVBMVEUhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISL///+1AOeOAAAAgXRSTlMADnyzyszJuIcgjNC6gUciP3my0qEDE+I5JcDzLBT9o4I06KICK0RDRUIadQmKeuWwscS8EewqrKgeD/Yww5/TTaqtv32eq4VRPp3VUMEWk5u01q+l3ePIva41Ci4vMTgyCHEbhNGGV5K5F3Ka6pnrpwspTnNTN/gFVd9SGEjZx74p5ZkQAAAAAWJLR0SCi7P/RAAAAAd0SU1FB+QHAw8fJBuRLbgAAAFDSURBVBjTY2AAA0YmZhZWVjZ2Dk4GJMDFzcPLx88pICgkLCIKExQTF5aAsSWlpGUgLFk5eWSdCo2KYFpJGUioqKqpa2hqgfjajTpAUlcJxNbTNzA0ZDYyZASyjZkZGExMzUDC5hYg0pKdFUhaWdswKNiCTbITgZhs7wAkHJ0YnF3AXFeotW7uQMJDmIHbE8z1UoYIe/sACRNfBm4/MNc/AETqBAaBecEMwmIgT4SEhhkKCQWGR0AURTJwRwGpaBuhwJjYuPgEiEmJEQy2SUA6OSXV3j2NIx3qUY1UhoxMIJ2VzcaanZ3tARXOyWTgz5UFsTLykIIlX4OBoSAcbIkQQjSHG0gUNmoDyaIiuGhxIzgu4hpLkAO2tLEUwigrD/aDCVbkyVXC2DqBVV7VNUBP12bUGSHHpoRdfYNBpHG4hQOEDwAJ2TKlKIyHtAAAAABJRU5ErkJggg==" />
					</svg>
				</span>
				<span class="order__properties-name">Бюджет:</span>
				<span class="order__properties-value"><?=formatPrice($arResult['PROPERTIES']['BUDGET']['VALUE'])?> ₽</span>
			</div>
			<div class="order__properties-item">
				<span class="order__properties-ico">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="22px" height="22px">
						<image x="0px" y="0px" width="22px" height="22px" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAWCAMAAADzapwJAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABLFBMVEUhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISL///8zfiPNAAAAY3RSTlMAqM7Jysjd1N/PpuUkGxzRfBoi4U8j08CyV8Qnr8MHt2EGNLO4XGSn28HFhw8zQEEs7D0DbC0mE+fGTq0Vg7a62rSEBItfYGWwGKwXEOg8UhZYL8IC13AKAWPrNseuqaXmubUu5/IhAAAAAWJLR0RjXL4tqgAAAAd0SU1FB+QHAw8gBHghJ0wAAADySURBVBjTbY7XUsJAFEAvJIuAhl5iSUSaBqICSolCQLDQwUoP6v9/BMtuyMCQ83DP7pndmQsWK8NikO0AYTGs3cEwViccHnEuDOf2eNfy+QMcFwxBGCj8MfXJKR5nIIj0eh4hukBRPGJGjidMs5A0zZdXUiq9n+Xrm9sMgJjN7WSdu3sXyWHYB2c2XyAUt7/wUFIEgv9hqz+CUpYJUoWWtKqq1RpI+oOnOlEj+/zy+oaMTZoeIhG18GwbudOludc33XswNMubvTf5/WMnf37R4/cPdXQEMOahpr8QJtTTGR5zqC40gk3ROiAv279/mrb4XwGMDSQx0tGDngAAAABJRU5ErkJggg==" />
					</svg>
				</span>
				<span class="order__properties-name">Объем работ:</span>
				<span class="order__properties-value"><?=$arResult['PROPERTIES']['WORK_SCOPE']['VALUE']?></span>
			</div>
			<div class="order__properties-item">
				<span class="order__properties-ico">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="22px" height="22px">
						<image x="0px" y="0px" width="22px" height="22px" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAWCAMAAADzapwJAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABxVBMVEUhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISL///9tQ2TpAAAAlXRSTlMAXa2vqDZza8PAlJeR2GcStcKlD3HmnLzZftaKApU5Cb4VQevOUVjeEL8LwRwgEQ64X9qDGo48COdHidXSbFDbd66kGAHToUkdqxOZBkbFVdAKncswQM8ZKKZgYjG9Y/bgDSPvJrspxEOB3XsnzRtbyipt+WoX6JpF3B/lsbR5gni2i8jXHiL4x4ijJT+i4WgFB8bJuiW/05IAAAABYktHRJaRaSs5AAAAB3RJTUUH5AcDDyASjPWSHQAAAWNJREFUGNNjYGBkYmZmZmFlYGBjZ0ACHJxc3Dy8fPwCghxCwghhEVEWMXEJSSlpBhlZKTm4sLyCIoOSsooqiK2mLgIT1tDUUtQW0IFwdAWZ9CAsfQNDI2MI08SEQcaU1wzMNrdQtwQzhK2sbWwZGOzsHUAqHJ2cQYIu7K5uujpS7gwMHgKeXgyuU719gKK+fhwgzf4BgQw+QVODGUJCw8IZIiJV1MAGRU2NZjCxCY0Bmh3LHxefkAgSTHITTeZjSFEBOzA1LT3DMZOBwcsqy8I1W54hwg8sLK7LmaOVm5cf4FfgXQgUgAprJhb5KXKoF5fEccgwIAmXyoSVFThIe5ZPFfRBEjZhqKjUqqrWDqqprUMRZmCpb2hsSmpWamlFEW6zb1foYOjkqutCEWbo7ultYNB3tRSsgAuHw8NeTb2vjiEHLKwnmOcCAgwgzNU/waWwGSQ8cVLuZDiIVWieHDCFgQEA0gZRz9add4YAAAAASUVORK5CYII=" />
					</svg>
				</span>
				<span class="order__properties-name">Начать работы:</span>
				<span class="order__properties-value"><?=$arResult['PROPERTIES']['WORK_START']['VALUE']?></span>
			</div>
			<div class="order__properties-item">
				<span class="order__properties-ico">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="22px" height="22px">
						<image x="0px" y="0px" width="22px" height="22px" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAWCAMAAADzapwJAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABv1BMVEUhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISL///+wC3VxAAAAk3RSTlMAju3LoCAbmcfsnov3BBpbRabAnMGoTjXrodPETyHg3qNWdXtRn9rmKjjKBgW8F9jqfXSDbPB41fYYWQ0B7q5SsDdayDt+DhCBPrcJ30JNoksHLOOUsZGlpMM9Ay/NmslguhbW2xV3bZ15RmiPgl6/AtFJtHbOEh5ER4mtm0pvMmFlbhkc+IgPCHzd0HqrX5KztZac39NrAAAAAWJLR0SUf2dKFQAAAAd0SU1FB+QHAw8gH/JE7qAAAAFWSURBVBjTY2BgZGJmYWWAAzZ2Dk4uBgZuHl6+yfwMAoJCwuwiomIMApPFGSQkGaT4GKRlZOXkFRSVlFVU1dQ1NLW0dUQYdPX0DZQMDIHajYyBhImpmbmFJRcDy2Qr9snWCLNtbO3sJzswOIo5Obu4ggTc3EGkh6cXv4AAA4O3jy+DH0iA0R9EBgQGBbMB6ZBQqPawcChDMAJIRKqgC0cxRzPExMLsi7ODWRsfwJCQ6J0EUZOcYmUjC7Q2lcEujSE9I1MJLMySmTU5kjOdgSFbOSeXIS+foQAsHFNoXqTDCjSRl6G4hEGjtAwWSOUws5krGBiE+BnQQGU8kKgqRheurgEFgk+tTh1CLD21Xh1sbIOpXyNCOLvWpwnCYpycCqKaW0Ckd6sCTEWTT1s7ULsyA0NHZ5chQmfx5G67HrPMtN7CyboI0T5nBtn+CcXFEycFMXAYgUQAGE5BbccZ2K0AAAAASUVORK5CYII=" />
					</svg>
				</span>
				<span class="order__properties-name">Сроки:</span>
				<span class="order__properties-value"><?=$arResult['PROPERTIES']['DEADLINE']['VALUE']?></span>
			</div>
		</div> -->
		<div class="order-slider swiper-container">
			<div class="swiper-wrapper">
				<?foreach ($arResult['PROPERTIES']['PHOTOS']['VALUE'] as $photo) {
					$photoRes = CFile::ResizeImageGet($photo, array('width' => 288, 'height' => 216), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);?>
						<div class="swiper-slide"><a data-fancybox="gallery" href="<?=CFile::GetPath($photo)?>"><img class="lazyload" src="" data-src="<?=$photoRes['src']?>" alt="Изображение"></a></div>
				<?}?>
			</div>
			<div class="js-slider-img swiper-button-next" data-icon="5"></div>
			<div class="js-slider-img swiper-button-prev" data-icon="4"></div>
		</div>
	</div>
	<div class="order__contacts <?=$arResult['USER']['CLASS']?>">
		<div class="order__contacts-title">
			Контакты:
		</div>
		<div class="order__contacts-block">
			<p><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/lock.svg" alt=""><?=$arResult['USER']['NAME']?></p>
			<p><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/lock.svg" alt="">
				<a href="tel:<?=str_replace(['(',')',' ','-'],'',$arResult['USER']['PERSONAL_PHONE'])?>"><?=$arResult['USER']['PERSONAL_PHONE']?></a>
			</p>
			<p><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/lock.svg" alt=""><?=$arResult['USER']['EMAIL']?></p>
		</div>
	</div>
	<div class="order__contacts-auth <?=$arResult['USER']['CLASS']?>">
		<a href="/lk/" class="a_grey">Авторизуйтесь</a>, чтобы увидеть контакты заказчика <a href="/lk/" class="btn btn--red">Войти</a>
	</div>
</div>
