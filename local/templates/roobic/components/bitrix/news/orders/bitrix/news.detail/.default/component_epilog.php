<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $templateData
 * @var array $arParams
 * @var string $templateFolder
 * @global CMain $APPLICATION
 */

global $APPLICATION;

// dump($arResult);
$title = 'Заказ на работы '.$arResult['ID'].' - '.$arResult['NAME'];
$description = $title.'. Строительные заказы на сайте СтройМан. Более 100 тысяч профессиональных строителей уже с нами!';
$APPLICATION->SetPageProperty("title", $title);
$APPLICATION->SetPageProperty('description', $description);
?>
