<?
global $USER;
if ($USER->IsAuthorized())
{
  if($arResult['PROPERTIES']['USER']['VALUE'])
  {
    $by = 'id'; $sort = 'asc'; // поле сортировки
    $filter = ['ID' => $arResult['PROPERTIES']['USER']['VALUE']];
    $arParams = [ // наши поля
      'FIELDS' => ['ID','NAME','EMAIL','PERSONAL_PHONE'],
    ];
    $rsUsers = CUser::GetList($by, $sort, $filter, $arParams);
    $arResult['USER'] = $rsUsers->Fetch();
  }
  else
  {
    $arResult['USER']['NAME'] = $arResult['PROPERTIES']['NAME']['VALUE'];
    $arResult['USER']['EMAIL'] = $arResult['PROPERTIES']['EMAIL']['VALUE'];
    $arResult['USER']['PERSONAL_PHONE'] = $arResult['PROPERTIES']['PHONE']['VALUE'];
  }
}
else
{
  $arResult['USER']['NAME'] = 'Имя заказчика скрыто';
  $arResult['USER']['EMAIL'] = 'ХХХ@ХХХХ.XXX';
  $arResult['USER']['PERSONAL_PHONE'] = '+7 9ХХ-ХХХ-ХХ-ХХ';
  $arResult['USER']['CLASS'] = 'not-auth';
}

// получим иконку главного раздела
// $arService = getElHL(3,[],['UF_XML_ID'=>$arResult['PROPERTIES']['SERVICE']['VALUE']],['ID','UF_MAIN_SECTION']);
// $arService = array_values($arService);
// $arResult['ICON'] = getIconMainSection($arService[0]['UF_MAIN_SECTION']);
$arResult['ICON'] = getIconMainSection(777);
?>
