<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if ($arResult["SHOW_SMS_FIELD"] == true) CJSCore::Init('phone_auth');
// echo "<pre>";print_r($_REQUEST);echo "</pre>";
$regGroup = ($_REQUEST['group']) ? $_REQUEST['group'] : 6;
?>
<div class="modal-dialog modal-dialog--login" role="document">
	<div class="modal-content">
		<div class="modal-close js-video-close" data-dismiss="modal" aria-label="Close">
			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20px" height="20px">
				<path fill-rule="evenodd" fill="rgb(149, 149, 149)" d="M1.515,0.100 L19.899,18.485 L18.485,19.899 L0.100,1.515 L1.515,0.100 Z" />
				<path fill-rule="evenodd" fill="rgb(149, 149, 149)" d="M18.485,0.100 L19.899,1.515 L12.828,8.586 L11.414,7.171 L18.485,0.100 Z" />
				<path fill-rule="evenodd" fill="rgb(149, 149, 149)" d="M7.171,11.414 L8.586,12.828 L1.515,19.899 L0.100,18.485 L7.171,11.414 Z" />
			</svg>
		</div>
		<div class="modal-body">
			<div class="modal-header">
				<h4 class="modal-header__title">
					Регистрация
				</h4>
			</div>
			<div class="modal-container">

				<?if(!empty($arParams["~AUTH_RESULT"])):
					$text = str_replace(array("<br>", "<br />"), "\n", $arParams["~AUTH_RESULT"]["MESSAGE"]);?>
					<div class="alert <?=($arParams["~AUTH_RESULT"]["TYPE"] == "OK"? "alert-success":"alert-danger")?>"><?=nl2br(htmlspecialcharsbx($text))?></div>
				<?endif?>

				<form method="post" action="<?=$arResult["AUTH_URL"]?>" name="bform" enctype="multipart/form-data" class="regForm">

					<input type="hidden" name="AUTH_FORM" value="Y" />
					<input type="hidden" name="TYPE" value="REGISTRATION" />

					<div class="lk-form-block">
						<label>
							<input type="text" name="USER_NAME" value="<?=$arResult["USER_NAME"]?>" class="form-input" placeholder="ФИО" required>
							<span class="placeholder">ФИО</span>
						</label>
					</div>

					<?if($arResult["PHONE_REGISTRATION"]):?>
						<div class="lk-form-block">
							<label>
								<input type="tel" name="USER_PHONE_NUMBER" value="<?=$arResult["USER_PHONE_NUMBER"]?>" class="form-input regPhone" placeholder="Номер телефона" required>
								<span class="placeholder">Номер телефона</span>
							</label>
						</div>
					<?endif?>

					<?if($arResult["EMAIL_REGISTRATION"]):?>
						<div class="lk-form-block">
							<label>
								<input type="email" name="USER_EMAIL" value="<?=$arResult["USER_EMAIL"]?>" class="form-input" placeholder="Адрес электронной почты" required>
								<span class="placeholder">Адрес электронной почты</span>
							</label>
						</div>
					<?endif?>

					<div class="lk-form-block">
						<p class="form-desc">Нажимая на кнопку, вы даете согласие на обработку персональных данных и соглашаетесь с <a href="/politika-konfidentsialnosti/">Политикой Конфиденциальности</a></p>
					</div>

					<?$new_pass = substr(str_shuffle('0123456789'), 0, 6);
					$new_pass = ($arResult["USER_PASSWORD"]) ? $arResult["USER_PASSWORD"] : $new_pass;
					$new_pass_conf = ($arResult["USER_CONFIRM_PASSWORD"]) ? $arResult["USER_CONFIRM_PASSWORD"] : $new_pass;?>
					<input type="hidden" name="USER_PASSWORD" value="<?=$new_pass?>" autocomplete="off">
					<input type="hidden" name="USER_CONFIRM_PASSWORD" value="<?=$new_pass_conf?>" autocomplete="off">
					<input type="hidden" name="UF_PASSWORD" value="<?=$new_pass?>" autocomplete="off">
					<input type="hidden" name="USER_LOGIN" value="<?=$arResult["USER_LOGIN"]?>" class="regLogin">
					<input type="hidden" name="PERSONAL_ICQ" value="<?=$regGroup?>" class="regGroup">

					<input type="submit" class="btn btn--red" name="Register" value="<?=GetMessage("AUTH_REGISTER")?>">

					<div class="form-block-bottom">
						<a href="" class="" data-toggle="modal" data-target="#modalLogin" data-dismiss="modal">Войти</a>
						<p class="form-desc">При входе, вы принимаете условия <a href="/polzovatelskoe-soglashenie/">Пользовательского соглашения</a> и <a href="/politika-konfidentsialnosti/">Политики конфиденциальности</a></p>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>
