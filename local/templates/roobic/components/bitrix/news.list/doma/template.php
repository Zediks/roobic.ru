<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use Bitrix\Main\Grid\Declension;
$bedroomDeclension = new Declension('спальня', 'спальни', 'спален');
$bedroomDeclensionURL = new Declension('spalnya', 'spalni', 'spalen');
$bathroomDeclension = new Declension('санузел', 'санузла', 'санузлов');
$bathroomDeclensionURL = new Declension('sanuzel', 'sanuzla', 'sanuzla');
global $typeURL, $typeURLOne;

$arPurpose = [
	'fam_01' => 'Семья с одним ребенком',
	'fam_02' => 'Семья с двумя детьми',
	'fam_03' => 'Для большой семьи',
];
?>
<div class="object-list object-list--catalog">
	<?foreach($arResult["ITEMS"] as $arItem):
		// dump($arItem);

		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

		$comp_active = ($arItem['COMPARISON'] == 'Y') ? 'control--favorites--active' : '';
		$fav_active = ($arItem['FAVORITES'] == 'Y') ? 'control--favorites--active' : '';
		$comp_text = ($arItem['COMPARISON'] == 'Y') ? 'Удалить из сравнения' : 'Добавить к сравнению';
		$fav_text = ($arItem['FAVORITES'] == 'Y') ? 'Удалить из избранного' : 'Добавить в избранное';

		// о компании
		$idCompany = $arItem['PROPERTIES']['COMPANY']['VALUE'];
		$arCompany = $arResult["COMPANIES"][$idCompany];

		// выводим правильные окончания
		$cntBedroom = $arItem['PROPERTIES']['CNT_BEDROOM']['VALUE'];
		if (!$cntBedroom) $cntBedroom = 0;
		$bedroomText = $bedroomDeclension->get($cntBedroom);
		$bedroomText = ($cntBedroom > 0) ? $cntBedroom.' '.$bedroomText : 'Нет спален';
		$bedroomURL = '/'.$typeURLOne.'-'.$cntBedroom.'-'.$bedroomDeclensionURL->get($cntBedroom).'/';
		// санузлы
		$cntBathroom = $arItem['PROPERTIES']['CNT_BATHROOM']['VALUE'];
		if (!$cntBathroom) $cntBathroom = 0;
		$bathroomText = $bathroomDeclension->get($cntBathroom);
		$bathroomText = ($cntBathroom > 0) ? $cntBathroom.' '.$bathroomText : 'Нет санузлов';
		$bathroomURL = '/'.$typeURL.'-'.$cntBathroom.$bathroomDeclensionURL->get($cntBathroom).'/';

		if (getBath($arItem['PROPERTIES']['TYPE']['VALUE']) == 'бани')
			$floorText = ($arItem['PROPERTIES']['CNT_FLOORS']['VALUE'] == 1)?'одноэтажной':'двухэтажной';
		else
			$floorText = ($arItem['PROPERTIES']['CNT_FLOORS']['VALUE'] == 1)?'одноэтажного':'двухэтажного';

		// ссылка площади
		if ($arItem['PROPERTIES']['AREA']['VALUE'] < 200)
			$areaURL = round($arItem['PROPERTIES']['AREA']['VALUE'],'-1');
		elseif ($arItem['PROPERTIES']['AREA']['VALUE'] < 500)
			$areaURL = round($arItem['PROPERTIES']['AREA']['VALUE'],'-1.5');
		$areaURL = '/'.$typeURL.'-'.$areaURL.'m/';

		$priceVal = $arItem['PROPERTIES']['PRICE']['VALUE'];

		// ссылка на стоимость дома
		if ($priceVal <= 1000000)
			$priceUrl = '/doma-do1mln/';

		$j=1;
		for ($i=2000000; $i <= 10000000; $i+=1000000) {
			$j++;
			if (!$priceUrl && $priceVal <= $i)
				$priceUrl = '/doma-'.$j.'mln/';
		}

		if(!$priceUrl) $priceUrl = '/doma-15mln/';

		$photoAll = array_merge($arItem['PROPERTIES']['PHOTO']['VALUE'], $arItem['PROPERTIES']['FACADES']['VALUE']);

		if ($arItem['PROPERTIES']['BATH']['VALUE']) $arItem["DETAIL_PAGE_URL"] = '/bani/'.$arItem["CODE"].'/';
		$typeName = ($arItem['PROPERTIES']['BATH']['VALUE']) ? 'Баня' : 'Дом';

		$size = ($arItem['PROPERTIES']['SIZE_LIST']['VALUE'])?$arItem['PROPERTIES']['SIZE_LIST']['VALUE']:$arItem['PROPERTIES']['SIZE']['VALUE'];

		if ($cntBedroom > 3)
			$purpose = 'fam_03';
		elseif($cntBedroom > 2)
			$purpose = 'fam_02';
		else
			$purpose = 'fam_01';
		?>
		<div class="object" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<div class="row">
				<div class="col-md-5">
					<div class="object__images">
						<div class="object-slider swiper-container">
							<div class="right-control">
								<div class="control control--favorites <?=$comp_active?>">
									<div class="control__icon js-add-compare" data-id="<?=$arItem['ID']?>" data-cookie="comparison_house">
										<svg id="icon-compare" class="icon-compare" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14 14" style="enable-background:new 0 0 14 14;"
											xml:space="preserve">
											<path d="M2,14H0V1.2C0,0.5,0.4,0,1,0l0,0c0.6,0,1,0.5,1,1.2V14z" />
											<path d="M6,14H4V7.2C4,6.5,4.4,6,5,6l0,0c0.6,0,1,0.5,1,1.2V14z" />
											<path d="M10,14H8V1.2C8,0.5,8.4,0,9,0l0,0c0.6,0,1,0.5,1,1.2V14z" />
											<path d="M14,14h-2V7.2C12,6.5,12.4,6,13,6l0,0c0.6,0,1,0.5,1,1.2V14z" />
										</svg>
									</div>
									<div class="control__container">
										<div class="control__main"><?=$comp_text?></div>
									</div>
								</div>
								<div class="control control--favorites <?=$fav_active?>">
									<div class="control__icon js-add-favorites" data-id="<?=$arItem['ID']?>" data-cookie="favorites_house">
										<svg id="icon-favorites" viewBox="0 0 50 50" width="100%" height="100%">
											<path d="M33.2,17.8l-0.1-0.1c-0.9-1.1-2.2-1.6-3.6-1.6s-2.6,0.6-3.6,1.6l-0.9,1l-0.9-1c-0.9-1.1-2.2-1.6-3.6-1.6s-2.6,0.6-3.6,1.6l-0.1,0.1c-0.9,1-1.4,2.4-1.4,3.9c0,1.5,0.5,2.8,1.4,3.9l0,0l7.7,8.3c0.1,0.1,0.3,0.2,0.5,0.2s0.4-0.1,0.5-0.2l7.7-8.3l0,0c0.9-1,1.4-2.4,1.4-3.9C34.6,20.2,34.1,18.8,33.2,17.8z"></path>
										</svg>
									</div>
									<div class="control__container">
										<div class="control__main"><?=$fav_text?></div>
									</div>
								</div>
							</div>
							<div class="swiper-wrapper">
								<?foreach ($photoAll as $photo) {
									$photoRes = CFile::ResizeImageGet($photo, array('width' => 880, 'height' => 548), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);?>
									<div class="swiper-slide"><img class="lazyload" src="" data-src="<?=$photoRes['src']?>" alt="Изображение"></div>
								<?}?>
							</div>
							<div class="swiper-pagination"></div>
							<div class="js-slider-img swiper-button-next" data-icon="5"></div>
							<div class="js-slider-img swiper-button-prev" data-icon="4"></div>
						</div>
					</div>
				</div>
				<div class="col-md-7">
					<div class="object-body">
						<a class="object__name" href="<?=$arItem["DETAIL_PAGE_URL"]?>" style="font-size: 19px;">Проект <?=$floorText?> <?=getBath($arItem['PROPERTIES']['TYPE']['VALUE'])?> <?=$arItem["NAME"]?></a>
						<div class="object__desc">
							<a href="/<?=$arItem['PROPERTIES']['TYPE']['VALUE']?>/"><?=$arItem['DISPLAY_PROPERTIES']['TYPE']['DISPLAY_VALUE']?></a>
						</div>
						<div class="object__developer">
							<div class="proect__description">
								<?if($arCompany['URL']):?>
									<a href="<?=$arCompany['URL']?>" target="_blank" class="proect__cat"><?=$arCompany["NAME"]?></a>
									<span class="proect__raiting"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/sorting_02.svg" alt=""><?=round($arCompany["RATING"],1)?></span>
									<a href="<?=$arCompany['URL']?>#reviews-block" class="proect__rew"><?=$arCompany["REVIEWS"]?></a>
								<?else:?>
									<span class="proect__cat"><?=$arItem['PROPERTIES']['COMPANY_TEXT']['VALUE']?></span>
								<?endif;?>
							</div>
						</div>
						<div class="object__comfort">
							<div class="row">
								<div class="col-xl-8 col-md-12">
									<div class="row object__comfort-list">
										<div class="col-md-6 col-sm-6 proect__comfort-property"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/area_02.svg" alt=""><span>Площадь: <span><a href="<?=$areaURL?>"><?=round($arItem['PROPERTIES']['AREA']['VALUE'])?> м<sup>2</sup></a></span></span></div>
										<div class="col-md-6 col-sm-6 proect__comfort-property"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/area.svg" alt=""><span>Размер: <span><?=$size?></span></span></div>
										<div class="col-md-6 col-sm-6 proect__comfort-property"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/bed.svg" alt=""><span><a href="<?=$bedroomURL?>"><?=$bedroomText?></a></span></div>
										<div class="col-md-6 col-sm-6  proect__comfort-property"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/bath.svg" alt=""><span><a href="<?=$bathroomURL?>"><?=$bathroomText?></a></span></div>
									</div>
								</div>
							</div>
						</div>
						<div class="object__bottom">
							<div class="object__bottom-left">
								<div class="foryou">
									<div class="for"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/<?=$purpose?>.svg" alt=""></div>
									<div class="control__container">
										<div class="control__main">
											<strong><?=$typeName?> подходит для:</strong>
											<?=$arPurpose[$purpose]?>
										</div>
									</div>
								</div>
							</div>
							<div class="object__bottom-right">
								<div class="proect__specifications-item proect__specifications-price">
									<div class="proect__specifications-value">
										<?if($arItem['PROPERTIES']['OLD_PRICE']['VALUE']){?>
											<span class="old-price"><s><?=formatPrice($arItem['PROPERTIES']['OLD_PRICE']['VALUE'])?> ₽</s></span>
										<?}?>
										<span class="new-price"><a href="<?=$priceUrl?>"><?=formatPrice($priceVal)?> ₽</a></span>
									</div>
								</div>
								<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="btn btn--red">Подробнее</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="object__bottom mobile">
				<div class="object__bottom-left">
					<div class="foryou">
						<div class="for"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/<?=$purpose?>.svg" alt=""></div>
						<div class="control__container">
							<div class="control__main">
								<strong><?=$typeName?> подходит для:</strong>
								<?=$arPurpose[$purpose]?>
							</div>
						</div>
					</div>
				</div>
				<div class="object__bottom-right">
					<div class="proect__specifications-item proect__specifications-price">
						<div class="proect__specifications-value">
							<?foreach ($arItem['PROPERTIES']['PURPOSE']['VALUE'] as $key => $value) {?>
								<div class="for">
									<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/<?=$arItem['PROPERTIES']['PURPOSE']['VALUE_XML_ID'][$key]?>.svg" alt="">
									<div class="control__container">
										<div class="control__main">
											<strong><?=$typeName?> подходит для:</strong>
											<?=$value?>
										</div>
									</div>
								</div>
							<?}?>
							<span class="new-price"><a href="<?=$priceUrl?>"><?=formatPrice($priceVal)?> ₽</a></span>
							<?if($arItem['PROPERTIES']['OLD_PRICE']['VALUE']){?>
								<span class="old-price"><s><?=formatPrice($arItem['PROPERTIES']['OLD_PRICE']['VALUE'])?> ₽</s></span>
							<?}?>
						</div>
					</div>
					<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="btn btn--red">Подробнее</a>
				</div>
			</div>
		</div>
	<?
		unset($priceUrl);
		endforeach;
	?>
</div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>
