<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

// dump($arResult);

// dump($_COOKIE); // разбираем куки
if(isset($_COOKIE['comparison_house'])){
	$arComparison = explode('-',$_COOKIE['comparison_house']);
}
if(isset($_COOKIE['favorites_house'])){
	$arFavorites = explode('-',$_COOKIE['favorites_house']);
}

use Bitrix\Main\Grid\Declension;
// выводим правильное окончание
$reviewsDeclension = new Declension('отзыв', 'отзыва', 'отзывов');

foreach($arResult["ITEMS"] as $key => $arItem)
{
  $arResult["ITEMS"][$key]['COMPARISON'] = (in_array($arItem['ID'],$arComparison)) ? 'Y' : 'N';
  $arResult["ITEMS"][$key]['FAVORITES'] = (in_array($arItem['ID'],$arFavorites)) ? 'Y' : 'N';

	$idCompany = $arItem['PROPERTIES']['COMPANY']['VALUE'];
  $arIdsCompany[] = $idCompany;
	$arCompany = $arItem['DISPLAY_PROPERTIES']['COMPANY']['LINK_ELEMENT_VALUE'][$idCompany];

	$arResult['COMPANIES'][$idCompany] = [
		'NAME' => $arCompany['NAME'],
		'URL' => $arCompany['DETAIL_PAGE_URL'],
	];
}

// получим отзывы
$arElHL = getElHL(9,[],['UF_COMPANY' => $arIdsCompany],['ID','UF_COMPANY','UF_EXPERTISE','UF_DEADLINE','UF_SERVICE','UF_PRICE','UF_QUALITY']);
foreach ($arElHL as $key => $value)
{
	$sumScore = (int)$value['UF_EXPERTISE'] + (int)$value['UF_DEADLINE'] + (int)$value['UF_SERVICE'] + (int)$value['UF_PRICE'] + (int)$value['UF_QUALITY'];
  $rating = $sumScore / 5;
  $value['RATING'] = round($rating,2);

	$arReviews[$value['UF_COMPANY']][] = $value;
}

// переберем компании
foreach ($arIdsCompany as $companyID)
{
	$cntReviews = count($arReviews[$companyID]);
	if ($cntReviews) // рейтинг
	{
		$ratingSum = 0;
		foreach ($arReviews[$companyID] as $value)
			$ratingSum += $value['RATING'];

		$rating = $ratingSum / $cntReviews;
	}
	else $rating = 0;

  $arResult['COMPANIES'][$companyID]['RATING'] = $rating;

  // отзывы
  $reviewsText = $reviewsDeclension->get($cntReviews);
  $reviewsText = ($cntReviews > 0) ? $cntReviews.' '.$reviewsText : 'Нет отзывов';

  $arResult['COMPANIES'][$companyID]['REVIEWS'] = $reviewsText;
}
// dump($arResult['COMPANIES']);
