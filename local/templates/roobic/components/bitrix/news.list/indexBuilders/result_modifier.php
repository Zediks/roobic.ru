<?
// dump($_COOKIE); // разбираем куки
if(isset($_COOKIE['comparison_bild'])){
	$arComparison = explode('-',$_COOKIE['comparison_bild']);
}
if(isset($_COOKIE['favorites_bild'])){
	$arFavorites = explode('-',$_COOKIE['favorites_bild']);
}

foreach ($arResult["ITEMS"] as $key => $arItem) {
  $arResult["ITEMS"][$key]['COMPARISON'] = (in_array($arItem['ID'],$arComparison)) ? 'Y' : 'N';
  $arResult["ITEMS"][$key]['FAVORITES'] = (in_array($arItem['ID'],$arFavorites)) ? 'Y' : 'N';
	$arIds[] = $arItem['ID'];
} // dump($arIds);

// получим отзывы
$arElHL = getElHL(9,[],['UF_COMPANY' => $arIds],['ID','UF_COMPANY','UF_EXPERTISE','UF_DEADLINE','UF_SERVICE','UF_PRICE','UF_QUALITY']);
foreach ($arElHL as $key => $value)
{
	$sumScore = (int)$value['UF_EXPERTISE'] + (int)$value['UF_DEADLINE'] + (int)$value['UF_SERVICE'] + (int)$value['UF_PRICE'] + (int)$value['UF_QUALITY'];
  $rating = $sumScore / 5;
  $value['RATING'] = round($rating,2);

	$arReviews[$value['UF_COMPANY']][] = $value;
}

// получим выполненые работы
$completedWork = getElHL(8,[],['UF_COMPANY' => $arIds],['ID','UF_COMPANY']);
foreach ($completedWork as $id => $value)
	$arCompletedWork[$value['UF_COMPANY']][] = $value['ID'];

$arResult['REVIEWS'] = $arReviews;
$arResult['COMPLETED_WORK'] = $arCompletedWork;
?>
