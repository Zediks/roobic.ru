<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use Bitrix\Main\Grid\Declension;
// выводим правильное окончание
$reviewsDeclension = new Declension('отзыв', 'отзыва', 'отзывов');
?>
<h2 class="title title--black title--small">Строительные компании</h2>
<div class="company-list swiper-container">
	<div class="swiper-wrapper">
		<?foreach($arResult["ITEMS"] as $arItem): // dump($arItem['PROPERTIES']['REVIEWS']);?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

			$cntReviews = count($arResult['REVIEWS'][$arItem['ID']]);
			if ($cntReviews) // рейтинг
			{
				$ratingSum = 0;
				foreach ($arResult['REVIEWS'][$arItem['ID']] as $value)
					$ratingSum += $value['RATING'];

				$rating = $ratingSum / $cntReviews;
			}
			else $rating = 0;

			$services = implode(', ',$arItem['DISPLAY_PROPERTIES']['SERVICES']['DISPLAY_VALUE']); // услуги
			// отзывы
			$reviewsText = $reviewsDeclension->get($cntReviews);
			$reviewsText = ($cntReviews > 0) ? $cntReviews.' '.$reviewsText : 'Нет отзывов';

			if ($arItem['PREVIEW_PICTURE']) array_unshift($arItem['PROPERTIES']['PHOTO']['VALUE'],$arItem['PREVIEW_PICTURE']['ID']);

			if (!$arItem['PROPERTIES']['PHOTO']['VALUE']) $arItem['PROPERTIES']['PHOTO']['VALUE'][] = $arItem['PREVIEW_PICTURE']['ID'];
			?>
			<div class="swiper-slide">
				<div class="object company-object" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
					<a class="object__images" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
						<div class="object-slider swiper-container">
							<div class="swiper-wrapper">
								<?foreach ($arItem['PROPERTIES']['PHOTO']['VALUE'] as $photo) {
									$photoRes = CFile::ResizeImageGet($photo, array('width' => 692, 'height' => 484), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);?>
									<div class="swiper-slide"><img class="lazyload" src="" data-src="<?=$photoRes['src']?>" alt="Изображение"></div>
								<?}?>
							</div>
							<div class="swiper-pagination"></div>
							<div class="js-slider-img swiper-button-next" data-icon="5"></div>
							<div class="js-slider-img swiper-button-prev" data-icon="4"></div>
						</div>
					</a>
					<div class="object-body">
						<a class="object__name" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
						<div class="reviews">
							<div class="reviews-stars">
								<?for ($x=1; $x<6; $x++) {?>
									<div class="reviews__star <?if ($rating > $x) echo 'reviews__star--active'?>">
										<svg class="icon">
											<use xlink:href="#icon_star"></use>
										</svg>
									</div>
								<?}?>
								<div class="reviews__count"><?=$rating?></div>
							</div>
							<a class="reviews__link" href="<?=$arItem["DETAIL_PAGE_URL"]?>#reviews-block"><?=$reviewsText?></a>
						</div>
						<div class="object-count-list">
							<div class="object-count"> <span>Количество работ на сайте:</span><strong><?=count($arResult['COMPLETED_WORK'][$arItem['ID']])?></strong></div>
							<div class="object-count"> <span>Уровень цен на услуги:</span><strong><?=$arItem['PROPERTIES']['PRICE']['VALUE']?></strong></div>
						</div>
						<p><?=$services?></p>
						<a class="btn" href="<?=$arItem["DETAIL_PAGE_URL"]?>">Подробнее</a>
					</div>
				</div>
			</div>
		<?endforeach;?>
	</div>
	<div class="company-list-bottom">
		<a class="show-all" href="/stroiteli/">Все компании</a>
		<div class="swiper-btn">
			<div class="swiper-button-prev js-btns">
				<svg class="icon">
					<use xlink:href="#icon-arrow_work"></use>
				</svg>
			</div>
			<div class="swiper-button-next js-btns">
				<svg class="icon">
					<use xlink:href="#icon-arrow_work"> </use>
				</svg>
			</div>
		</div>
	</div>
</div>
