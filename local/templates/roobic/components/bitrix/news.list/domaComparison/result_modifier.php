<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

// dump($arResult);

// dump($_COOKIE); // разбираем куки
if(isset($_COOKIE['comparison_house'])){
	$arComparison = explode('-',$_COOKIE['comparison_house']);
}
if(isset($_COOKIE['favorites_house'])){
	$arFavorites = explode('-',$_COOKIE['favorites_house']);
}

foreach($arResult["ITEMS"] as $key => $arItem)
{
  $arResult["ITEMS"][$key]['COMPARISON'] = (in_array($arItem['ID'],$arComparison)) ? 'Y' : 'N';
  $arResult["ITEMS"][$key]['FAVORITES'] = (in_array($arItem['ID'],$arFavorites)) ? 'Y' : 'N';
}

$arElHL = getElHL(7,[],[],['ID','UF_NAME','UF_XML_ID','UF_TYPE']);
foreach ($arElHL as $key => $value)
	$arResult['MATERIALS'][$value['UF_XML_ID']] = $value['UF_NAME'];
