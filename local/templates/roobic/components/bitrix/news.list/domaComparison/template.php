<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use Bitrix\Main\Grid\Declension;
$bedroomDeclension = new Declension('спальня', 'спальни', 'спален');
$bathroomDeclension = new Declension('санузел', 'санузла', 'санузлов');

$arPropComp = ['PRICE','MATERIAL_WALL','MATERIAL_ROOF','AREA','AREA_LIVING','CNT_FLOORS','SIZE','CNT_BEDROOM','CNT_BATHROOM','PURPOSE','GARAGE','HEIGHT','AREA_ROOF','FINISHING','STYLE','FORM','TYPE_ROOF','ANNEX','FINISHING_FACADE','INSULATION','GUARANTEES','FOUNDATION_LIST','CLASS','AREA_BALCONY'];
?>
<div class="compare-list">
	<div class="compare-tr">
		<?foreach($arResult["ITEMS"] as $arItem):
			$comp_active = ($arItem['COMPARISON'] == 'Y') ? 'control--favorites--active' : '';
			$fav_active = ($arItem['FAVORITES'] == 'Y') ? 'control--favorites--active' : '';
			$comp_text = ($arItem['COMPARISON'] == 'Y') ? 'Удалить из сравнения' : 'Добавить к сравнению';
			$fav_text = ($arItem['FAVORITES'] == 'Y') ? 'Удалить из избранного' : 'Добавить в избранное';

			foreach ($arItem['PROPERTIES'] as $key => $value)
			{
				if (in_array($key,$arPropComp))
				{
					if ($key == 'PRICE') $value['VALUE'] = formatPrice($arItem['PROPERTIES']['PRICE']['VALUE']).' ₽';

					if ($key == 'MATERIAL_WALL' || $key == 'MATERIAL_ROOF'){
						foreach ($value['VALUE'] as $val)
							$arValue[] = $arResult['MATERIALS'][$val];

						$value['VALUE'] = implode(', ',$arValue); unset($arValue);
					}

					if ($key == 'AREA' || $key == 'AREA_LIVING') $value['VALUE'] .= ' м²';

					if ($key == 'PURPOSE') $value['VALUE'] = implode(', ',$value['VALUE']);

					$itemsProperty[$key]['ID'] = $arItem['ID'];
					$itemsProperty[$key]['NAME'] = $value['NAME'];
					$itemsProperty[$key]['VALUES'][$arItem['ID']] = $value['VALUE'];
				}
			}
		?>
			<div class="compare-item" data-id="<?=$arItem['ID']?>">
				<div class="compare-delete js-add-favorites" data-id="<?=$arItem['ID']?>" data-cookie="comparison_house">
					<svg class="icon icon-close">
						<use xlink:href="#icon-close"></use>
					</svg>
				</div>
				<div class="compare-item-img">
					<div class="object__images">
						<div class="object-slider swiper-container">
							<div class="swiper-wrapper">
								<?foreach ($arItem['PROPERTIES']['PHOTO']['VALUE'] as $photo) {
									$photoRes = CFile::ResizeImageGet($photo, array('width' => 680, 'height' => 448), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);?>
									<div class="swiper-slide"><img class="lazyload" src="" data-src="<?=$photoRes['src']?>" alt="Изображение"></div>
								<?}?>
							</div>
							<div class="swiper-pagination"></div>
							<div class="js-slider-img swiper-button-next" data-icon="5"></div>
							<div class="js-slider-img swiper-button-prev" data-icon="4"></div>
						</div>
					</div>
				</div>
				<div class="compare-item__title">
					<a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a>
				</div>
			</div>
		<?endforeach;?>
	</div>
	<?$j = 0; // dump($itemsProperty);?>
	<?foreach ($itemsProperty as $itemProperty) { $i = 0; $j++;?>
		<div class="compare-cheracteristics">
			<?foreach($itemProperty["VALUES"] as $value): $i++;?>
				<div class="compare-cheracteristics-item">
					<?if($i == 1):?>
						<div class="compare-cheracteristics-item__title">
							<?=$itemProperty['NAME']?>
						</div>
					<?endif;?>
					<div class="compare-cheracteristics-item__value tr_<?=$j?>">
						<?=$value?>
					</div>
				</div>
			<?endforeach;?>
		</div>
	<?}?>
</div>
