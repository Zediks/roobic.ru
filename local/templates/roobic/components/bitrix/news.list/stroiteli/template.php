<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use Bitrix\Main\Grid\Declension;
// выводим правильное окончание
$reviewsDeclension = new Declension('отзыв', 'отзыва', 'отзывов');
$i = 0;
// dump($arResult['REVIEWS']);
?>
<div class="object-list object-list--catalog">
	<?foreach($arResult["ITEMS"] as $arItem): $i++; // dump($arItem);
		$comp_active = ($arItem['COMPARISON'] == 'Y') ? 'control--favorites--active' : '';
		$fav_active = ($arItem['FAVORITES'] == 'Y') ? 'control--favorites--active' : '';
		$comp_text = ($arItem['COMPARISON'] == 'Y') ? 'Удалить из сравнения' : 'Добавить к сравнению';
		$fav_text = ($arItem['FAVORITES'] == 'Y') ? 'Удалить из избранного' : 'Добавить в избранное';?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

		$cntReviews = count($arResult['REVIEWS'][$arItem['ID']]);
		if ($cntReviews) // рейтинг
		{
			$ratingSum = 0;
			foreach ($arResult['REVIEWS'][$arItem['ID']] as $value)
				$ratingSum += $value['RATING'];

			$rating = $ratingSum / $cntReviews;
		}
		else $rating = 0;

		$services = implode(', ',$arItem['DISPLAY_PROPERTIES']['SERVICES']['DISPLAY_VALUE']); // услуги

		// отзывы
		$reviewsText = $reviewsDeclension->get($cntReviews);
		$reviewsText = ($cntReviews > 0) ? $cntReviews.' '.$reviewsText : 'Нет отзывов';

		if ($arItem['PREVIEW_PICTURE']) array_unshift($arItem['PROPERTIES']['PHOTO']['VALUE'],$arItem['PREVIEW_PICTURE']['ID']);

		if (!$arItem['PROPERTIES']['PHOTO']['VALUE']) $arItem['PROPERTIES']['PHOTO']['VALUE'][] = $arItem['PREVIEW_PICTURE']['ID'];
		?>
		<div class="object" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<div class="row">
				<div class="col-md-5">
					<div class="object__images">
						<div class="object-slider swiper-container">
							<div class="right-control">
								<div class="control control--favorites <?=$fav_active?>">
									<div class="control__icon js-add-favorites" data-id="<?=$arItem['ID']?>" data-cookie="favorites_bild">
										<svg id="icon-favorites" viewBox="0 0 50 50" width="100%" height="100%">
											<path d="M33.2,17.8l-0.1-0.1c-0.9-1.1-2.2-1.6-3.6-1.6s-2.6,0.6-3.6,1.6l-0.9,1l-0.9-1c-0.9-1.1-2.2-1.6-3.6-1.6s-2.6,0.6-3.6,1.6l-0.1,0.1c-0.9,1-1.4,2.4-1.4,3.9c0,1.5,0.5,2.8,1.4,3.9l0,0l7.7,8.3c0.1,0.1,0.3,0.2,0.5,0.2s0.4-0.1,0.5-0.2l7.7-8.3l0,0c0.9-1,1.4-2.4,1.4-3.9C34.6,20.2,34.1,18.8,33.2,17.8z"></path>
										</svg>
									</div>
									<div class="control__container">
										<div class="control__main"><?=$fav_text?></div>
									</div>
								</div>
							</div>
							<div class="swiper-wrapper">
								<?foreach ($arItem['PROPERTIES']['PHOTO']['VALUE'] as $photo) {
									$photoRes = CFile::ResizeImageGet($photo, array('width' => 692, 'height' => 484), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);?>
									<div class="swiper-slide"><img class="lazyload" src="" data-src="<?=$photoRes['src']?>" alt="Изображение"></div>
								<?}?>
							</div>
							<div class="swiper-pagination"></div>
							<div class="js-slider-img swiper-button-next" data-icon="5"></div>
							<div class="js-slider-img swiper-button-prev" data-icon="4"></div>
						</div>
					</div>
				</div>
				<div class="col-md-7">
					<div class="object-body">
						<a class="object__name" href="<?=$arItem["DETAIL_PAGE_URL"]?>" style="font-size: 19px;">
							Строительная компания <?=$arItem["NAME"]?>
						</a>
						<!-- <div class="reviews">
							<div class="reviews-stars">
								<?for ($x=1; $x<6; $x++) {?>
									<div class="reviews__star <?if ($rating > $x) echo 'reviews__star--active'?>">
										<svg class="icon">
											<use xlink:href="#icon_star"></use>
										</svg>
									</div>
								<?}?>
								<div class="reviews__count"><?=$rating?></div>
							</div>
							<a class="reviews__link" href="<?=$arItem["DETAIL_PAGE_URL"]?>#reviews-block"><?=$reviewsText?></a>
						</div> -->
						<div class="object-count-list">
							<div class="object-count"> <span>Проектов в портфолио:</span><strong><?=count($arResult['COMPLETED_WORK'][$arItem['ID']])?></strong></div>
							<div class="object-count"> <span>Уровень цен на услуги:</span><strong><?=$arItem['PROPERTIES']['PRICE']['VALUE']?></strong></div>
						</div>
						<p><?=$services?></p>
						<a class="btn" href="<?=$arItem["DETAIL_PAGE_URL"]?>">Подробнее</a>
					</div>
				</div>
			</div>
		</div>
		<?//if ($i==2) echo '<div class="blurb"><span>Рекламный баннер</span></div>'; unset($services);?>
	<?endforeach;?>
</div>
<?=$arResult["NAV_STRING"]?>
