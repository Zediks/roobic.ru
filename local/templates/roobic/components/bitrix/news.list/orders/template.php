<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

// dump($arParams);
?>
<div class="orders__items">
	<?foreach($arResult["ITEMS"] as $arItem):
		$comp_active = ($arItem['COMPARISON'] == 'Y') ? 'control--favorites--active' : '';
		$fav_active = ($arItem['FAVORITES'] == 'Y') ? 'control--favorites--active' : '';
		$comp_text = ($arItem['COMPARISON'] == 'Y') ? 'Удалить из сравнения' : 'Добавить к сравнению';
		$fav_text = ($arItem['FAVORITES'] == 'Y') ? 'Удалить из избранного' : 'Добавить в избранное';

		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

		$dateShow = ($arItem["DISPLAY_ACTIVE_FROM"] == date('d.m.Y')) ? 'Сегодня' : $arItem["DISPLAY_ACTIVE_FROM"];
		$budget = ($arItem['PROPERTIES']['BUDGET']['VALUE']) ? formatPrice($arItem['PROPERTIES']['BUDGET']['VALUE']).' ₽':'';

		$countText = strlen($arItem['DETAIL_TEXT']);
		if ($countText > 1000) {
			$string = strip_tags($arItem['DETAIL_TEXT']);
			$string = substr($string, 0, 1000);
			$string = rtrim($string, "!,.-");
			$string = substr($string, 0, strrpos($string, ' '));
			$text = $string."...";
		} else {
			$text = $arItem['DETAIL_TEXT'];
		}?>
		<div class="orders__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<div class="orders__item-head">
				<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="orders__item-title"><?=$arItem["NAME"]?></a>
				<span class="orders__item-price"><?=$budget?>
					<span class="control control--favorites orders__item-favourites <?=$fav_active?>">
						<div class="control__icon js-add-favorites" data-id="<?=$arItem['ID']?>" data-cookie="favorites_orders">
							<svg class="icon icon-favorites" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 46.9" style="enable-background:new 0 0 50 46.9;" xml:space="preserve">
								<path d="M46.4,4.4l-0.3-0.3C43.8,1.3,40.4,0,36.7,0s-6.8,1.6-9.4,4.2L25,6.8l-2.3-2.6C20.3,1.3,16.9,0,13.3,0S6.5,1.6,3.9,4.2L3.6,4.4C1.3,7,0,10.7,0,14.6s1.3,7.3,3.6,10.2l20.1,21.6c0.3,0.3,1.3,0.5,1.3,0.5s1-0.3,1.3-0.5l20.1-21.6c2.3-2.6,3.6-6.3,3.6-10.2C50,10.7,48.7,7,46.4,4.4z" />
							</svg>
						</div>
						<div class="control__container">
							<div class="control__main"><?=$fav_text?></div>
						</div>
					</span>
				</span>
				<div class="order__head-ico d-block d-md-none">
					<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/<?=getIconMainSection($arResult['CATEGORY'][$arItem['PROPERTIES']['CATEGORY']['VALUE']])?>">
				</div>
				<a href="/zakazy/<?=$arItem['PROPERTIES']['REGION']['VALUE']?>/<?=$arItem['PROPERTIES']['CATEGORY']['VALUE']?>/" class="orders__item-cat"><?=$arItem['DISPLAY_PROPERTIES']['CATEGORY']['DISPLAY_VALUE']?></a>
				<div class="control control--favorites orders__item-favourites <?=$fav_active?>">
					<div class="control__icon js-add-favorites" data-id="<?=$arItem['ID']?>" data-cookie="favorites_orders">
						<svg class="icon icon-favorites" version="1.1" id="Capa_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 46.9" style="enable-background:new 0 0 50 46.9;" xml:space="preserve">
							<path d="M46.4,4.4l-0.3-0.3C43.8,1.3,40.4,0,36.7,0s-6.8,1.6-9.4,4.2L25,6.8l-2.3-2.6C20.3,1.3,16.9,0,13.3,0S6.5,1.6,3.9,4.2L3.6,4.4C1.3,7,0,10.7,0,14.6s1.3,7.3,3.6,10.2l20.1,21.6c0.3,0.3,1.3,0.5,1.3,0.5s1-0.3,1.3-0.5l20.1-21.6c2.3-2.6,3.6-6.3,3.6-10.2C50,10.7,48.7,7,46.4,4.4z" />
						</svg>
					</div>
					<div class="control__container">
						<div class="control__main"><?=$fav_text?></div>
					</div>
				</div>
			</div>
			<div class="orders__item-content">
				<div class="orders__item-ico d-none d-md-block d-lg-block">
					<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/<?=getIconMainSection($arResult['CATEGORY'][$arItem['PROPERTIES']['CATEGORY']['VALUE']])?>">
				</div>
				<div class="orders__item-desc">
					<?=$text?>
				</div>
			</div>
			<div class="orders__item-bottom">
				<div class="orders__item-status"><?=($arItem['PROPERTIES']['STATUS']['VALUE'])?$arItem['PROPERTIES']['STATUS']['VALUE']:'Открыт'?></div>
				<div class="orders__item-time"><?=$dateShow?></div>
				<div class="orders__item-place"><?=$arItem['PROPERTIES']['ADDRESS']['VALUE']?></div>
			</div>
		</div>
	<?endforeach;?>
</div>
<?=$arResult["NAV_STRING"]?>
