<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if ($arResult["SHOW_SMS_FIELD"] == true) CJSCore::Init('phone_auth');
// dump($arResult);
// echo "<pre>";print_r($arResult);echo "</pre>";
$regGroup = ($_REQUEST['group']) ? $_REQUEST['group'] : 6;
?>
<?if(!$_REQUEST['group']):?>
<div class="modal-dialog modal-dialog--login" role="document">
	<div class="modal-content">
		<div class="modal-body">
			<div class="modal-header choose_reg">
				<a class="btn" href="/lk/reg.php?group=6">Я - Заказчик</a>
				<a class="btn" href="/lk/reg.php?group=5">Я - Исполнитель</a>
			</div>
		</div>
	</div>
</div>
<?else:?>
<div class="modal-dialog modal-dialog--login" role="document">
	<div class="modal-content">
		<div class="modal-body">
			<div class="modal-header">
				<h4 class="modal-header__title">
					Регистрация
				</h4>
			</div>
			<div class="modal-container">

				<?if (count($arResult["ERRORS"]) > 0):
					foreach ($arResult["ERRORS"] as $key => $error)
						if (intval($key) == 0 && $key !== 0)
							$arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);

					ShowError(implode("<br />", $arResult["ERRORS"]));
				elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):
				?>
					<p><?=GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
				<?endif;?>

				<form method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform" class="regForm" enctype="multipart/form-data">

					<?if($arResult["BACKURL"] <> ''):?>
						<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
					<?endif;?>

					<div class="lk-form-block">
						<label>
							<input type="text" name="REGISTER[NAME]" value="<?=$arResult["VALUES"]["NAME"]?>" class="form-input" placeholder="ФИО" required>
							<span class="placeholder">ФИО</span>
						</label>
					</div>

					<?if($arResult["PHONE_REGISTRATION"]):?>
						<div class="lk-form-block">
							<label>
								<input type="tel" name="REGISTER[PHONE_NUMBER]" value="<?=$arResult["VALUES"]["PHONE_NUMBER"]?>" class="form-input regPhone" placeholder="Номер телефона" required>
								<span class="placeholder">Номер телефона</span>
							</label>
						</div>
					<?endif?>
						<div class="lk-form-block">
							<label>
								<input type="tel" name="REGISTER[PERSONAL_PHONE]" value="<?=$arResult["VALUES"]["PERSONAL_PHONE"]?>" class="form-input regPhone" placeholder="Номер телефона" required>
								<span class="placeholder">Номер телефона</span>
							</label>
						</div>

					<?if($arResult["EMAIL_REGISTRATION"]):?>
						<div class="lk-form-block">
							<label>
								<input type="email" name="REGISTER[EMAIL]" value="<?=$arResult["VALUES"]["EMAIL"]?>" class="form-input" placeholder="Адрес электронной почты" required>
								<span class="placeholder">Адрес электронной почты</span>
							</label>
						</div>
					<?endif?>

					<?
					if ($arResult["USE_CAPTCHA"] == "Y")
					{
						?>
						<div class="lk-form-block block__captcha">
							<label>
								<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
								<input type="text" name="captcha_word" autocomplete="off" class="form-input" required/>
								<span class="placeholder"><?=GetMessage("REGISTER_CAPTCHA_PROMT")?></span>
							</label>
							<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA"/>
						</div>
						<?
					}
					?>

					<div class="lk-form-block">
						<p class="form-desc">Нажимая на кнопку, вы даете согласие на обработку персональных данных и соглашаетесь с <a href="/politika-konfidentsialnosti/">Политикой Конфиденциальности</a></p>
					</div>

					<?$new_pass = substr(str_shuffle('0123456789'), 0, 6);
					$new_pass = ($arResult["VALUES"]["PASSWORD"]) ? $arResult["VALUES"]["PASSWORD"] : $new_pass;
					$new_pass_conf = ($arResult["VALUES"]["CONFIRM_PASSWORD"]) ? $arResult["VALUES"]["CONFIRM_PASSWORD"] : $new_pass;?>
					<input type="hidden" name="REGISTER[PASSWORD]" value="<?=$new_pass?>" autocomplete="off">
					<input type="hidden" name="REGISTER[CONFIRM_PASSWORD]" value="<?=$new_pass_conf?>" autocomplete="off">
					<input type="hidden" name="REGISTER[UF_PASSWORD]" value="<?=$new_pass?>" autocomplete="off">
					<input type="hidden" name="REGISTER[LOGIN]" value="<?=$arResult["VALUES"]["LOGIN"]?>" class="regLogin">
					<input type="hidden" name="REGISTER[PERSONAL_ICQ]" value="<?=$regGroup?>" class="regGroup">

					<input type="submit" class="btn btn--red" name="register_submit_button" value="<?=GetMessage("AUTH_REGISTER")?>">

					<div class="form-block-bottom">
						<a href="/lk/">Войти</a> | <a href="/lk/?forgot_password=yes">Восстановить пароль</a>
						<p class="form-desc">При входе, вы принимаете условия <a href="/polzovatelskoe-soglashenie/">Пользовательского соглашения</a> и <a href="/politika-konfidentsialnosti/">Политики конфиденциальности</a></p>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>
<?endif;?>
