<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$sendOk = ($arParams['AUTH_RESULT']['TYPE'] == 'OK') ? true : false;?>
<div class="modal-dialog password-recovery">
	<div class="modal-content">
		<div class="modal-body center">
			<form name="bform" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
			<?
			if (strlen($arResult["BACKURL"]) > 0)
			{
			?>
				<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
			<?
			}
			?>
				<input type="hidden" name="AUTH_FORM" value="Y">
				<input type="hidden" name="TYPE" value="SEND_PWD">
				<?if($sendOk):?>
					<br><br><br>
					<p>
						Мы отправили инструкцию для восстановления пароля на ваш адрес электронной почты.
					</p>
					<p>
						Если нет письма, проверьте папку «Спам»
					</p>
				<?else:?>
				<?
					ShowMessage($arParams["~AUTH_RESULT"]);
				?>

			  <div class="modal-header display_block">
					<h4><?echo GetMessage("sys_forgot_pass_label")?></h4>
				</div>

				<div class="modal-container">

					<p>Введите адрес элетронной почты, который вы<br>использовали при регистрации.</p>

					<div class="lk-form-block">
						<label>
							<input type="text" name="USER_LOGIN" value="<?=$arResult["USER_LOGIN"]?>" class="form-input" />
							<span class="placeholder"><?=GetMessage("sys_forgot_pass_login1")?></span>
							<input type="hidden" name="USER_EMAIL" />
						</label>
						<!-- <div><?echo GetMessage("sys_forgot_pass_note_email")?></div> -->
					</div>

				<?if($arResult["PHONE_REGISTRATION"]):?>
					<div class="lk-form-block">
						<label>
							<input type="text" name="USER_PHONE_NUMBER" value="<?=$arResult["USER_PHONE_NUMBER"]?>" class="form-input" />
							<span class="placeholder"><?=GetMessage("sys_forgot_pass_phone")?></span>
						</label>
						<div><?echo GetMessage("sys_forgot_pass_note_phone")?></div>
					</div>
				<?endif;?>

				<?if($arResult["USE_CAPTCHA"]):?>
					<div style="margin-top: 16px">
						<div>
							<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
							<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
						</div>
						<div><?echo GetMessage("system_auth_captcha")?></div>
						<div><input type="text" name="captcha_word" maxlength="50" value="" /></div>
					</div>
				<?endif?>
					<div class="btn-center">
						<input type="submit" name="send_account_info" value="<?=GetMessage("AUTH_SEND")?>" class="btn btn--red" />
					</div>
				</div>
			</form>

			<?endif;?>

			<div class="form-block-bottom">
				<p>
					<a href="<?=$arResult["AUTH_AUTH_URL"]?>"><b><?=GetMessage("AUTH_AUTH")?></b></a> | <a href="/lk/reg.php">Зарегистрироваться</a>
				</p>
			</div>

			<script type="text/javascript">
			document.bform.onsubmit = function(){document.bform.USER_EMAIL.value = document.bform.USER_LOGIN.value;};
			document.bform.USER_LOGIN.focus();
			</script>
		</div>
	</div>
</div>
