<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<form name="form_auth" class="lk-form-auth" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
	<?if($arResult["BACKURL"] <> ''):?>
		<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
	<?endif?>
	<?foreach ($arResult["POST"] as $key => $value):?>
		<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
	<?endforeach?>
	<input type="hidden" name="AUTH_FORM" value="Y" />
	<input type="hidden" name="TYPE" value="AUTH" />

	<div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
				<!-- <div class="modal-close" data-dismiss="modal" aria-label="Close">
          <svg class="icon">
            <use xlink:href="#icon-close"></use>
          </svg>
        </div> -->
				<?
				ShowMessage($arParams["~AUTH_RESULT"]);
				ShowMessage($arResult['ERROR_MESSAGE']);
				?>
        <div class="modal-header">
          <h4 class="modal-header__title">
						<?=($_REQUEST['from'] == 'reg') ? 'Спасибо за регистрацию' : 'Вход'?>
          </h4>
        </div>
        <div class="modal-container">
					<?if($_REQUEST['from'] == 'reg'):?>
						<p class="center">
							Вам отправлено письмо на почту, которое вы указали при регистрации с логином и паролем для входа в Личный кабинет, Пожалуйста, проверьте почту.
						</p>
					<?endif;?>
          <div class="lk-form-block">
            <label for="">
              <input class="form-input" type="text" name="USER_LOGIN" value="<?=$arResult["LAST_LOGIN"]?>" placeholder="Номер телефона или почта" required>
              <span class="placeholder">Номер телефона или почта</span>
            </label>
          </div>
          <div class="lk-form-block">
            <label for="">
              <input class="form-input" type="password" name="USER_PASSWORD" placeholder="Пароль" autocomplete="off" required>
              <span class="placeholder">Пароль</span>
            </label>
          </div>
          <div class="lk-form-block form-block-link">
            <label class="fake-check align-items-center konkurs" for="USER_REMEMBER">
              <input type="checkbox" id="USER_REMEMBER" name="USER_REMEMBER" value="Y" checked="checked">
              <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 22 22" style="enable-background:new 0 0 22 22;" xml:space="preserve">
                <g>
                  <path class="checks" d="M9.6,15.6l-3.2-3.1c-0.1-0.1-0.2-0.3-0.2-0.4c0-0.2,0-0.3,0.2-0.5c0.1-0.1,0.3-0.2,0.4-0.2s0.3,0.1,0.4,0.2l2.7,2.6l4.8-7.7c0.1-0.1,0.2-0.2,0.4-0.2c0.2,0,0.3,0,0.5,0.1c0.1,0.1,0.2,0.2,0.3,0.4c0,0.2,0,0.3-0.1,0.5l-5.2,8.4c0,0,0,0.1,0,0.1c0,0,0,0-0.1,0.1l0,0c-0.1,0-0.1,0.1-0.2,0.1c0,0,0,0-0.1,0c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1-0.1C9.7,15.7,9.7,15.7,9.6,15.6C9.6,15.7,9.6,15.7,9.6,15.6C9.6,15.6,9.6,15.6,9.6,15.6z"></path>
                </g>
              </svg>
              <span>Запомнить пароль</span>
            </label>
            <a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" rel="nofollow">Забыли пароль?</a>
          </div>
          <div class="btn-center">
						<input type="submit" name="Login" value="Войти" class="btn btn--red"/>
					</div>
          <div class="form-block-bottom">
            <a href="<?=$arResult["AUTH_REGISTER_URL"]?>" rel="nofollow">Зарегистрироваться</a>
            <p class="form-desc">При входе, вы принимаете условия <a href="/polzovatelskoe-soglashenie/">Пользовательского соглашения</a> и <a href="/politika-konfidentsialnosti/">Политики конфиденциальности</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
