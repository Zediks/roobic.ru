<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

<div class="navbar-header">

	<?
	foreach($arResult as $arItem):
		if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) continue;
		$selected = ($arItem["SELECTED"]) ? 'selected' : '';
	?>
		<a class="navbar-header__link <?=$selected?>" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
		
	<?endforeach?>

</div>

<?endif?>
