<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

<ul class="footer-menu">

	<?
	foreach($arResult as $arItem):
		if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) continue;
		$selected = ($arItem["SELECTED"]) ? 'selected' : '';
	?>
		<li class="footer-menu__item"><a class="footer-menu__link <?=$selected?>" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>

	<?endforeach?>

</ul>

<?endif?>
