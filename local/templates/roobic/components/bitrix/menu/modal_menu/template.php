<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

<ul class="menu-modal__menu">

	<?
	foreach($arResult as $arItem):
		if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) continue;
		$selected = ($arItem["SELECTED"]) ? 'selected' : '';
	?>
		<li class="menu-modal__item"><a class="menu-modal__link <?=$selected?>" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>

	<?endforeach?>

</ul>

<?endif?>
