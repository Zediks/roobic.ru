<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die();
use Bitrix\Main\Page\Asset;
  Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/style/libs.min.css');
  Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/style/main.css');
  Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/style/lk.css');
  Asset::getInstance()->addJs('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js');
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/libs.min.js');
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/jquery.cookie.js');
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/script.js');
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/myScript.js');

global $USER;

// dump($_COOKIE); // разбираем куки
if(isset($_COOKIE['comparison_orders'])){
	$arComparisonOrders = explode('-',$_COOKIE['comparison_orders']);
}
if(isset($_COOKIE['favorites_orders'])){
	$arFavoritesOrders = explode('-',$_COOKIE['favorites_orders']);
}
if(isset($_COOKIE['comparison_house'])){
	$arComparisonHouse = explode('-',$_COOKIE['comparison_house']);
}
if(isset($_COOKIE['favorites_house'])){
	$arFavoritesHouse = explode('-',$_COOKIE['favorites_house']);
}
if(isset($_COOKIE['comparison_bild'])){
	$arComparisonBild = explode('-',$_COOKIE['comparison_bild']);
}
if(isset($_COOKIE['favorites_bild'])){
	$arFavoritesBild = explode('-',$_COOKIE['favorites_bild']);
}
if(isset($_COOKIE['comparison_ads'])){
	$arComparisonAds = explode('-',$_COOKIE['comparison_ads']);
}
if(isset($_COOKIE['favorites_ads'])){
	$arFavoritesAds = explode('-',$_COOKIE['favorites_ads']);
}
// setcookie("favorites_bild","",time()-3600,"/");
$sumComparison = count($arComparisonOrders) + count($arComparisonHouse) + count($arComparisonBild) + count($arComparisonAds);
$sumFavorites = count($arFavoritesOrders) + count($arFavoritesHouse) + count($arFavoritesBild) + count($arFavoritesAds);
CJSCore::Init(array("fx"));

global $typeURL;
global $typeURLOne;
$ourDir = $APPLICATION->GetCurDir();
$posBani = (strpos($ourDir, 'bani') !== false || strpos($ourDir, 'banya') !== false) ? true : false;
$typeURL = ($posBani) ? 'bani' : 'doma';
$typeURLOne = ($posBani ) ? 'banya' : 'dom';
?>
<!DOCTYPE html>
<html lang="<?=LANGUAGE_ID?>">
  <head>
    <title><?$APPLICATION->ShowTitle();?></title>
    <meta name="viewport" content="width=device-width"/>
    <meta name="theme-color" content="#ffffff"/>
    <!-- <meta property="og:url"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Компания строительный квадрат"/>
    <meta property="og:description" content="Описание страницы"/>
    <meta property="og:image" content="images/bh.jpg"/> -->
    <link rel="shortcut icon" href="/favicon.svg" type="image/x-icon"/>
    <link rel="apple-touch-icon" href="/favicon.png"/>
    <?$APPLICATION->ShowHead();?>
  </head>
  <body <?$APPLICATION->ShowViewContent('body-class'); // класс для раздела заказов и ЛК профиль?>>
    <div id="panel"><?$APPLICATION->ShowPanel();?></div>
    <?$APPLICATION->ShowViewContent('cart_screen'); // класс для детальной строители?>
    <?if($USER->IsAuthorized()):?>
    <div class="top-header">
      <div class="regions-choose">
        <?$APPLICATION->IncludeComponent(
           "sotbit:regions.choose",
           "stroiman",
           Array(
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => 36000000,
           )
        );?>
      </div>
      <!-- <a href="#" class="select-city">
        <span>
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="11px" height="16px">
            <image x="0px" y="0px" width="11px" height="16px" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAQCAMAAAD3Y3VMAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAA5FBMVEUcHB0bGxwbGx0bGxwbGxwcHB0bGxwbGx0cHB0bGxwbGxwbGxwbGxwbGxwcHB0bGxwcHB0cHB0bGxwbGxwbGxwbGx0bGxwbGxwcHBwbGxwbGxwbGxwcHB0cHBwcHB0bGxwbGx0cHB0bGx0bGx0bGxwbGxwbGxwcHB0bGxwbGxwbGx0bGxwcHB0bGxwbGxwbGxwbGx0bGxwcHB0cHB0cHB0bGx0bGx0bGxwcHB0bGx0bGxwcHB0bGxwbGxwbGx0bGx0bGx0bGx0bGx0bGxwcHB0bGxwbGxwcHB0bGxwcHB0bGx3///+vjENAAAAASHRSTlMAAAAzqOPwyngQZfrCGjf55Lkdu9VhQJX99onpSazY9McOXv3o8+pFpP7T0k4kivhl/PrWNBPshgcDa94PzrwIlCsSwWJbnRUFcEAvAAAAAWJLR0RLaQuFUAAAAAd0SU1FB+QKCg8KMLNmlOoAAACfSURBVAjXJY7XWoJBEEOzQ5GOSO9dqoI0BQHpZPb9H8j94eTmfLnIDGB8/kDwJRQ2BohEqS6xuEHCSfKVlqk3pMlMNpcvsFhCUssVGFOl1lBnowkjrTY76Op7z/X9AYdoWI7GH58T1vOYkvZrplbnCyxXSoe13z9Yb9y243drIOHd427W+wGViSr3IgKRvwNt8Ph0OZ31Ag/ncr3dPf0HqccaSxWRp2IAAAAASUVORK5CYII=" />
          </svg>
        </span>
        Москва</a> -->
      <div class="top-header__container">
       <?if(CSite::InGroup([6])): // если клиент?>
        <a href="/lk/client_order" class="new-order hidden-sm">
          <span>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px">
              <defs>
                <linearGradient id="PSgrad_0" x1="0%" x2="0%" y1="100%" y2="0%">
                  <stop offset="0%" stop-color="rgb(233,237,240)" stop-opacity="1" />
                  <stop offset="100%" stop-color="rgb(255,255,255)" stop-opacity="1" />
                </linearGradient>
              </defs>
              <path fill-rule="evenodd" fill="rgb(231, 117, 97)" d="M9.000,-0.000 C13.971,-0.000 18.000,4.029 18.000,9.000 C18.000,13.971 13.971,18.000 9.000,18.000 C4.029,18.000 -0.000,13.971 -0.000,9.000 C-0.000,4.029 4.029,-0.000 9.000,-0.000 Z" />
              <path class="orange" fill-rule="evenodd" fill="url(#PSgrad_0)" d="M5.000,8.000 L13.000,8.000 L13.000,10.000 L5.000,10.000 L5.000,8.000 Z" />
              <path class="orange" fill-rule="evenodd" fill="url(#PSgrad_0)" d="M8.000,5.000 L10.000,5.000 L10.000,13.000 L8.000,13.000 L8.000,5.000 Z" />
            </svg>
          </span>
          Разместить заказ</a>
       <?endif;?>
       <?if(CSite::InGroup([5])): // если компания?>
        <a href="/lk/company_card" class="performer hidden-sm">
          <span>
            <svg width="14px" version="1.1" id="Layer_35" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 368 464" style="enable-background:new 0 0 368 464;" xml:space="preserve">
              <style type="text/css">
                .st0 {
                  fill: #E77561;
                }
              </style>
              <g>
                <path class="st0" d="M296,160H72v64c0,61.8,50.2,112,112,112s112-50.2,112-112V160z" />
                <path class="st0" d="M305.1,62.2l-36.7,24.4c-1.4,0.9-2.9,1.3-4.4,1.3c-2.1,0-4.1-0.8-5.7-2.3l-16-16c-2.7-2.7-3.1-6.9-1-10.1l25.9-38.9c-13.4-9.1-28.7-15.5-45.1-18.6l-14.3,71.4c-0.7,3.7-4,6.4-7.8,6.4h-32c-3.8,0-7.1-2.7-7.8-6.4L145.9,2.1c-14.2,2.7-27.5,7.9-39.5,15.1l28.2,42.4c2.1,3.2,1.7,7.4-1,10.1l-16,16c-1.5,1.6-3.6,2.4-5.7,2.4c-1.5,0-3.1-0.4-4.4-1.3L65.2,58.4C55.7,74.3,49.7,92.5,48.4,112h271.2C318.4,94,313.4,77.2,305.1,62.2z" />
                <path class="st0" d="M32,128h304v16H32V128z" />
                <path class="st0" d="M312,224c0,5-0.4,9.9-0.9,14.8c9.8-3,16.9-12.1,16.9-22.8c0-10.4-6.7-19.2-16-22.5V224z" />
                <path class="st0" d="M117.7,63L93.2,26.3c-7,5.6-13.4,12-19,19L111,69.7L117.7,63z" />
                <path class="st0" d="M265,69.7l31.4-20.9c-4.9-6.6-10.4-12.7-16.5-18.1L258.3,63L265,69.7z" />
                <path class="st0" d="M193.4,64l12.7-63.7C204.1,0.2,202.1,0,200,0h-32c-2.1,0-4.1,0.2-6.2,0.3L174.6,64H193.4z" />
                <path class="st0" d="M56,352H40c-22.1,0-40,17.9-40,40v72h24v-40c0-4.4,3.6-8,8-8h24V352z" />
                <path class="st0" d="M40,432h288v32H40V432z" />
                <path class="st0" d="M40,216c0,10.8,7.2,19.8,16.9,22.8C56.4,233.9,56,229,56,224v-30.5C46.7,196.8,40,205.6,40,216L40,216z" />
                <path class="st0" d="M280,352h16v64h-16V352z" />
                <path class="st0" d="M328,352h-16v64h24c4.4,0,8,3.6,8,8v40h24v-72C368,369.9,350.1,352,328,352z" />
                <path class="st0" d="M128.6,352H104v64h160v-64h-24.6c-3.9,27.1-27.2,48-55.4,48S132.5,379.1,128.6,352L128.6,352z" />
                <path class="st0" d="M72,352h16v64H72V352z" />
                <path class="st0" d="M223.2,352h-78.4c3.7,18.2,19.9,32,39.2,32S219.5,370.2,223.2,352z" />
              </g>
            </svg>
          </span>
          Стать исполнителем</a>
       <?endif;?>
        <a href="/lk/notifications" class="notification">
          <svg width="18px" class="icon" version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 54.5" style="enable-background:new 0 0 50 54.5;" xml:space="preserve">
            <path class="st0" d="M47.7,24.7c-1.3,0-2.3-1-2.3-2.3c0-6.4-2.5-12.4-7-16.9c-0.9-0.9-0.9-2.3,0-3.2c0.9-0.9,2.3-0.9,3.2,0C47,7.7,50,14.9,50,22.5C50,23.7,49,24.7,47.7,24.7z" />
            <path class="st0" d="M2.3,24.7c-1.3,0-2.3-1-2.3-2.3C0,14.9,3,7.7,8.3,2.4c0.9-0.9,2.3-0.9,3.2,0c0.9,0.9,0.9,2.3,0,3.2c-4.5,4.5-7,10.5-7,16.9C4.5,23.7,3.5,24.7,2.3,24.7z" />
            <path class="st0" d="M46.3,38.4c-3.4-2.9-5.4-7.2-5.4-11.6v-6.3c0-8-5.9-14.6-13.6-15.7V2.3C27.3,1,26.3,0,25,0c-1.3,0-2.3,1-2.3,2.3v2.5C15,5.8,9.1,12.5,9.1,20.5v6.3c0,4.5-2,8.7-5.4,11.7c-0.9,0.8-1.4,1.9-1.4,3c0,2.2,1.8,4,4,4h37.5c2.2,0,4-1.8,4-4C47.7,40.3,47.2,39.2,46.3,38.4z" />
            <path class="st0" d="M25,54.5c4.1,0,7.6-2.9,8.4-6.8H16.7C17.4,51.6,20.9,54.5,25,54.5z" />
          </svg>
        </a>
        <a href="/lk/messages" class="message">
          <svg width="22px" class="icon" version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 48" style="enable-background:new 0 0 50 48;" xml:space="preserve">
            <path class="st0" d="M43.1,0H6.9C3.1,0,0,3.1,0,6.9v24.2c0,3.8,3.1,6.9,6.8,6.9V48l14.5-10.1h21.8c3.8,0,6.9-3.1,6.9-6.9V6.9C50,3.1,46.9,0,43.1,0z M36.6,26.8H13.4v-2.9h23.2V26.8z M36.6,20.5H13.4v-2.9h23.2V20.5z M36.6,14.3H13.4v-2.9h23.2V14.3z" />
          </svg>
        </a>
        <a href="/lk/" class="login js-open-login">
          <span>
            <svg width="14px" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 266.2 350" style="enable-background:new 0 0 266.2 350;" xml:space="preserve">
              <g>
                <path class="st0" d="M133.1,171.2c38.9,0,70.5-38.3,70.5-85.6c0-47.3-10.4-85.6-70.5-85.6S62.6,38.3,62.6,85.6C62.6,132.9,94.2,171.2,133.1,171.2z" />
                <path class="st0" d="M0,301.9C0,299,0,301,0,301.9L0,301.9z" />
                <path class="st0" d="M266.2,304.1C266.2,303.3,266.2,298.6,266.2,304.1L266.2,304.1z" />
                <path class="st0" d="M266,298.4c-1.3-82.3-12.1-105.8-94.4-120.7c0,0-11.6,14.8-38.6,14.8s-38.6-14.8-38.6-14.8c-81.4,14.7-92.8,37.8-94.3,118c-0.1,6.5-0.2,6.9-0.2,6.1c0,1.4,0,4.1,0,8.7c0,0,19.6,39.5,133.1,39.5c113.5,0,133.1-39.5,133.1-39.5c0-3,0-5,0-6.4C266.2,304.6,266.1,303.7,266,298.4z" />
              </g>
            </svg>
          </span>
          <b class="hidden-sm">Личный кабинет</b>
        </a>
      </div>
    </div>
    <?else:?>
      <div class="top-header">
        <div class="regions-choose">
          <?$APPLICATION->IncludeComponent(
             "sotbit:regions.choose",
             "stroiman",
             Array(
                  "CACHE_TYPE" => "A",
                  "CACHE_TIME" => 36000000,
             )
          );?>
        </div>
        <div class="top-header__container">
          <a href="/lk/reg.php?group=6" class="new-order hidden-sm defGroup" data-group="6">
            <span>
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px">
                <defs>
                  <linearGradient id="PSgrad_0" x1="0%" x2="0%" y1="100%" y2="0%">
                    <stop offset="0%" stop-color="rgb(233,237,240)" stop-opacity="1" />
                    <stop offset="100%" stop-color="rgb(255,255,255)" stop-opacity="1" />
                  </linearGradient>
                </defs>
                <path fill-rule="evenodd" fill="rgb(231, 117, 97)" d="M9.000,-0.000 C13.971,-0.000 18.000,4.029 18.000,9.000 C18.000,13.971 13.971,18.000 9.000,18.000 C4.029,18.000 -0.000,13.971 -0.000,9.000 C-0.000,4.029 4.029,-0.000 9.000,-0.000 Z" />
                <path class="orange" fill-rule="evenodd" fill="url(#PSgrad_0)" d="M5.000,8.000 L13.000,8.000 L13.000,10.000 L5.000,10.000 L5.000,8.000 Z" />
                <path class="orange" fill-rule="evenodd" fill="url(#PSgrad_0)" d="M8.000,5.000 L10.000,5.000 L10.000,13.000 L8.000,13.000 L8.000,5.000 Z" />
              </svg>
            </span>
            Разместить заказ</a>
          <a href="/lk/reg.php?group=5" class="performer hidden-sm defGroup" data-group="5">
            <span>
              <svg width="14px" version="1.1" id="Layer_35" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 368 464" style="enable-background:new 0 0 368 464;" xml:space="preserve">
                <style type="text/css">
                  .st0 {
                    fill: #E77561;
                  }
                </style>
                <g>
                  <path class="st0" d="M296,160H72v64c0,61.8,50.2,112,112,112s112-50.2,112-112V160z" />
                  <path class="st0" d="M305.1,62.2l-36.7,24.4c-1.4,0.9-2.9,1.3-4.4,1.3c-2.1,0-4.1-0.8-5.7-2.3l-16-16c-2.7-2.7-3.1-6.9-1-10.1
                      l25.9-38.9c-13.4-9.1-28.7-15.5-45.1-18.6l-14.3,71.4c-0.7,3.7-4,6.4-7.8,6.4h-32c-3.8,0-7.1-2.7-7.8-6.4L145.9,2.1
                      c-14.2,2.7-27.5,7.9-39.5,15.1l28.2,42.4c2.1,3.2,1.7,7.4-1,10.1l-16,16c-1.5,1.6-3.6,2.4-5.7,2.4c-1.5,0-3.1-0.4-4.4-1.3
                      L65.2,58.4C55.7,74.3,49.7,92.5,48.4,112h271.2C318.4,94,313.4,77.2,305.1,62.2z" />
                  <path class="st0" d="M32,128h304v16H32V128z" />
                  <path class="st0" d="M312,224c0,5-0.4,9.9-0.9,14.8c9.8-3,16.9-12.1,16.9-22.8c0-10.4-6.7-19.2-16-22.5V224z" />
                  <path class="st0" d="M117.7,63L93.2,26.3c-7,5.6-13.4,12-19,19L111,69.7L117.7,63z" />
                  <path class="st0" d="M265,69.7l31.4-20.9c-4.9-6.6-10.4-12.7-16.5-18.1L258.3,63L265,69.7z" />
                  <path class="st0" d="M193.4,64l12.7-63.7C204.1,0.2,202.1,0,200,0h-32c-2.1,0-4.1,0.2-6.2,0.3L174.6,64H193.4z" />
                  <path class="st0" d="M56,352H40c-22.1,0-40,17.9-40,40v72h24v-40c0-4.4,3.6-8,8-8h24V352z" />
                  <path class="st0" d="M40,432h288v32H40V432z" />
                  <path class="st0" d="M40,216c0,10.8,7.2,19.8,16.9,22.8C56.4,233.9,56,229,56,224v-30.5C46.7,196.8,40,205.6,40,216L40,216z" />
                  <path class="st0" d="M280,352h16v64h-16V352z" />
                  <path class="st0" d="M328,352h-16v64h24c4.4,0,8,3.6,8,8v40h24v-72C368,369.9,350.1,352,328,352z" />
                  <path class="st0" d="M128.6,352H104v64h160v-64h-24.6c-3.9,27.1-27.2,48-55.4,48S132.5,379.1,128.6,352L128.6,352z" />
                  <path class="st0" d="M72,352h16v64H72V352z" />
                  <path class="st0" d="M223.2,352h-78.4c3.7,18.2,19.9,32,39.2,32S219.5,370.2,223.2,352z" />
                </g>
              </svg>
            </span>
            Стать исполнителем</a>
          <a href="/lk/" class="login">
            <span>
              <!-- <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="19px" height="20px">
                <image x="0px" y="0px" width="19px" height="20px" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAAUCAMAAABYi/ZGAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAApVBMVEX///8hISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISIhISL///91Y/1vAAAANXRSTlMAsO05NVZLRUTBQbLlVEofJ5lHcJ3Qu7+xku+o4MnMw57rW2WLUlMmM63oKE1CPLRJmvr3Qzco7OkAAAABYktHRACIBR1IAAAAB3RJTUUH5AoKDwwfTu0ONQAAAItJREFUGNNtz+cSgjAQRtFVJGBBEXtFRYol9u/9X01Mhgmy3D/JnEkmG6KiBlRNi0wtWwjhuG10iNXtcfPArV+yga9XUTIbDjMaImBGI4W5jSdT3Wy+WMJTtlpvdNtwt8ehejf6Hfu3I+LqGwkSNkua8Znr/lYU19jpbPaXq5TyZt3xMPbEKw/vD30B9YkPAUVwfMAAAAAASUVORK5CYII=" />
              </svg> -->
              <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/input.svg" width="20">
            </span>
            Войти</a>
        </div>
      </div>
    <?endif;?>
    <div class="h-top">
      <header>
        <a class="logo" href="/">
          <img src="<?=SITE_TEMPLATE_PATH?>/images/logo_2color.svg" alt="" width="200">
        </a>
        <?$APPLICATION->IncludeComponent(
        	"bitrix:menu",
        	"top_menu",
        	Array(
        		"ALLOW_MULTI_SELECT" => "N",
        		"CHILD_MENU_TYPE" => "left",
        		"DELAY" => "N",
        		"MAX_LEVEL" => "1",
        		"MENU_CACHE_GET_VARS" => array(""),
        		"MENU_CACHE_TIME" => "3600",
        		"MENU_CACHE_TYPE" => "N",
        		"MENU_CACHE_USE_GROUPS" => "N",
        		"ROOT_MENU_TYPE" => "top",
        		"USE_EXT" => "N"
        	)
        );?>
        <div class="header__right">
          <div class="navbar-favorits">
            <div class="search d-block d-md-none">
            <div class="icons js-open-mobile-search">
                <svg class="icon icon-loop">
                  <use xlink:href="#icon-loop"></use>
                </svg>
              </div>
            </div>
            <div class="search d-none d-md-block">
              <form class="search-form-pc search-form place_show" action="/poisk/">
                <input class="search-form__input" type="text" name="q" placeholder="Поиск" value="<?=$_REQUEST['q']?>"/>
                <button class="search-form__button">
                  <svg class="icon icon-loop">
                    <use xlink:href="#icon-loop"></use>
                  </svg>
                </button>
              </form>
              <div class="icons js-open-search">
                <svg class="icon icon-loop">
                  <use xlink:href="#icon-loop"></use>
                </svg>
              </div>
            </div>
            <a class="icons favorites" href="/izbrannoe/" aria-label="Избранное">
              <svg class="icon icon-favorites">
                <use xlink:href="#header_favorites"></use>
              </svg>
              <div class="favorites-count" id="favHeader"><?=$sumFavorites?></div>
            </a>
            <a class="icons favorites" href="/sravnenie/" aria-label="Сравнение">
              <svg class="icon icon-favorites">
                <use xlink:href="#header_comparison"></use>
              </svg>
              <div class="favorites-count" id="compHeader"><?=$sumComparison?></div>
            </a>
          </div>
          <div class="menu"><span>Меню</span>
            <div class="menu__icon js-open-menu"><span></span><span></span><span></span></div>
          </div>
        </div>
      </header>
    </div>
    <div class="search search---mobile">
      <form class="search-form search-form-mobile place_show" action="/poisk/">
        <input class="search-form__input" type="text" name="q" placeholder="Поиск" value="<?=$_REQUEST['q']?>"/>
        <button class="search-form__button">
            <svg class="icon icon-loop">
            <use xlink:href="#icon-loop"></use>
            </svg>
        </button>
        </form>
      </div>
