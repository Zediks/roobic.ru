<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die();?>
<footer>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">
        <a class="logo" href="/">
          <img class="lazyload" data-src="<?=SITE_TEMPLATE_PATH?>/images/logo_2color.svg" alt="" />
        </a>
        <div class="foot-text">Маркетплейс <br>строительных услуг</div>
      </div>
      <div class="col-md-9">
        <div class="row">
          <div class="col-lg-4 col-md-6">
            <?$APPLICATION->IncludeComponent(
            	"bitrix:menu",
            	"footer_menu",
            	array(
            		"ALLOW_MULTI_SELECT" => "N",
            		"CHILD_MENU_TYPE" => "left",
            		"DELAY" => "N",
            		"MAX_LEVEL" => "1",
            		"MENU_CACHE_GET_VARS" => array(
            		),
            		"MENU_CACHE_TIME" => "3600",
            		"MENU_CACHE_TYPE" => "N",
            		"MENU_CACHE_USE_GROUPS" => "N",
            		"ROOT_MENU_TYPE" => "modal",
            		"USE_EXT" => "N",
            		"COMPONENT_TEMPLATE" => "footer_menu"
            	),
            	false
            );?>
          </div>
          <div class="col-lg-4 col-md-6">
            <?$APPLICATION->IncludeComponent(
            	"bitrix:menu",
            	"footer_menu",
            	array(
            		"ALLOW_MULTI_SELECT" => "N",
            		"CHILD_MENU_TYPE" => "left",
            		"DELAY" => "N",
            		"MAX_LEVEL" => "1",
            		"MENU_CACHE_GET_VARS" => array(
            		),
            		"MENU_CACHE_TIME" => "3600",
            		"MENU_CACHE_TYPE" => "A",
            		"MENU_CACHE_USE_GROUPS" => "Y",
            		"ROOT_MENU_TYPE" => "footer_right",
            		"USE_EXT" => "N",
            		"COMPONENT_TEMPLATE" => "footer_menu"
            	),
            	false
            );?>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="cons">
              <div class="cons-title">Контакты: </div>
              <a class="cons-email" href="mailto:welcome@stroiman.ru">welcome@stroiman.ru</a>
              <a class="btn" href="#" data-toggle="modal" data-target="#modal-write-to-us">Написать нам</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="copy">© <?=date('Y')?> Все права защищены</div>
  </div>
</footer>
<?$APPLICATION->IncludeComponent(
  "bitrix:main.include",
  "",
  Array(
    "AREA_FILE_SHOW" => "file",
    "AREA_FILE_SUFFIX" => "inc",
    "EDIT_TEMPLATE" => "",
    "PATH" => SITE_TEMPLATE_PATH."/include/modal.php"
  )
);?>
<?$APPLICATION->IncludeComponent(
  "bitrix:main.include",
  "",
  Array(
    "AREA_FILE_SHOW" => "file",
    "AREA_FILE_SUFFIX" => "inc",
    "EDIT_TEMPLATE" => "",
    "PATH" => SITE_TEMPLATE_PATH."/include/svg.php"
  )
);?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();
   for (var j = 0; j < document.scripts.length; j++) {if (document.scripts[j].src === r) { return; }}
   k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(96491579, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/96491579" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>
