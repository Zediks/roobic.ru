<div class="modal fade" id="modalVideo" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog--video" role="document">
    <div class="modal-content">
      <div class="modal-close js-video-close">
        <svg class="icon">
          <use xlink:href="#icon-close"></use>
        </svg>
      </div>
      <iframe id="video-youtube" title="Видео" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-calculate" tabindex="-1" role="dialog" aria-labelledby="modal-calculate" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="modal-close" data-dismiss="modal" aria-label="Close">
          <svg class="icon">
            <use xlink:href="#icon-close"></use>
          </svg>
        </div>
        <div class="list-pricing">
          <div class="pricing-item">
            <div class="pricing-item__name">Примерная стоимость работ:</div><strong class="pricing-item__warn">от <span class="js-option-price">3 150 000</span> ₽</strong>
          </div>
          <div class="pricing-item">
            <div class="pricing-item__name">Услуги:</div><strong class="js-option-1">Кровельные работы</strong>
          </div>
          <div class="pricing-item">
            <div class="pricing-item__name">Материал:</div><strong class="js-option-2">Металлочерепица</strong>
          </div>
          <div class="pricing-item">
            <div class="pricing-item__name">Объем:</div><strong class="js-option-3 pricing-item__val">180</strong>
          </div>
        </div>
        <div class="modal-warn">* Данный расчет является ориентировочным. <br>Для точного расчета стоимости работ, отправьте заявку</div>
        <div class="d-flex justify-content-center"><a class="btn btn--red js-modal-callback" href="">Оставить заявку</a></div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade modal-callback" id="modal-callback" tabindex="-1" role="dialog" aria-labelledby="modal-callback" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="modal-close" data-dismiss="modal" aria-label="Close">
          <svg class="icon">
            <use xlink:href="#icon-close"></use>
          </svg>
        </div>
        <form action="" method="post" class="modal-form send-form place_show">
          <input type="hidden" name="act" value="sendForm">
          <h5 class="h-title">Оставить заявку</h5>
          <label for="name">
            <input class="form-input" type="text" id="name" name="name" placeholder="Ваше имя" required />
          </label>
          <label for="phone">
            <input class="form-input" type="tel" id="phone" name="phone" placeholder="Номер телефона" required />
          </label>
          <label for="email">
            <input class="form-input" type="email" id="email" name="email" placeholder="E-mail" required />
          </label>
          <div class="politic">
            <label class="fake-check" for="politics">
              <input type="checkbox" id="politics" checked required />
              <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 22 22" style="enable-background:new 0 0 22 22;" xml:space="preserve">
                <g>
                  <path class="checks" d="M9.6,15.6l-3.2-3.1c-0.1-0.1-0.2-0.3-0.2-0.4c0-0.2,0-0.3,0.2-0.5c0.1-0.1,0.3-0.2,0.4-0.2s0.3,0.1,0.4,0.2l2.7,2.6l4.8-7.7c0.1-0.1,0.2-0.2,0.4-0.2c0.2,0,0.3,0,0.5,0.1c0.1,0.1,0.2,0.2,0.3,0.4c0,0.2,0,0.3-0.1,0.5l-5.2,8.4c0,0,0,0.1,0,0.1c0,0,0,0-0.1,0.1l0,0c-0.1,0-0.1,0.1-0.2,0.1c0,0,0,0-0.1,0c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1-0.1C9.7,15.7,9.7,15.7,9.6,15.6C9.6,15.7,9.6,15.7,9.6,15.6C9.6,15.6,9.6,15.6,9.6,15.6z" />
                </g>
              </svg>
              <span>Нажимая на кнопку, вы даете согласие на обработку персональных данных и соглашаетесь с <a href="/politika-konfidentsialnosti/"> Политикой Конфиденциальности</a></span>
            </label>
          </div>
          <button class="btn btn--red">Отправить</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade modal-callback" id="modal-write-to-us" tabindex="-1" role="dialog" aria-labelledby="modal-write-to-us" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="modal-close" data-dismiss="modal" aria-label="Close">
          <svg class="icon">
            <use xlink:href="#icon-close"></use>
          </svg>
        </div>
        <form action="" method="post" class="send-form">
          <input type="hidden" name="act" value="sendWriteToUs">
          <h5 class="h-title mb-1">Написать нам</h5>
          <div class="h-title-desc mb-3">Если у вас появились вопросы или предложения, заполните форму
          </div>
          <div class="lk-form-block mb-3">
            <label for="name">
              <input class="form-input mb-0" type="text" id="name" name="name" placeholder="Ваше имя" />
              <span class="placeholder">Ваше имя</span>
            </label>
          </div>
          <div class="lk-form-block mb-3">
            <label for="phone">
              <input class="form-input mb-0" type="tel" id="phone" name="phone" placeholder="Номер телефона" />
              <span class="placeholder">Номер телефона</span>
            </label>
          </div>
          <div class="lk-form-block  mb-3">
            <label for="text">
              <textarea name="text" id="text"></textarea>
              <span class="placeholder">Текст обращения</span>
            </label>
          </div>
          <div class="politic">
            <label class="fake-check" for="politics_2">
              <input type="checkbox" id="politics_2" checked required/>
              <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 22 22" style="enable-background:new 0 0 22 22;" xml:space="preserve">
                <g>
                  <path class="checks" d="M9.6,15.6l-3.2-3.1c-0.1-0.1-0.2-0.3-0.2-0.4c0-0.2,0-0.3,0.2-0.5c0.1-0.1,0.3-0.2,0.4-0.2s0.3,0.1,0.4,0.2
                  l2.7,2.6l4.8-7.7c0.1-0.1,0.2-0.2,0.4-0.2c0.2,0,0.3,0,0.5,0.1c0.1,0.1,0.2,0.2,0.3,0.4c0,0.2,0,0.3-0.1,0.5l-5.2,8.4
                  c0,0,0,0.1,0,0.1c0,0,0,0-0.1,0.1l0,0c-0.1,0-0.1,0.1-0.2,0.1c0,0,0,0-0.1,0c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1-0.1
                  C9.7,15.7,9.7,15.7,9.6,15.6C9.6,15.7,9.6,15.7,9.6,15.6C9.6,15.6,9.6,15.6,9.6,15.6z" />
                </g>
              </svg>
              <span>Нажимая на кнопку, вы даете согласие на обработку персональных данных и
                соглашаетесь с <a href=""> Политикой Конфиденциальности</a></span>
            </label>
          </div>
          <button class="btn btn--red">Отправить</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="menu-modal">
  <div class="menu-modal__body">
    <div class="menu-modal__header">
      <div class="menu-close">
        <svg class="icon">
          <use xlink:href="#icon-close"></use>
        </svg><span>Закрыть</span>
      </div>
    </div>
    <div class="menu-modal__container">
      <?$APPLICATION->IncludeComponent(
      	"bitrix:menu",
      	"modal_menu",
      	array(
      		"ALLOW_MULTI_SELECT" => "N",
      		"CHILD_MENU_TYPE" => "left",
      		"DELAY" => "N",
      		"MAX_LEVEL" => "1",
      		"MENU_CACHE_GET_VARS" => array(
      		),
      		"MENU_CACHE_TIME" => "3600",
      		"MENU_CACHE_TYPE" => "A",
      		"MENU_CACHE_USE_GROUPS" => "Y",
      		"ROOT_MENU_TYPE" => "modal",
      		"USE_EXT" => "N",
      		"COMPONENT_TEMPLATE" => "modal_menu"
      	),
      	false
      );?>
      <ul class="menu-modal__menu">
        <li class="menu-modal__item">
          <div class="mb-4">
            <a class="btn" href="/lk/client_order">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px">
                <path fill-rule="evenodd" fill="rgb(231, 117, 97)" d="M9.000,-0.000 C13.971,-0.000 18.000,4.029 18.000,9.000 C18.000,13.971 13.971,18.000 9.000,18.000 C4.029,18.000 -0.000,13.971 -0.000,9.000 C-0.000,4.029 4.029,-0.000 9.000,-0.000 Z" />
                <path fill-rule="evenodd" fill="rgb(255,255,255)" d="M5.000,8.000 L13.000,8.000 L13.000,10.000 L5.000,10.000 L5.000,8.000 Z" />
                <path fill-rule="evenodd" fill="rgb(255,255,255)" d="M8.000,5.000 L10.000,5.000 L10.000,13.000 L8.000,13.000 L8.000,5.000 Z" />
              </svg>
              Разместить заказ</a></div>
        </li>
        <li class="menu-modal__item">
          <div class="mb-4">
            <a class="btn" href="/lk/company_card">
              <svg width="14px" version="1.1" id="Layer_35" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 368 464" style="enable-background:new 0 0 368 464;" xml:space="preserve">
                <style type="text/css">
                  .st0 {
                    fill: #E77561;
                  }
                </style>
                <g>
                  <path class="st0" d="M296,160H72v64c0,61.8,50.2,112,112,112s112-50.2,112-112V160z" />
                  <path class="st0" d="M305.1,62.2l-36.7,24.4c-1.4,0.9-2.9,1.3-4.4,1.3c-2.1,0-4.1-0.8-5.7-2.3l-16-16c-2.7-2.7-3.1-6.9-1-10.1l25.9-38.9c-13.4-9.1-28.7-15.5-45.1-18.6l-14.3,71.4c-0.7,3.7-4,6.4-7.8,6.4h-32c-3.8,0-7.1-2.7-7.8-6.4L145.9,2.1c-14.2,2.7-27.5,7.9-39.5,15.1l28.2,42.4c2.1,3.2,1.7,7.4-1,10.1l-16,16c-1.5,1.6-3.6,2.4-5.7,2.4c-1.5,0-3.1-0.4-4.4-1.3L65.2,58.4C55.7,74.3,49.7,92.5,48.4,112h271.2C318.4,94,313.4,77.2,305.1,62.2z" />
                  <path class="st0" d="M32,128h304v16H32V128z" />
                  <path class="st0" d="M312,224c0,5-0.4,9.9-0.9,14.8c9.8-3,16.9-12.1,16.9-22.8c0-10.4-6.7-19.2-16-22.5V224z" />
                  <path class="st0" d="M117.7,63L93.2,26.3c-7,5.6-13.4,12-19,19L111,69.7L117.7,63z" />
                  <path class="st0" d="M265,69.7l31.4-20.9c-4.9-6.6-10.4-12.7-16.5-18.1L258.3,63L265,69.7z" />
                  <path class="st0" d="M193.4,64l12.7-63.7C204.1,0.2,202.1,0,200,0h-32c-2.1,0-4.1,0.2-6.2,0.3L174.6,64H193.4z" />
                  <path class="st0" d="M56,352H40c-22.1,0-40,17.9-40,40v72h24v-40c0-4.4,3.6-8,8-8h24V352z" />
                  <path class="st0" d="M40,432h288v32H40V432z" />
                  <path class="st0" d="M40,216c0,10.8,7.2,19.8,16.9,22.8C56.4,233.9,56,229,56,224v-30.5C46.7,196.8,40,205.6,40,216L40,216z" />
                  <path class="st0" d="M280,352h16v64h-16V352z" />
                  <path class="st0" d="M328,352h-16v64h24c4.4,0,8,3.6,8,8v40h24v-72C368,369.9,350.1,352,328,352z" />
                  <path class="st0" d="M128.6,352H104v64h160v-64h-24.6c-3.9,27.1-27.2,48-55.4,48S132.5,379.1,128.6,352L128.6,352z" />
                  <path class="st0" d="M72,352h16v64H72V352z" />
                  <path class="st0" d="M223.2,352h-78.4c3.7,18.2,19.9,32,39.2,32S219.5,370.2,223.2,352z" />
                </g>
              </svg>
              Стать исполнителем</a></div>
        </li>
      </ul>
    </div>
    <div class="menu-modal__footer">
      <div class="mb-4"><a class="menu-modal__email" href="">email@company.com</a></div>
      <div><a class="btn" href="#" data-toggle="modal" data-target="#modal-write-to-us">Написать нам</a></div>
    </div>
  </div>
</div>

<div class="modal fade show" id="modalLogin" tabindex="-1" role="dialog" aria-hidden="true">
  <?$APPLICATION->IncludeComponent(
  	"bitrix:system.auth.authorize",
  	"",
  	Array()
  );?>
</div>

<div class="modal fade" id="modalSignUp" tabindex="-1" role="dialog" aria-hidden="true">
  <?$APPLICATION->IncludeComponent(
   	"bitrix:main.register",
   	"roobic",
   	Array(
   		"AUTH" => "Y",
   		"REQUIRED_FIELDS" => array("NAME"),
   		"SET_TITLE" => "N",
   		"SHOW_FIELDS" => array("NAME","PERSONAL_ICQ"),
   		"SUCCESS_PAGE" => "/lk/",
   		"USER_PROPERTY" => array("UF_PASSWORD"),
   		"USER_PROPERTY_NAME" => "",
   		"USE_BACKURL" => "Y"
   	)
  );?>
</div>
