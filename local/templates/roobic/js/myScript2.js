function setCookie(cookieName, id_el, idHeader) {
    var cntHeader = $('#' + idHeader).text();
    if ($.cookie(cookieName)) {
        comparison = $.cookie(cookieName);
        ar_cookie = comparison.split('-');
        // console.log(ar_cookie);
        if ($.inArray(id_el, ar_cookie) == -1) { // добавление в избранное
            // console.log('add');
            new_cookie = comparison + '-' + id_el;
            cntHeader++;
        } else { // удаление из избранного
            // console.log('del');
            new_cookie = '';
            cntHeader--;
            $.each(ar_cookie, function (key, val) {
                if (val != id_el) {
                    if (new_cookie == '') new_cookie = val;
                    else new_cookie = new_cookie + '-' + val;
                }
            });
        }
    } else {
        new_cookie = id_el;
        if (!cntHeader) cntHeader = 1;
        else cntHeader++;
    }
    // console.log(new_cookie);
    if (new_cookie != '')
        $.cookie(cookieName, new_cookie, { expires: 30, path: '/' });
    else
        $.removeCookie(cookieName, { path: '/' });

    $('#' + idHeader).text(cntHeader); // обновим в шапке
}

$(document).ready(function () {

    $('.sorting-btn').on('click', function () {
        $('.sorting-block').slideToggle();
    });

    // показ фильтра на моб
    $('.filterShow').on('click', function () {
        $('.filter-form').slideToggle();
    });

    // очистка формы
    $('button[type=reset]').on('click', function () {
        url = $(this).attr('data-url');
        location.href = url;
    });

    // удаление из сравнения
    $('.compare-delete').on('click', function () {
        location.reload();
    });

    $('#compare-show').on('click', function () {
        if ($(this).is(':checked')) {
            for (var i = 1; i < 15; i++) {
                var show_dif = true;
                var j = 0;
                $('.tr_' + i).each(function () { // переберем строки
                    td_text = $.trim($(this).text());
                    // console.log('tr_'+i+': '+td_text);
                    if (j > 0)
                        if (td_text != td_text_old) show_dif = false;
                    td_text_old = td_text; j++;
                });
                if (show_dif) $('.tr_' + i).parent().parent().fadeOut();
            }
        }
        else
            $('.compare-cheracteristics').fadeIn();
    });

    // фильтр в избранном
    $('.chooseFav').on('click', 'a', function () {
        blockFav = $(this).attr('data-id');
        $(this).addClass('filter-item--active').siblings().removeClass('filter-item--active');
        $('.' + blockFav).slideDown().siblings().slideUp();
    });

    // фильтр по заказам
    $('.order__filter input, .order__filter select, .order__tags input').change(function () {
        if ($(this).hasClass('changeUrl'))
            urlValue = $(this).attr('data-url') + $(this).val() + '/';
        else
            urlValue = $(this).attr('data-url');

        if (urlValue)
            location.href = urlValue;
        else
            $(this).closest('form').submit();
    });

    // сортировка
    $('#sorting').change(function () {
        sort = $(this).val();
        location.href = '?sort=' + sort;
    });

    // добавление в сравнение / избранное
    $(document).on('click', '.js-add-compare', function () {
        id_el = $(this).attr('data-id');
        cookie_name = $(this).attr('data-cookie');
        setCookie(cookie_name, id_el, 'compHeader');
        parent_el = $(this).parent();
        if (parent_el.hasClass("control--favorites--active")) {
            parent_el.removeClass("control--favorites--active");
            parent_el.find(".control__main").text("Добавить в Сравнение");
        } else {
            parent_el.addClass("control--favorites--active");
            parent_el.find(".control__main").text("Удалить из Сравнения");
        }
    });
    $(document).on('click', '.js-add-favorites', function () {
        id_el = $(this).attr('data-id');
        cookie_name = $(this).attr('data-cookie');
        setCookie(cookie_name, id_el, 'favHeader');
        parent_el = $(this).parent();
        if (parent_el.hasClass("control--favorites--active")) {
            parent_el.removeClass("control--favorites--active");
            parent_el.find(".control__main").text("Добавить в Избранное");
        } else {
            parent_el.addClass("control--favorites--active");
            parent_el.find(".control__main").text("Удалить из Избранного");
        }
    });

    $(document).on('change', 'select.select_service', function () {
        mainSectionID = $(this).val();
        if (mainSectionID) {
            // console.log(mainSectionID);
            var block_service = $(this).parents('.clone_block_service');
            block_service.find('.block_material optgroup, .block_material option').prop('disabled', true);
            block_service.find('.block_material optgroup[data-main=' + mainSectionID + '], .block_material option[data-main=' + mainSectionID + ']').prop('disabled', false);

            block_service.find('.block_material select').selectpicker('destroy').selectpicker();
            block_service.find('.add-item-material').attr('data-main', mainSectionID);
        }
    });

    // показ остальных элементов
    $(".show-all-completed-work").on("click", function (event) {
        event.preventDefault();
        $(this).toggleClass("openeds");

        if ($(this).hasClass("openeds")) {
            $(this).parent().find(".object").addClass("show-block");
            $(this).find("span").text("Свернуть");
        } else {
            $(this).parent().find(".object").removeClass("show-block");
            $(this).find("span").text("Показать еще");
        }
    });

    // изменение при потери фокуса
    $('.change_focusout').focusout(function () {
        valEl = $(this).val();
        nameEl = $(this).attr('name');
        $.ajax({
            url: '/local/ajax/changeFields.php',
            type: 'post',
            data: { action: 'profile_field', valEl: valEl, nameEl: nameEl },
            success: function (result) { // console.log(result);
                // form.html(result);
            }
        });
    });

    // const template = (file) => {
    //     var url = URL.createObjectURL(file);
    //     return `
    //     <div class="list list__added">
    //         <label class="label-file has-img">
    //             <img src="${url}" alt="">
    //         </label>
    //         <span class="delete-file">х</span>
    //     </div>`
    // }
    //
    // $(document).on('change', 'input[type=file]', function (event) {
    //
    //     for (let index = 0; index <= 2; index++) {
    //         $(".download-photo").find(".list:not(.list__added)").hide();
    //         const element = event.target.files[index];
    //         $(".download-photo").append(template(element))
    //     }
    // });
    //

    // запоминаем область перетаскивания файла
    // const dropFileZone = document.querySelector(".download-photo");
    // // запоминаем кнопку добавления файла
    // const uploadInput = document.querySelector(".form-upload__input");
    // // запоминаем кнопку отправки
    // const submitButton = document.querySelector('.form-upload__submit');
    // // записываем в переменную адрес, куда отправятся файлы после загрузки
    // const uploadUrl = "/unicorns";
    //
    // // добавляем обработчики событий "dragover" и "drop" для документа
    // ["dragover", "drop"].forEach(function (event) {
    //     // блокируем стандартное поведение браузера для события и возвращаем false
    //     document.addEventListener(event, function (evt) {
    //         evt.preventDefault();
    //         return false;
    //     });
    // });
    //
    // // добавляем обработчик события для входа в зону перетаскивания файла"
    // dropFileZone.addEventListener("dragenter", function () {
    //     // добавляем класс стиля, красим форму
    //     dropFileZone.classList.add("_active");
    // });
    //
    // // добавляем обработчик события для выхода из зоны перетаскивания файла"
    // dropFileZone.addEventListener("dragleave", function () {
    //     // возвращаем цвет неактивной формы"
    //     dropFileZone.classList.remove("_active");
    // });
    //
    // // добавляем обработчик события "drop" для зоны перетаскивания"
    // dropFileZone.addEventListener("drop", function () {
    //     // удаляем класс активности при сбросе файла
    //     dropFileZone.classList.remove("_active");
    //
    //     uploadInput.files = event.dataTransfer.files;
    //
    //
    //     for (let index = 0; index <= 2; index++) {
    //         $(".download-photo").find(".list:not(.list__added)").hide();
    //         const element = uploadInput.files[index];
    //         $(".download-photo").append(template(element))
    //     }
    //     return false;
    // });

    // добавляем обработчик события для файлов, добавленных кнопкой"
    // uploadInput.addEventListener("change", (event) => {
    //     // получаем первый файл в списке
    //     const file = uploadInput.files?.[0];
    //     // проверяем, что файл есть
    //     if (file) {
    //         // готовим файл к отправке
    //         uploadInput.files = event.dataTransfer.files;
    //     }
    // });

    // // добавляем обработчик события "click" для кнопки отправки
    // submitButton.addEventListener("click", function (event) {
    //     // блокируем стандартное поведение кнопки (отправку формы)
    //     event.preventDefault();
    //     // вызываем функцию для отправки файла
    //     processingUploadFile();
    // });

    // функция для обработки загрузки файла
    function processingUploadFile(file) {
        // проверяем, что файл был отправлен
        if (file) {
            // создаём объект для отправки данных формы
            const dropZoneData = new FormData();
            // создаём объект для отправки запроса на сервер
            const xhr = new XMLHttpRequest();

            // добавляем файл из формы в объект FormData
            dropZoneData.append("file", file);

            // открываем соединение с сервером
            xhr.open("POST", uploadUrl, true);

            // отправляем данные на сервер
            xhr.send(dropZoneData);

            // устанавливаем обработчик события onload для выполнения действий после завершения загрузки
            xhr.onload = function () {
                // проверяем статус ответа сервера
                if (xhr.status == 200) {
                    // сообщаем об успехе в консоли"
                    console.log("Всё загружено");
                } else {
                    // соообщаем об ошибке в консоли"
                    console.log("Ошибка загрузки");
                }
                // скрываем элемент
                HTMLElement.style.display = "none";
            };
        }
    }








    // отправка форм
    $('.send-form').on("submit", function () {
        event.preventDefault();
        var form = $(this);
        var fields = form.serialize();
        $.ajax({
            url: '/local/ajax/sendForm.php',
            type: 'post',
            data: fields,
            success: function (result) { // console.log(result);
                form.html(result);
            }
        });
    });

    // добавления отзыва
    $('#formSendReview').submit(function (event) {
        event.preventDefault();
        name = $(this).find('input[name=review-name]').val();
        fname = $(this).find('input[name=review-surname]').val();
        expertise = $(this).find('select[name=review-expertise]').val();
        deadline = $(this).find('select[name=review-deadline]').val();
        service = $(this).find('select[name=review-service]').val();
        price = $(this).find('select[name=review-price]').val();
        quality = $(this).find('select[name=review-quality]').val();
        text = $(this).find('textarea[name=review-text]').val();
        email = $(this).find('input[name=review-email]').val();
        company = $('#company').val();

        $.post("/local/ajax/sendForm.php", {
            act: 'sendReview',
            name: name,
            fname: fname,
            expertise: expertise,
            deadline: deadline,
            service: service,
            price: price,
            quality: quality,
            text: text,
            email: email,
            company: company,
        }, function (data) {
            $('#formSendReview').html(data);
        }
        );
    });

    // регистрация
    $('.regForm').submit(function (event) {
        // event.preventDefault();
        phone = $(this).find('.regPhone').val()
        $(this).find('.regLogin').val(phone);
    });

    // определим нужную группу
    $('.defGroup').click(function () {
        group = $(this).attr('data-group');
        $('.regGroup').val(group);
    });

    // клонирование
    // $('.add-item, .order__new-btn').on("click", function() {
    //   event.preventDefault();
    //   data_block = $(this).attr('data-block');
    //   var data_block_clone = $('.clone_'+data_block).clone(); // клонируем нужный элемент
    //   $('.'+data_block).children('.insert_after').after(data_block_clone); // вставляем после
    //   $('.'+data_block+' .clone_'+data_block+' select').selectpicker(); // делаем selectpicker
    //   $('.'+data_block+' .clone_'+data_block).removeClass('clone_'+data_block); // удаляем класс клона
    // });

    // тапы в избранном
    $('.leads_tabs a').on("click", function () {

        data_id = $(this).attr('data-id');
        if (data_id == 'all') {
            $('.object-list .tab-block').show();
        } else {
            $('.object-list .tab-block').hide();
            $('.object-list .tab-block.' + data_id).show();
        }

        $('.leads_tabs a').removeClass('active');
        $(this).addClass('active');

    });

    // прочитано уведомление
    $('.notifications__item.active').on("click", function () {
        dataID = $(this).attr('data-id');
        $(this).removeClass('active');
        $.ajax({
            url: '/local/ajax/changeFields.php',
            type: 'post',
            data: { action: 'notif_read', dataID: dataID },
            success: function (result) {
                // console.log(result);
            }
        });
    });
    //  прочитаны все уведомления
    $('.readAllNotif').on("click", function () {
        event.preventDefault();
        $('.notifications__item.active').each(function () {
            dataID = $(this).attr('data-id');
            $(this).removeClass('active');
            $.ajax({
                url: '/local/ajax/changeFields.php',
                type: 'post',
                data: { action: 'notif_read', dataID: dataID },
                success: function (result) {
                    // console.log(result);
                }
            });
        });
    });

    // изменение фото пользователя
    $('form.photo_user input[type=file]').on("change", function () {
        $('form.photo_user').submit();
    });

    // отправка отклика
    $('.sendResponse').on("click", function () {
        event.preventDefault();

        var orderID = $(this).attr('data-order-id');
        var userID = $(this).attr('data-user-id');

        $.ajax({
            url: '/local/ajax/sendForm.php',
            type: 'post',
            data: { act: 'sendResponse', orderID: orderID, userID: userID },
            success: function (result) {
                // console.log(result);
                $('.leave-esponse').html(result);
            }
        });
    });

    //
    $('.form-question a').on("click", function () {
        event.preventDefault();
        val = $(this).attr('data-val');
        $('input[name=work_show]').val(val);
        $('.form-question a').removeClass('active');
        $(this).addClass('active');
    });

    // слайдеры детальной дома
    var plan = new Swiper('.plan-sliders', {
        spaceBetween: 30,
        slidesPerView: 2,
        breakpoints: {
            320: {
                slidesPerView: 1,
                spaceBetween: 10
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 10
            }
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
    var galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 20,
        slidesPerView: 4,
        freeMode: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            320: {
                slidesPerView: 1,
                spaceBetween: 10
            },
            480: {
                slidesPerView: 2,
                spaceBetween: 10
            },
            768: {
                slidesPerView: 4,
                spaceBetween: 20
            },
            992: {
                slidesPerView: 3,
                spaceBetween: 20
            },
            1280: {
                slidesPerView: 4,
                spaceBetween: 20
            }
        }
    });
    var galleryTop = new Swiper('.gallery-top', {
        spaceBetween: 10,
        pagination: {
            el: '.swiper-pagination',
            type: 'fraction'
        },
        thumbs: {
            swiper: galleryThumbs
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

    // слайдер детальной заказа
    $('.order-slider ').each(function () {
        var swiper2 = new Swiper(this, {
            slidesPerView: 5,
            spaceBetween: 22,
            centeredSlides: false,
            pagination: {
                el: '.swiper-pagination',
                type: 'fraction'
            },
            navigation: {
                nextEl: $(this).closest(this).find('.js-slider-img.swiper-button-next'),
                prevEl: $(this).closest(this).find('.js-slider-img.swiper-button-prev')
            },
            breakpoints: {
                320: {
                    slidesPerView: 1,
                    spaceBetween: 10
                },
                480: {
                    slidesPerView: 2,
                    spaceBetween: 10
                },
                768: {
                    slidesPerView: 4,
                    spaceBetween: 20
                },
                992: {
                    slidesPerView: 4,
                    spaceBetween: 20
                },
                1280: {
                    slidesPerView: 5,
                    spaceBetween: 20
                }
            }
        });
    });
});



$(document).on("click", ".delete-file", function () { // удаление файла
    $(this).closest(".list").remove();
    let count = $(".download-photo").find(".list__added");
    if (count.length == 0) {
        $(".download-photo").find(".list:not(.list__added)").show()
    }
});
