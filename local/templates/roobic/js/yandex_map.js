function init() {
  var placemarkCity,
    mapCity = new ymaps.Map('yandex_map', { // адрес город
      center: [55.753994, 37.622093],
      zoom: 9
    }, {
      searchControlProvider: 'yandex#search'
    });

  // Слушаем клик на карте.
  mapCity.events.add('click', function(e) {
    var coords = e.get('coords');

    if (placemarkCity) { // Если метка уже создана – просто передвигаем ее
      placemarkCity.geometry.setCoordinates(coords);
    } else { // Если нет – создаем
      placemarkCity = createPlacemark(coords);
      mapCity.geoObjects.add(placemarkCity);
      // Слушаем событие окончания перетаскивания на метке
      placemarkCity.events.add('dragend', function() {
        getAddressCity(placemarkCity.geometry.getCoordinates());
      });
    }
    getAddressCity(coords);
  });

  // Создание метки
  function createPlacemark(coords) {
    return new ymaps.Placemark(coords, {
      iconCaption: 'поиск...'
    }, {
      preset: 'islands#greenDotIconWithCaption',
      draggable: true
    });
  }

  // Определяем адрес по координатам (обратное геокодирование).
  function getAddressCity(coords) {
    placemarkCity.properties.set('iconCaption', 'поиск...');
    ymaps.geocode(coords).then(function(res) {
      var firstGeoObject = res.geoObjects.get(0);

      placemarkCity.properties.set({
        // Формируем строку с данными об объекте.
        iconCaption: [
          // Название населенного пункта или вышестоящее административно-территориальное образование.
          firstGeoObject.getLocalities().length ? firstGeoObject.getLocalities() : firstGeoObject.getAdministrativeAreas(),
          // Получаем путь до топонима, если метод вернул null, запрашиваем наименование здания.
          firstGeoObject.getThoroughfare() || firstGeoObject.getPremise()
        ].filter(Boolean).join(', '),
        // В качестве контента балуна задаем строку с адресом объекта.
        balloonContent: firstGeoObject.getAddressLine()
      });
      $('#input_address').val(firstGeoObject.getAddressLine()).trigger('click');
    });
  }

  var input_address = $('#input_address').val();

  if (input_address != '') { // поставим метку
    address = input_address;
    // console.log(address);
    $('#yandex_map .ymaps-2-1-79-searchbox-input__input').val(address);
    $('#yandex_map .ymaps-2-1-79-searchbox-button-text').trigger('click');
  }
}

$(document).ready(function(){

  ymaps.ready(init);

  // метка на яндекс картах
  $('#input_address').on('change keypress',function(){
    address = $(this).val();
    // console.log(address);
    $('#yandex_map .ymaps-2-1-79-searchbox-input__input').val(address);
    $('#yandex_map .ymaps-2-1-79-searchbox-button-text').trigger('click');
  });

});
