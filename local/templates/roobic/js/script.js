"use strict";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

$(document).ready(function () {

    jQuery(function ($) {
        $(document).mouseup(function (e) { // событие клика по веб-документу
            var div = $(".login-modal"); // тут указываем ID элемента
            if (!div.is(e.target) // если клик был не по нашему блоку
                && div.has(e.target).length === 0) { // и не по его дочерним элементам
                $("body").removeClass("active-login");
            }
        });
    });

    $('.form-input').each(function () {
        if ($(this).val() == '') {
            $(this).parent('.form-block label, .lk-form-block label').removeClass('not-empty label-up');
        }
        else {
            $(this).parent('.form-block label, .lk-form-block label').addClass('not-empty label-up');
        }
    });
    // $(".js-add-materials").on("click", function (e) {
    //     e.preventDefault();
    //     $('.block_material').first().clone().addClass('clone').appendTo("#block_main");
    //     $('.block_material.clone select').selectpicker();
    // });

    $('body').on('click', '.add-item', function () { // , .order__new-btn
        event.preventDefault();
        var data_block = $(this).attr('data-block');
        var data_block_clone = $('.hide .clone_' + data_block).clone();
        $('.' + data_block).children('.insert_after').after(data_block_clone);
        if (innerWidth > 767) {
            $('.' + data_block + ' .clone_' + data_block + ' select').selectpicker();
        }


        $('.' + data_block + ' .clone_' + data_block).removeClass('clone_' + data_block);
    });

    $('body').on('click', '.add-item-material', function () {
        event.preventDefault();
        var sid = $(this).attr('data-sid');
        var mainSectionID = $(this).attr('data-main');
        var data_block_clone = $('.hide .clone_block_material').clone();
        var block_material = $(this).parents('.block_material');
        block_material.children('.insert_before').before(data_block_clone);

        // отобразим нудные элементы для показа
        if (mainSectionID) {
            block_material.find('optgroup, option').prop('disabled', true);
            block_material.find('optgroup[data-main=' + mainSectionID + '], option[data-main=' + mainSectionID + ']').prop('disabled', false);
        }

        if (innerWidth > 767) {
            $('.block_material .clone_block_material select').selectpicker();
        }




        // проставим id
        block_material.find('.select_material').attr('name', 'material_' + sid + '[]').removeClass('select_material');
        block_material.find('.input_price').attr('name', 'material_price_' + sid + '[]').removeClass('input_price');
    });

    $('body').on('click', '.add-item_link', function () {
        event.preventDefault();
        var data_block_clone = $('.clone_block_plan').clone();
        $('.insert_after').after(data_block_clone);
        $('.house_plan .clone_block_plan').removeClass('clone_block_plan');
    });

    $('body').on('click', '.add-service', function () { // добавление услуги
        event.preventDefault();
        var cnt_service = $('.form-main-block.block_service .clone_block_service').length;
        var data_block_clone = $('.hide .clone_block_service').clone();
        var block_service = $(this).parents('.block_service');
        block_service.children('.form-bottom').before(data_block_clone);
        $('.block_service .clone_block_service .form-block-service select').selectpicker();
        // проставим id
        block_service.find('.select_material').attr('name', 'material_' + cnt_service + '[]').removeClass('select_material');
        block_service.find('.input_price').attr('name', 'material_price_' + cnt_service + '[]').removeClass('input_price');
        block_service.find('.a_material').attr('data-sid', cnt_service).removeClass('a_material');
    });

    $('body').on('click', '.block_remove', function () {
        event.preventDefault();
        $(this).closest('.row').remove();
    });

    $('body').on('click', '.service_remove', function () {
        event.preventDefault();
        $(this).parents('.clone_block_service').remove();
    });
    $('body').on('click', '.staff_remove', function () {
        event.preventDefault();
        $(this).parents('.staff__container').remove();
    });

    // $(".edit-input").on("click", function (event) {
    //   event.preventDefault();
    //   $(this).parent('.lk-form-block').find('input').prop('disabled', false);
    // });

    $(document).on("click", ".form-block label, .lk-form-block label", function (event) {
        // event.preventDefault();
        $(this).addClass('label-up');
    });

    $(".edit-password").on("click", function (event) {
        event.preventDefault();
        if ($(this).hasClass('on')) {
            $('#changePassword').submit();
        }
        $(".password").addClass('on');
        $(this).addClass('on');
    });

    $('.form-input').on('keyup', function () {
        var $this = $(this),
            val = $this.val();

        if (val.length >= 1) {
            $(this).parent('label').addClass('not-empty');
        } else {
            $(this).parent('label').removeClass('not-empty');
        }
    });

    $(".notifications__item").on("click", function (event) {
        event.preventDefault();
        $(this).toggleClass('on');
    });

    $(".technologies__item input").change(function () {
        $(this).parent('.technologies__item').addClass('on');
    });

    var target = $('footer');
    var targetPos = target.offset().top;
    var winHeight = $(window).height();
    var scrollToElem = targetPos - winHeight;
    $(window).scroll(function () {
        var winScrollTop = $(this).scrollTop();
        if (winScrollTop > scrollToElem) {
            $('.btn-filter').hide();
        } else {
            $('.btn-filter').show();
        }
    });

    $("input[type=tel]").inputmask({
        mask: "+7 (999) 999-99-99",
        placeholder: "",
        showMaskOnHover: false
    });

    $(".js-number").on('keyup', function () {
        $(this).val($(this).val().replace(/[A-Za-zА-Яа-яЁё]/, ''));
    });

    $("select").on("change", function () {
        $(this).closest("label.select").next("label.select").removeClass("disabled");
    });

    $("#option-1").on("change", function () {
        $("label[for=option-1]").removeClass("error");
        // отобразим нужные подкатегории
        var mainSectionID = $(this).val();
        $("#option-2 option").prop('disabled', true);
        $("#option-2 option[data-main=" + mainSectionID + "]").prop('disabled', false);
        if (innerWidth > 767) $("#option-2").selectpicker('destroy').selectpicker();
    });

    $("#option-2").on("change", function () {
        $(".js-number").prop('disabled', false);
    });

    $("#option-2").on("click", function () {
        if ($(this).filter("disabled")) {
            $("label[for=option-1]").addClass("error");
        }
    });

    if (innerWidth > 767) {
        $('.js-select__letter').selectpicker();
        $('.form-block select:not(.not_selectpicker), .js-select__services').selectpicker();

        $(".js-select-letter").on("click", function () {
            $(".js-select__letter").selectpicker('val', $('input[name=region]:checked').attr("id"));
            $("#modal-district").modal("hide");
        });

        $('.js-select__letter').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
            $("#modal-district").find("input[id=" + $(this).val() + "]").attr('checked', 'checked');
            return false;
        });

        $(".js-select-services").on("click", function () {
            var list = [];
            $('input[name=services]:checked,input[name=type]:checked').each(function () {
                list.push($(this).attr('id'));
            });
            $(".js-select__services").selectpicker('val', list);
            $("#modal-services").modal("hide");
        });

        $('.js-select__services').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
            var arr = $(this).val();
            arr.map(function (value) {
                $("#modal-services").find("input[id=" + value + "]").attr('checked', 'checked');
            });
            return false;
        });
    }

    $("#modal-services").find("input[type=checkbox]").on("change", function () {
        if ($(this).closest(".labes").find(".labes-list").length == 1) {
            if ($(this).is(':checked')) {
                $(this).closest(".labes").find(".labes-list input").prop('checked', true);
            } else {
                $(this).closest(".labes").find(".labes-list input").prop('checked', false);
            }
        }
    });
    $(".js-arrow").on("click", function () {
        $(this).next(".labes-list").slideToggle();
        $(this).toggleClass("active");
    });
    $("form[name=js-calculate]").on("submit", function (event) {
        event.preventDefault();

        if ($(this).find("[name=option-1]").val() == null) {
            $("label[for=option-1]").addClass("error");
        } else {
            $("label[for=option-1]").removeClass("error");
        }

        if ($(this).find("[name=option-2]").val() == null) {
            $("label[for=option-2]").addClass("error");
        } else {
            $("label[for=option-2]").removeClass("error");
        }

        if (!$(".js-number").val().length) {
            $("label[for=number]").addClass("error");
        }

        if ($(".js-number").val().length) {
            // подсчет
            var option_2__val = $('#option-2').val();
            var itemPrice = $('#price-' + option_2__val).attr('data-price');
            var priceWork = itemPrice * $('.js-number').val();
            priceWork = String(priceWork).replace(/(\d)(?=(\d{3})+([^\d]|$))/g, '$1 ');
            $("#modal-calculate").find(".js-option-price").text(priceWork);
            $("#modal-calculate").find(".js-option-1").text($(this).find("[name=option-1] option:selected").text());
            $("#modal-calculate").find(".js-option-2").text($(this).find("[name=option-2] option:selected").text());
            $("#modal-calculate").find(".js-option-3").text($(this).find(".js-number").val());
            $("#modal-calculate").modal("show");
        }
    });
    $(".js-modal-callback").on("click", function (event) {
        event.preventDefault();
        $("#modal-calculate").modal("hide");
        setTimeout(function () {
            $("#modal-callback").modal("show");
        }, 500);
    });
    $(".js-open-video").on("click", function (event) {
        event.preventDefault();
        var src = $(this).data("src");
        var lnk = src.match(/=\w(.*)/)[0].slice(1);
        $('#video-youtube').attr("src", "https://www.youtube.com/embed/" + lnk + "?enablejsapi=1&rel=0&amp;amp;showinfo=0");
        setTimeout(function () {
            document.getElementById('video-youtube').contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', '*');
        }, 1000);
    });
    $(".js-video-close").on("click", function () {
        $("#modalVideo").modal("hide");
        $("#video-youtube").attr("src", "");
        document.getElementById('video-youtube').contentWindow.postMessage('{"event":"command","func":"stopVideo","args":""}', '*');
    });
    $(".js-scroll").on("click", function (event) {
        event.preventDefault();
        var el = $(this);
        var dest = el.attr("href");

        if (dest !== undefined && dest !== "") {
            $("html").animate({
                scrollTop: $(dest).offset().top
            }, 500);
        }
    });
    // $(".js-add-favorites").on("click", function () {
    //   if ($(".control--favorites").hasClass("control--favorites--active"))
    //   {
    //     $(this).closest(".control--favorites").removeClass("control--favorites--active");
    //     $(this).closest(".control--favorites").find(".control__main").text("Добавить в Избранное");
    //     // return false;
    //   }
    //   else
    //   {
    //     $(this).closest(".control--favorites").addClass("control--favorites--active");
    //     $(this).closest(".control--favorites").find(".control__main").text("Удалить из Избранного");
    //   }
    // });
    $(".js-control-share").on("click", function () {
        $(this).addClass("show-share");
    });
    $(document).mouseup(function (e) {
        var share = $(".js-control-share");

        if (share.has(e.target).length === 0) {
            share.removeClass("show-share");
        }
    });
    $(".js-copylink").on("click", function (event) {
        event.preventDefault();
        var hr = window.location.href;
        copyTextToClipboard(hr);
        return false;
    });

    function copyTextToClipboard(text) {
        function fallbackCopyTextToClipboard(text) {
            var textArea = document.createElement("textarea");
            textArea.value = text;
            document.body.appendChild(textArea);
            textArea.focus();
            textArea.select();

            try {
                var successful = document.execCommand('copy');
                var msg = successful ? 'successful' : 'unsuccessful'; // console.log('Fallback: Copying text command was ' + msg);
            } catch (err) {
                console.error('Fallback: Oops, unable to copy', err);
            }

            document.body.removeChild(textArea);
        }

        ;

        if (!navigator.clipboard) {
            fallbackCopyTextToClipboard(text);
            return;
        }

        navigator.clipboard.writeText(text).then(function () {// console.log('Async: Copying to clipboard was successful!');
        }, function (err) {
            console.error('Async: Could not copy text: ', err);
        });
    }

    $('.company-list').each(function () {
        var swiper = new Swiper(this, {
            slidesPerView: 'auto',
            spaceBetween: 30,
            fadeEffect: {
                crossFade: true
            },
            navigation: {
                nextEl: $(this).parent().find('.js-btns.swiper-button-next'),
                prevEl: $(this).parent().find('.js-btns.swiper-button-prev')
            }
        });
    });
    $(".js-open-search").on("click", function () {
        $(".search-form-pc").addClass("search-form--active");
    });

    $(".js-open-mobile-search").on("click", function () {
        $(".search-form-mobile").toggleClass("search-form--active");
    });



    $(".js-open-menu").on("click", function () {
        $("body").addClass("active-menu");
    });
    $(".js-open-login").on("click", function () {
        $("body").addClass("active-login");
    });
    $(".menu-close").on("click", function () {
        $("body").removeClass("active-menu");
    });
    $('.object-slider').each(function () {
        var swiper2 = new Swiper(this, {
            slidesPerView: 1,
            spaceBetween: 0,
            centeredSlides: true,
            pagination: {
                el: '.swiper-pagination',
                type: 'fraction'
            },
            navigation: {
                nextEl: $(this).closest(this).find('.js-slider-img.swiper-button-next'),
                prevEl: $(this).closest(this).find('.js-slider-img.swiper-button-prev')
            }
        });
    });
    $('.partners-list').each(function () {
        var swiper = new Swiper(this, {
            pagination: {
                el: '.swiper-pagination',
                type: 'fraction'
            },
            slidesPerView: 2,
            spaceBetween: 0,
            breakpoints: {
                640: {
                    slidesPerView: 2,
                    spaceBetween: 10
                },
                768: {
                    slidesPerView: 4,
                    spaceBetween: 10
                },
                1150: {
                    slidesPerView: 6,
                    spaceBetween: 30
                }
            },
            navigation: {
                nextEl: $(this).parent().find('.js-btns.swiper-button-next'),
                prevEl: $(this).parent().find('.js-btns.swiper-button-prev')
            }
        });
    });
    $(".js-alltext").on("click", function (event) {
        event.preventDefault();
        var th = $(this).closest(".body-text");
        th.find(".small-text").hide();
        th.find(".all-text").show();
    });
    $('.sertificate-slider').each(function () {
        var _ref;

        var swipersertificate = new Swiper(this, (_ref = {
            slidesPerView: 'auto',
            spaceBetween: 10
        }, _defineProperty(_ref, "slidesPerView", 2), _defineProperty(_ref, "breakpoints", {
            540: {
                slidesPerView: 2,
                spaceBetween: 10
            },
            640: {
                slidesPerView: 3,
                spaceBetween: 10
            },
            1000: {
                slidesPerView: 4,
                spaceBetween: 10
            },
            1150: {
                slidesPerView: 5,
                spaceBetween: 30
            }
        }), _defineProperty(_ref, "fadeEffect", {
            crossFade: true
        }), _defineProperty(_ref, "navigation", {
            nextEl: $(this).parent().find('.swiper-button-next'),
            prevEl: $(this).parent().find('.swiper-button-prev')
        }), _ref));
    });
    $(".services-arrow").on("click", function () {
        var th = $(this).closest(".services");
        th.addClass("opened");
        $(this).hide(300);
    });
    $(".specifications__more").click(function () {
        $('.specifications-block ul').addClass('on');
    });

    $(".specifications__more").on("click", function (event) {
        event.preventDefault();
        $(this).toggleClass("openeds");

        if ($(this).hasClass("openeds")) {
            $('.specifications-block ul').addClass('on');
            $(this).find("span").text("Свернуть");
        } else {
            $('.specifications-block ul').removeClass("on");
            $(this).find("span").text("Показать еще");
        }
    });

    if ($("#select").length) {
        var tabs = document.querySelector('#tabs');

        document.querySelector('#select').addEventListener('change', function () {
            tabs.querySelector('.active').classList.remove('active');
            tabs.querySelector(`#${this.value}`).classList.add('active');
        });
    }


    $('#select').on('change', function () {
        var $val = $('#select option:selected').data('img');
        $(this).css('background-image', 'url(' + $val + ')');
    });


    $(".js-services-show-all").on("click", function (event) {
        event.preventDefault();
        $(this).toggleClass("openeds");

        if ($(this).hasClass("openeds")) {
            $(".services-ls").addClass("show-services");
            $(this).find("span").text("Свернуть");
        } else {
            $(".services-ls").removeClass("show-services");
            $(this).find("span").text("Показать еще");
        }
    });
    $('.team-slider').each(function () {
        var swiper2 = new Swiper(this, {
            slidesPerView: 'auto',
            spaceBetween: 10,
            breakpoints: {
                640: {
                    slidesPerView: 2,
                    spaceBetween: 10
                },
                768: {
                    slidesPerView: 3,
                    spaceBetween: 10
                },
                1025: {
                    slidesPerView: 3,
                    spaceBetween: 30
                },
                1150: {
                    slidesPerView: 4,
                    spaceBetween: 30
                }
            },
            fadeEffect: {
                crossFade: true
            },
            navigation: {
                nextEl: $(this).parent().find('.swiper-button-next'),
                prevEl: $(this).parent().find('.swiper-button-prev')
            }
        });
    });

});
