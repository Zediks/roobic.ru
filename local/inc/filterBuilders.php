<?
// Получим районы
$arRegions = getElHL(2,[],[],['*']);
$prevLetter = '';
foreach ($arRegions as $key => $value) { // Группируем районы по первым буквам названий
	$firstLetter = mb_strtoupper(mb_substr($value['UF_NAME'], 0, 1));
	if ($firstLetter != $prevLetter) $prevLetter = $firstLetter;
	$arRegion[$firstLetter][] = [
		'NAME' => $value['UF_NAME'],
		'CODE' => $value['UF_XML_ID'],
	];
}

// получим услуги
$arServices = getElHL(3,[],[],['*']);
foreach ($arServices as $key => $value) { // распределим для формирования
	if($value['UF_ON_SERVICE'])
		$services[$value['UF_MAIN_SECTION']][$value['UF_SUBSECTION']][$value['UF_ON_SERVICE']]['ON'][] = $value;
	else
		$services[$value['UF_MAIN_SECTION']][$value['UF_SUBSECTION']][$value['ID']] = $value;
}
$arMainSection = getListProperty([],['USER_FIELD_ID' => 72]);
$arSubSection = getListProperty([],['USER_FIELD_ID' => 73]);

// получим цены
$property_enums = CIBlockPropertyEnum::GetList(['SORT'=>'ASC'], Array("IBLOCK_ID"=>2, "CODE"=>"PRICE"));
while($enum_fields = $property_enums->GetNext())
	$arPrice[] = $enum_fields;

// запишем в куки для фильтра
if ($_REQUEST['services']) {
  $strType = implode('|',$_REQUEST['services']);
  setcookie("build_services", $strType, time()+3600);
}
$buildServices = ($_REQUEST['services']) ? $_REQUEST['services'] : explode('|',$_COOKIE['build_services']);

if ($_REQUEST['price'])
	setcookie("build_price", $_REQUEST['price'], time()+3600);
$buildPrice = ($_REQUEST['price']) ? $_REQUEST['price'] : $_COOKIE['build_price'];

// очистка
if ($_REQUEST['reset'] == 'y') {
  setcookie('build_services', '', time() - 3600);
	unset($buildServices);
  setcookie('build_price', '', time() - 3600);
	unset($buildPrice);
}
?>

<div class="row">
  <div class="col-sm-12">
    <button class="btn btn--red filterShow">Фильтр</button>
  </div>
</div>

<form class="filter-form filter-form__builders" action="/stroiteli/" method="get">
  <div class="row">
    <!-- <div class="col-md-6 col-lg-3">
      <div class="form-cont">
        <label class="select" for="states"><a class="open-modal" href="" data-toggle="modal" data-target="#modal-district">Район</a>
          <select class="js-select__letter select__select" name="region" data-live-search="true" data-size="5" title="Выберите район">
            <?foreach ($arRegions as $value) {?>
              <option value="<?=$value['UF_XML_ID']?>" <?if($_REQUEST['region'] == $value['UF_XML_ID'])echo 'selected';?>><?=$value['UF_NAME']?></option>
            <?}?>
          </select>
        </label>
      </div>
    </div> -->
    <div class="col-md-6 col-lg-4">
      <div class="form-cont">
				<!-- <a class="open-modal" href="" data-toggle="modal" data-target="#modal-services">Услуги</a> -->
        <label class="select" for="states">
          <select class="js-select__services select__select" data-count-selected-text="Выбрано {0} услуг" data-selected-text-format="count &gt; 3" data-size="5" name="services[]" multiple="multiple" data-live-search="true" title="Выберите услугу">
            <?foreach ($arServices as $value) {?>
              <option value="<?=$value['UF_XML_ID']?>" <?if(in_array($value['UF_XML_ID'],$buildServices))echo 'selected';?>><?=$value['UF_NAME']?></option>
            <?}?>
          </select>
        </label>
      </div>
    </div>
    <div class="col-md-6 col-lg-4">
      <label class="select" for="price">
        <select class="js-select__letter select__select select__styler" name="price" id="price" title="Ценовой диапазон">
          <?foreach ($arPrice as $price) {?>
            <option value="<?=$price['ID']?>" <?if($buildPrice == $price['ID'])echo'selected'?>><?=$price['VALUE']?></option>
          <?}?>
        </select>
      </label>
    </div>
    <div class="col-md-6 col-lg-4">
			<div class="row">
				<div class="col-sm-6">
					<button type="reset" class="btn btn--reset" data-url="<?echo $APPLICATION->GetCurDir();?>?reset=y">Очистить фильтр</button>
				</div>
				<div class="col-sm-6">
					<button class="btn btn--red">Искать</button>
				</div>
			</div>
    </div>
  </div>
</form>

<div class="modal fade" id="modal-district" tabindex="-1" role="dialog" aria-labelledby="modal-district" aria-hidden="true">
  <div class="modal-dialog modal-dialog--modals" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="modal-close" data-dismiss="modal" aria-label="Close">
          <svg class="icon">
            <use xlink:href="#icon-close"></use>
          </svg>
        </div><strong>Районы МО</strong>
        <div class="district-list-columns">
          <div class="district-list-flex row">
            <div class="col-xl-3 col-lg-4 col-md-6">
              <?foreach ($arRegion as $letter => $region) { // dump($region);
                if ($letter == 'К' || $letter == 'О' || $letter == 'Ф') echo '</div><div class="col-xl-3 col-lg-4 col-md-6">';?>
                <div class="district-list">
                  <div class="district-list-letter"><?=$letter?></div>
                  <div class="district-list-list">
                    <?foreach ($region as $value) {?>
                      <label class="radio" for="<?=$value['CODE']?>">
                        <input type="radio" name="region" value="<?=$value['NAME']?>" id="<?=$value['CODE']?>" <?if ($_REQUEST['region'] == $value['CODE']) echo 'checked';?> />
                        <div class="checks"></div><span><?=$value['NAME']?></span>
                      </label>
                    <?}?>
                  </div>
                </div>
              <?}?>
            </div>
          </div>
          <div class="row justify-content-end">
            <button class="btn btn--red js-select-letter">Готово</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-services" tabindex="-1" role="dialog" aria-labelledby="modal-services" aria-hidden="true">
  <div class="modal-dialog modal-dialog--modals" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="modal-close" data-dismiss="modal" aria-label="Close">
          <svg class="icon">
            <use xlink:href="#icon-close"></use>
          </svg>
        </div>
        <div class="row">
          <div class="col-xl-3 col-lg-4 col-md-6">
            <?foreach ($services as $idMain => $mainSection) {?>
              <div class="column-cont">
                <div class="r-text"><?=$arMainSection[$idMain]?></div>
                <?foreach ($mainSection as $idSub => $subSection) {?>
                  <?if($idSub){?><div class="s-text"><?=$arSubSection[$idSub]?></div><?}?>
                  <div class="column">
                    <?foreach ($subSection as $section) { // dump($section);
                      $teg = ($section['UF_SELECT']) ? 'strong' : 'span';?>
                      <div class="labes">
                        <label class="radio" for="<?=$section['UF_XML_ID']?>">
                          <input type="checkbox" name="services" value="<?=$section['UF_XML_ID']?>" id="<?=$section['UF_XML_ID']?>" <?if(in_array($section['UF_XML_ID'],$buildServices))echo 'checked'?> />
                          <div class="checks"></div><<?=$teg?>><?=$section['UF_NAME']?></<?=$teg?>>
                        </label>
                        <?if($section['ON']){?>
                          <div class="arrows js-arrow"></div>
                          <ul class="labes-list">
                            <?foreach ($section['ON'] as $section) {
                              $teg = ($section['UF_SELECT']) ? 'strong' : 'span';?>
                              <li class="labes-list__item">
                                <div class="labes">
                                  <label class="radio" for="<?=$section['UF_XML_ID']?>">
                                    <input type="checkbox" name="services" value="<?=$section['UF_XML_ID']?>" id="<?=$section['UF_XML_ID']?>" <?if(in_array($section['UF_XML_ID'],$buildServices))echo 'checked'?> />
                                    <div class="checks"></div><<?=$teg?>><?=$section['UF_NAME']?></<?=$teg?>>
                                  </label>
                                </div>
                              </li>
                            <?}?>
                          </ul>
                        <?}?>
                      </div>
                    <?}?>
                  </div>
                <?}?>
              </div>
              <?if ($idMain == 4 || $idMain == 6 || $idMain == 7 || $idMain == 26) echo '</div><div class="col-xl-3 col-lg-4 col-md-6">'?>
            <?}?>
          </div>
        </div>
        <div class="row justify-content-end">
          <button class="btn btn--red js-select-services">Готово</button>
        </div>
      </div>
    </div>
  </div>
</div>
