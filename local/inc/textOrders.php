<div class="order__recommendations">
  <div class="order__recommendations-title">Простые советы для работы с заказами</div>
  <div class="order__recommendations-item">
    <span class="order__recommendations-ico">&mdash;</span>
    <span>Чтобы увидеть контакты Заказчика, <a href="/lk/reg.php" class="a_grey">зарегистрируйтесь</a></span>
  </div>
  <div class="order__recommendations-item">
    <span class="order__recommendations-ico">&mdash;</span>
    <span>Уделяйте больше внимания заказам с подробным описанием</span>
  </div>
  <div class="order__recommendations-item">
    <span class="order__recommendations-ico">&mdash;</span>
    <span>Фильтруйте заказы по интересным категориям <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/corner-right-down.svg" alt=""></span>
  </div>
</div>
