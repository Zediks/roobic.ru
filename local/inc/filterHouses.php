<?
global $typeURL;

// получим материалы
$arMaterials = getElHL(11,[],['UF_ACTIVE'=>true],['*']);
foreach ($arMaterials as $key => $value) { // распределим для формирования
  $materials[$value['UF_MAIN_SECTION']][$value['ID']] = $value;
}

// dump($_REQUEST);
if ($_REQUEST['type'])
{
  foreach ($_REQUEST['type'] as $type)
  {
    if (getBath($type) == 'бани') $arTypeBath[] = $type;
    else $arTypeHouse[] = $type;
  }
  // if ($arTypeBath) {
  //   $strTypeBath = implode('|',$arTypeBath);
  //   setcookie("bath_type", $strTypeBath, time()+3600, '/');
  // }
  // if ($arTypeHouse) {
  //   $strTypeHouse = implode('|',$arTypeHouse);
  //   setcookie("house_type", $strTypeHouse, time()+3600, '/');
  // }

} elseif ($_REQUEST['SEND_FORM']) { // сбросим
  setcookie('house_type', '', time() - 3600, '/');
  setcookie('bath_type', '', time() - 3600, '/');
  unset($houseType);
}

$cookieType = ($typeURL == 'bani') ? 'bath_type' : 'house_type';

$houseType = ($_REQUEST['type']) ? $_REQUEST['type'] : explode('|',$_COOKIE[$cookieType]);

$areaData = [
  'do-100' => 'до 100 м²',
  '100-150' => 'от 100 до 150 м²',
  '150-200' => 'от 150 до 200 м²',
  '200-300' => 'от 200 до 300 м²',
  '300-500' => 'от 300 до 500 м²',
  'ot-500' => 'свыше 500 м²',
];

// if ($_REQUEST['area'])
// 	setcookie("house_area", $_REQUEST['area'], time()+3600, '/');
$houseArea = ($_REQUEST['area']) ? $_REQUEST['area'] : $_COOKIE['house_area'];

$priceData = [
  // 'do-1000000' => 'до 1 млн',
  // '1000000-2000000' => 'от 1 до 2 млн',
  '2000000-3000000' => 'от 2 до 3 млн',
  '3000000-4000000' => 'от 3 до 4 млн',
  '4000000-5000000' => 'от 4 до 5 млн',
  '5000000-7000000' => 'от 5 до 7 млн',
  '7000000-9000000' => 'от 7 до 9 млн',
  'ot-9000000' => 'свыше 9 млн',
];

// if ($_REQUEST['price'])
// 	setcookie("house_price", $_REQUEST['price'], time()+3600, '/');
$housePrice = ($_REQUEST['price']) ? $_REQUEST['price'] : $_COOKIE['house_price'];

// очистка
if ($_REQUEST['reset'] == 'y') {
  setcookie('house_type', '', time() - 3600, '/');
  setcookie('bath_type', '', time() - 3600, '/');
	unset($houseType);
  setcookie('house_area', '', time() - 3600, '/');
	unset($houseArea);
  setcookie('house_price', '', time() - 3600, '/');
	unset($housePrice);
}
?>

<div class="row">
  <div class="col-sm-12">
    <button class="btn btn--red filterShow">Фильтр</button>
  </div>
</div>

<form class="filter-form" action="/<?=$typeURL?>/" method="get">
  <div class="row">
    <div class="col-md-6 col-lg-3">
      <div class="form-cont filter-form__margin-5">
        <a class="open-modal" href="" data-toggle="modal" data-target="#modal-services">Материал</a>
        <label class="select" for="states">
          <select class="js-select__services select__select js-select__letter" data-count-selected-text="Выбрано {0} материалов" data-selected-text-format="count &gt; 3" data-size="5" name="type[]" multiple="multiple" data-live-search="true" title="Выберите материал">
            <option value="" default="default">Выберите материал</option>
            <?foreach ($materials as $idMain => $mainSection) {
              if ($typeURL == 'bani' && $idMain != 'Бани') continue;
              if ($typeURL != 'bani' && $idMain == 'Бани') continue;?>
              <optgroup label="<?=$idMain?>">
                <?foreach ($mainSection as $section) {?>
                  <option value="<?=$section['UF_XML_ID']?>" <?if(in_array($section['UF_XML_ID'],$houseType))echo 'selected'?>><?=$section['UF_NAME']?></option>
                <?}?>
              </optgroup>
            <?}?>
          </select>
        </label>
      </div>
    </div>
    <div class="col-md-6 col-lg-3">
      <label class="select filter-form__margin-5" for="area">
        <select class="select__select select__styler js-select__letter" name="area" id="area" title="Площадь, м²">
          <option value="">Площадь, м²</option>
          <?foreach ($areaData as $key => $value) {?>
            <option value="<?=$key?>" <?if($houseArea == $key)echo 'selected';?>><?=$value?></option>
          <?}?>
        </select>
      </label>
    </div>
    <div class="col-md-6 col-lg-3">
      <label class="select filter-form__margin-5" for="price">
        <select class="select__select select__styler js-select__letter" name="price" id="price" title="Ценовой диапазон">
          <option value="">Ценовой диапазон</option>
          <?foreach ($priceData as $key => $value) {?>
            <option value="<?=$key?>" <?if($housePrice == $key)echo 'selected';?>><?=$value?></option>
          <?}?>
        </select>
      </label>
    </div>
    <div class="col-md-6 col-lg-3">
        <div class="filter-form__margin-5">
            <div class="row">
                <div class="col-sm-6">
                    <button type="reset" class="btn btn--reset" data-url="<?echo $APPLICATION->GetCurDir();?>?reset=y" name="reset">Очистить фильтр</button>
                </div>
                <div class="col-sm-6">
                    <button class="btn btn--red" name="SEND_FORM" value="Y">Искать</button>
                </div>
            </div>
        </div>
    </div>
  </div>
</form>

<div class="modal fade" id="modal-services" tabindex="-1" role="dialog" aria-labelledby="modal-services" aria-hidden="true">
  <div class="modal-dialog modal-dialog--modals" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="modal-close" data-dismiss="modal" aria-label="Close">
          <svg class="icon">
            <use xlink:href="#icon-close"></use>
          </svg>
        </div>
        <div class="row">
          <div class="col-xl-3 col-lg-4 col-md-6">
            <?foreach ($materials as $idMain => $mainSection) {
              if ($typeURL == 'bani' && $idMain != 'Бани') continue;
              if ($typeURL != 'bani' && $idMain == 'Бани') continue;?>
              <div class="column-cont">
                <div class="r-text"><?=$idMain?></div>
                <div class="column">
                  <?foreach ($mainSection as $section) {?>
                    <div class="labes">
                      <label class="radio" for="<?=$section['UF_XML_ID']?>">
                        <input type="checkbox" name="type" value="<?=$section['UF_XML_ID']?>" id="<?=$section['UF_XML_ID']?>" <?if(in_array($section['UF_XML_ID'],$houseType))echo 'checked'?> />
                        <div class="checks"></div><span><?=$section['UF_NAME']?></span>
                      </label>
                    </div>
                  <?}?>
                </div>
              </div>
              <?if ($idMain != 'Беседки') echo '</div><div class="col-xl-3 col-lg-4 col-md-6">'?>
            <?}?>
          </div>
        </div>
        <div class="row justify-content-end">
          <button class="btn btn--red js-select-services">Готово</button>
        </div>
      </div>
    </div>
  </div>
</div>

<style>
  .filter-form__margin-5 {
    margin: 0 -5px;
    width: calc(100% + 10px)
  }
  @media screen and (max-width: 767px) {
    .filter-form__margin-5 {
      margin: 0;
      width: 100%
    }
  }
</style>
