<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
	Loader::includeModule('iblock');
	Loader::includeModule('highloadblock');
use Bitrix\Main\Application;
use Bitrix\Highloadblock as HL, Bitrix\Main\Entity;
use Bitrix\Main\Page\Asset;

// yandex_map
Asset::getInstance()->addJs("https://api-maps.yandex.ru/2.1/?lang=ru_RU&apikey=40336fa1-cf4d-44d7-a4fa-8c7ac8e9f5a5
");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/yandex_map.js");

class client extends CBitrixComponent
{
	function order()
	{
		global $USER;
		$userID = $USER->GetID();
		$iblockID = 5;

		$params = [
			"max_len" => "100", // обрезает символьный код до 100 символов
			"change_case" => "L", // буквы преобразуются к нижнему регистру
			"replace_space" => "-", // меняем пробелы на тире
			"replace_other" => "-", // меняем левые символы на тире
			"delete_repeat_replace" => "true", // удаляем повторяющиеся символы
			"use_google" => "false", // отключаем использование google
		];

		// получим услуги
		// $arService = getElHL(3,[],[],['ID','UF_MAIN_SECTION','UF_SUBSECTION','UF_NAME']);
		// $arServicesMain = getListProperty([],["USER_FIELD_ID" => 72]); // Главный раздел
		// $arServicesSub = getListProperty([],["USER_FIELD_ID" => 73]); // Подраздел
		// $arResult['MATERIALS'] = getElHL(7,[],[],['ID','UF_NAME']);

		$arResult['REGIONS'] = getElHL(16,[],[],['ID','UF_NAME','UF_XML_ID']);

		$arCategoriesType = getListProperty([],["USER_FIELD_ID" => 149]); // Тип
		$arElHLCategories = getElHL(15,[],[],['ID','UF_TYPE','UF_NAME','UF_XML_ID']);
		foreach ($arElHLCategories as $value) {
			$arResult['CATEGORIES'][$value['UF_TYPE']]['NAME'] = $arCategoriesType[$value['UF_TYPE']];
			$arResult['CATEGORIES'][$value['UF_TYPE']]['ITEMS'][] = $value;
		}

		// получим разделы
		$arOrder = ['SORT'=>'ASC'];
		$arFilter = ['IBLOCK_ID'=>$iblockID,'ACTIVE'=>'Y'];
		$arSelect = ['ID','CODE'];
		$rsSections = CIBlockSection::GetList($arOrder,$arFilter,false,$arSelect,false);
		while ($arSection = $rsSections->Fetch())
			$arRegions[$arSection['CODE']] = $arSection['ID'];

		// сформируем услуги
		// foreach ($arService as $value)
		// {
		// 	// dump($value);
		// 	$mainSectionID = $value['UF_MAIN_SECTION'];
		// 	$subSectionID = $value['UF_SUBSECTION'];
		// 	$mainSection = $arServicesMain[$mainSectionID];
		// 	$subSection = $arServicesSub[$subSectionID];
		// 	$arResult['SERVICES'][$mainSectionID]['NAME'] = $mainSection;
		// 	$arResult['SERVICES'][$mainSectionID]['ITEMS'][$subSectionID]['NAME'] = $subSection;
		// 	$arResult['SERVICES'][$mainSectionID]['ITEMS'][$subSectionID]['ITEMS'][$value['ID']] = [
		// 		'NAME' => $value['UF_NAME'],
		// 	];
		// 	$arSectionShow[$value['ID']] = [
		// 		'MAIN_SECTION' => $mainSectionID,
		// 		'SUBSECTION' => $subSectionID,
		// 	];
		// }

		$request = Application::getInstance()->getContext()->getRequest();
		$orderID = $request->getQuery("order_id");
		$sendFrom = $request->getPost("sendFrom");

		if($sendFrom){ // форма отправлена

			$order_name = $request->getPost("order_name");
			// $order_code = CUtil::translit($order_name, "ru", $params);

			$filesPhotos = $request->getFile("order_photos"); // dump($filesPhotos);
			foreach ($filesPhotos as $key => $arPhoto) {
				foreach($arPhoto as $key2 => $val2){
					$arPhotos[$key2][$key] = $val2;
				}
			}

			$orderElement = new CIBlockElement;

			$orderStatus = $request->getPost("order_status");
			if (!$orderStatus) $orderStatus = 5; // заказ открыт

			$orderRegion = $request->getPost("order_region");

			$arFields = [
				'ADDRESS' => $request->getPost("order_address"),
				// 'SERVICE' => $request->getPost("order_service"),
				'PHOTOS' => $arPhotos,
				'STATUS' => $orderStatus,
				'USER' => $userID,
				'REGION' => $orderRegion,
				'CATEGORY' => $request->getPost("order_category"),
			];

			$arLoadProductArray = Array(
		    "IBLOCK_ID"					=> $iblockID,
				"IBLOCK_SECTION_ID"	=> $arRegions[$orderRegion], // раздел
				"MODIFIED_BY"				=> $userID, // элемент изменен текущим пользователем
				"ACTIVE"         		=> "N", // активен
		    "NAME"           		=> $order_name,
		    // "CODE"           		=> $order_code,
		    "DETAIL_TEXT"   		=> $request->getPost("about"),
				"DETAIL_TEXT_TYPE" 	=> 'html',
				"PROPERTY_VALUES"		=> $arFields,
		  ); // dump($arLoadProductArray);

			if ($orderID)
			{
				$orderElement->Update($orderID,$arLoadProductArray);

				// удаление файлов
				$delFile = $request->getPost("del_file");
				if($delFile){
					$arDelFile = explode(',',$delFile); // dump($arDelFile);
					foreach ($arDelFile as $key => $value) {
						$arDeleteImages[$value] = ['VALUE' => ['del' => 'Y']];
					}
					\CIBlockElement::SetPropertyValueCode($orderID, 'PHOTOS', $arDeleteImages);
				}
				$action = 'Изменение объявления';
			} else {
				$arLoadProductArray['ACTIVE_FROM'] = date('d.m.Y');
				$orderID = $orderElement->Add($arLoadProductArray); // добавим
				$orderElement->Update($orderID,['CODE'=>$orderID]);
				$action = 'Новое объявление';
			}

			$urlLK = 'https://stroiman.ru/lk/client_ad?order_id='.$orderID;
			$urlAdmin = 'https://stroiman.ru/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=5&type=content&lang=ru&ID='.$orderID.'&find_section_section=0&WF=Y';

			$mailFields = array(
				"action" => $action,
				"NAME" => $order_name,
				"TEXT" => $arLoadProductArray['DETAIL_TEXT'],
		    "urlLK" => $urlLK,
		    "urlAdmin" => $urlAdmin,
		  );
		  if (CEvent::Send("SEND_MODERATION", "s1", $mailFields)) echo '<p>Спасибо! Данные успешно отправлены на модерацию!</p>';
		  else echo '<p>Ошибка! Данные не отправлены!</p>';

			// if ($orderID)
			// {
			// 	// добавим услуги
			// 	$service = $request->getPost("service");
			// 	if($service)
			// 	{
			// 		$hlblock_id = 4; // id HL
			// 		$hlblock = HL\HighloadBlockTable::getById($hlblock_id)->fetch();
			// 		$entity = HL\HighloadBlockTable::compileEntity($hlblock);
			// 		$entity_data_class = $entity->getDataClass();
			//
			// 		// получим Услуги и Цены
			// 		$arElHL = array_values(getElHL(4,[],['UF_AD' => $orderID],['ID']));
			// 		foreach ($arElHL as $value)
			// 			$priceListAvial[] = $value['ID'];
			//
			// 		foreach ($service as $key => $val)
			// 		{
			// 			$service_id = $request->getPost("service_id_".$key);
			// 			$material = $request->getPost("material_".$key);
			// 			$material_price = $request->getPost("material_price_".$key);
			//
			// 			foreach ($material as $keyM => $value)
			// 			{
			// 				$serviceId = $service_id[$keyM];
			// 				$priceListNew[] = $serviceId;
			//
			// 				$data = [
			// 					"UF_SERVICE" => $material[$keyM],
			// 			    "UF_PRICE" => $material_price[$keyM],
			// 					"UF_AD" => $orderID
			// 			  ];
			//
			// 				if ($serviceId)
			// 					$entity_data_class::update($serviceId,$data);
			// 				else
			// 					$result = $entity_data_class::add($data);
			// 			}
			// 		}
			//
			// 		// удалим услуги
			// 		foreach ($priceListAvial as $value) {
			// 			if(!in_array($value,$priceListNew))
			// 				$entity_data_class::delete($value);
			// 		}
			// 	}
			// }

			header('Location: /lk/client_ads');
		}

		if ($orderID) // получим наш заказ
		{
			$arOrder = Array('SORT'=>'ASC');
			$arFilter = Array('IBLOCK_ID'=>$iblockID,'ID'=>$orderID);
			$arSelect = Array('ID','NAME','DETAIL_TEXT','PROPERTY_ADDRESS','PROPERTY_SERVICE','PROPERTY_BUDGET','PROPERTY_WORK_SCOPE','PROPERTY_WORK_START','PROPERTY_DEADLINE','PROPERTY_PHOTOS','PROPERTY_STATUS','PROPERTY_REGION','PROPERTY_CATEGORY'); // ,'PROPERTY_'
			$rsElements = CIBlockElement::GetList($arOrder,$arFilter,false,false,$arSelect);
			if($arElement = $rsElements->GetNext()) { // dump($arElement);
				// получим файлы
				foreach ($arElement['PROPERTY_PHOTOS_VALUE'] as $key => $value) {
					$arElement['PHOTOS'][$key] = CFile::ResizeImageGet($value, array('width'=>488, 'height'=>360), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
				}
				$arResult['ORDER'] = $arElement;
			}

			// получим Услуги и Цены
			// $arResult['PRICE_LIST'] = array_values(getElHL(4,[],['UF_AD' => $orderID],['*']));
			// foreach ($arResult['PRICE_LIST'] as $value)
			// {
			// 	$mainSectionID = $arSectionShow[$value['UF_SERVICE']]['MAIN_SECTION'];
			// 	$subSectionID = $arSectionShow[$value['UF_SERVICE']]['SUBSECTION'];
			// 	$value['MAIN_SECTION'] = $mainSectionID;
			// 	$value['SUBSECTION'] = $subSectionID;
			// 	$arResult['PRICE_LIST_SHOW'][$mainSectionID][] = $value;
			// }
			// $arResult['PRICE_LIST_SHOW'] = array_values($arResult['PRICE_LIST_SHOW']);

		}

		return $arResult;
	}

	public function executeComponent()
	{
		$this->arResult = array_merge($this->arResult,$this->order());
		$this->includeComponentTemplate();
	}
}?>
