<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// dump($arResult);?>
<?$this->SetViewTarget('body-class');?>
	class="lk-page"
<?$this->EndViewTarget();?>
<div class="completed__wrap">
	<div class="row">
		<div class="col-lg-8">
			<div class="completed__block-name d-none d-xs-block d-sm-block d-md-hide">
				<div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_10.svg" alt=""></div><strong>Размещение объявления</strong>
				<div class="completed__block-desc">
					Укажите стоимость и виды работ, которые готовы предоставлять
				</div>
			</div>
			<div class="completed__container">
				<form action="" method="post" enctype="multipart/form-data">
					<div class="form-main-block">
						<div class="form-block">
							<div class="form-block__name hide-xs">Предложение услуг</div>
							<label for="order_name">
								<input class="form-input" type="text" name="order_name" value="<?=$arResult['ORDER']['NAME']?>" placeholder="Заголовок. Например, Ремонт квартир, домов, офисных помещений*" required>
								<span class="placeholder">Заголовок. Например, Ремонт квартир, домов, офисных помещений*</span>
								<a href="#" class="edit-input">
									<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
								</a>
							</label>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-block">
								<label class="select" for="order_region">
									<select class="select__select select__styler" name="order_region" id="order_region" title="Регион*" data-live-search="true" required>
										<option value="">Регион*</option>
										<?foreach ($arResult['REGIONS'] as $value) {?>
											<option value="<?=$value['UF_XML_ID']?>" <?if($value['UF_XML_ID'] == $arResult['ORDER']['PROPERTY_REGION_VALUE']) echo 'selected';?>><?=$value['UF_NAME']?></option>
										<?}?>
									</select>
								</label>
							</div>
						</div>
					</div>
					<div class="form-block">
						<label for="input_address">
							<input class="form-input" type="text" name="order_address" value="<?=$arResult['ORDER']['PROPERTY_ADDRESS_VALUE']?>" placeholder="Место оказания услуг, задайте город или область или район" id="input_address">
							<span class="placeholder">Место оказания услуг, задайте город или область или район</span>
							<a href="#" class="edit-input">
								<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
							</a>
						</label>
					</div>
					<div class="form-map" id="yandex_map"></div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-block">
								<label class="select" for="order_category">
									<select class="select__select select__styler" name="order_category" id="order_category" title="Категория*" data-live-search="true" required>
										<option value="">Категория*</option>
										<?foreach ($arResult['CATEGORIES'] as $typeID => $categories) {?>
											<optgroup data-main="<?=$typeID?>" label="<?=$categories['NAME']?>">
												<?foreach ($categories['ITEMS'] as $value) {?>
													<option value="<?=$value['UF_XML_ID']?>" <?if($value['UF_XML_ID'] == $arResult['ORDER']['PROPERTY_CATEGORY_VALUE']) echo 'selected';?>><?=$value['UF_NAME']?></option>
												<?}?>
											</optgroup>
										<?}?>
									</select>
								</label>
							</div>
						</div>
					</div>
					<?/*?>
					<div class="row">
						<div class="col-md-12">
							<div class="form-main-block block_service">
								<div class="clone_block_service">
		            <div class="row">
		              <div class="col-md-12 hide-xs">
		                <div class="form-block">
		                  <div class="form-block__name">Услуги и цены</div>
		                </div>
		              </div>
		              <div class="col-md-12">
		                <div class="form-block">
											<?//dump($arResult['SERVICES'])?>
		                  <label class="select select-up" for="service">
		                    <select class="select__select select__styler select_service" name="service[]" id="service" title="Услуги*" required>
		                      <option value="">Услуги*</option>
													<?foreach ($arResult['SERVICES'] as $key => $value) {?>
		                        <option value="<?=$key?>" <?if($key == $arResult['PRICE_LIST_SHOW'][0][0]['MAIN_SECTION']) echo 'selected';?>><?=$value['NAME']?></option>
		                      <?}?>
		                    </select>
		                  </label>
		                </div>
		              </div>
		            </div>
								<div class="block_material">
									<? $i_material = 0;
									do { ?>
				            <div class="row">
				              <div class="col-md-6">
				                <div class="form-block">
				                  <label class="select" for="material">
				                    <select class="select__select select__styler" name="material_0[]" id="material" title="Материал, тип услуг*" data-hide-disabled='true' required>
				                      <option value="">Материал, тип услуг*</option>
															<?foreach ($arResult['SERVICES'] as $mainSectionID => $mainSection) {
																	foreach ($mainSection['ITEMS'] as $subSectionID => $subSection)
																	{
																		if ($subSectionID) echo '<optgroup data-main="'.$mainSectionID.'" label="'.$subSection['NAME'].'">';
																			foreach ($subSection['ITEMS'] as $key => $value) {?>
																				<option value="<?=$key?>" <?if($key == $arResult['PRICE_LIST_SHOW'][0][$i_material]['UF_SERVICE']) echo 'selected';?> data-main="<?=$mainSectionID?>"><?=$value['NAME']?></option>
																			<?}
																		if ($subSectionID) echo '</optgroup>';
																	}
																}?>
				                    </select>
				                  </label>
													<input type="hidden" name="service_id_0[]" value="<?=$arResult['PRICE_LIST_SHOW'][0][$i_material]['ID']?>">
				                </div>
				              </div>
				              <div class="col-md-6">
				                <div class="form-block">
				                  <label>
				                    <input class="form-input" type="text" name="material_price_0[]" placeholder="Цена, от*" value="<?=$arResult['PRICE_LIST_SHOW'][0][$i_material]['UF_PRICE']?>" required>
				                    <span class="placeholder">Цена, от*</span>
				                    <a href="#" class="edit-input">
				                      <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
				                    </a>
				                  </label>
				                </div>
				              </div>
											<?if($i_material != 0):?>
												<a href="" class="block_remove">
									        <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/remove.svg">
									      </a>
											<?endif;?>
				            </div>
									<? $i_material++;
									} while ($i_material < count($arResult['PRICE_LIST_SHOW'][0]));?>
									<div class="row insert_before">
										<div class="col-md-12">
											<div class="form-block">
			                  <a href="#" class="add-item-material" data-block="block_material" data-sid="0"><span class="add-item_ico">+</span> <span class="add-item_link">Добавить материал, тип услуг</span></a>
			                </div>
										</div>
									</div>
								</div></div>
								<?$i_service = 1;
								while ($i_service < count($arResult['PRICE_LIST_SHOW'])) {?>
									<div class="clone_block_service">
									  <div class="row block_service">
									    <div class="col-md-12">
									      <div class="form-block">
													<a href="#" class="service_remove">
							              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30px" height="30px">
							                <image x="0px" y="0px" width="30px" height="30px" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAMAAAAM7l6QAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAaVBMVEXk5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7j5e3Bwsa7vL6np6f///98V12LAAAAHXRSTlMABUyRxuj4EIDq61gDlq2TlFfpBpLExflN7IHH+vrIk+sAAAABYktHRCJdZVysAAAAB3RJTUUH5AkLDiEgJn8OZQAAAMdJREFUKM+Nk1sSgyAMACNKlfoGq/URpfe/ZKtVBKvQ/WCG7AABEoANj/gBvd1o4BMPjoQRQ8U9Ck0bUzSgsSaTFH9IE2UzPCHbfI6nFF9b4gXlkjO/0nzOP8JLIgDBrjUTQLTpqIYVAtU+meRHjXLaIzk89rXyJcdlUKEatKNnZVhk0KDhDYuNSzs2r+2pOS5mfZYniNb6qI4vsXwoC63lsJZjcW6L/0rRUchzGxzy43obzPl32v3b7tBESwtWPR8G3ldEqOAbo1NPex7+7UEAAAAASUVORK5CYII=" />
							              </svg>
							              <span>Удалить услугу</span></a>
							            <div class="clr"></div>
									        <label class="select select-up" for="service_m">
									          <select class="select__select select__styler select_service" name="service[]" id="service_M" title="Услуги*" >
									            <option value="">Услуги*</option>
															<?foreach ($arResult['SERVICES'] as $key => $value) {?>
				                        <option value="<?=$key?>" <?if($key == $arResult['PRICE_LIST_SHOW'][$i_service][0]['MAIN_SECTION']) echo 'selected';?>><?=$value['NAME']?></option>
				                      <?}?>
									          </select>
									        </label>
									      </div>
									    </div>
									  </div>
									  <div class="block_material">
											<? $i_material = 0;
											do { ?>
												<div class="row">
											    <div class="col-md-6">
											      <div class="form-block">
											        <label class="select" for="material_m">
											          <select class="select__select select__styler" name="material_<?=$i_service?>[]" id="material_m" title="Материал, тип услуг*" data-hide-disabled='true'>
											            <option value="">Материал, тип услуг*</option>
																	<?foreach ($arResult['SERVICES'] as $mainSectionID => $mainSection) {
																			foreach ($mainSection['ITEMS'] as $subSectionID => $subSection)
																			{
																				if ($subSectionID) echo '<optgroup data-main="'.$mainSectionID.'" label="'.$subSection['NAME'].'">';
																					foreach ($subSection['ITEMS'] as $key => $value) {?>
																						<option value="<?=$key?>" <?if($key == $arResult['PRICE_LIST_SHOW'][$i_service][$i_material]['UF_SERVICE']) echo 'selected';?> data-main="<?=$mainSectionID?>"><?=$value['NAME']?></option>
																					<?}
																				if ($subSectionID) echo '</optgroup>';
																			}
																		}?>
											          </select>
											        </label>
															<input type="hidden" name="service_id_<?=$i_service?>[]" value="<?=$arResult['PRICE_LIST_SHOW'][$i_service][$i_material]['ID']?>">
											      </div>
											    </div>
											    <div class="col-md-6">
											      <div class="form-block">
											        <label>
											          <input class="form-input" type="text" name="material_price_<?=$i_service?>[]" placeholder="Цена, от*" value="<?=$arResult['PRICE_LIST_SHOW'][$i_service][$i_material]['UF_PRICE']?>">
											          <span class="placeholder">Цена, от*</span>
											          <a href="#" class="edit-input">
											            <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
											          </a>
											        </label>
											      </div>
											    </div>
													<?if($i_material != 0):?>
														<a href="" class="block_remove">
											        <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/remove.svg">
											      </a>
													<?endif;?>
												</div>
											<? $i_material++;
											} while ($i_material < count($arResult['PRICE_LIST_SHOW'][$i_service]));?>
											<div class="row insert_before">
								        <div class="col-md-12">
								          <div class="form-block">
								            <a href="#" class="add-item-material" data-block="block_material" data-sid="<?=$i_service?>"><span class="add-item_ico">+</span> <span class="add-item_link">Добавить материал, тип услуг</span></a>
								          </div>
								        </div>
								      </div>
									  </div>
									</div>
								<? $i_service++; } ?>
								<!-- <div class="form-bottom">
									<a href="#" class="add-service btn" data-block="block_service">Добавить услугу</a>
								</div> -->
		          </div>
						</div>
					</div>
					<?*/?>
					<div class="row">
						<div class="col-md-12">
							<div class="form-block">
								<div class="form-block__name">Подробнее</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-block">
								<label for="about">
									<textarea class="form-textarea" name="about" id="about" cols="30" rows="10" placeholder="Описание объявления"><?=$arResult['ORDER']['DETAIL_TEXT']?></textarea>
								</label>
								<p>Описание должно содержать не менее 200, и не более 1000 символов</p>
							</div>
						</div>
					</div>
					<div class="form-main-block">
						<div class="form-block">
							<div class="form-block__name">Фото работ</div>
							<div class="form-block__desc">Загрузите примеры ваших работ с объекта</div>
							<div class="download-photo">
                                <div class="list">
                                    <label class="label-file <?if($arResult['ORDER']['PHOTOS'][$i])echo'has-img'?>" >
                                        <input type="file" accept=".png, .jpg, .jpeg" name="order_photos[]" class="form-upload__input" multiple>
                                        <span>Добавьте файл для загрузки</span>
                                        <span>в это поле</span>
                                        <img src="<?=$arResult['ORDER']['PHOTOS'][$i]['src']?>" alt="">
                                    </label>
                                    <span class="delete-file">х</span>
                                </div>
								<input type="hidden" name="del_file" value="">
							</div>
						</div>
					</div>
					<div class="row">
            <div class="col-md-12">
              <div class="form-bottom">
                <!-- <a href="#" class="btn off">Сохранить</a> -->
								<input type="hidden" name="order_status" value="<?=$arResult['ORDER']['PROPERTY_STATUS_ENUM_ID']?>">
								<input type="submit" name="sendFrom" value="Сохранить" class="btn form-upload__submit">
              </div>
            </div>
          </div>
				</form>
			</div>
		</div>
		<div class="col-md-3 d-none d-lg-block d-xl-block">
			<div class="completed__block-name">
				<div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_11.svg" alt=""></div><strong>Размещение объявления</strong>
				<div class="completed__block-desc">
					Укажите стоимость и виды работ, которые готовы предоставлять
				</div>
			</div>
		</div>
	</div>
</div>

<?/*?>
<div class="hide">
	<!-- услуги -->
	<div class="clone_block_service">
		<div class="row block_service">
			<div class="col-md-12">
				<div class="form-block-service">
					<a href="#" class="service_remove">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30px" height="30px">
							<image x="0px" y="0px" width="30px" height="30px" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAMAAAAM7l6QAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAaVBMVEXk5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7j5e3Bwsa7vL6np6f///98V12LAAAAHXRSTlMABUyRxuj4EIDq61gDlq2TlFfpBpLExflN7IHH+vrIk+sAAAABYktHRCJdZVysAAAAB3RJTUUH5AkLDiEgJn8OZQAAAMdJREFUKM+Nk1sSgyAMACNKlfoGq/URpfe/ZKtVBKvQ/WCG7AABEoANj/gBvd1o4BMPjoQRQ8U9Ck0bUzSgsSaTFH9IE2UzPCHbfI6nFF9b4gXlkjO/0nzOP8JLIgDBrjUTQLTpqIYVAtU+meRHjXLaIzk89rXyJcdlUKEatKNnZVhk0KDhDYuNSzs2r+2pOS5mfZYniNb6qI4vsXwoC63lsJZjcW6L/0rRUchzGxzy43obzPl32v3b7tBESwtWPR8G3ldEqOAbo1NPex7+7UEAAAAASUVORK5CYII=" />
						</svg>
						<span>Удалить услугу</span></a>
					<div class="clr"></div>
					<label class="select select-up" for="service_m">
						<select class="select__select select__styler select_service" name="service[]" id="service_M" title="Услуги*" >
							<option value="">Услуги*</option>
							<?foreach ($arResult['SERVICES'] as $key => $value) {?>
								<option value="<?=$key?>"><?=$value['NAME']?></option>
							<?}?>
						</select>
					</label>
				</div>
			</div>
		</div>
		<div class="block_material">
			<div class="row">
				<div class="col-md-6">
					<div class="form-block-service">
						<label class="select" for="material_m">
							<select class="select__select select__styler select_material" name="material[]" id="material_m" title="Материал, тип услуг*" data-hide-disabled='true'>
								<option value="">Материал, тип услуг*</option>
								<?foreach ($arResult['SERVICES'] as $mainSectionID => $mainSection) {
										foreach ($mainSection['ITEMS'] as $subSectionID => $subSection)
										{
											if ($subSectionID) echo '<optgroup data-main="'.$mainSectionID.'" label="'.$subSection['NAME'].'">';
												foreach ($subSection['ITEMS'] as $key => $value) {?>
													<option value="<?=$key?>" data-main="<?=$mainSectionID?>"><?=$value['NAME']?></option>
												<?}
											if ($subSectionID) echo '</optgroup>';
										}
									}?>
							</select>
						</label>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-block">
						<label>
							<input class="form-input input_price" type="text" name="material_price[]" placeholder="Цена, от*">
							<span class="placeholder">Цена, от*</span>
							<a href="#" class="edit-input">
								<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
							</a>
						</label>
					</div>
				</div>
			</div>
			<div class="row insert_before">
				<div class="col-md-12">
					<div class="form-block">
						<a href="#" class="add-item-material a_material" data-block="block_material"><span class="add-item_ico">+</span> <span class="add-item_link">Добавить материал, тип услуг</span></a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- материалы -->
	<div class="row clone_block_material">
		<div class="col-md-6">
			<div class="form-block">
				<label class="select" for="material_m">
					<select class="select__select select__styler not_selectpicker select_material" name="material[]" id="material_m" title="Материал, тип услуг*" data-hide-disabled="true">
						<option value="">Материал, тип услуг*</option>
						<?foreach ($arResult['SERVICES'] as $mainSectionID => $mainSection) {
								foreach ($mainSection['ITEMS'] as $subSectionID => $subSection)
								{
									if ($subSectionID) echo '<optgroup data-main="'.$mainSectionID.'" label="'.$subSection['NAME'].'">';
										foreach ($subSection['ITEMS'] as $key => $value) {?>
											<option value="<?=$key?>" data-main="<?=$mainSectionID?>"><?=$value['NAME']?></option>
										<?}
									if ($subSectionID) echo '</optgroup>';
								}
							}?>
					</select>
				</label>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-block">
				<label>
					<input class="form-input input_price" type="text" name="material_price[]" placeholder="Цена, от*">
					<span class="placeholder">Цена, от*</span>
					<a href="#" class="edit-input">
						<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
					</a>
				</label>
			</div>
		</div>
		<a href="" class="block_remove">
			<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/remove.svg">
		</a>
	</div>
</div>
<?*/?>
