<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Application;
use Bitrix\Highloadblock as HL, Bitrix\Main\Entity;

class company extends CBitrixComponent
{
	function profile()
	{
		global $USER;
		$userID = $USER->GetID();
		$iblockID = 4;
		$contentManager = CSite::InGroup([8]);

		// получим компанию
		$by = 'id'; $sort = 'asc'; // поле сортировки
	  $filter = ['ID' => $userID];
	  $arParams = [ // наши поля
			'FIELDS' => ['ID'],'SELECT' => ['UF_COMPANIES']
	  ];
	  $rsUsers = CUser::GetList($by, $sort, $filter, $arParams);
	  if($arUser = $rsUsers->GetNext())
			$companyID = $arUser['UF_COMPANIES'][0];

		// изменение активности
		$request = Application::getInstance()->getContext()->getRequest();
		$workID = $request->getQuery("work_id");
		$workStatus = $request->getQuery("status");
		if($workID && $workStatus == 'del'){
			$hlblock_id = 8; // id HL
			$hlblock = HL\HighloadBlockTable::getById($hlblock_id)->fetch();
			$entity = HL\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class = $entity->getDataClass();
			$data = ['UF_ACTIVE' => false];
			$result = $entity_data_class::update($workID,$data);
		}

		if ($companyID || $contentManager)
		{
			if ($contentManager)
				$arFilter = [];
			else
				$arFilter = ['UF_COMPANY'=>$companyID];

			$arWorks = getElHL(8,[],$arFilter,['*']); // ,'UF_ACTIVE'=>true
			$arResult['WORKS'] = $arWorks;
		}

		$arResult['SERVICES'] = getElHL(3,[],[],['ID','UF_NAME']);

		return $arResult;
	}

	public function executeComponent()
	{
		$this->arResult = array_merge($this->arResult,$this->profile());
		$this->includeComponentTemplate();
	}
}?>
