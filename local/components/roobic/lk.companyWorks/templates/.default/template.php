<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// dump($arResult);?>
<?$this->SetViewTarget('body-class');?>
	class="completed-work"
<?$this->EndViewTarget();?>
<div class="leads">
		<div class="leads__top">
			<div class="leads__top-title">Выполненные работы</div>
			<div class="leads__top-desc">Здесь вы видите Заказы, размещенные вами, текстовое описание</div>
		</div>
		<div class="leads__container">
			<div class="leads__items">
				<?foreach ($arResult['WORKS'] as $id => $work) {
					foreach ($work['UF_MATERIAL'] as $value)
						$arMaterials[] = $arResult['MATERIALS'][$value]['UF_NAME'];
				?>
					<div class="orders__item">
						<div class="orders__item-head">
							<a href="/lk/company_work?work_id=<?=$id?>" class="orders__item-title hidden-sm"><?=$work['UF_NAME']?></a>
							<div class="orders__item-address hidden-sm"><svg class="icon">
									<use xlink:href="#icon-address"></use>
								</svg> <?=$work['UF_ADDRESS']?></div>
						</div>
						<div class="orders__item-content">
							<div class="row">
								<div class="col-md-5">
									<a class="object__images" href="/lk/company_work?work_id=<?=$id?>">
										<div class="object__area"><?=$work['UF_AREA']?> м<sup>2</sup></div>
										<div class="object-slider swiper-container">
											<div class="swiper-wrapper">
												<?foreach ($work['UF_IMAGE'] as $photo) {
													$photoRes = CFile::ResizeImageGet($photo, array('width' => 880, 'height' => 548), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);?>
													<div class="swiper-slide"><img class="lazyload" src="" data-src="<?=$photoRes['src']?>" alt="Изображение"></div>
												<?}?>
											</div>
											<div class="swiper-pagination"></div>
											<div class="js-slider-img swiper-button-next" data-icon="5"></div>
											<div class="js-slider-img swiper-button-prev" data-icon="4"></div>
										</div>
									</a>
								</div>
								<div class="col-md-7">
									<div class="object__info">
										<a href="/lk/company_work?work_id=<?=$id?>" class="orders__item-title hidden-md visible-sm"><?=$work['UF_NAME']?></a>
										<div class="orders__item-address hidden-md visible-sm"><svg class="icon">
												<use xlink:href="#icon-address"></use>
											</svg> <?=$work['UF_ADDRESS']?></div>
										<div class="object__strong">
											<svg class="icon">
												<use xlink:href="#icon-home-2"></use>
											</svg><span><?=$arResult['SERVICES'][$work['UF_SERVICE']]['UF_NAME']?></span>
										</div>
										<div class="object__characteristics">
											<div class="object-characteristic">
												<div class="object-characteristic-lft">
													<div class="object-characteristic__icon">
														<svg class="icon">
															<use xlink:href="#icon-price"></use>
														</svg>
													</div>
													<div class="object-characteristic__name">Стоимость:</div>
												</div>
												<div class="object-characteristic__value"><strong><?=formatPrice($work['UF_PRICE'])?> ₽</strong></div>
											</div>
											<div class="object-characteristic">
												<div class="object-characteristic-lft">
													<div class="object-characteristic__icon">
														<svg class="icon">
															<use xlink:href="#icon-area"></use>
														</svg>
													</div>
													<div class="object-characteristic__name">Площадь:</div>
												</div>
												<div class="object-characteristic__value"><strong><?=$work['UF_AREA']?> м<sup>2</sup></strong></div>
											</div>
											<div class="object-characteristic">
												<div class="object-characteristic-lft">
													<div class="object-characteristic__icon">
														<svg class="icon">
															<use xlink:href="#icon-deadlines"></use>
														</svg>
													</div>
													<div class="object-characteristic__name">Сроки:</div>
												</div>
												<div class="object-characteristic__value"><strong><?=$work['UF_DEADLINE']?></strong></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="orders__item-btn">
							<div class="orders__item__icon-items">
								<a href="/lk/company_work?work_id=<?=$id?>" class="orders__item__icon-item">
									<span class="orders__item__ico">
										<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
											<g>
												<g>
													<g>
														<g>
															<polygon points="0,405.3 0,512 106.7,512 421.5,197.2 314.8,90.5                 " />
															<path d="M503.7,74.7L437.3,8.3c-11.1-11.1-29.2-11.1-40.3,0l-52.1,52.1l106.7,106.7l52.1-52.1C514.8,103.9,514.8,85.8,503.7,74.7z" />
														</g>
													</g>
												</g>
											</g>
										</svg>
									</span>
									<span class="orders__item__ico-name">Редактировать</span>
								</a>
								<a href="/lk/company_works?work_id=<?=$id?>&status=del" class="orders__item__icon-item">
									<span class="orders__item__ico">
										<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 448 512" style="enable-background:new 0 0 448 512;" xml:space="preserve">
											<path d="M32,448c0,35.3,28.7,64,64,64h256c35.3,0,64-28.7,64-64V128H32L32,448z" />
											<path d="M288,32V0H160v32H0v64h448V32H288z" />
										</svg>
									</span>
									<span class="orders__item__ico-name">Снять с публикации</span>
								</a>
							</div>
						</div>
					</div>
					<?unset($arMaterials);
				}?>
			</div>
			<div class="form-bottom btn__send">
				<a href="/lk/company_work" class="btn">Добавить работу</a>
			</div>
		</div>
	</div>
