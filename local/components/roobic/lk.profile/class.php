<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Application;

class company extends CBitrixComponent
{
	function profile()
	{
		global $USER;
		$userID = $USER->GetID();

		$by = 'id'; $sort = 'asc'; // поле сортировки
	  $filter = ['ID' => $userID];
	  $arParams = [ // наши поля
			'FIELDS' => ['ID','NAME','EMAIL','PERSONAL_PHONE']
	  ];
	  $rsUsers = CUser::GetList($by, $sort, $filter, $arParams);
	  $arUser = $rsUsers->GetNext(); // dump($arUser);
		$arResult['USER'] = $arUser;

		$request = Application::getInstance()->getContext()->getRequest();
		$new_password = $request->getPost("new_password");
		$confirm_password = $request->getPost("confirm_password");
		// dump($_REQUEST);
		if($new_password && $new_password == $confirm_password){ // если форма отправлена
			// обновление пользователя
			$user = new CUser;
			$arFieldsUser = Array(
				'PASSWORD' => $new_password,
				'CONFIRM_PASSWORD' => $confirm_password,
			);

			if($user->Update($userID,$arFieldsUser))
				$arResult['USER'] = $arFieldsUser;
			else
				echo $user->LAST_ERROR;
		}

		return $arResult;
	}

	public function executeComponent()
	{
		$this->arResult = array_merge($this->arResult,$this->profile());
		$this->includeComponentTemplate();
	}
}?>
