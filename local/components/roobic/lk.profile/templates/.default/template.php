<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// dump($arResult);?>
<?$this->SetViewTarget('body-class');?>
	class="clients_my-orders"
<?$this->EndViewTarget();?>
<div class="lk">
    <div class="lk__header">
      <div class="lk__header-title"><?=(CSite::InGroup([5])) ? 'Профиль исполнителя':'Профиль заказчика'?></div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="lk-form-block-title">Контактные данные</div>
      </div>
      <div class="col-md-6">
        <div class="lk-form-block">
          <label for="user_name">
            <input class="form-input change_focusout" type="text" value="<?=$arResult['USER']['NAME']?>" placeholder="ФИО" name="user_name" id="user_name">
            <span class="placeholder">ФИО</span>
            <a href="#" class="edit-input">
              <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
            </a>
          </label>
        </div>
        <div class="lk-form-block">
          <label for="user_email">
            <input class="form-input change_focusout" type="email" value="<?=$arResult['USER']['EMAIL']?>" placeholder="Адрес электронной почты" name="user_email" id="user_email">
            <span class="placeholder">Адрес электронной почты</span>
            <a href="#" class="edit-input">
              <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
            </a>
          </label>
        </div>
      </div>
      <div class="col-md-6">
        <div class="lk-form-block">
          <label for="user_phone change_focusout">
            <input class="form-input" type="tel" value="<?=$arResult['USER']['PERSONAL_PHONE']?>" placeholder="Номер телефона" name="user_phone" id="user_phone">
            <span class="placeholder">Номер телефона</span>
            <a href="#" class="edit-input">
              <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
            </a>
          </label>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="lk-form-block-title">Безопасность</div>
				<form action="" method="post" enctype="multipart/form-data" id="changePassword">
	        <div class="password">
	          <div class="row">
	            <div class="col-md-5 col-lg-5">
	              <div class="lk-form-block place_show">
	                <label for="">
	                  <input class="form-input" type="password" name="new_password" placeholder="Новый пароль" required>
	                </label>
	              </div>
	            </div>
	          </div>
	          <div class="row">
	            <div class="col-md-5 col-lg-5">
	              <div class="lk-form-block place_show">
	                <label for="">
	                  <input class="form-input" type="password" name="confirm_password" placeholder="Повторить пароль" required>
	                </label>
	              </div>
	            </div>
	          </div>
	        </div>
	        <div class="lk-form-block">
						<!-- <input type="submit" name="changePassword" value="Изменить пароль" class="btn edit-password"> -->
	          <a href="#" class="btn edit-password">Изменить пароль</a>
	          <!-- <a href="#" class="btn edit-password on">Изменить пароль</a> -->
	        </div>
				</form>
      </div>
    </div>
  </div>
