<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// dump($arResult);?>
<?$this->SetViewTarget('body-class');?>
	class="clients_my-orders"
<?$this->EndViewTarget();?>
<div class="leads">
		<div class="leads__top">
			<div class="leads__top-title">Объявления</div>
		</div>
		<div class="leads__container">
			<div class="leads__items">
				<?foreach ($arResult['ORDERS'] as $order) {
					$off = ($order['ACTIVE'] == 'N') ? 'off' : '';
					$cntResponse = count($arResult['RESPONSES'][$order['ID']]);
					$cntResponse = ($cntResponse) ? $cntResponse.' заявок' : 'нет заявок' ;
					$contract = ($order['PROPERTY_CONTRACT_ENUM_ID']) ? 'Исполнитель выбран' : $cntResponse;?>
					<div class="orders__item <?=$off?>">
						<div class="orders__item-head">
							<a href="/lk/client_ad?order_id=<?=$order['ID']?>" class="orders__item-title"><?=$order['NAME']?></a>
							<?if($off){?><span class="orders__item-status">В архиве</span><?}?>
							<a href="/lk/client_ad?order_id=<?=$order['ID']?>" class="orders__item-cat"><?=implode(' | ',$arResult['PRICE_LIST'][$order['ID']])?></a>
							<div class="order__head-ico d-block d-md-none">
								<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/<?=getIconMainSection($arResult['ICONS'][$order['ID']])?>">
							</div>
						</div>
						<div class="orders__item-content">
							<div class="orders__item-ico d-none d-md-block d-lg-block">
								<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/<?=getIconMainSection($arResult['ICONS'][$order['ID']])?>">
							</div>
							<div class="orders__item-desc">
								<?=$order['DETAIL_TEXT']?>
							</div>
						</div>
						<div class="orders__item-bottom">
							<div class="orders__item-place"><?=$order['PROPERTY_ADDRESS_VALUE']?></div>
						</div>
						<div class="orders__item-btn">
							<!-- <div class="orders__item-stat"><a href="#"><?=$contract?></a></div> -->
							<div class="orders__item__icon-items">
								<?if($off){ // если закрыт?>
									<a href="/lk/client_ads?order_id=<?=$order['ID']?>&active=Y" class="orders__item__icon-item">
										<span class="orders__item__ico">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
												<g>
													<g>
														<g>
															<g>
																<polygon class="st0" points="0,405.3 0,512 106.7,512 421.5,197.2 314.8,90.5                 " />
																<path class="st0" d="M503.7,74.7L437.3,8.3c-11.1-11.1-29.2-11.1-40.3,0l-52.1,52.1l106.7,106.7l52.1-52.1C514.8,103.9,514.8,85.8,503.7,74.7z" />
															</g>
														</g>
													</g>
												</g>
											</svg>
										</span>
										<span class="orders__item__ico-name">Восстановить</span>
									</a>
									<a href="/lk/client_ads?order_id=<?=$order['ID']?>&del=y" class="orders__item__icon-item">
										<span class="orders__item__ico">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 448 512" style="enable-background:new 0 0 448 512;" xml:space="preserve">
												<path class="st0" d="M32,448c0,35.3,28.7,64,64,64h256c35.3,0,64-28.7,64-64V128H32L32,448z" />
												<path class="st0" d="M288,32V0H160v32H0v64h448V32H288z" />
											</svg>
										</span>
										<span class="orders__item__ico-name">Удалить</span>
									</a>
								<?}else{?>
									<a href="/obyavleniya/<?=$order['CODE']?>/" class="orders__item__icon-item" target="_blank">
										<span class="orders__item__ico">
											<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 18 18" style="enable-background:new 0 0 18 18;" xml:space="preserve">
												<g id="settings"></g>
												<g id="Слой_2">
													<path class="st0" d="M16,9c-0.6,0-1,0.4-1,1v5c0,0.6-0.4,1-1,1H3c-0.6,0-1-0.4-1-1V4c0-0.6,0.4-1,1-1h5c0.6,0,1-0.4,1-1c0-0.6-0.4-1-1-1H3C1.3,1,0,2.3,0,4v11c0,1.7,1.3,3,3,3h11c1.7,0,3-1.3,3-3v-5C17,9.4,16.5,9,16,9z" />
													<path class="st0" d="M17.9,0.6L17.9,0.6c-0.1-0.3-0.3-0.4-0.6-0.6C17.2,0,17.1,0,17,0l-3.7,0c-0.3,0-0.5,0.1-0.7,0.3S12.3,0.7,12.3,1c0,0.3,0.1,0.5,0.3,0.7C12.7,1.9,13,2,13.3,2c0,0,0,0,0,0h1.3l-3.7,3.7c-0.2,0.2-0.4,0.4-0.3,0.7c0,0.3,0.1,0.5,0.3,0.7c0.1,0.1,0.3,0.3,0.7,0.3c0.2,0,0.5-0.1,0.7-0.3L16,3.4v1.3c0,0.2,0.1,0.5,0.2,0.7l0,0c0,0,0,0,0,0l0.1,0.1c0.2,0.2,0.4,0.2,0.6,0.2c0.3,0,0.5-0.1,0.7-0.3C17.9,5.3,18,5,18,4.8L18,1C18,0.9,18,0.8,17.9,0.6z" />
												</g>
												<g id="Слой_3"></g>
											</svg>
										</span>
										<span class="orders__item__ico-name">Открыть</span>
									</a>
									<a href="/lk/client_ad?order_id=<?=$order['ID']?>" class="orders__item__icon-item">
										<span class="orders__item__ico">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
												<g>
													<g>
														<g>
															<g>
																<polygon class="st0" points="0,405.3 0,512 106.7,512 421.5,197.2 314.8,90.5                 " />
																<path class="st0" d="M503.7,74.7L437.3,8.3c-11.1-11.1-29.2-11.1-40.3,0l-52.1,52.1l106.7,106.7l52.1-52.1C514.8,103.9,514.8,85.8,503.7,74.7z" />
															</g>
														</g>
													</g>
												</g>
											</svg>
										</span>
										<span class="orders__item__ico-name">Редактировать</span>
									</a>
									<a href="/lk/client_ads?order_id=<?=$order['ID']?>&active=N" class="orders__item__icon-item">
										<span class="orders__item__ico">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 448 512" style="enable-background:new 0 0 448 512;" xml:space="preserve">
												<path class="st0" d="M32,448c0,35.3,28.7,64,64,64h256c35.3,0,64-28.7,64-64V128H32L32,448z" />
												<path class="st0" d="M288,32V0H160v32H0v64h448V32H288z" />
											</svg>
										</span>
										<span class="orders__item__ico-name">Снять с публикации</span>
									</a>
								<?}?>
							</div>
						</div>
					</div>
				<?}?>
			</div>
			<?//if(CSite::InGroup([6])): // если заказчик?>
			<div class="form-bottom btn__send">
				<a href="/lk/client_ad" class="btn">Разместить объявление</a>
			</div>
			<?//endif;?>
		</div>
	</div
