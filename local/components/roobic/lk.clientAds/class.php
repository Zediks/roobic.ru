<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Application;

class company extends CBitrixComponent
{
	function profile()
	{
		global $USER;
		$userID = $USER->GetID();
		$iblockID = 5;

		// изменение статуса
		$request = Application::getInstance()->getContext()->getRequest();
		$orderID = $request->getQuery("order_id");
		// $orderStatus = $request->getQuery("status");
		// if($orderID && $orderStatus)
		// 	CIBlockElement::SetPropertyValues($orderID, $iblockID, $orderStatus, "STATUS");
		$orderActive = $request->getQuery("active");
		if ($orderID && $orderActive)
		{
			$orderElement = new CIBlockElement;
			$orderElement->Update($orderID,['ACTIVE'=>$orderActive]);
		}

		// удаление
		$orderDelete = $request->getQuery("del");
		if ($orderID && $orderDelete == 'y')
			CIBlockElement::Delete($orderID);

		// получим сами заказы
		$arOrder = Array('SORT'=>'ASC');
		$arFilter = Array('IBLOCK_ID'=>$iblockID,'PROPERTY_USER'=>$userID);
		$arSelect = Array('ID','ACTIVE','NAME','CODE','DETAIL_TEXT','PROPERTY_ADDRESS','PROPERTY_STATUS','PROPERTY_CONTRACT');
		$rsElements = CIBlockElement::GetList($arOrder,$arFilter,false,false,$arSelect);
		while($arElement = $rsElements->GetNext()) // dump($arElement);
		{
			$arOrderId[] = $arElement['ID'];
			$arResult['ORDERS'][] = $arElement;
		}

		if ($arOrderId)
		{
			// получим отклики на заказы
			$arHL = getElHL(12,[],['UF_ORDER'=>$arOrderId],['*']); // dump($arHL);
			foreach ($arHL as $value)
				$arResult['RESPONSES'][$value['UF_ORDER']][] = $value['UF_COMPANY'];

			// получим Услуги и Цены
			$priceList = getElHL(4,[],['UF_AD'=>$arOrderId],['ID','UF_SERVICE','UF_PRICE','UF_AD']);
			foreach ($priceList as $value)
				$arIdsService[] = $value['UF_SERVICE'];

			// получим услуги
			$arService = getElHL(3,[],['ID'=>$arIdsService],['ID','UF_NAME','UF_MAIN_SECTION']);

			foreach ($priceList as $value)
			{
				$arResult['PRICE_LIST'][$value['UF_AD']][] = $arService[$value['UF_SERVICE']]['UF_NAME'].' от '.$value['UF_PRICE'].' ₽/м2';
				$arResult['ICONS'][$value['UF_AD']] = $arService[$value['UF_SERVICE']]['UF_MAIN_SECTION'];
			}
		}

		return $arResult;
	}

	public function executeComponent()
	{
		$this->arResult = array_merge($this->arResult,$this->profile());
		$this->includeComponentTemplate();
	}
}?>
