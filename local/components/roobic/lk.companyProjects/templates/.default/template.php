<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// dump($arResult);?>
<?$this->SetViewTarget('body-class');?>
	class="completed-work"
<?$this->EndViewTarget();?>
<div class="leads">
		<div class="leads__top">
			<div class="leads__top-title">Проекты</div>
			<div class="leads__top-desc">Здесь вы видите Проекты, размещенные вами, текстовое описание</div>
		</div>
		<div class="leads__container">
			<div class="leads__items">
				<?foreach ($arResult['PROJECTS'] as $id => $project) {?>
					<div class="orders__item">
						<div class="orders__item-head">
							<a href="/lk/company_project?project_id=<?=$project['ID']?>" class="orders__item-title hidden-sm">Проект дома <?=$project['NAME']?></a>
							<div class="orders__item-desc hidden-sm"><?=$arResult['TYPE_HOUSE'][$project['PROPERTY_TYPE_VALUE']]?></div>
						</div>
						<div class="orders__item-content">
							<div class="row">
								<div class="col-md-5">
									<a class="object__images" href="/lk/company_project?project_id=<?=$project['ID']?>">
										<div class="object-slider swiper-container">
											<div class="swiper-wrapper">
												<?foreach ($project['PROPERTY_PHOTO_VALUE'] as $photo) {
													$photoRes = CFile::ResizeImageGet($photo, array('width' => 880, 'height' => 548), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);?>
													<div class="swiper-slide"><img class="lazyload" src="" data-src="<?=$photoRes['src']?>" alt="Изображение"></div>
												<?}?>
											</div>
											<div class="swiper-pagination"></div>
											<div class="js-slider-img swiper-button-next" data-icon="5"></div>
											<div class="js-slider-img swiper-button-prev" data-icon="4"></div>
										</div>
									</a>
								</div>
								<div class="col-md-7">
									<a href="/lk/company_project?project_id=<?=$project['ID']?>" class="orders__item-title hidden-md visible-sm">Проект дома <?=$project['NAME']?></a>
									<div class="orders__item-desc hidden-md visible-sm"><?=$arResult['TYPE_HOUSE'][$project['PROPERTY_TYPE_VALUE']]?></div>
									<div class="row object__comfort-list">
										<div class="col-md-6 col-sm-6 proect__comfort-property"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/area_02.svg" alt=""><span>Площадь: <span><?=$project['PROPERTY_AREA_VALUE']?> м<sup>2</sup></span></span></div>
										<div class="col-md-6 col-sm-6 proect__comfort-property"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/area.svg" alt=""><span>Размер: <span><?=$project['PROPERTY_SIZE_VALUE']?></span></span></div>
										<div class="col-md-6 col-sm-6 proect__comfort-property"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/bed.svg" alt=""><span><?=$project['BEDROOM_TEXT']?></span></div>
										<div class="col-md-6 col-sm-6  proect__comfort-property"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/bath.svg" alt=""><span><?=$project['BATHROOM_TEXT']?></span></div>
									</div>
									<div class="row hidden-sm">
										<div class="col-md-6">
											<?foreach ($project['PROPERTY_PURPOSE_VALUE'] as $key => $value) {?>
												<div class="foryou">
													<div class="for"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/family_01.svg" alt="">
													</div>
													<div class="control__container">
														<div class="control__main">
															<strong>Дом подходит для:</strong>
															<?=$value?>
														</div>
													</div>
												</div>
											<?}?>
										</div>
										<div class="col-md-6">
											<div class="proect__specifications-value">
												<?if($project['PROPERTY_OLD_PRICE_VALUE']){?>
													<span class="old-price"><s><?=formatPrice($project['PROPERTY_OLD_PRICE_VALUE'])?> ₽</s></span>
												<?}?>
												<span class="new-price"><?=formatPrice($project['PROPERTY_PRICE_VALUE'])?> ₽</span></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="orders__item-btn">
							<div class="hidden-md">
								<?foreach ($project['PROPERTY_PURPOSE_VALUE'] as $key => $value) {?>
									<div class="foryou">
										<div class="for"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/family_01.svg" alt=""></div>
										<div class="control__container">
											<div class="control__main">
												<strong>Дом подходит для:</strong>
												<?=$value?>
											</div>
										</div>
									</div>
								<?}?>
								<div class="proect__specifications-value">
									<?if($project['PROPERTY_OLD_PRICE_VALUE']){?>
										<span class="old-price"><s><?=formatPrice($project['PROPERTY_OLD_PRICE_VALUE'])?> ₽</s></span>
									<?}?>
										<span class="new-price"><?=formatPrice($project['PROPERTY_PRICE_VALUE'])?> ₽</span></div>
							</div>
							<div class="orders__item__icon-items">
								<a href="/lk/company_project?project_id=<?=$project['ID']?>" class="orders__item__icon-item">
									<span class="orders__item__ico">
										<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
											<g>
												<g>
													<g>
														<g>
															<polygon  points="0,405.3 0,512 106.7,512 421.5,197.2 314.8,90.5                 " />
															<path  d="M503.7,74.7L437.3,8.3c-11.1-11.1-29.2-11.1-40.3,0l-52.1,52.1l106.7,106.7l52.1-52.1C514.8,103.9,514.8,85.8,503.7,74.7z" />
														</g>
													</g>
												</g>
											</g>
										</svg>
									</span>
									<span class="orders__item__ico-name">Редактировать</span>
								</a>
								<a href="/lk/company_projects?project_id=<?=$project['ID']?>&status=del" class="orders__item__icon-item">
									<span class="orders__item__ico">
										<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 448 512" style="enable-background:new 0 0 448 512;" xml:space="preserve">
											<path  d="M32,448c0,35.3,28.7,64,64,64h256c35.3,0,64-28.7,64-64V128H32L32,448z" />
											<path  d="M288,32V0H160v32H0v64h448V32H288z" />
										</svg>
									</span>
									<span class="orders__item__ico-name">Снять с публикации</span>
								</a>
							</div>
						</div>
					</div>
				<?}?>
			</div>
			<a href="/lk/company_project" class="btn">Добавить проект</a>
		</div>
	</div>