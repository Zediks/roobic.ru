<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Application;
use Bitrix\Main\Grid\Declension;


class company extends CBitrixComponent
{
	function profile()
	{
		global $USER;
		$userID = $USER->GetID();
		$iblockID = 3;
		$contentManager = CSite::InGroup([8]);

		// получим компанию
		$by = 'id'; $sort = 'asc'; // поле сортировки
	  $filter = ['ID' => $userID];
	  $arParams = [ // наши поля
			'FIELDS' => ['ID'],'SELECT' => ['UF_COMPANIES']
	  ];
	  $rsUsers = CUser::GetList($by, $sort, $filter, $arParams);
	  if($arUser = $rsUsers->Fetch())
			$companyID = $arUser['UF_COMPANIES'][0];

		// изменение активности
		$request = Application::getInstance()->getContext()->getRequest();
		$projectID = $request->getQuery("project_id");
		$projectStatus = $request->getQuery("status");
		if($projectID && $projectStatus == 'del'){
			$projectElement = new CIBlockElement;
			$arLoadProductArray = ['ACTIVE' => 'N'];
			$projectElement->Update($projectID,$arLoadProductArray);
		}

		$bedroomDeclension = new Declension('спальня', 'спальни', 'спален');
		$bathroomDeclension = new Declension('санузел', 'санузла', 'санузлов');

		if ($companyID || $contentManager)
		{
			// получим сами проекты
			$arOrder = Array('SORT'=>'ASC');

			if ($contentManager)
				$arFilter = Array('IBLOCK_ID'=>$iblockID);
			else
				$arFilter = Array('IBLOCK_ID'=>$iblockID,'PROPERTY_COMPANY'=>$companyID);

			$arSelect = Array('ID','NAME','PROPERTY_TYPE','PROPERTY_PHOTO','PROPERTY_AREA','PROPERTY_SIZE','PROPERTY_CNT_BEDROOM','PROPERTY_CNT_BATHROOM','PROPERTY_PURPOSE','PROPERTY_OLD_PRICE','PROPERTY_PRICE'); // ,'PROPERTY_'
			$rsElements = CIBlockElement::GetList($arOrder,$arFilter,false,false,$arSelect);
			while($arElement = $rsElements->Fetch()) { // dump($arElement);

				// выводим правильные окончания
				$cntBedroom = $arElement['PROPERTY_CNT_BEDROOM_VALUE'];
				if (!$cntBedroom) $cntBedroom = 0;
				$bedroomText = $bedroomDeclension->get($cntBedroom);
				$bedroomText = ($cntBedroom > 0) ? $cntBedroom.' '.$bedroomText : 'Нет спален';
				$arElement['BEDROOM_TEXT'] = $bedroomText;

				// санузлы
				$cntBathroom = $arElement['PROPERTY_CNT_BATHROOM_VALUE'];
				if (!$cntBathroom) $cntBathroom = 0;
				$bathroomText = $bathroomDeclension->get($cntBathroom);
				$bathroomText = ($cntBathroom > 0) ? $cntBathroom.' '.$bathroomText : 'Нет санузлов';
				$arElement['BATHROOM_TEXT'] = $bathroomText;

				$xmlIdTypes[] = $arElement['PROPERTY_TYPE_VALUE'];

				$arResult['PROJECTS'][] = $arElement;
			}
		}

		$typesHouse = getElHL(11,[],['UF_XML_ID'=>$xmlIdTypes],['ID','UF_NAME','UF_XML_ID']);
		foreach ($typesHouse as $key => $value)
			$arResult['TYPE_HOUSE'][$value['UF_XML_ID']] = $value['UF_NAME'];

		// dump($arResult['TYPE_HOUSE']);

		return $arResult;
	}

	public function executeComponent()
	{
		$this->arResult = array_merge($this->arResult,$this->profile());
		$this->includeComponentTemplate();
	}
}?>
