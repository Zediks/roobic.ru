<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
	Loader::includeModule('iblock');
	Loader::includeModule('highloadblock');
use Bitrix\Main\Application;
use Bitrix\Highloadblock as HL, Bitrix\Main\Entity;
use Bitrix\Main\Page\Asset;

// yandex_map
Asset::getInstance()->addJs("https://api-maps.yandex.ru/2.1/?lang=ru_RU&apikey=40336fa1-cf4d-44d7-a4fa-8c7ac8e9f5a5
");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/yandex_map.js");

class company extends CBitrixComponent
{
	function editWork()
	{
		global $USER;
		$userID = $USER->GetID();
		$iblockID = 2;
		$contentManager = CSite::InGroup([8]);

		// получим компанию для привязки
		$by = 'id'; $sort = 'asc'; // поле сортировки
	  $filter = ['ID' => $userID];
	  $arParams = [ // наши поля
			'FIELDS' => ['ID'],'SELECT' => ['UF_COMPANIES']
	  ];
	  $rsUsers = CUser::GetList($by, $sort, $filter, $arParams);
	  if($arUser = $rsUsers->GetNext()) // dump($arUser);
			$companyID = $arUser['UF_COMPANIES'][0];

		if ($companyID && !$contentManager)
		{
			$arOrder = Array('SORT'=>'ASC');
			$arFilter = Array('IBLOCK_ID'=>$iblockID,'ID'=>$companyID);
			$arSelect = Array('ID','PROPERTY_COMPLETED_WORK'); // PROPERTY_
			$rsElements = CIBlockElement::GetList($arOrder,$arFilter,false,false,$arSelect);
			if($arElement = $rsElements->GetNext()) // dump($arElement);
				$arComletedWork = $arElement['PROPERTY_COMPLETED_WORK_VALUE'];
		}

		// получим услуги
		$arService = getElHL(3,[],[],['ID','UF_MAIN_SECTION','UF_SUBSECTION','UF_NAME']);
		$arServicesMain = getListProperty([],["USER_FIELD_ID" => 72]); // Главный раздел
		$arServicesSub = getListProperty([],["USER_FIELD_ID" => 73]); // Подраздел

		// сформируем услуги
		foreach ($arService as $value) {
			$mainSectionID = $value['UF_MAIN_SECTION'];
			$subSectionID = $value['UF_SUBSECTION'];
			$mainSection = $arServicesMain[$mainSectionID];
			$subSection = $arServicesSub[$subSectionID];
			$arResult['SERVICES'][$mainSectionID]['NAME'] = $mainSection;
			$arResult['SERVICES'][$mainSectionID]['ITEMS'][$subSectionID]['NAME'] = $subSection;
			$arResult['SERVICES'][$mainSectionID]['ITEMS'][$subSectionID]['ITEMS'][$value['ID']] = [
				'NAME' => $value['UF_NAME'],
			];
		}

		// $arResult['DEADLINE'] = getListProperty([],["USER_FIELD_ID" => 70]);

		$request = Application::getInstance()->getContext()->getRequest();
		$workID = $request->getQuery("work_id");
		$sendFrom = $request->getPost("sendFrom");

		if($sendFrom){ // если форма отправлена

			$hlblock_id = 8; // id HL
			$hlblock = HL\HighloadBlockTable::getById($hlblock_id)->fetch();
			$entity = HL\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class = $entity->getDataClass();

			$filesPhotos = $request->getFile("work_photos");
			foreach ($filesPhotos as $key => $arPhoto) { // сформируем для добавления
				foreach ($arPhoto as $key2 => $val2) {
					$arPhotos[$key2][$key] = $val2;
				}
			}

			// удаление файлов
			$delFile = $request->getPost("del_file");
			if ($delFile) $arDelFile = explode(',',$delFile);

			$ourFile = $request->getPost("our_file");
			if($ourFile || $delFile){
				$arOurFile = explode(',',$ourFile);
				foreach ($arOurFile as $key => $value) {
					if (!in_array($value,$arDelFile)) $arPhotos[$key] = CFile::MakeFileArray($value);
				}
			}

			$data = [
				"UF_NAME" => $request->getPost('work_name'),
				"UF_ADDRESS" => $request->getPost('work_address'),
				"UF_SERVICE" => $request->getPost('work_service'),
				"UF_PRICE" => $request->getPost('work_price'),
				"UF_AREA" => $request->getPost('work_area'),
				"UF_DEADLINE" => $request->getPost('work_deadline'),
				// "UF_SHOW_OBJECT" => $request->getPost('work_show'),
				"UF_IMAGE" => $arPhotos,
				// "UF_COMPANY" => $companyID,
				// "UF_ACTIVE" => true,
				"UF_SHOW" => $request->getPost('work_show'),
			]; // dump($data);

			if ($workID) { // обновим
				$result = $entity_data_class::update($workID,$data);
			} elseif ($companyID) { // добавим
				$data["UF_COMPANY"] = $companyID;
				$data["UF_ACTIVE"] = false;
				$result = $entity_data_class::add($data);
				$resultID = $result->getId();
				if($resultID > 0){
					$xmlID = 'company_id_'.$companyID.'_el_id_'.$resultID;
					$data['UF_XML_ID'] = $xmlID;
					$entity_data_class::update($resultID,$data); // обновим UF_XML_ID
					// положим в массив выполненных работ
					array_push($arComletedWork,$xmlID); // dump($arComletedWork);

					CIBlockElement::SetPropertyValues($companyID, $iblockID, $arComletedWork, "COMPLETED_WORK");
				}
			}
			header('Location: /lk/company_works');
		}

		if($workID){ // получим нашу работу
			$arResult['WORK'] = getElHL(8,[],['ID'=>$workID],['*'])[$workID];

			if (($arResult['WORK']['UF_COMPANY'] != $companyID) && !$contentManager) die('Доступ запрещен!');

			foreach ($arResult['WORK']['UF_IMAGE'] as $key => $value) { // для отображения фото
				$arResult['WORK']['PHOTOS'][$key] = CFile::ResizeImageGet($value, array('width'=>488, 'height'=>360), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
			} // dump($arResult['WORK']);
		}

    return $arResult;
	}

	public function executeComponent()
	{
		$this->arResult = array_merge($this->arResult,$this->editWork());
		$this->includeComponentTemplate();
	}
}?>
