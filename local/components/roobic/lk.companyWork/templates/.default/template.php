<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// dump($arResult);?>
<?$this->SetViewTarget('body-class');?>
	class="lk-page"
<?$this->EndViewTarget();?>
<div class="completed__wrap">
  <div class="row">
    <div class="col-lg-8">
      <div class="completed__block-name d-none d-xs-block d-sm-block d-md-hide">
        <div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_10.svg" alt=""></div>
        <strong>Расскажите о выполненной работе</strong>
        <div class="completed__block-desc">
          Покажите реальные примеры работ заказчику
        </div>
      </div>
      <div class="completed__container">
        <form action="" method="post" enctype="multipart/form-data">
          <div class="form-main-block">
            <div class="form-block">
              <div class="form-block__name hide-xs">Расскажите о выполненной работе</div>
              <label for="">
                <input class="form-input" type="text" name="work_name" value="<?=$arResult['WORK']['UF_NAME']?>" placeholder="Заголовок. Например. Строительство 1-этажного дома из бруса под ключ*">
                <span class="placeholder">Заголовок. Например. Строительство 1-этажного дома из бруса под ключ*</span>
                <a href="#" class="edit-input">
                  <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
                </a>
              </label>
            </div>
          </div>
          <div class="form-block">
            <label for="input_address">
              <input class="form-input" type="text" name="work_address" value="<?=$arResult['WORK']['UF_ADDRESS']?>" placeholder="Адрес объекта*" id="input_address">
              <span class="placeholder">Адрес объекта*</span>
              <a href="#" class="edit-input">
                <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
              </a>
            </label>
          </div>
          <div class="form-map" id="yandex_map"></div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-block">
                <!-- <label class="select" for="material_m">
                  <select class="select__select select__styler" name="work_material[]" id="material_m" title="Выберите материалы*" multiple>
                    <option value="">Выберите материалы*</option>
										<?foreach ($arResult['MATERIALS'] as $key => $value) {?>
				              <option value="<?=$key?>" <?if(in_array($key,$arResult['WORK']['UF_MATERIAL']))echo'selected';?>><?=$value['UF_NAME']?></option>
				            <?}?>
                  </select>
                </label> -->
								<label class="select" for="work_service">
									<select class="select__select select__styler" name="work_service" id="work_service" title="Выберите категорию деятельности*" data-live-search="true">
										<option value="">Выберите категорию деятельности*</option>
										<?foreach ($arResult['SERVICES'] as $mainSectionID => $mainSection) {
												foreach ($mainSection['ITEMS'] as $subSectionID => $subSection)
												{
													if ($subSectionID) echo '<optgroup data-main="'.$mainSectionID.'" label="'.$subSection['NAME'].'">';
														foreach ($subSection['ITEMS'] as $key => $value) {?>
															<option value="<?=$key?>" data-main="<?=$mainSectionID?>" <?if($key == $arResult['WORK']['UF_SERVICE']) echo 'selected';?>><?=$value['NAME']?></option>
														<?}
													if ($subSectionID) echo '</optgroup>';
												}
											}?>
									</select>
								</label>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-block">
                <label for="">
                  <input class="form-input" type="text" name="work_price" value="<?=$arResult['WORK']['UF_PRICE']?>" placeholder="Стоимость за работу, ₽*">
                  <span class="placeholder">Стоимость за работу, ₽*</span>
                  <a href="#" class="edit-input">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
                  </a>
                </label>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-block">
                <label for="">
                  <input class="form-input" type="text" name="work_area" value="<?=$arResult['WORK']['UF_AREA']?>" placeholder="Площадь или объем работ*">
                  <span class="placeholder">Площадь или объем работ*</span>
                  <a href="#" class="edit-input">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
                  </a>
                </label>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-block">
                <!-- <label class="select" for="work_deadline">
                  <select class="select__select select__styler" name="work_deadline" id="work_deadline" title="Выберите сроки*">
                    <option value="">Выберите сроки*</option>
										<?foreach ($arResult['DEADLINE'] as $key => $value) {?>
				              <option value="<?=$key?>" <?if($key == $arResult['WORK']['UF_DEADLINE'])echo'selected';?>><?=$value?></option>
				            <?}?>
                  </select>
                </label> -->
								<label for="">
                  <input class="form-input" type="text" name="work_deadline" value="<?=$arResult['WORK']['UF_DEADLINE']?>" placeholder="Укажите сроки*">
                  <span class="placeholder">Укажите сроки*</span>
                  <a href="#" class="edit-input">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
                  </a>
                </label>
              </div>
            </div>
          </div>
          <div class="form-main-block">
            <div class="form-block">
              <div class="form-block__name">Фото с объекта</div>
              <div class="form-block__desc">Загрузите фото с объекта</div>
							<div class="download-photo">
								<?for ($i=0; $i < 3; $i++) {?>
									<div class="list">
										<label class="label-file <?if($arResult['WORK']['PHOTOS'][$i])echo'has-img'?>">
											<input type="file" accept=".png, .jpg, .jpeg" name="work_photos[]" value="<?=$arResult['WORK']['PHOTOS'][$i]['src']?>">
											<span>Добавьте файл для загрузки</span>
											<span>в это поле</span>
											<img src="<?=$arResult['WORK']['PHOTOS'][$i]['src']?>" alt="">
										</label>
										<span class="delete-file" data-id="<?=$arResult['WORK']['UF_IMAGE'][$i]?>">х</span>
									</div>
								<?}?>
								<input type="hidden" name="del_file" value="">
								<input type="hidden" name="our_file" value="<?=implode(',',$arResult['WORK']['UF_IMAGE'])?>">
							</div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-question">
                <span>Вы можете показать клиенту данный объект?</span>
								<?if($arResult['WORK']['UF_SHOW'])
										$yesActive = 'class="active"';
									else
										$noActive = 'class="active"';
								?>
                <a href="#" <?=$yesActive?> data-val="1">Да</a>
                <a href="#" <?=$noActive?> data-val="0">Нет</a>
								<input type="hidden" name="work_show" value="<?=$arResult['WORK']['UF_SHOW']?>">
              </div>
              <div class="form-bottom">
                <!-- <a href="#" class="btn off">Сохранить</a> -->
								<input type="submit" name="sendFrom" value="Сохранить" class="btn">
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    <div class="col-md-3 d-none d-lg-block d-xl-block">
      <div class="completed__block-name">
        <div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_10.svg" alt=""></div><strong>Расскажите о выполненной работе </strong>
        <div class="completed__block-desc">
          Покажите реальные примеры работ заказчику
        </div>
      </div>
    </div>
  </div>
</div>
