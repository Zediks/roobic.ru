<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
  Loader::includeModule("iblock");

class company extends CBitrixComponent
{
	function profile()
	{
		global $USER;
		$userID = $USER->GetID();
		$iblockID = 2;
		$contentManager = CSite::InGroup([8]);

		// изменение статуса
		$request = Application::getInstance()->getContext()->getRequest();
		$orderID = $request->getQuery("company_id");
		// $orderStatus = $request->getQuery("status");
		// if($orderID && $orderStatus)
		// 	CIBlockElement::SetPropertyValues($orderID, $iblockID, $orderStatus, "STATUS");
		$orderActive = $request->getQuery("active");
		if ($orderID && $orderActive)
		{
			$orderElement = new CIBlockElement;
			$orderElement->Update($orderID,['ACTIVE'=>$orderActive]);
		}

    $arServiceElHL = getElHL(3,[],[],['ID','UF_MAIN_SECTION','UF_SUBSECTION','UF_NAME','UF_XML_ID']);
    foreach ($arServiceElHL as $value) {
      $arService[$value['UF_XML_ID']] = $value['UF_NAME'];
    }

		// получим сами компании
		$arOrder = Array('SORT'=>'ASC');

		if ($contentManager)
			$arFilter = Array('IBLOCK_ID'=>$iblockID, 'ACTIVE'=>'Y');
		else
			$arFilter = Array('IBLOCK_ID'=>$iblockID,'PROPERTY_COMPANY'=>$companyID);

		$arSelect = Array('ID','NAME','PREVIEW_PICTURE','PROPERTY_PHOTO','PROPERTY_ADDRESS','PROPERTY_PRICE','PROPERTY_SERVICES');
		$rsElements = CIBlockElement::GetList($arOrder,$arFilter,false,false,$arSelect);
		while($arElement = $rsElements->Fetch())
		{ // dump($arElement);
			$arIds[] = $arElement['ID'];

      $arElement['PHOTOS'] = $arElement['PROPERTY_PHOTO_VALUE'];
      array_unshift($arElement['PHOTOS'],$arElement['PREVIEW_PICTURE']);

      foreach ($arElement['PROPERTY_SERVICES_VALUE'] as $value)
        $arElement['SERVICES'][] = $arService[$value];

			$arResult['COMPANIES'][$arElement['ID']] = $arElement;
		}

		if ($arIds)
		{
      $arOrder = ['SORT'=>'ASC'];
      $arFilter = ['IBLOCK_ID'=>3,'ACTIVE'=>'Y','PROPERTY_COMPANY' => $arIds];
      $arSelect = ['ID','PROPERTY_COMPANY'];
      $rsElements = CIBlockElement::GetList($arOrder,$arFilter,false,false,$arSelect);
      while ($arElement = $rsElements->Fetch())
      	$arResult['COMPANIES'][$arElement['PROPERTY_COMPANY_VALUE']]['WORKS'][] = $arElement['ID'];
		}
    
		return $arResult;
	}

	public function executeComponent()
	{
		$this->arResult = array_merge($this->arResult,$this->profile());
		$this->includeComponentTemplate();
	}
}?>
