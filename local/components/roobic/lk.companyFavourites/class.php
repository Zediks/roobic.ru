<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Application;

class company extends CBitrixComponent
{
	function favourites()
	{

		if(isset($_COOKIE['favorites_orders'])){

			$arFavorites = explode('-',$_COOKIE['favorites_orders']);

			$arOrder = Array("SORT"=>"ASC");
			$arFilter = Array("IBLOCK_ID"=>4,"ID"=>$arFavorites);
			$arSelect = Array("ID","NAME","ACTIVE_FROM","PREVIEW_TEXT","DETAIL_PAGE_URL","PROPERTY_BUDGET","PROPERTY_STATUS","PROPERTY_ADDRESS","PROPERTY_SERVICE");
			$rsElements = CIBlockElement::GetList($arOrder,$arFilter,false,false,$arSelect);
			while($arElement = $rsElements->GetNext()){ // dump($arElement);

				$arElement['COMPARISON'] = (in_array($arElement['ID'],$arComparison)) ? 'Y' : 'N';
			  $arElement['FAVORITES'] = (in_array($arElement['ID'],$arFavorites)) ? 'Y' : 'N';

				$arResult['FAVORITES_ORDERS'][] = $arElement;

				$arServicesXML[] = $arElement['PROPERTY_SERVICE_VALUE'];
			}

			// получим услуги
			$arServices = getElHL(3,[],['UF_XML_ID' => $arServicesXML],['ID','UF_NAME','UF_XML_ID']);
			foreach ($arServices as $key => $value) {
				$arResult['SERVICES'][$value['UF_XML_ID']] = $value;
			}
		}

		return $arResult;
	}

	public function executeComponent()
	{
		$this->arResult = array_merge($this->arResult,$this->favourites());
		$this->includeComponentTemplate();
	}
}?>
