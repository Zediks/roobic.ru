<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// dump($arResult);?>
<?$this->SetViewTarget('body-class');?>
	class="myfavourites-page"
<?$this->EndViewTarget();?>
<div class="col-md-12 col-lg-8 d-lg-block">
	<div class="leads">
		<div class="leads__container">
			<div class="leads__items">
				<?if(count($arResult['FAVORITES_ORDERS']) > 0) {
					foreach ($arResult['FAVORITES_ORDERS'] as $arItem) {

						$comp_active = ($arItem['COMPARISON'] == 'Y') ? 'control--favorites--active' : '';
						$fav_active = ($arItem['FAVORITES'] == 'Y') ? 'control--favorites--active' : '';
						$comp_text = ($arItem['COMPARISON'] == 'Y') ? 'Удалить из сравнения' : 'Добавить к сравнению';
						$fav_text = ($arItem['FAVORITES'] == 'Y') ? 'Удалить из избранного' : 'Добавить в избранное';

						$dateShow = ($arItem["ACTIVE_FROM"] == date('d.m.Y')) ? 'Сегодня' : $arItem["ACTIVE_FROM"];?>
						<div class="orders__item">
							<div class="orders__item-head">
								<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="orders__item-title"><?=$arItem["NAME"]?></a>
								<span class="orders__item-price"><?=formatPrice($arItem['PROPERTY_BUDGET_VALUE'])?> ₽
									<span class="control control--favorites orders__item-favourites <?=$fav_active?>">
										<div class="control__icon js-add-favorites" data-id="<?=$arItem['ID']?>" data-cookie="favorites_orders">
											<svg class="icon icon-favorites" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 46.9" style="enable-background:new 0 0 50 46.9;"
												xml:space="preserve">
												<path class="st0" d="M46.4,4.4l-0.3-0.3C43.8,1.3,40.4,0,36.7,0s-6.8,1.6-9.4,4.2L25,6.8l-2.3-2.6C20.3,1.3,16.9,0,13.3,0S6.5,1.6,3.9,4.2L3.6,4.4C1.3,7,0,10.7,0,14.6s1.3,7.3,3.6,10.2l20.1,21.6c0.3,0.3,1.3,0.5,1.3,0.5s1-0.3,1.3-0.5l20.1-21.6c2.3-2.6,3.6-6.3,3.6-10.2C50,10.7,48.7,7,46.4,4.4z"></path>
											</svg>
										</div>
										<div class="control__container">
											<div class="control__main"><?=$fav_text?></div>
										</div>
									</span>
								</span>
								<div class="order__head-ico d-block d-md-none">
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="36px" height="30px">
										<image x="0px" y="0px" width="36px" height="30px" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAeCAYAAABE4bxTAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH5AcDER8FQUCznAAAB6RJREFUWMONl32MVOUZxX/n3jsz+8UIaqy6gES0YitoaxUqEtIIklpFWxUR02qtGI2Vpm1qmmobk1YbTaoNQWxr/UDA+oFVUBRsrNYi2CJaWQQUUTYCwors9yyzM3NP/5g7uys7izzJnZvMfe/7nue85zzPe+VbruEwog7zS+AcRB7QIUfbIIEPGmn+Adxb9Z0w4LPcAaLDQQNkEVcArcBbmGgQJCd39f30gzFGnIOYURVQKqK5o5tpK9YSYXMYEQG9oEeBheCwb7UKC0qYQZVHpeR5gIiBG4FLBjFZk2FrawcXP/8GbfkC0Rexn0QeCBHzMJOAYwdwUi0EdCT3YQmzXwO2fw5MXYZN+zq44oU3aOnJ0xCFRNRlDj1tTy/YbcBvgduRL02YijFhmZkKSwKIE3YuSpJYDlyG2Y1YiIEogIZamnbu46pV69nR2cNRtWni2ES/eOXtIfEUDH889zSIKWD/DTgTdEZCfz+51fQkSkCIfTOQRXoXeI50REtHjlvXNPH+px180Jnj6JpUn3KihZubhwRUAj7tybN0xjegUAoSLYWD3FONWZNChMk/EZAmDNTRk/fM1f9l/adtjKhJc3RNithOyIVoRCY19LwSL3zcwqwX1/PkBWeLQinA1uHJLpG6EttJQVe+oCnL17q5s5vj62pAIj5IicEXzVqfili9s4UZK9aV4ky6yGG6oE9ble2XClNXrI0/6siRTUVDOuJQdWgEMFaQzoRhaUNLa2rZ1uZRs8adkOdA/rAwIegtlUinUsU39+4/cU8uP7kmDIqGIpA6KLki0D0kIEm/Am4G2kNBIY7Z3pEbRqAVVSpw9QgoSQqwR67d/dnXu4qlC7PpqNZ2lnJZyAFhAuYYYPdQdeg44HLbdwNLBIQEpAPdBjQOAqAqoIQw7akgmEIYnIqYGwV60+Y64FpJs4Ed4FogZ/sZoDmqlp6kK23XAA8Kmgtl5aWAYxBxX2Xuf6EaTb2Jl28mCLZs3N+5rLcUqy4Kvwl6xeZV9c/xVdBoiV9XE3UDMEfSakl7DpRieoolEOMlTSemHVdjZBDTnUiTMedRm1n87z2tMXiizVm2HxmQPMBNknfZfr0aoGngscCTQD4IRG0UgpljO0D0DmJooJbspKdRxEyioXbvXa9veikfx9SnopvAG4GXKxnZHgO6zNajoPZAEgMv0MWgvbY3AGSCgFSgy8E3JMt+scUksIuko2hXe9df72/6cGdXoXh1Kgi+C6wCiuW1XAu6R3IErAB8sMu+IjHF9mhgJrDBcLXhKqBbqB7pKOCUxGkVhuOEnzC59xIGYzHF+W9t62nPF+6rj8KrbXcA5wHPASdRdvEU0AKJHTC4Dl1guwRaDL4t0dOHkn4CeqenFC8o9ORnFIvFSbWpqFwkgyBuy/emgTCbThWC2CUCuRSG6eXv74yf2r77jlDB5jDgWtB7NguAV2z2SsQS3bafBQoAQWXLbY6xfRnwgsQ8UAGYD0wVLAGaHnmv+eGRi1bn7m366IfUZU5ieMOEpz/8ZPykZ9ZsOufZNVte3tUygSOzE1p6CyfPefE/s69/bWN7Hu6sjYKpiSbfAR4DcuDzQats3gE2VRgZyNDZkk4A5tqeLanO9lJJ3YFEJgoyuUJp5v7ewrrmrp6VW/d14JpU7u6N28ft6cmf0RCFP/2oI7d3y/4OVmz9mKe27z732PqaNsHfgYItwHVJSXke6AYuBj8E7O2TX2PjKIA08CegDvx9YB1oPXgeUJCE7WmCpSW4KVcsLSuWjG2GZaIH0mEw2WZiV6HU2VsskgrDMcPS0UvAU8Ctrpwk8feAP4OmgidK3GkzA9hYAVQR5Vjg3ITSBaAzga2VfQWmA48YwgBtbohChmdSw0bUpG+JFFxn867tzvooZERN+rSGVPQQ+GRgm/uPyBcCDwNdkruAObZeBZo+Z9DGxlECfgQ8AOyX2AJ02f4SaCUwRmIa8DZwFOXj6Bqb6cmi60BnSn7C1hHAd8CfgVolssBKm5MlTwettTkOOAAeWTYLyw8CNLJeYpXNkZIWA4/bzklaCFxq+5/AEknLgAm2HwcdD34QtAS8QdL1tu8B3gcWJeNj2wslXWSzWmJJ4qaJEo/Z2p6UgJ6BgCJgImg8+Ergxb6ia3cmdP4A2JVQv0NSp+0/ALckOZFsbytwJfDWgPlbgX8lc7QkbeJ/QBd4NehzYCoaugbYDKyyy0KVNE7St4G/ALsG9IlLbA+nXAb6wmYe8FoZTLmv2D4LdD6wWKIlSRLbs2wywFL6mmL/FUg6z2Yp4CSDwPYNLlOycgCY0cDPJL3W7woDniUxGbRowNgQ+LFEb8JQJU4Cfg5eBXxAlQhswqTRZYFRlL8sbwS1gWqAI4BvgRZLGg0UQcOBI4G5kubbbpBIgbOJY++XmG3TlRzGjgCmgp4FtYJ+wxChxsaRd0maa3uTpLE2OfAiSTNthped51NtrQOvlZhrax84kDQKWGgzulwavA30ZYlPwE/YugR8rKRdwDjba4B5oOahjplhNpvdBjoOdIpEk8TtwGOSPgFOB+qAp4E7gJcAJJ0uqQ08H3Q/8Cb4xIThtyX9DvQk+GNgPFAPegL4PbDjUN8J/wdt3mygE9LYDgAAAABJRU5ErkJggg=="></image>
									</svg>
								</div>
								<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="orders__item-cat"><?=$arResult['SERVICES'][$arItem['PROPERTY_SERVICE_VALUE']]['UF_NAME']?></a>
								<div class="control control--favorites orders__item-favourites <?=$fav_active?>">
									<div class="control__icon js-add-favorites" data-id="<?=$arItem['ID']?>" data-cookie="favorites_orders">
										<svg class="icon icon-favorites" version="1.1" id="Capa_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 46.9" style="enable-background:new 0 0 50 46.9;"
											xml:space="preserve">
											<path class="st0" d="M46.4,4.4l-0.3-0.3C43.8,1.3,40.4,0,36.7,0s-6.8,1.6-9.4,4.2L25,6.8l-2.3-2.6C20.3,1.3,16.9,0,13.3,0S6.5,1.6,3.9,4.2L3.6,4.4C1.3,7,0,10.7,0,14.6s1.3,7.3,3.6,10.2l20.1,21.6c0.3,0.3,1.3,0.5,1.3,0.5s1-0.3,1.3-0.5l20.1-21.6c2.3-2.6,3.6-6.3,3.6-10.2C50,10.7,48.7,7,46.4,4.4z"></path>
										</svg>
									</div>
									<div class="control__container">
										<div class="control__main"><?=$fav_text?></div>
									</div>
								</div>
							</div>
							<div class="orders__item-content">
								<div class="orders__item-ico d-none d-md-block d-lg-block">
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="66px" height="54px">
										<image x="0px" y="0px" width="66px" height="54px" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEIAAAA2CAMAAABEOb8NAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAA/1BMVEUAAAD/dmD/dWD/dmD/dV//dWD/dWD/dmD/dmD/dWD/dmD/dV//dmD/dV//dmD/dWD/dmD/dWD/dmD/dmAbGxzsbFkbGxz/dmAcHB0bGxwbGxwbGx2VSj8bGx0cHB0cHB0bGxwbGx0bGxwcHB0bGxwcHB0cHB0bGxwbGxwcHB0bGx0cHB0cHB0bGx0bGxwbGxwbGx3/dV8bGx0bGxwbGx3/dmD/dV8bGxwbGxwcHB0bGx0cHB0bGx3/dWAbGx0cHB3/dWC7WEocHB0cHB0bGx0cHB0bGx0bGxwbGx0bGx3/dmD/dWD/dV8bGxwcHB0bGx2jTkKyVUfOYE9qODP///9BMW73AAAASnRSTlMAEICAgFBwv++/z0BAv2BAcJ+fIL+PQN/vn89AYIBgQO8QYK9QMHAQMCBgz9+fgCDv39+vz48w34+/IBBQYDBQjzCPgK+fj3C/cCaD/QkAAAABYktHRFTkA4ilAAAAB3RJTUUH5AcDDhY7RpnxMwAABKtJREFUSMelVgtb4lYQzYJdxMW1dWmaYIXwMrwkFGxDFiutoNibzEwf//+/dG4el4tGzfft5VOGeycnM2fOTGIYL9aHUqlcLsu//ToqyZ2j2P7OeHd9FKEIQ8Er/idExJ/kO96ovA9RFseVSqVarchVZaNaiT/VePekGMRbTp+KQByF1TdOa0UgSm861cJCiTCRkUZolP6JhOXT9yE+q1KEkbwo0oojrQJRGLWaEB9rB+usdnYivq/VfgjPa18KQBiGiGov9iqC90pRqRAAQ4izfIgjUS4MkROFjKwcFofIiSKUURSGCMUrXJS/LZHw2yFkcmVRuCLha4mERwUhole5eI/OOiASIEBeFFUJ8aP4G2IPQEK2+IK67mQiH4Bcr0rrJ/EPEMY+ICHYtnQnGxsmrwvTzKNTRnEWip/Z4RKg2WpdSGcHbM2nDdBRFXkZRZWTOw9PpNmFRrrZA+hrPlfguIk1yNfFpy9CfGZriDhKN7s01n0mmMV0nd+pZyVx/IGtKWb38gA7mksPs5jcWV4ip2Htl+hcWnOV/xQPyFzgTWqNMF+dv0bitzh/HKR7Piw1D48gS9BHEeV16nEUz7wAsvybgF81jyVYq8S6JciL4pTn5u/8bVJWOHcOB7qaZTIbOEC5PRKJY/66myGkeSwA15pDE1Mq1s4rAq+I8A8mwmFlm/L31wahrztwe4DttdcBgeNgmMPFqQj/7LGb5cONabTuZwC00ckEGMdNgxB4I3j25IiSh/q/soEa/RZ3UtJK4GkQ9zA3Lmfcg92mTOY4wUjeBsL07SD8D3Arjy9uuL/GmzF19SgdmEplZAQ31Q1cwGkmHCXmHvGxB3SrIXQAXGMIlKm1rsCmiJ7KVR3LYto414MI4IGVodTqkarWHBcZGDipNURoGq6jwovrA2TKZO4zZlS1uJkzMJ8yMT8Sg3UIhhrEI/ElO6JUrav9DQKapNYOMb3EdWQxxyq8RJk0kqLM2OLi3ymOrhKr72BDHdPAGAD0dGWCs2KElEK3y5pJ5sGIZ2PC+h0fP8VW2+aRuTKu6UCZT7C9ZEmgI7XfmxDraydJ3Ur1bCXqVIrOkePkwuchTzvXAV2ZdygHus+T1OkupMeVTdjYcgtgsEPyF08S3rTAqi98vtX0HsiXKtivJcvV2bhG32LREdT7xqouHyfkj2RxOCaa2W2jP5MPDwhMw61zqtd6HhZ0e/GocMc42SW0c3mmZiICB+rr+I6rLVq7pGhrhGfKVPpTxexS/cWx4WCWv40TPYgAt5n+CF6KOYB942TKWFmkK3MAmBXYlzJPtIYzJVx1HCgxNfGgzR9UTHwXM9WfpSbzvv4toEzrk0MyfXxMrQZk3kvK2nOA+2PVODsCU4dASn/aiswO1zFFcABaaTgAQXpMeDBrjFlCnPeUKtFwH6XS4ir3LB7Ff8WUyFePZD5viMbtAwibrI65tnkYstfGvLi3ABszbPQuRiww54msptm7tjgGpIVpXk14fK4OEAyPL5Gqs2452/jlA5cui0EOYwiGQzn0+ZezZjFkx8az5V2zV9CUsW3mQDdLyaTZJZg9SJbaNvNRv5I37ow5gkfvOcD/iV8qF0m480QAAAAASUVORK5CYII="></image>
									</svg>
								</div>
								<div class="orders__item-desc">
									<?=$arItem['PREVIEW_TEXT']?>
								</div>
							</div>
							<div class="orders__item-bottom">
								<div class="orders__item-status"><?=$arItem['PROPERTY_STATUS_VALUE']?></div>
								<div class="orders__item-time"><?=$dateShow?></div>
								<div class="orders__item-place"><?=$arItem['PROPERTY_ADDRESS_VALUE']?></div>
							</div>
							<div class="orders__item-btn">
								<div class="orders__item-stat">Только для вас</div>
								<a href="#" class="orders__item-send btn btn--red" data-toggle="modal" data-target="#modalReply">Отправить отклик</a>
							</div>
						</div>
					<?}
				}else{?>
					<div class="orders__item">Нет заказов в избранном!</div>
				<?}?>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalReply" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog--reply" role="document">
		<div class="modal-content">
			<div class="modal-close js-video-close" data-dismiss="modal" aria-label="Close">
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20px" height="20px">
					<path fill-rule="evenodd" fill="rgb(149, 149, 149)" d="M1.515,0.100 L19.899,18.485 L18.485,19.899 L0.100,1.515 L1.515,0.100 Z" />
					<path fill-rule="evenodd" fill="rgb(149, 149, 149)" d="M18.485,0.100 L19.899,1.515 L12.828,8.586 L11.414,7.171 L18.485,0.100 Z" />
					<path fill-rule="evenodd" fill="rgb(149, 149, 149)" d="M7.171,11.414 L8.586,12.828 L1.515,19.899 L0.100,18.485 L7.171,11.414 Z" />
				</svg>
			</div>
			<div class="modal-body">
				<div class="modal-header">
					<h4 class="modal-header__title">
						Отправить отклик
					</h4>
				</div>
				<div class="modal-container">
					<div class="lk-form-block">
						<label for="">
							<input class="form-input" type="text" value="" placeholder="ФИО">
							<span class="placeholder">ФИО</span>
						</label>
					</div>
					<div class="lk-form-block">
						<label for="">
							<input class="form-input" type="text" value="" placeholder="Примерная стоимость работ">
							<span class="placeholder">Примерная стоимость работ</span>
						</label>
					</div>
					<div class="form-block">
						<label class="select" for="material">
							<select class="select__select select__styler" name="material[]" id="material" title="Сроки работ*">
								<option value="">Сроки работ*</option>
								<option value="1">Металочерепица</option>
								<option value="2">Мягкая черепица</option>
								<option value="3">Цементно-песчаная черепица</option>
							</select>
						</label>
					</div>
					<div class="form-block">
						<label class="select" for="material">
							<select class="select__select select__styler" name="material[]" id="material" title="Начало работ*">
								<option value="">Начало работ*</option>
								<option value="1">Металочерепица</option>
								<option value="2">Мягкая черепица</option>
								<option value="3">Цементно-песчаная черепица</option>
							</select>
						</label>
					</div>
					<div class="form-block">
						<label for="name">
							<textarea class="form-textarea" name="" id="" cols="30" rows="3" placeholder="Введите текст"></textarea>
						</label>
					</div>
					<div class="form-block form-block-file">
						<div class="form-contancts--column place_show">
							<div class="file-input">
								<input type="file">
								<svg class="icon">
									<svg id="icon-clip" viewBox="0 0 22 22">
										<path fill="#FFFFFF" d="M18,22H4c-2.2,0-4-1.8-4-4V4c0-2.2,1.8-4,4-4h14c2.2,0,4,1.8,4,4v14C22,20.2,20.2,22,18,22z"></path>
										<g>
											<g>
												<path fill="#959595" d="M15.5,5.9c-1.2-1.2-3.1-1.2-4.2,0l-4.5,4.5c-0.1,0.1-0.1,0.4,0,0.5c0.1,0.1,0.4,0.1,0.5,0l4.5-4.5
															c0.9-0.9,2.4-0.9,3.2,0c0.9,0.9,0.9,2.3,0,3.2l-6.2,6.3c-0.5,0.5-1.5,0.5-2,0c-0.5-0.5-0.5-1.4,0-2l6-6c0.2-0.2,0.5-0.2,0.7,0
															c0.2,0.2,0.2,0.5,0,0.7l-5.7,5.8c-0.1,0.1-0.1,0.4,0,0.5c0.1,0.1,0.4,0.1,0.5,0L14,9.1c0.5-0.5,0.5-1.3,0-1.7
															c-0.5-0.5-1.3-0.5-1.7,0l-6,6c-0.8,0.8-0.8,2.2,0,3C6.7,16.8,7.2,17,7.8,17s1.1-0.2,1.5-0.6l6.2-6.3C16.6,8.9,16.6,7,15.5,5.9z"></path>
											</g>
										</g>
									</svg>
								</svg><span>Прикрепить</span>
							</div>
						</div>
						<a href="#" class="btn btn--red">Отправить</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
