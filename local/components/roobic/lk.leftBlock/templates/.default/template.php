<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// dump($arResult);?>
  <div class="company__sidebar">
    <div class="company__header">
      <div class="company__photo">
        <img src="<?=$arResult['PHOTO']?>" alt="">
        <form action="" method="post" enctype="multipart/form-data" class="photo_user">
          <label class="label company__photo-edit">
            <i class="material-icons"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <image x="0px" y="0px" width="14px" height="12px" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAASCAQAAADx2TabAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfkCB4PECyy8aHWAAAAzklEQVQoz43QXXHCQADE8V8YBAQHVAGR0ChgcJA6oO+ZoSiIhEZCUAASzkHBQRzQh14y5KOl+7S795+7uU1Kvb6sDXX10tlFXxYTjLWis0tkUhzM6WCNRkjKvcpzvSflfVJe8DouF6N8tJLLrRyHB8tBelNH1/pw9Tl/Y61G5e6u6vMMeEJhD/aK2MyALTZ92sTml8/cZtwE3KIWQFDHJupxx1YuxA0vyJylczemzhH6mfwBG++YOgtO2Mr+GhyyMdI9HfxHYWGneQK1GrtvBw0xdujnEBcAAAAASUVORK5CYII=" />
              </svg></i>
            <input type="file" name="photo_user">
          </label>
        </form>
      </div>
      <div class="company__name"><span><?=$arResult['NAME']?></span></div>
    </div>
    <div class="company__balance hide">
      <div class="company__balance-title">Баланс на сегодня:</div>
      <div class="company__balance-value">85,00 ₽ <span class="company__balance-ico"></span> </div>
    </div>
    <ul class="company__menu">
      <?if(CSite::InGroup([6,8])){ // если заказчик?>
      <li>
        <span>
          <svg class="icon" version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 43.3" style="enable-background:new 0 0 50 43.3;" xml:space="preserve">
            <circle class="st0" cx="3.6" cy="3.6" r="3.6" />
            <path class="st0" d="M48.6,7.1H12.1c-0.8,0-1.4-0.6-1.4-1.4V1.4c0-0.8,0.6-1.4,1.4-1.4l36.4,0C49.4,0,50,0.6,50,1.4v4.3C50,6.5,49.4,7.1,48.6,7.1z" />
            <circle class="st0" cx="3.6" cy="21.6" r="3.6" />
            <path class="st0" d="M48.6,25.2H12.1c-0.8,0-1.4-0.6-1.4-1.4v-4.3c0-0.8,0.6-1.4,1.4-1.4h36.4c0.8,0,1.4,0.6,1.4,1.4v4.3C50,24.6,49.4,25.2,48.6,25.2z" />
            <circle class="st0" cx="3.6" cy="39.7" r="3.6" />
            <path class="st0" d="M48.6,43.3H12.1c-0.8,0-1.4-0.6-1.4-1.4v-4.3c0-0.8,0.6-1.4,1.4-1.4h36.4c0.8,0,1.4,0.6,1.4,1.4v4.3C50,42.6,49.4,43.3,48.6,43.3z" />
          </svg>
        </span>
        <a href="/lk/client_orders"><?=(CSite::InGroup([5])) ? 'Избранные заказы':'Мои заказы'?></a>
      </li>
      <?}?>
      <li>
        <span>
          <!-- <svg class="icon" version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 43.3" style="enable-background:new 0 0 50 43.3;" xml:space="preserve">
            <circle class="st0" cx="3.6" cy="3.6" r="3.6" />
            <path class="st0" d="M48.6,7.1H12.1c-0.8,0-1.4-0.6-1.4-1.4V1.4c0-0.8,0.6-1.4,1.4-1.4l36.4,0C49.4,0,50,0.6,50,1.4v4.3C50,6.5,49.4,7.1,48.6,7.1z" />
            <circle class="st0" cx="3.6" cy="21.6" r="3.6" />
            <path class="st0" d="M48.6,25.2H12.1c-0.8,0-1.4-0.6-1.4-1.4v-4.3c0-0.8,0.6-1.4,1.4-1.4h36.4c0.8,0,1.4,0.6,1.4,1.4v4.3C50,24.6,49.4,25.2,48.6,25.2z" />
            <circle class="st0" cx="3.6" cy="39.7" r="3.6" />
            <path class="st0" d="M48.6,43.3H12.1c-0.8,0-1.4-0.6-1.4-1.4v-4.3c0-0.8,0.6-1.4,1.4-1.4h36.4c0.8,0,1.4,0.6,1.4,1.4v4.3C50,42.6,49.4,43.3,48.6,43.3z" />
          </svg> -->
          <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/adt.svg" class="icon" alt="">
        </span>
        <a href="/lk/client_ads">Мои объявления</a>
      </li>
      <li><span>
          <svg class="icon" version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 59.9" style="enable-background:new 0 0 50 59.9;" xml:space="preserve">
            <path class="st0" d="M24.6,28.9c4,0,7.4-1.4,10.2-4.2c2.8-2.8,4.2-6.2,4.2-10.2c0-4-1.4-7.4-4.2-10.2C32,1.4,28.6,0,24.6,0c-4,0-7.4,1.4-10.2,4.2s-4.2,6.2-4.2,10.2c0,4,1.4,7.4,4.2,10.2C17.2,27.4,20.7,28.9,24.6,28.9z" />
            <path class="st0" d="M49.9,46.1c-0.1-1.2-0.2-2.4-0.5-3.8c-0.2-1.4-0.6-2.6-0.9-3.8c-0.4-1.2-0.9-2.4-1.6-3.6c-0.7-1.2-1.5-2.2-2.4-3.1c-0.9-0.9-2.1-1.6-3.4-2.1c-1.3-0.5-2.8-0.8-4.3-0.8c-0.6,0-1.2,0.3-2.3,1c-0.7,0.5-1.5,1-2.4,1.6c-0.8,0.5-1.8,1-3.2,1.4c-1.3,0.4-2.6,0.6-3.9,0.6s-2.6-0.2-3.9-0.6c-1.3-0.4-2.4-0.9-3.2-1.4c-0.9-0.6-1.7-1.1-2.4-1.6
                                c-1.1-0.7-1.7-1-2.3-1c-1.6,0-3,0.3-4.3,0.8c-1.3,0.5-2.5,1.2-3.4,2.1c-0.9,0.9-1.7,1.9-2.4,3.1c-0.7,1.1-1.2,2.3-1.6,3.6
                                c-0.4,1.2-0.7,2.5-0.9,3.8c-0.2,1.3-0.4,2.6-0.5,3.8C0,47.2,0,48.4,0,49.6c0,3.1,1,5.7,3,7.5c1.9,1.8,4.5,2.8,7.6,2.8h28.9
                                c3.1,0,5.7-0.9,7.6-2.8c2-1.9,3-4.4,3-7.5C50,48.4,50,47.2,49.9,46.1z" />
          </svg>
        </span><a href="/lk/">Профиль</a></li>
    </ul>
    <ul class="company__menu">
      <li>
        <span>
          <svg class="icon" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 46.9" style="enable-background:new 0 0 50 46.9;" xml:space="preserve">
            <path class="st0" d="M46.4,4.4l-0.3-0.3C43.8,1.3,40.4,0,36.7,0s-6.8,1.6-9.4,4.2L25,6.8l-2.3-2.6C20.3,1.3,16.9,0,13.3,0
                                S6.5,1.6,3.9,4.2L3.6,4.4C1.3,7,0,10.7,0,14.6s1.3,7.3,3.6,10.2l20.1,21.6c0.3,0.3,1.3,0.5,1.3,0.5s1-0.3,1.3-0.5l20.1-21.6
                                c2.3-2.6,3.6-6.3,3.6-10.2C50,10.7,48.7,7,46.4,4.4z" />
          </svg>
        </span>
        <a href="/izbrannoe/" target="_blank">Избранное</a>
      </li>
      <!-- <li><span>
          <svg class="icon" version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 48" style="enable-background:new 0 0 50 48;" xml:space="preserve">
            <path class="st0" d="M43.1,0H6.9C3.1,0,0,3.1,0,6.9v24.2c0,3.8,3.1,6.9,6.8,6.9V48l14.5-10.1h21.8c3.8,0,6.9-3.1,6.9-6.9V6.9
                                C50,3.1,46.9,0,43.1,0z M36.6,26.8H13.4v-2.9h23.2V26.8z M36.6,20.5H13.4v-2.9h23.2V20.5z M36.6,14.3H13.4v-2.9h23.2V14.3z" />
          </svg>
        </span><a href="/lk/messages">Сообщения</a></li> -->
      <li><span>
          <svg class="icon" version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 54.5" style="enable-background:new 0 0 50 54.5;" xml:space="preserve">
            <path class="st0" d="M47.7,24.7c-1.3,0-2.3-1-2.3-2.3c0-6.4-2.5-12.4-7-16.9c-0.9-0.9-0.9-2.3,0-3.2c0.9-0.9,2.3-0.9,3.2,0
                                C47,7.7,50,14.9,50,22.5C50,23.7,49,24.7,47.7,24.7z" />
            <path class="st0" d="M2.3,24.7c-1.3,0-2.3-1-2.3-2.3C0,14.9,3,7.7,8.3,2.4c0.9-0.9,2.3-0.9,3.2,0c0.9,0.9,0.9,2.3,0,3.2
                                c-4.5,4.5-7,10.5-7,16.9C4.5,23.7,3.5,24.7,2.3,24.7z" />
            <path class="st0" d="M46.3,38.4c-3.4-2.9-5.4-7.2-5.4-11.6v-6.3c0-8-5.9-14.6-13.6-15.7V2.3C27.3,1,26.3,0,25,0
                                c-1.3,0-2.3,1-2.3,2.3v2.5C15,5.8,9.1,12.5,9.1,20.5v6.3c0,4.5-2,8.7-5.4,11.7c-0.9,0.8-1.4,1.9-1.4,3c0,2.2,1.8,4,4,4h37.5
                                c2.2,0,4-1.8,4-4C47.7,40.3,47.2,39.2,46.3,38.4z" />
            <path class="st0" d="M25,54.5c4.1,0,7.6-2.9,8.4-6.8H16.7C17.4,51.6,20.9,54.5,25,54.5z" />
          </svg>
        </span><a href="/lk/notifications">Уведомления</a></li>
    </ul>
    <?if(CSite::InGroup([5,8])){ // если компания?>
      <ul class="company__menu">
        <li><span>
            <svg class="icon" version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 56.3" style="enable-background:new 0 0 50 56.3;" xml:space="preserve">
              <g id="Capa_1">
                <polygon class="st0" points="1.5,7.6 3.3,2.7 10.5,1.5 32.9,1.5 32.9,12.8 34.3,16.5 38.7,17.6 46.7,17 48,23.2 48.6,47.5
                                      47.7,51.7 45.2,54.7 18.9,55.4 5.9,54 2.8,52.3 1,48.3    " />
                <path class="st0" d="M49.5,16.2L33.8,0.5C33.5,0.3,33.2,0,32.7,0H6.6C3,0,0,3,0,6.6v43.1c0,3.6,3,6.6,6.6,6.6h36.8
                                      c3.6,0,6.6-3,6.6-6.6V17.3C50,16.8,49.7,16.5,49.5,16.2z M37.6,19h9.1v30.8c0,1.9-1.4,3.3-3.3,3.3H6.6c-1.6,0-3.3-1.4-3.3-3.3V6.6
                                      c0-1.6,1.4-3.3,3.3-3.3h24.7v9.1C31,15.9,34.1,19,37.6,19z M34.3,12.4V5.5l9.9,9.9h-6.9C35.7,15.7,34.3,14.3,34.3,12.4z" />
                <path class="st1" d="M35.2,33H9.3C8.5,33,8,33.5,8,34.3s0.5,1.4,1.4,1.4h25.5c0.8,0,1.4-0.5,1.4-1.4C36.5,33.8,35.7,33,35.2,33z" />
                <path class="st1" d="M35.2,41.2H9.3c-0.8,0-1.4,0.5-1.4,1.4S8.5,44,9.3,44h25.5c0.8,0,1.4-0.5,1.4-1.4C36.5,42,35.7,41.2,35.2,41.2
                                      z" />
                <path class="st1" d="M29.9,24.7H9.3c-0.8,0-1.4,0.5-1.4,1.4s0.5,1.4,1.4,1.4h20.6c0.8,0,1.4-0.5,1.4-1.4
                                      C31.3,25.5,30.8,24.7,29.9,24.7z" />
                <path class="st1" d="M24.5,16.5H9.3C8.5,16.5,8,17,8,17.9c0,0.8,0.8,1.6,1.4,1.6h15.1c0.8,0,1.4-0.5,1.4-1.4
                                      C25.8,17.3,25.3,16.5,24.5,16.5z" />
              </g>
              <g id="Слой_2_1_">
              </g>
            </svg>
          </span><a href="<?=(CSite::InGroup([8]))?'/lk/company_cards':'/lk/company_card'?>">Карточка компании</a></li>
        <li><span>
            <svg class="icon" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 51.8" style="enable-background:new 0 0 50 51.8;" xml:space="preserve">
              <g>
                <path class="st0" d="M24.5,44.6c0.4,0.4,1.1,0.6,1.7,0.3l6.9-3.1L27.3,36l-3.1,6.9C23.9,43.5,24.1,44.1,24.5,44.6L24.5,44.6z" />
                <path class="st0" d="M49.6,23.8l-4.3-4.3c-0.6-0.6-1.6-0.6-2.1,0l-14,14l6.4,6.4l14-14C50.1,25.4,50.1,24.4,49.6,23.8L49.6,23.8z" />
                <path class="st0" d="M11.6,10.3h16.2c0.8,0,1.5-0.7,1.5-1.5v-4c0-0.8-0.7-1.5-1.5-1.5h-2.5V1.5c0-0.8-0.7-1.5-1.5-1.5h-8.1
                                      c-0.8,0-1.5,0.7-1.5,1.5v1.7h-2.5c-0.8,0-1.5,0.7-1.5,1.5v4C10.1,9.6,10.8,10.3,11.6,10.3z" />
                <path class="st0" d="M35.3,44.1l-7.9,3.6c-0.6,0.3-1.2,0.4-1.9,0.4c-1.2,0-2.4-0.5-3.2-1.3c-1.3-1.3-1.7-3.4-0.9-5.1l3.6-7.9
                                      c0.2-0.5,0.5-0.9,0.9-1.3l13.5-13.5V8.3c0-0.8-0.7-1.5-1.5-1.5h-5.6v2c0,2.5-2,4.6-4.6,4.6H11.6c-2.5,0-4.6-2-4.6-4.6v-2H1.5
                                      C0.7,6.8,0,7.5,0,8.3v42c0,0.8,0.7,1.5,1.5,1.5H38c0.8,0,1.5-0.7,1.5-1.5v-9.9l-2.8,2.8C36.3,43.6,35.8,43.9,35.3,44.1z M10.1,20.2
                                      c0.6-0.6,1.6-0.6,2.1,0l1.5,1.5l4-4c0.6-0.6,1.6-0.6,2.1,0c0.6,0.6,0.6,1.6,0,2.1l-5.1,5.1c-0.3,0.3-0.7,0.4-1.1,0.4
                                      c-0.4,0-0.8-0.1-1.1-0.4l-2.5-2.5C9.5,21.7,9.5,20.8,10.1,20.2z M10.1,30.1c0.6-0.6,1.6-0.6,2.1,0l1.5,1.5l4-4
                                      c0.6-0.6,1.6-0.6,2.1,0c0.6,0.6,0.6,1.6,0,2.1l-5.1,5.1c-0.3,0.3-0.7,0.4-1.1,0.4s-0.8-0.1-1.1-0.4l-2.5-2.5
                                      C9.5,31.6,9.5,30.6,10.1,30.1z M6.1,44.5c-0.8,0-1.5-0.7-1.5-1.5s0.7-1.5,1.5-1.5s1.5,0.7,1.5,1.5S6.9,44.5,6.1,44.5z M6.1,34.7
                                      c-0.8,0-1.5-0.7-1.5-1.5c0-0.8,0.7-1.5,1.5-1.5s1.5,0.7,1.5,1.5C7.6,34,6.9,34.7,6.1,34.7z M6.1,24.8c-0.8,0-1.5-0.7-1.5-1.5
                                      c0-0.8,0.7-1.5,1.5-1.5s1.5,0.7,1.5,1.5C7.6,24.1,6.9,24.8,6.1,24.8z M13.7,45c-0.4,0-0.8-0.1-1.1-0.4l-2.5-2.5
                                      c-0.6-0.6-0.6-1.6,0-2.1c0.6-0.6,1.6-0.6,2.1,0l1.5,1.5l4-4c0.6-0.6,1.6-0.6,2.1,0c0.6,0.6,0.6,1.6,0,2.1l-5.1,5.1
                                      C14.4,44.9,14.1,45,13.7,45z" />
              </g>
            </svg>
          </span><a href="/lk/company_works">Выполненные работы</a></li>
        <li><span>
            <svg class="icon" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 45.9" style="enable-background:new 0 0 50 45.9;" xml:space="preserve">
              <g>
                <path class="st0" d="M7.4,23.4h4.9V20c0-0.8,0.7-1.5,1.5-1.5s1.5,0.7,1.5,1.5v8.3h1.5c0.8,0,1.5,0.7,1.5,1.5c0,0.8-0.7,1.5-1.5,1.5h-2.9c-0.8,0-1.5-0.7-1.5-1.5v-3.4H7.4v10.7h12.7v-2.4c0-0.8,0.7-1.5,1.5-1.5c0.8,0,1.5,0.7,1.5,1.5v2.4h4.9V26.4h-8.3c-0.8,0-1.5-0.7-1.5-1.5c0-0.8,0.7-1.5,1.5-1.5h2.4v-2.9h-1.5c-0.8,0-1.5-0.7-1.5-1.5c0-0.8,0.7-1.5,1.5-1.5h2.9c0.8,0,1.5,0.7,1.5,1.5v4.4h2.9v-8.8H7.4V23.4z" />
                <path class="st0" d="M44.1,0c-3.2,0-5.9,2.6-5.9,5.9v27.6c1.6-1.4,3.6-2.2,5.9-2.2c2.2,0,4.3,0.8,5.9,2.2V5.9C50,2.6,47.4,0,44.1,0z" />
                <path class="st0" d="M44.4,34.2c-2.9-0.1-5.4,1.9-6,4.5c-0.2,0.8-0.8,1.3-1.6,1.3h0c-0.8,0-1.5-0.7-1.5-1.5V5.9H1.5C0.7,5.9,0,6.5,0,7.3v37.1c0,0.8,0.7,1.5,1.5,1.5h42.7c3.3,0,6-2.8,5.9-6.1C49.9,36.8,47.4,34.3,44.4,34.2z M30.9,38.6c0,0.8-0.7,1.5-1.5,1.5H6c-0.8,0-1.5-0.7-1.5-1.5V13.2c0-0.8,0.7-1.5,1.5-1.5h23.4c0.8,0,1.5,0.7,1.5,1.5V38.6z" />
              </g>
            </svg>
          </span><a href="/lk/company_projects">Проекты домов</a></li>
      </ul>
    <?}?>
    <ul class="company__menu logout">
      <li><span>
          <svg class="icon" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 436.8 471.2" style="enable-background:new 0 0 436.8 471.2;" xml:space="preserve">
            <g>
              <g>
                <path class="st0" d="M212.9,457.7c0,7.5,6,13.5,13.5,13.5h122.9c48.2,0,87.5-39.2,87.5-87.5V87.5c0-48.2-39.2-87.5-87.5-87.5L224.4,0c-7.5,0-13.5,6-13.5,13.5s6,13.5,13.5,13.5h124.9c33.3,0,60.5,27.1,60.5,60.5v296.2c0,33.3-27.1,60.5-60.5,60.5H226.4C219,444.2,212.9,450.2,212.9,457.7z" />
                <path class="st0" d="M4.1,245.3l85.8,85.8c2.6,2.7,6.1,4,9.5,4s6.9-1.4,9.5-4c5.3-5.3,5.3-13.8,0-19.1l-62.8-62.8H320c7.5,0,13.5-6,13.5-13.5s-6-13.5-13.5-13.5H46.1l62.8-62.8c5.3-5.3,5.3-13.8,0-19.1c-5.3-5.3-13.8-5.3-19.1,0L4,226.1C-1.3,231.3-1.3,239.9,4.1,245.3z" />
              </g>
            </g>
          </svg>
        </span><a href="?logout=yes">Выйти</a></li>
    </ul>
  </div>
