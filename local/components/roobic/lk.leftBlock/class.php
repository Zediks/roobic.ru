<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Application;

class lk extends CBitrixComponent
{
	function userInfo()
	{
		global $USER;
		$userID = $USER->GetID();

		$request = Application::getInstance()->getContext()->getRequest();

		// измение фото
		$filesUser = $request->getFile("photo_user"); // dump($files);
		if($filesUser){

			// обновление пользователя
		  $user = new CUser;
		  $arFieldsUser['PERSONAL_PHOTO'] = $filesUser;

		  if (!$user->Update($userID,$arFieldsUser)) echo $user->LAST_ERROR;
		}

		$by = 'id'; $sort = 'asc'; // поле сортировки
	  $filter = ['ID' => $userID];
	  $arParams = [ // наши поля
			'FIELDS' => ['ID','NAME','PERSONAL_PHOTO']
	  ];
		if (CSite::InGroup([5])) // если компания
			$arParams['SELECT'] = ['UF_COMPANIES'];

	  $rsUsers = CUser::GetList($by, $sort, $filter, $arParams);
	  if($arUser = $rsUsers->GetNext()){ // dump($arUser);
			$arResult['NAME'] = $arUser['NAME'];
			if($arUser['PERSONAL_PHOTO']){ // фото
				$arPhoto = CFile::ResizeImageGet($arUser['PERSONAL_PHOTO'], array('width'=>160, 'height'=>160), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
				$arResult['PHOTO'] = $arPhoto['src'];
			}else
				$arResult['PHOTO'] = SITE_TEMPLATE_PATH.'/images/avatar.jpg';
		}

		if(CSite::InGroup([5])){ // если компания
			$companyID = $arUser['UF_COMPANIES'][0];
			$iblockID = 2;
			// получим название компании
			if ($companyID) {
				$arOrder = Array('SORT'=>'ASC');
				$arFilter = Array('IBLOCK_ID'=>$iblockID,'ID'=>$companyID);
				$arSelect = Array('ID','NAME'); // PROPERTY_
				$rsElements = CIBlockElement::GetList($arOrder,$arFilter,false,false,$arSelect);
				if($arElement = $rsElements->GetNext()){ // dump($arElement);
					$arResult['NAME'] = $arElement['NAME'];
				}
			} else
				$arResult['NAME'] = 'Название компании';
		} // dump($arResult);

    return $arResult;
	}

	public function executeComponent()
	{
		$this->arResult = array_merge($this->arResult,$this->userInfo());
		$this->includeComponentTemplate();
	}
}?>
