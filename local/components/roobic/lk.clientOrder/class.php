<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Application;
use Bitrix\Main\Page\Asset;

// yandex_map
Asset::getInstance()->addJs("https://api-maps.yandex.ru/2.1/?lang=ru_RU&apikey=40336fa1-cf4d-44d7-a4fa-8c7ac8e9f5a5
");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/yandex_map.js");

class client extends CBitrixComponent
{
	function order()
	{
		global $USER;
		$userID = $USER->GetID();
		$iblockID = 4;

		$params = [
			"max_len" => "100", // обрезает символьный код до 100 символов
			"change_case" => "L", // буквы преобразуются к нижнему регистру
			"replace_space" => "-", // меняем пробелы на тире
			"replace_other" => "-", // меняем левые символы на тире
			"delete_repeat_replace" => "true", // удаляем повторяющиеся символы
			"use_google" => "false", // отключаем использование google
		];

		// $arResult['SERVICES'] = getElHL(3,[],[],['ID','UF_NAME','UF_XML_ID']);
		$arResult['REGIONS'] = getElHL(16,[],[],['ID','UF_NAME','UF_XML_ID']);
		$arResult['CATEGORIES'] = getElHL(17,[],[],['ID','UF_NAME','UF_XML_ID']);

		// получим разделы
		$arOrder = ['SORT'=>'ASC'];
		$arFilter = ['IBLOCK_ID'=>$iblockID,'ACTIVE'=>'Y'];
		$arSelect = ['ID','CODE'];
		$rsSections = CIBlockSection::GetList($arOrder,$arFilter,false,$arSelect,false);
		while ($arSection = $rsSections->Fetch())
			$arRegions[$arSection['CODE']] = $arSection['ID'];

		$request = Application::getInstance()->getContext()->getRequest();
		$orderID = $request->getQuery("order_id");
		$sendFrom = $request->getPost("sendFrom");

		if($sendFrom){ // форма отправлена

			$order_name = $request->getPost("order_name");
			// $order_code = CUtil::translit($order_name, "ru", $params);

			$filesPhotos = $request->getFile("order_photos");
			foreach ($filesPhotos as $key => $arPhoto) {
				foreach($arPhoto as $key2 => $val2){
					$arPhotos[$key2][$key] = $val2;
				}
			} // dump($arPhotos);

			$orderElement = new CIBlockElement;

			$orderStatus = $request->getPost("order_status");
			if (!$orderStatus) $orderStatus = 5; // заказ открыт

			$orderRegion = $request->getPost("order_region");

			$arFields = [
				'ADDRESS' => $request->getPost("order_address"),
				'SERVICE' => $request->getPost("order_service"),
				'BUDGET' => $request->getPost("order_budget"),
				'WORK_SCOPE' => $request->getPost("order_work_scope"),
				'WORK_START' => $request->getPost("order_work_start"),
				'DEADLINE' => $request->getPost("order_deadline"),
				'PHOTOS' => $arPhotos,
				'STATUS' => $orderStatus,
				'USER' => $userID,
				'REGION' => $orderRegion,
				'CATEGORY' => $request->getPost("order_category"),
			];

			$arLoadProductArray = Array(
		    "IBLOCK_ID"					=> $iblockID,
				"IBLOCK_SECTION_ID"	=> $arRegions[$orderRegion], // раздел
				"MODIFIED_BY"				=> $userID, // элемент изменен текущим пользователем
				"ACTIVE"         		=> "N", // активен
		    "NAME"           		=> $order_name,
		    // "CODE"           		=> $order_code,
		    "DETAIL_TEXT"   		=> $request->getPost("about"),
				"DETAIL_TEXT_TYPE" 	=> 'html',
				"PROPERTY_VALUES"		=> $arFields,
		  ); // dump($arLoadProductArray);

			if ($orderID)
			{
				$orderElement->Update($orderID,$arLoadProductArray);

				// удаление файлов
				$delFile = $request->getPost("del_file");
				if($delFile){
					$arDelFile = explode(',',$delFile); // dump($arDelFile);
					foreach ($arDelFile as $key => $value) {
						$arDeleteImages[$value] = ['VALUE' => ['del' => 'Y']];
					}
					\CIBlockElement::SetPropertyValueCode($orderID, 'PHOTOS', $arDeleteImages);
				}
				$action = 'Изменение заказа';
			} else {
				$arLoadProductArray['ACTIVE_FROM'] = date('d.m.Y');
				$orderID = $orderElement->Add($arLoadProductArray); // добавим
				$orderElement->Update($orderID,['CODE'=>$orderID]);
				$action = 'Новой заказ';
			}

			$urlLK = 'https://stroiman.ru/lk/client_order?order_id='.$orderID;
			$urlAdmin = 'https://stroiman.ru/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=4&type=content&lang=ru&ID='.$orderID.'&find_section_section=0&WF=Y';

			$mailFields = array(
				"action" => $action,
				"NAME" => $order_name,
				"TEXT" => $arLoadProductArray['DETAIL_TEXT'],
		    "urlLK" => $urlLK,
		    "urlAdmin" => $urlAdmin,
		  );
		  if (CEvent::Send("SEND_MODERATION", "s1", $mailFields)) echo '<p>Спасибо! Данные успешно отправлены на модерацию!</p>';
		  else echo '<p>Ошибка! Данные не отправлены!</p>';

			header('Location: /lk/client_orders');
		}

		if ($orderID) { // получим наш заказ
			$arOrder = Array('SORT'=>'ASC');
			$arFilter = Array('IBLOCK_ID'=>$iblockID,'ID'=>$orderID);
			$arSelect = Array('ID','NAME','DETAIL_TEXT','PROPERTY_ADDRESS','PROPERTY_SERVICE','PROPERTY_BUDGET','PROPERTY_WORK_SCOPE','PROPERTY_WORK_START','PROPERTY_DEADLINE','PROPERTY_PHOTOS','PROPERTY_STATUS','PROPERTY_REGION','PROPERTY_CATEGORY'); // ,'PROPERTY_'
			$rsElements = CIBlockElement::GetList($arOrder,$arFilter,false,false,$arSelect);
			if($arElement = $rsElements->GetNext()) { // dump($arElement);
				// получим файлы
				foreach ($arElement['PROPERTY_PHOTOS_VALUE'] as $key => $value) {
					$arElement['PHOTOS'][$key] = CFile::ResizeImageGet($value, array('width'=>488, 'height'=>360), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
				}
				$arResult['ORDER'] = $arElement;
			}
		} // dump($arResult['ORDER']);

		return $arResult;
	}

	public function executeComponent()
	{
		$this->arResult = array_merge($this->arResult,$this->order());
		$this->includeComponentTemplate();
	}
}?>
