<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// dump($arResult);?>
<?$this->SetViewTarget('body-class');?>
	class="lk-page"
<?$this->EndViewTarget();?>
<div class="completed__wrap">
	<div class="row">
		<div class="col-lg-8">
			<div class="completed__block-name d-none d-xs-block d-sm-block d-md-hide">
				<div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_10.svg" alt=""></div><strong>Размещение заказа</strong>
				<div class="completed__block-desc">
					Опишите подробно какие работы нужно выполнить
				</div>
			</div>
			<div class="completed__container">
				<form action="" method="post" enctype="multipart/form-data">
					<div class="form-main-block">
						<div class="form-block">
							<div class="form-block__name hide-xs">Опишите задачу</div>
							<label for="order_name">
								<input class="form-input" type="text" name="order_name" value="<?=$arResult['ORDER']['NAME']?>" placeholder="Заголовок. Например. Строительство 1-этажного дома из бруса под ключ*" required>
								<span class="placeholder">Заголовок. Например. Строительство 1-этажного дома из бруса под ключ*</span>
								<a href="#" class="edit-input">
									<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
								</a>
							</label>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-block">
								<label class="select" for="order_region">
									<select class="select__select select__styler" name="order_region" id="order_region" title="Регион*" data-live-search="true" required>
										<option value="">Регион*</option>
										<?foreach ($arResult['REGIONS'] as $value) {?>
											<option value="<?=$value['UF_XML_ID']?>" <?if($value['UF_XML_ID'] == $arResult['ORDER']['PROPERTY_REGION_VALUE']) echo 'selected';?>><?=$value['UF_NAME']?></option>
										<?}?>
									</select>
								</label>
							</div>
						</div>
					</div>
					<div class="form-block">
						<label for="input_address">
							<input class="form-input" type="text" name="order_address" value="<?=$arResult['ORDER']['PROPERTY_ADDRESS_VALUE']?>" placeholder="Укажите адрес работ*" id="input_address">
							<span class="placeholder">Укажите адрес работ*</span>
							<a href="#" class="edit-input">
								<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
							</a>
						</label>
					</div>
					<div class="form-map" id="yandex_map"></div>
					<?/*?>
					<div class="row">
						<div class="col-md-12">
							<div class="form-block">
								<label class="select" for="order_service">
									<select class="select__select select__styler" name="order_service" id="order_service" title="Услуги*" data-live-search="true">
										<option value="">Услуги*</option>
										<?foreach ($arResult['SERVICES'] as $value) {?>
											<option value="<?=$value['UF_XML_ID']?>" <?if($value['UF_XML_ID'] == $arResult['ORDER']['PROPERTY_SERVICE_VALUE']) echo 'selected';?>><?=$value['UF_NAME']?></option>
										<?}?>
									</select>
								</label>
							</div>
						</div>
					</div>
					<?*/?>
					<div class="row">
						<div class="col-md-12">
							<div class="form-block">
								<label class="select" for="order_category">
									<select class="select__select select__styler" name="order_category" id="order_category" title="Категория*" data-live-search="true" required>
										<option value="">Категория*</option>
										<?foreach ($arResult['CATEGORIES'] as $value) {?>
											<option value="<?=$value['UF_XML_ID']?>" <?if($value['UF_XML_ID'] == $arResult['ORDER']['PROPERTY_CATEGORY_VALUE']) echo 'selected';?>><?=$value['UF_NAME']?></option>
										<?}?>
									</select>
								</label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-block">
								<label for="order_budget">
									<input class="form-input" type="text" name="order_budget" value="<?=$arResult['ORDER']['PROPERTY_BUDGET_VALUE']?>" placeholder="Стоимость за работу, ₽*" required>
									<span class="placeholder">Бюджет, ₽*</span>
									<a href="#" class="edit-input">
										<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
									</a>
								</label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-block">
								<label for="order_work_scope">
									<input class="form-input" type="text" name="order_work_scope" value="<?=$arResult['ORDER']['PROPERTY_WORK_SCOPE_VALUE']?>" placeholder="Площадь или объем работ*" required>
									<span class="placeholder">Площадь или объем работ*</span>
									<a href="#" class="edit-input">
										<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
									</a>
								</label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-block">
								<label for="order_work_start">
									<input class="form-input" type="text" name="order_work_start" value="<?=$arResult['ORDER']['PROPERTY_WORK_START_VALUE']?>" placeholder="Начало работ*" required>
									<span class="placeholder">Начало работ*</span>
									<a href="#" class="edit-input">
										<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
									</a>
								</label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-block">
								<label for="order_deadline">
									<input class="form-input" type="text" name="order_deadline" value="<?=$arResult['ORDER']['PROPERTY_DEADLINE_VALUE']?>" placeholder="Сроки*" required>
									<span class="placeholder">Сроки*</span>
									<a href="#" class="edit-input">
										<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
									</a>
								</label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-block">
								<label for="about">
									<textarea class="form-textarea" name="about" id="about" cols="30" rows="10" placeholder="Описание работ"><?=$arResult['ORDER']['DETAIL_TEXT']?></textarea>
								</label>
								<p>Описание должно содержать не менее 200, и не более 1000 символов</p>
							</div>
						</div>
					</div>
					<div class="form-main-block">
						<div class="form-block">
							<div class="form-block__desc">Загрузите фото с объекта</div>
							<div class="download-photo">
								<?for ($i=0; $i < 3; $i++) {?>
									<div class="list">
										<label class="label-file <?if($arResult['ORDER']['PHOTOS'][$i])echo'has-img'?>">
											<input type="file" accept=".png, .jpg, .jpeg" name="order_photos[]">
											<span>Добавьте файл для загрузки</span>
											<span>в это поле</span>
											<img src="<?=$arResult['ORDER']['PHOTOS'][$i]['src']?>" alt="">
										</label>
										<span class="delete-file" data-id="<?=$arResult['ORDER']['PROPERTY_PHOTOS_PROPERTY_VALUE_ID'][$i]?>">х</span>
									</div>
								<?}?>
								<input type="hidden" name="del_file" value="">
							</div>
						</div>
					</div>
					<div class="row">
            <div class="col-md-12">
              <div class="form-bottom">
                <!-- <a href="#" class="btn off">Сохранить</a> -->
								<input type="hidden" name="order_status" value="<?=$arResult['ORDER']['PROPERTY_STATUS_ENUM_ID']?>">
								<input type="submit" name="sendFrom" value="Сохранить" class="btn">
              </div>
            </div>
          </div>
				</form>
			</div>
		</div>
		<div class="col-md-3 d-none d-lg-block d-xl-block">
			<div class="completed__block-name">
				<div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_11.svg" alt=""></div><strong>Размещение заказа</strong>
				<div class="completed__block-desc">
					Опишите подробно какие работы нужно выполнить
				</div>
			</div>
		</div>
	</div>
</div>
