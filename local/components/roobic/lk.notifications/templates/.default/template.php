<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// dump($arResult);?>
<?$this->SetViewTarget('body-class');?>
	class="notifications-page"
<?$this->EndViewTarget();?>
<div class="col-md-12 col-lg-8">
	<div class="notifications">
		<div class="notifications__head">
			<div class="notifications__title">Уведомления</div>
			<a href="" class="readAllNotif">Отметить все как прочитанные</a>
		</div>
		<div class="notifications__container">
			<div class="notifications__items">
				<?foreach ($arResult['NOTIFICATIONS'] as $id => $notification) {
					$active = ($notification['UF_READ']) ? '' : 'active';?>
					<div class="notifications__item <?=$active?>" data-id="<?=$id?>">
						<div class="notifications__data mobile"><?=$notification['UF_DATE_TIME']->format('d.m.Y')?> <span class="notifications__time"><?=$notification['UF_DATE_TIME']->format('h:i')?></span></div>
						<div class="row">
							<div class="col-md-10 notifications__name">
								<?=$notification['UF_TEXT']?>
							</div>
							<div class="col-md-2 notifications__meta">
								<div class="notifications__data"><?=$notification['UF_DATE_TIME']->format('d.m.Y')?> <span class="notifications__time"><?=$notification['UF_DATE_TIME']->format('H:i')?></span></div>
							</div>
						</div>
						<?if(strlen($notification['UF_TEXT']) > 150){?>
								<a href="#" class="more"></a>
						<?}?>
					</div>
				<?}?>
			</div>
		</div>
	</div>
</div>
