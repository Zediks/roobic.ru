<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Application;

class company extends CBitrixComponent
{
	function notifications()
	{
		global $USER;
		$userID = $USER->GetID();

		$arNotifications = getElHL(13,[],['UF_ID_USER' => $userID],['*']);
		$arResult['NOTIFICATIONS'] = $arNotifications;

		return $arResult;
	}

	public function executeComponent()
	{
		$this->arResult = array_merge($this->arResult,$this->notifications());
		$this->includeComponentTemplate();
	}
}?>
