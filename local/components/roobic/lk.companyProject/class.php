<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
	Loader::includeModule('iblock');
	Loader::includeModule('highloadblock');
use Bitrix\Main\Application;
use Bitrix\Highloadblock as HL, Bitrix\Main\Entity;

class company extends CBitrixComponent
{
	function editProject()
	{
		global $USER;
		$userID = $USER->GetID();
		$iblockID = 3;
		$contentManager = CSite::InGroup([8]);

		$params = [
			"max_len" => "100", // обрезает символьный код до 100 символов
			"change_case" => "L", // буквы преобразуются к нижнему регистру
			"replace_space" => "-", // меняем пробелы на тире
			"replace_other" => "-", // меняем левые символы на тире
			"delete_repeat_replace" => "true", // удаляем повторяющиеся символы
			"use_google" => "false", // отключаем использование google
		];

		// получим компанию для привязки
		$by = 'id'; $sort = 'asc'; // поле сортировки
	  $filter = ['ID' => $userID];
	  $arParams = [ // наши поля
			'FIELDS' => ['ID'],'SELECT' => ['UF_COMPANIES']
	  ];
	  $rsUsers = CUser::GetList($by, $sort, $filter, $arParams);
	  if($arUser = $rsUsers->Fetch())
			$companyID = $arUser['UF_COMPANIES'][0];

		$request = Application::getInstance()->getContext()->getRequest();
		$projectID = $request->getQuery("project_id");
		$sendFrom = $request->getPost("sendFrom");

		if($sendFrom){ // если форма отправлена

			$project_name = trim($request->getPost("project_name"));
			$project_code = CUtil::translit($project_name, "ru", $params);
			// $project_code = substr($project_code, 0, strrpos($project_code, '-'));

			// фото проекта
			$filesProjectPhotos = $request->getFile("project_photos");
			foreach ($filesProjectPhotos as $key => $arPhoto) {
				foreach($arPhoto as $key2 => $val2){
					$arProjectPhotos[$key2][$key] = $val2;
				}
			}
			$arFields['PHOTO'] = $arProjectPhotos;

			// фото фасадов
			$filesFacadePhotos = $request->getFile("facade_photos");
			foreach ($filesFacadePhotos as $key => $arPhoto) {
				foreach($arPhoto as $key2 => $val2){
					$arFacadePhotos[$key2][$key] = $val2;
				}
			}
			$arFields['FACADES'] = $arFacadePhotos;

			// удаление файлов
			$delFile = $request->getPost("del_file");
			if ($delFile) $arDelFile = explode(',',$delFile);

			$projectElement = new CIBlockElement;
			$elementFields = ['AREA','SIZE','CNT_BEDROOM','CNT_BATHROOM','MATERIAL_WALL','MATERIAL_ROOF','PRICE','OLD_PRICE','VIDEO','AREA_LIVING','CNT_FLOORS','GARAGE','HEIGHT','AREA_ROOF','FOUNDATION','TYPE','INCLUDED_PRICE','PURPOSE','FINISHING','MULTISTORY','COMMUNICATIONS','STYLE','FORM','TYPE_ROOF','ANNEX','FINISHING_FACADE','INSULATION','GUARANTEES','SIZE_LIST','FOUNDATION_LIST','CLASS','SIZE_BEAM','THICKNESS_BEAM','AREA_BALCONY'];

			if ($companyID && !$contentManager) $arFields['COMPANY'] = $companyID; // присвоим компанию

			if($contentManager && $request->getPost("COMPANY")) $arFields['COMPANY'] = $request->getPost("COMPANY");

			foreach ($elementFields as $field) { // заполним с формы
				$arFields[$field] = $request->getPost('project_'.strtolower($field));
			} // dump($arFields);

			$typeElement = getBath($arFields['TYPE']);
			$arFields['BATH'] = ($typeElement == 'бани') ? 107 : ''; // метка бани

			$arLoadProductArray = Array(
		    "IBLOCK_ID"					=> $iblockID,
				"IBLOCK_SECTION_ID"	=> false, // элемент лежит в корне раздела
				"MODIFIED_BY"				=> $userID, // элемент изменен текущим пользователем
				"ACTIVE"         		=> "N", // активен
		    "NAME"           		=> $project_name,
		    "CODE"           		=> $project_code,
		    "DETAIL_TEXT"   		=> $request->getPost("about"),
				"DETAIL_TEXT_TYPE" 	=> 'html',
				"PROPERTY_VALUES"		=> $arFields,
		  ); // dump($arFields);

			if ($projectID)
			{
				// удаление файлов (фото и фасады)
				if($delFile){
					foreach ($arDelFile as $key => $value)
						$arDeleteImages[$value] = ['VALUE' => ['del' => 'Y']];

					\CIBlockElement::SetPropertyValueCode($projectID, 'PHOTO', $arDeleteImages);
					\CIBlockElement::SetPropertyValueCode($projectID, 'FACADES', $arDeleteImages);
				}

				if ($projectElement->Update($projectID,$arLoadProductArray))
					echo 'Успешно ID: '.$projectID.'<br>';
				else
					echo 'Ошибка: '.$projectElement->LAST_ERROR.'<br>';
			}
			else
			{
				// проверим на существование по символьному коду
				$arOrder = ['SORT'=>'ASC'];
				$arFilter = ['IBLOCK_ID'=>3,'CODE'=>$project_code];
				$arSelect = ['ID'];
				$rsElements = CIBlockElement::GetList($arOrder,$arFilter,false,false,$arSelect);
				if ($arElement = $rsElements->Fetch())
					$arLoadProductArray['CODE'] = $arLoadProductArray['CODE'].'-1';

				// if ($arFields['COMPANY']) $arLoadProductArray['CODE'] = $project_code.'-'.$arFields['COMPANY'];

				if ($projectID = $projectElement->Add($arLoadProductArray))
					echo 'Успешно ID: '.$projectID.'<br>';
				else
					echo 'Ошибка: '.$projectElement->LAST_ERROR.'<br>';
			}

			if ($projectID) // изменим / занесем инфу в HL-блок
			{
				// планировка проекта
				$layout_id = $request->getPost("layout_id");
				$layout_area = $request->getPost("layout_area");
				$layout_file = $request->getFile("layout_file");

				if ($layout_area)
				{
					foreach ($layout_file as $key => $arLayoutPhoto)
						foreach ($arLayoutPhoto as $key2 => $val2)
							$arLayoutPhotos[$key2][$key] = $val2;

					// фото планировка проекта
					$layoutFiles = $request->getPost("layout_files");
					if($layoutFiles || $delFile){
						$arOurFile = explode(',',$layoutFiles);
						foreach ($arOurFile as $key => $value) { // если не удалено и не заменено
							if (!in_array($value,$arDelFile) && !$arLayoutPhotos[$key]['name'])
								$arLayoutPhotos[$key] = CFile::MakeFileArray($value);
						}
					}

					$hlblock_id = 10; // id HL
					$hlblock = HL\HighloadBlockTable::getById($hlblock_id)->fetch();
					$entity = HL\HighloadBlockTable::compileEntity($hlblock);
					$entity_data_class = $entity->getDataClass();

					foreach ($layout_area as $key => $val)
					{
						$nFloor = $key + 1;
						$layoutId = $layout_id[$key];
						// $layout_xml = $project_code . '_' . $nFloor . '_' . time();
					  $data = [
					    "UF_PLOTTAGE" => $layout_area[$key],
							"UF_NAME" => 'Площадь ' . $nFloor . ' этажа',
							// "UF_XML_ID" => $layout_xml,
							"UF_FILE" => $arLayoutPhotos[$key],
							"UF_HOUSE" => $projectID
					  ]; // dump($data);

						if ($layoutId)
							$entity_data_class::update($layoutId,$data);
						else
							$result = $entity_data_class::add($data);

						// $arFields['LAYOUT'][] = $layout_xml;
					}
				}

				// материалы и перечень работ
				$hlblock_id = 14; // id HL
				$hlblock = HL\HighloadBlockTable::getById($hlblock_id)->fetch();
				$entity = HL\HighloadBlockTable::compileEntity($hlblock);
				$entity_data_class = $entity->getDataClass();

				// фото материалы и перечень работ
				$listWorkFiles = $request->getPost("list_work_files");
				if($listWorkFiles || $delFile){
					$arOurFile = explode(',',$listWorkFiles);
					foreach ($arOurFile as $key => $value) { // если не удалено и не заменено
						if (!in_array($value,$arDelFile)) $arListWorkPhotos[$key] = CFile::MakeFileArray($value);
					}
				}

				for ($i=1; $i <= 6; $i++)
				{
					$project_block_id = $request->getPost('project_block_id_'.$i);
					$project_block_steps = $request->getPost('project_block_steps_'.$i);
					$project_block_file = $request->getFile('project_block_file_'.$i);
					$project_block_type = $request->getPost('project_block_type_'.$i);
					if (!$project_block_file['name']) $project_block_file = $arListWorkPhotos[$i-1];

					if ($project_block_steps[0])
					{
						// $block_xml = $project_code . '_block_' . $i . '_' . time();
						$data = [
							// "UF_XML_ID" => $block_xml,
							"UF_STEPS" => $project_block_steps,
							"UF_FILE" => $project_block_file,
							"UF_TYPE" => $project_block_type,
							"UF_HOUSE" => $projectID
						]; // dump($data);

						if ($project_block_id)
							$entity_data_class::update($project_block_id,$data);
						else
							$result = $entity_data_class::add($data);

						// $arFields['LIST_WORKS'][] = $block_xml;
					}
				}
			}
			// header('Location: /lk/company_projects');
		}

		if($projectID){ // получим нашу проект
			$iblockID = 3;
			$arOrder = Array('SORT'=>'ASC');
			$arFilter = Array('IBLOCK_ID'=>$iblockID,'ID'=>$projectID);
			$arSelect = Array('ID','NAME','DETAIL_TEXT','PROPERTY_AREA','PROPERTY_SIZE','PROPERTY_CNT_BEDROOM','PROPERTY_CNT_BATHROOM','PROPERTY_MATERIAL_WALL','PROPERTY_MATERIAL_ROOF','PROPERTY_PRICE','PROPERTY_OLD_PRICE','PROPERTY_PHOTO','PROPERTY_LAYOUT','PROPERTY_FACADES','PROPERTY_AREA_LIVING','PROPERTY_CNT_FLOORS','PROPERTY_GARAGE','PROPERTY_HEIGHT','PROPERTY_AREA_ROOF','PROPERTY_FOUNDATION','PROPERTY_VIDEO','PROPERTY_TYPE','PROPERTY_LIST_WORKS','PROPERTY_INCLUDED_PRICE','PROPERTY_COMPANY','PROPERTY_PURPOSE','PROPERTY_FINISHING','PROPERTY_MULTISTORY','PROPERTY_COMMUNICATIONS','PROPERTY_STYLE','PROPERTY_FORM','PROPERTY_TYPE_ROOF','PROPERTY_ANNEX','PROPERTY_FINISHING_FACADE','PROPERTY_INSULATION','PROPERTY_GUARANTEES','PROPERTY_SIZE_LIST','PROPERTY_FOUNDATION_LIST','PROPERTY_CLASS','PROPERTY_SIZE_BEAM','PROPERTY_THICKNESS_BEAM','PROPERTY_AREA_BALCONY'); // ,'PROPERTY_'
			$rsElements = CIBlockElement::GetList($arOrder,$arFilter,false,false,$arSelect);
			if($arElement = $rsElements->GetNext()){ // dump($arElement);
				if ($arElement['PROPERTY_COMPANY_VALUE'])
					if (($arElement['PROPERTY_COMPANY_VALUE'] != $companyID) && !$contentManager) die('Доступ запрещен!');

				$arResult['PROJECT'] = $arElement;

				// получим фото
				foreach ($arElement['PROPERTY_PHOTO_VALUE'] as $key => $value) {
					$arResult['PROJECT']['PHOTO'][$key] = CFile::ResizeImageGet($value, array('width'=>488, 'height'=>360), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
				}
				$arResult['PROJECT']['PHOTO_ID'] = $arElement['PROPERTY_PHOTO_PROPERTY_VALUE_ID'];
				foreach ($arElement['PROPERTY_FACADES_VALUE'] as $key => $value) {
					$arResult['PROJECT']['FACADES'][$key] = CFile::ResizeImageGet($value, array('width'=>488, 'height'=>360), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
				}
				$arResult['PROJECT']['FACADES_ID'] = $arElement['PROPERTY_FACADES_PROPERTY_VALUE_ID'];
			}
		}

		$arElHL = getElHL(7,[],[],['ID','UF_NAME','UF_XML_ID','UF_TYPE']);
		foreach ($arElHL as $value) {
			if ($value['UF_TYPE'] == 46) $arResult['MATERIALS_WALL'][] = $value; // Материал стен
			elseif ($value['UF_TYPE'] == 47) $arResult['MATERIALS_ROOF'][] = $value; // Материал кровли
		}

		$arResult['LAYOUT'] = array_values(getElHL(10,[],['UF_HOUSE'=>$projectID],['ID','UF_PLOTTAGE','UF_FILE']));

		foreach ($arResult['LAYOUT'] as $key => $value) {
			if($value['UF_FILE']){
				$arResult['LAYOUT'][$key]['UF_FILE_AR'] = CFile::ResizeImageGet($value['UF_FILE'], array('width'=>488, 'height'=>360), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
				$arResult['LAYOUT_IMG'][] = $value['UF_FILE'];
			}
			// удаление
			// if($sendFrom && $layout_id && !in_array($value['ID'],$layout_id))
			// {
			// 	$hlblock_id = 10; // id HL
			// 	$hlblock = HL\HighloadBlockTable::getById($hlblock_id)->fetch();
			// 	$entity = HL\HighloadBlockTable::compileEntity($hlblock);
			// 	$entity_data_class = $entity->getDataClass();
			// 	$entity_data_class::delete($value['ID']);
			// 	unset($arResult['LAYOUT'][$key]);
			// }
		}

		// получим материалы
		$arMaterials = getElHL(11,[],['UF_ACTIVE'=>true],['*']);
		foreach ($arMaterials as $key => $value) // распределим для формирования
		  $arResult['TYPE_HOUSE'][$value['UF_MAIN_SECTION']][$value['ID']] = $value;

		$arListWorks = getElHL(14,[],['UF_HOUSE'=>$projectID],['*']);
		foreach ($arListWorks as $value) {

			$arResult['LIST_WORKS'][$value['UF_TYPE']] = [
				'ID' => $value['ID'],
				'STEPS' => $value['UF_STEPS'],
				'FILE' => $value['UF_FILE'],
			];

			if($value['UF_FILE']){
				$arResult['LIST_WORKS'][$value['UF_TYPE']]['UF_FILE_AR'] = CFile::ResizeImageGet($value['UF_FILE'], array('width'=>488, 'height'=>360), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
				$arResult['LIST_WORKS_IMG'][] = $value['UF_FILE'];
			}
		}

		// получим св-во Входит в стоимость
		$rsPropertyEnum = \CIBlockPropertyEnum::GetList([],["IBLOCK_ID"=>3, "CODE"=>"INCLUDED_PRICE"]);
		while($arPropertyEnum = $rsPropertyEnum->Fetch()){
			$arResult['INCLUDED_PRICE'][$arPropertyEnum['ID']] = [
				'XML_ID' => $arPropertyEnum['XML_ID'],
				'NAME' => $arPropertyEnum['VALUE'],
			];
		}

		// получим св-во Назначение дома
		$rsPropertyEnum = \CIBlockPropertyEnum::GetList([],["IBLOCK_ID"=>3, "CODE"=>"PURPOSE"]);
		while($arPropertyEnum = $rsPropertyEnum->Fetch()){
			$arResult['PURPOSE'][$arPropertyEnum['ID']] = [
				'XML_ID' => $arPropertyEnum['XML_ID'],
				'NAME' => $arPropertyEnum['VALUE'],
			];
		}

		// получим св-во Отделка
		$rsPropertyEnum = \CIBlockPropertyEnum::GetList([],["IBLOCK_ID"=>3, "CODE"=>"FINISHING"]);
		while($arPropertyEnum = $rsPropertyEnum->Fetch()){
			$arResult['FINISHING'][$arPropertyEnum['ID']] = [
				'XML_ID' => $arPropertyEnum['XML_ID'],
				'NAME' => $arPropertyEnum['VALUE'],
			];
		}

		// получим доп. св-ва
		$arResult['PROP_ADDITIONAL'] = [
			[
				'CODE' => 'STYLE',
				'NAME' => 'Стиль, разновидность',
				'REQUIRED' => false
			],
			[
				'CODE' => 'FORM',
				'NAME' => 'Форма',
				'REQUIRED' => false
			],
			[
				'CODE' => 'TYPE_ROOF',
				'NAME' => 'Тип кровли',
				'REQUIRED' => false
			],
			[
				'CODE' => 'ANNEX',
				'NAME' => 'Пристрой',
				'REQUIRED' => false
			],
			[
				'CODE' => 'FINISHING_FACADE',
				'NAME' => 'Отделка фасада',
				'REQUIRED' => false
			],
			[
				'CODE' => 'INSULATION',
				'NAME' => 'Утеплитель',
				'REQUIRED' => false
			],
			[
				'CODE' => 'SIZE_LIST',
				'NAME' => 'Размер (список)',
				'REQUIRED' => false
			],
			[
				'CODE' => 'FOUNDATION_LIST',
				'NAME' => 'Фундамент',
				'REQUIRED' => false
			],
			[
				'CODE' => 'CLASS',
				'NAME' => 'Класс дома',
				'REQUIRED' => false
			],
			[
				'CODE' => 'SIZE_BEAM',
				'NAME' => 'Размер бруса',
				'REQUIRED' => false
			],
			[
				'CODE' => 'THICKNESS_BEAM',
				'NAME' => 'Толщина бруса',
				'REQUIRED' => false
			],
		];

		foreach ($arResult['PROP_ADDITIONAL'] as $value) {
			$propCode = $value['CODE'];
			$rsPropertyEnum = \CIBlockPropertyEnum::GetList([],["IBLOCK_ID"=>3, "CODE"=>$propCode]);
			while($arPropertyEnum = $rsPropertyEnum->Fetch()){
				$arResult[$propCode][$arPropertyEnum['ID']] = $arPropertyEnum['VALUE'];
			}
		}

    return $arResult;
	}

	public function executeComponent()
	{
		$this->arResult = array_merge($this->arResult,$this->editProject());
		$this->includeComponentTemplate();
	}
}?>
