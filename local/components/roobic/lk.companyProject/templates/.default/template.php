<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// dump($arResult);
?>
<?$this->SetViewTarget('body-class');?>
	class="lk-page"
<?$this->EndViewTarget();?>
<form action="" method="post" enctype="multipart/form-data">
	<div class="completed__item">
		<div class="row">
			<div class="col-lg-8 col-md-12">
				<div class="completed__block-name d-none d-xs-block d-sm-block d-md-hide">
					<div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_10.svg" alt=""></div><strong>Основная информация о проекте</strong>
					<div class="completed__block-desc">
						Расскажите подробнее о проекте
					</div>
				</div>
				<div class="completed__container">
					<div class="form-main-block hide-xs">
						<div class="form-block">
							<div class="form-block__name">Опишите проект дома</div>
						</div>
					</div>
					<div class="row align-items-center">
						<div class="col-md-12">
							<div class="form-block">
								<label for="project_name">
									<input class="form-input" type="text" id="project_name" name="project_name" value="<?=$arResult['PROJECT']['NAME']?>" placeholder="Название компании*" required>
									<span class="placeholder">Название проекта, 1 или 2 слова. Например, Венеция*</span>
									<a href="#" class="edit-input">
										<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
									</a>
								</label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-block">
								<label class="select" for="project_type">
									<select class="select__select select__styler" id="project_type" name="project_type" title="Тип дома*" required>
										<option value="">Тип дома*</option>
										<?foreach ($arResult['TYPE_HOUSE'] as $idMain => $mainSection) {?>
				              <optgroup label="<?=$idMain?>">
				                <?foreach ($mainSection as $section) {?>
				                  <option value="<?=$section['UF_XML_ID']?>" <?if($section['UF_XML_ID'] == $arResult['PROJECT']['PROPERTY_TYPE_VALUE'])echo 'selected'?>><?=$section['UF_NAME']?></option>
				                <?}?>
				              </optgroup>
				            <?}?>
									</select>
								</label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-block">
								<label for="project_area">
									<input class="form-input" type="text" id="project_area" name="project_area" value="<?=$arResult['PROJECT']['PROPERTY_AREA_VALUE']?>" placeholder="Площадь, м2*" required>
									<span class="placeholder">Площадь, м2*</span>
									<a href="#" class="edit-input">
										<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
									</a>
								</label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-block">
								<label for="project_size">
									<input class="form-input" type="text" id="project_size" name="project_size" value="<?=$arResult['PROJECT']['PROPERTY_SIZE_VALUE']?>" placeholder="Размеры, м×м*" required>
									<span class="placeholder">Размеры, м×м*</span>
									<a href="#" class="edit-input">
										<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
									</a>
								</label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-block">
								<label class="select" for="project_cnt_bedroom">
									<select class="select__select select__styler" name="project_cnt_bedroom" id="project_cnt_bedroom" title="Количество спален*" required>
											<option value="">Количество спален*</option>
										<?for ($i=1; $i <= 10; $i++) {?>
											<option value="<?=$i?>" <?if($i == $arResult['PROJECT']['PROPERTY_CNT_BEDROOM_VALUE'])echo 'selected'?>><?=$i?></option>
										<?}?>
									</select>
								</label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-block">
								<label class="select" for="project_cnt_bathroom">
									<select class="select__select select__styler" name="project_cnt_bathroom" id="project_cnt_bathroom" title="Количество санузлов*" required>
											<option value="">Количество санузлов*</option>
										<?for ($i=1; $i <= 4; $i++) {?>
											<option value="<?=$i?>" <?if($i == $arResult['PROJECT']['PROPERTY_CNT_BATHROOM_VALUE'])echo 'selected'?>><?=$i?></option>
										<?}?>
									</select>
								</label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-block">
								<label class="select" for="project_material_wall">
									<select class="select__select select__styler" name="project_material_wall" id="project_material_wall" title="Материал стен*" required>
											<option value="">Материал стен*</option>
										<?foreach ($arResult['MATERIALS_WALL'] as $value) {?>
											<option value="<?=$value['UF_XML_ID']?>" <?if(in_array($value['UF_XML_ID'],$arResult['PROJECT']['PROPERTY_MATERIAL_WALL_VALUE']))echo 'selected'?>><?=$value['UF_NAME']?></option>
										<?}?>
									</select>
								</label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-block">
								<label class="select" for="project_material_roof">
									<select class="select__select select__styler" name="project_material_roof" id="project_material_roof" title="Материал кровли*" required>
											<option value="">Материал кровли*</option>
										<?foreach ($arResult['MATERIALS_ROOF'] as $value) {?>
											<option value="<?=$value['UF_XML_ID']?>" <?if(in_array($value['UF_XML_ID'],$arResult['PROJECT']['PROPERTY_MATERIAL_ROOF_VALUE']))echo 'selected'?>><?=$value['UF_NAME']?></option>
										<?}?>
									</select>
								</label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-block">
								<label for="project_price">
									<input class="form-input" type="number" id="project_price" name="project_price" value="<?=$arResult['PROJECT']['PROPERTY_PRICE_VALUE']?>" placeholder="Цена, руб*" required>
									<span class="placeholder">Цена, руб*</span>
									<a href="#" class="edit-input">
										<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
									</a>
								</label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-block">
								<label for="project_old_price">
									<input class="form-input" type="number" id="project_old_price" name="project_old_price" value="<?=$arResult['PROJECT']['PROPERTY_OLD_PRICE_VALUE']?>" placeholder="Старая цена, руб">
									<span class="placeholder">Старая цена, руб</span>
									<a href="#" class="edit-input">
										<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
									</a>
								</label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-block">
								<label class="select" for="project_purpose">
									<select class="select__select select__styler" name="project_purpose" id="project_purpose" title="Назначение дома*" required>
											<option value="">Назначение дома*</option>
										<?foreach ($arResult['PURPOSE'] as $key => $value) {?>
											<option value="<?=$key?>" <?if(array_key_exists($key,$arResult['PROJECT']['PROPERTY_PURPOSE_VALUE']))echo 'selected'?>><?=$value['NAME']?></option>
										<?}?>
									</select>
								</label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-block">
								<label class="select" for="project_finishing">
									<select class="select__select select__styler" name="project_finishing" id="project_finishing" title="Отделка*" required>
											<option value="">Отделка*</option>
										<?foreach ($arResult['FINISHING'] as $key => $value) {?>
											<option value="<?=$key?>" <?if($key == $arResult['PROJECT']['PROPERTY_FINISHING_ENUM_ID'])echo 'selected'?>><?=$value['NAME']?></option>
										<?}?>
									</select>
								</label>
							</div>
						</div>
					</div>
					<!-- <div class="row align-items-center">
						<div class="col-md-6">
							<div class="form-block">
								<label class="radio" for="project_multistory">
                  <input type="checkbox" name="project_multistory" value="21" id="project_multistory" <?if($arResult['PROJECT']['PROPERTY_MULTISTORY_ENUM_ID'])echo'checked'?>>
                  <div class="checks"></div><span>Многоэтажный</span>
                </label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-block">
								<label class="radio" for="project_communications">
									<input type="checkbox" name="project_communications" value="22" id="project_communications" <?if($arResult['PROJECT']['PROPERTY_COMMUNICATIONS_ENUM_ID'])echo'checked'?>>
									<div class="checks"></div><span>Наличие коммуникаций</span>
								</label>
							</div>
						</div>
					</div> -->
				</div>
			</div>
			<div class="col-lg-4 col-xl-3 d-none d-lg-block">
				<div class="completed__block-name">
					<div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_10.svg" alt=""></div><strong>Основная информация о проекте</strong>
					<div class="completed__block-desc">
						Расскажите подробнее о проекте
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="completed__item">
		<div class="row">
			<div class="col-lg-8 col-md-12">
				<div class="completed__block-name d-none d-xs-block d-sm-block d-md-hide">
					<div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_10.svg" alt=""></div><strong>Дополнительная информация о проекте</strong>
				</div>
				<div class="completed__container">
					<div class="form-main-block">
						<div class="row">
							<?foreach ($arResult['PROP_ADDITIONAL'] as $propAdditional) {
								// показывать только для значений из бруса
								$arBeam = ['doma-iz-brusa','dom-iz-strogannogo-brusa','dom-iz-profilirovannnogo-brusa','dom-iz-kleennogo-brusa','dom-iz-dvoynogo-brusa','bani-iz-brusa','bani-iz-strogannogo-brusa','bani-iz-profilirovannogo-brusa','bani-iz-kleenogo-brusa'];
								$classHide = (in_array($propAdditional['CODE'],['SIZE_BEAM','THICKNESS_BEAM']) && !in_array($arResult['PROJECT']['PROPERTY_TYPE_VALUE'],$arBeam))?'hide':'';?>
								<div class="col-md-6 <?=$classHide?>" id="block-<?=strtolower($propAdditional['CODE'])?>">
									<div class="form-block">
										<label class="select" for="project_<?=strtolower($propAdditional['CODE'])?>">
											<select class="select__select select__styler" name="project_<?=strtolower($propAdditional['CODE'])?>" id="project_<?=strtolower($propAdditional['CODE'])?>" title="<?=$propAdditional['NAME']?><?if($propAdditional['REQUIRED'])echo'*';?>" <?if($propAdditional['REQUIRED'])echo'required';?>>
													<option value=""><?=$propAdditional['NAME']?></option>
												<?foreach ($arResult[$propAdditional['CODE']] as $key => $value) {?>
													<option value="<?=$key?>" <?if($key == $arResult['PROJECT']['PROPERTY_'.$propAdditional['CODE'].'_ENUM_ID'])echo 'selected'?>><?=$value?></option>
												<?}?>
											</select>
										</label>
									</div>
								</div>
							<?}?>
							<div class="col-md-6">
								<div class="form-block">
									<label>
										<input class="form-input" type="text" name="project_area_balcony" value="<?=$arResult['PROJECT']['PROPERTY_AREA_BALCONY_VALUE']?>" placeholder="Площадь террасы/балкона">
										<span class="placeholder">Площадь террасы/балкона</span>
										<a href="#" class="edit-input">
											<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
										</a>
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-xl-3 d-none d-lg-block">
				<div class="completed__block-name">
					<div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_10.svg" alt=""></div><strong>Дополнительная информация о проекте</strong>
				</div>
			</div>
		</div>
	</div>

	<div class="completed__item">
		<div class="row">
			<div class="col-lg-8 col-md-12">
				<div class="completed__block-name d-none d-xs-block d-sm-block d-md-hide">
					<div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_07.svg" alt=""></div><strong>Загрузите фото</strong>
				</div>
				<div class="completed__container">
					<div class="form-main-block block_service">
						<div class="row">
							<div class="col-md-12">
								<div class="form-block">
									<div class="form-block__name">Фото проекта</div>
									<div class="download-photo">
										<?for ($i=0; $i < 3; $i++) {?>
											<div class="list">
												<label class="label-file <?if($arResult['PROJECT']['PHOTO'][$i])echo'has-img'?>">
													<input type="file" accept=".png, .jpg, .jpeg" name="project_photos[]">
													<span>Добавьте файл для загрузки</span>
													<span>в это поле</span>
													<img src="<?=$arResult['PROJECT']['PHOTO'][$i]['src']?>" alt="">
												</label>
												<span class="delete-file" data-id="<?=$arResult['PROJECT']['PHOTO_ID'][$i]?>">х</span>
											</div>
										<?}?>
										<input type="hidden" name="del_file" value="">
										<input type="hidden" name="layout_files" value="<?=implode(',',$arResult['LAYOUT_IMG'])?>">
										<input type="hidden" name="list_work_files" value="<?=implode(',',$arResult['LIST_WORKS_IMG'])?>">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-xl-3 d-none d-lg-block">
				<div class="completed__block-name">
					<div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_07.svg" alt=""></div><strong>Загрузите фото</strong>
				</div>
			</div>
		</div>
	</div>
	<div class="completed__item">
		<div class="row">
			<div class="col-lg-8">
				<div class="completed__block-name d-none d-xs-block d-sm-block d-md-hide">
					<div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_kp_03.svg" alt=""></div><strong>Расскажите о планировке</strong>
					<div class="completed__block-desc">
						Покажите для наглядности клиенту планировку проекта по этажам
					</div>
				</div>
				<div class="house_plan completed__container">
					<div class="form-main-block">
						<div class="row">
							<div class="col-md-12">
								<div class="form-block">
									<div class="form-block__name">Планировка проекта</div>
								</div>
							</div>
						</div>
						<? $iFloor = 0;
						do { $nFloor = $iFloor + 1;
							// $inlineClass = ($iFloor != 0) ? 'inline' : '';?>
							<div class="row house_plan-item <?if ($iFloor == count($arResult['LAYOUT'])-1 || count($arResult['LAYOUT']) == 0) echo 'insert_after'?>">
								<div class="col-md-6">
									<div class="form-block">
										<label>
											<input class="form-input" type="text" name="layout_area[]" value="<?=$arResult['LAYOUT'][$iFloor]['UF_PLOTTAGE']?>" placeholder="Площадь <?=$nFloor+1?>-го этажа, м2*">
											<span class="placeholder">Площадь <?=$nFloor?>-го этажа, м2*</span>
											<a href="#" class="edit-input">
												<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
											</a>
										</label>
										<input type="hidden" name="layout_id[]" value="<?=$arResult['LAYOUT'][$iFloor]['ID']?>">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-block inline">
										<div class="example-2">
											<div class="form-group download-photo">
												<label class="btn-tertiary js-labelFile <?if($arResult['LAYOUT'][$iFloor]['UF_FILE'])echo'has-img'?>">
													<input type="file" name="layout_file[]" class="input-file">
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px">
														<image x="0px" y="0px" width="12px" height="12px" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAQAAAD8fJRsAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfkCgUXGhsfTp1lAAAA50lEQVQY00WQu0oDUQBEz1zXIibFtmJjYSEq+BemMYiF4AOsbGS/YH1gZSCQIIsg2oRooRAVwd4PsLUTq5gUYiNJ5wPGwsdOeYrhzCgDQBhghD2tM+xjqhF5ihyxwK6/2OE+5FgHVLTpQ871qVL0jxssK/EFBdXd10P4Lamz4o7HiKi6oneXA1BSw6t0GdIL+yzy5lF6gQI1r/HEBzVmNU9PMdtcBpYoc0rsK6aZ8ysTbHGGlV3rkRNuKTKg7ymlNA0EAjEdNmi560lSN380I26c6dl3FDTjlNbfrIg24yQkDEhp5zd8A7H2TNMiTyyJAAAAAElFTkSuQmCC" />
													</svg>
													<img src="<?=$arResult['LAYOUT'][$iFloor]['UF_FILE_AR']['src']?>" alt="">
													<span class="js-fileName">Выберите фото</span>
												</label>
												<span class="delete-file" data-id="<?=$arResult['LAYOUT'][$iFloor]['UF_FILE']?>">х</span>
											</div>
										</div>
										<?if($iFloor != 0){?>
											<a href="" class="block_remove">
						            <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/remove.svg">
						          </a>
										<?}?>
									</div>
								</div>
							</div>
						<? $iFloor++; } while ($iFloor < count($arResult['LAYOUT'])); ?>
						<div class="row house_plan-bottom">
							<div class="col-md-6"></div>
							<div class="col-md-6">
								<a href="#" data-block="block_material" class="add-item" data-block="block_staff"><span class="add-item_ico">+</span> <span class="add-item_link">Добавить этаж</span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-xl-3 d-none d-lg-block">
				<div class="completed__block-name">
					<div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_kp_03.svg" alt=""></div><strong>Расскажите о планировке</strong>
					<div class="completed__block-desc">
						Покажите для наглядности клиенту планировку проекта по этажам
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="completed__item">
		<div class="row">
			<div class="col-lg-8 col-md-12">
				<div class="completed__block-name d-none d-xs-block d-sm-block d-md-hide">
					<div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_kp_04.svg" alt=""></div><strong>Покажите фасады проекта</strong>
					<div class="completed__block-desc">
						Загрузите изображения фасадов
					</div>
				</div>
				<div class="completed__container block_staff">
					<div class="form-main-block">
						<div class="row hide-xs">
							<div class="col-md-12">
								<div class="form-block">
									<div class="form-block__name">Фасады</div>
									<div class="form-block__desc">Загрузите 4 изображения фасадов проекта со всех сторон</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="download-photo">
									<?for ($i=0; $i < 4; $i++) {?>
										<div class="list">
											<label class="label-file <?if($arResult['PROJECT']['FACADES'][$i])echo'has-img'?>">
												<input type="file" accept=".png, .jpg, .jpeg" name="facade_photos[]">
												<span>Добавьте файл для загрузки</span>
												<span>в это поле</span>
												<img src="<?=$arResult['PROJECT']['FACADES'][$i]['src']?>" alt="">
											</label>
											<span class="delete-file" data-id="<?=$arResult['PROJECT']['FACADES_ID'][$i]?>">х</span>
										</div>
									<?}?>
								</div>
							</div>
							<!-- <div class="col-md-3">
								Изображение 1
								<div class="example-2">
									<div class="form-group download-photo">
										<input type="file" name="facade_photos[]" id="facade-1" class="input-file">
										<label for="facade-1" class="btn-tertiary js-labelFile">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px">
												<image x="0px" y="0px" width="12px" height="12px" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAQAAAD8fJRsAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfkCgUXGhsfTp1lAAAA50lEQVQY00WQu0oDUQBEz1zXIibFtmJjYSEq+BemMYiF4AOsbGS/YH1gZSCQIIsg2oRooRAVwd4PsLUTq5gUYiNJ5wPGwsdOeYrhzCgDQBhghD2tM+xjqhF5ihyxwK6/2OE+5FgHVLTpQ871qVL0jxssK/EFBdXd10P4Lamz4o7HiKi6oneXA1BSw6t0GdIL+yzy5lF6gQI1r/HEBzVmNU9PMdtcBpYoc0rsK6aZ8ysTbHGGlV3rkRNuKTKg7ymlNA0EAjEdNmi560lSN380I26c6dl3FDTjlNbfrIg24yQkDEhp5zd8A7H2TNMiTyyJAAAAAElFTkSuQmCC" />
											</svg>
											<span class="js-fileName">Выберите фото</span>
										</label>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								Изображение 2
								<div class="example-2">
									<div class="form-group">
										<input type="file" name="facade_photos[]" id="facade-2" class="input-file">
										<label for="facade-2" class="btn-tertiary js-labelFile">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px">
												<image x="0px" y="0px" width="12px" height="12px" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAQAAAD8fJRsAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfkCgUXGhsfTp1lAAAA50lEQVQY00WQu0oDUQBEz1zXIibFtmJjYSEq+BemMYiF4AOsbGS/YH1gZSCQIIsg2oRooRAVwd4PsLUTq5gUYiNJ5wPGwsdOeYrhzCgDQBhghD2tM+xjqhF5ihyxwK6/2OE+5FgHVLTpQ871qVL0jxssK/EFBdXd10P4Lamz4o7HiKi6oneXA1BSw6t0GdIL+yzy5lF6gQI1r/HEBzVmNU9PMdtcBpYoc0rsK6aZ8ysTbHGGlV3rkRNuKTKg7ymlNA0EAjEdNmi560lSN380I26c6dl3FDTjlNbfrIg24yQkDEhp5zd8A7H2TNMiTyyJAAAAAElFTkSuQmCC" />
											</svg>
											<span class="js-fileName">Выберите фото</span>
										</label>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								Изображение 3
								<div class="example-2">
									<div class="form-group">
										<input type="file" name="facade_photos[]" id="facade-3" class="input-file">
										<label for="facade-3" class="btn-tertiary js-labelFile">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px">
												<image x="0px" y="0px" width="12px" height="12px" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAQAAAD8fJRsAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfkCgUXGhsfTp1lAAAA50lEQVQY00WQu0oDUQBEz1zXIibFtmJjYSEq+BemMYiF4AOsbGS/YH1gZSCQIIsg2oRooRAVwd4PsLUTq5gUYiNJ5wPGwsdOeYrhzCgDQBhghD2tM+xjqhF5ihyxwK6/2OE+5FgHVLTpQ871qVL0jxssK/EFBdXd10P4Lamz4o7HiKi6oneXA1BSw6t0GdIL+yzy5lF6gQI1r/HEBzVmNU9PMdtcBpYoc0rsK6aZ8ysTbHGGlV3rkRNuKTKg7ymlNA0EAjEdNmi560lSN380I26c6dl3FDTjlNbfrIg24yQkDEhp5zd8A7H2TNMiTyyJAAAAAElFTkSuQmCC" />
											</svg>
											<span class="js-fileName">Выберите фото</span>
										</label>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								Изображение 4
								<div class="example-2">
									<div class="form-group">
										<input type="file" name="facade_photos[]" id="facade-4" class="input-file">
										<label for="facade-4" class="btn-tertiary js-labelFile">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px">
												<image x="0px" y="0px" width="12px" height="12px" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAQAAAD8fJRsAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfkCgUXGhsfTp1lAAAA50lEQVQY00WQu0oDUQBEz1zXIibFtmJjYSEq+BemMYiF4AOsbGS/YH1gZSCQIIsg2oRooRAVwd4PsLUTq5gUYiNJ5wPGwsdOeYrhzCgDQBhghD2tM+xjqhF5ihyxwK6/2OE+5FgHVLTpQ871qVL0jxssK/EFBdXd10P4Lamz4o7HiKi6oneXA1BSw6t0GdIL+yzy5lF6gQI1r/HEBzVmNU9PMdtcBpYoc0rsK6aZ8ysTbHGGlV3rkRNuKTKg7ymlNA0EAjEdNmi560lSN380I26c6dl3FDTjlNbfrIg24yQkDEhp5zd8A7H2TNMiTyyJAAAAAElFTkSuQmCC" />
											</svg>
											<span class="js-fileName">Выберите фото</span>
										</label>
									</div>
								</div>
							</div> -->
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-xl-3 d-none d-lg-block">
				<div class="completed__block-name">
					<div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_kp_04.svg" alt=""></div><strong>Покажите фасады проекта</strong>
					<div class="completed__block-desc">
						Загрузите изображения фасадов
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="completed__item">
		<div class="row">
			<div class="col-lg-8 col-md-12">
				<div class="completed__block-name d-none d-xs-block d-sm-block d-md-hide">
					<div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_01.svg" alt=""></div><strong>О проекте</strong>
					<div class="completed__block-desc">
						Поделитесь своим профилем в социальных сетях
					</div>
				</div>
				<div class="completed__container">
					<div class="form-main-block">
						<div class="row">
							<div class="col-md-12 hide-xs">
								<div class="form-block">
									<div class="form-block__name">О проекте</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-block">
									<label>
										<textarea class="form-textarea" name="about" cols="30" rows="10" placeholder="Описание деятельности"><?=$arResult['PROJECT']['DETAIL_TEXT']?></textarea>
									</label>
									<p>Описание должно содержать не менее 200, и не более 1000 символов</p>

									<label for="project_guarantees">
										<input class="form-input" type="text" id="project_guarantees" name="project_guarantees" value="<?=$arResult['PROJECT']['PROPERTY_GUARANTEES_VALUE']?>" placeholder="Гарантии">
										<span class="placeholder">Гарантии</span>
										<a href="#" class="edit-input">
											<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
										</a>
									</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-block">
									<div class="form-block__name">Видео с YouTube <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/youtube_icon.svg" alt="" class="form-block__ico"></div>
								</div>
								<div class="form-block">
									<label>
										<input class="form-input" type="text" name="project_video" value="<?=$arResult['PROJECT']['PROPERTY_VIDEO_VALUE']?>" placeholder="Ссылка на видео с YouTube">
										<span class="placeholder">Ссылка на видео с YouTube</span>
										<a href="#" class="edit-input">
											<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
										</a>
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-xl-3 d-none d-lg-block">
				<div class="completed__block-name">
					<div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_01.svg" alt=""></div><strong>Опишите проект</strong>
					<div class="completed__block-desc">
						Опишите более подробно проект, укажите ссылку на видео с Youtube
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="completed__item">
		<div class="row">
			<div class="col-lg-8 col-md-12">
				<div class="completed__block-name d-none d-xs-block d-sm-block d-md-hide">
					<div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_06.svg" alt=""></div><strong>Основные характеристики</strong>
					<div class="completed__block-desc">
						Заполните ключевые характеристики проекта, например, общая и жилая площадь, количество этажей и т.д.
					</div>
				</div>
				<div class="completed__container">
					<div class="form-main-block">
						<div class="row">
							<div class="col-md-12">
								<div class="form-block">
									<div class="form-block__name">Характеристики</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-block">
									<label>
										<input class="form-input" type="text" name="project_area_living" value="<?=$arResult['PROJECT']['PROPERTY_AREA_LIVING_VALUE']?>" placeholder="Жилая площадь, м2*">
										<span class="placeholder">Жилая площадь, м2*</span>
										<a href="#" class="edit-input">
											<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
										</a>
									</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-block">
									<label>
										<input class="form-input" type="text" name="project_cnt_floors" value="<?=$arResult['PROJECT']['PROPERTY_CNT_FLOORS_VALUE']?>" placeholder="Количество этажей*">
										<span class="placeholder">Количество этажей*</span>
										<a href="#" class="edit-input">
											<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
										</a>
									</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-block">
									<label>
										<input class="form-input" type="text" name="project_garage" value="<?=$arResult['PROJECT']['PROPERTY_GARAGE_VALUE']?>" placeholder="Гараж, м2">
										<span class="placeholder">Гараж, м2</span>
										<a href="#" class="edit-input">
											<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
										</a>
									</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-block">
									<label>
										<input class="form-input" type="text" name="project_height" value="<?=$arResult['PROJECT']['PROPERTY_HEIGHT_VALUE']?>" placeholder="Высота дома, м*">
										<span class="placeholder">Высота дома, м*</span>
										<a href="#" class="edit-input">
											<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
										</a>
									</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-block">
									<label>
										<input class="form-input" type="text" name="project_area_roof" value="<?=$arResult['PROJECT']['PROPERTY_AREA_ROOF_VALUE']?>" placeholder="Площадь кровли, м2*">
										<span class="placeholder">Площадь кровли, м2*</span>
										<a href="#" class="edit-input">
											<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
										</a>
									</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-block">
									<label>
										<input class="form-input" type="text" name="project_foundation" value="<?=$arResult['PROJECT']['PROPERTY_FOUNDATION_VALUE']?>" placeholder="Фундамент*">
										<span class="placeholder">Фундамент*</span>
										<a href="#" class="edit-input">
											<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
										</a>
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-xl-3 d-none d-lg-block">
				<div class="completed__block-name">
					<div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_06.svg" alt=""></div><strong>Основные характеристики</strong>
					<div class="completed__block-desc">
						Заполните ключевые характеристики проекта, например, общая и жилая площадь, количество этажей и т.д.
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="completed__item">
		<div class="row">
			<div class="col-lg-8 col-md-12">
				<div class="completed__block-name d-none d-xs-block d-sm-block d-md-hide">
					<div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_03.svg" alt=""></div><strong>Материалы и перечень работ</strong>
					<div class="completed__block-desc">
						Опишите материалы, используемые вами в работе, и перечень работ
					</div>
				</div>
				<div class="completed__container">
					<div class="form-main-block">
						<div class="row">
							<div class="col-md-12">
								<div class="form-block">
									<div class="form-block__name">Фундамент</div>
									<div class="form-block__desc">Опишите пошагово, какой перечень работ входит в стоимость. Каждый шаг должен содержать не менее 10 символов</div>
									<input type="hidden" name="project_block_id_1" value="<?=$arResult['LIST_WORKS'][13]['ID']?>">
									<input type="hidden" name="project_block_type_1" value="13">
								</div>
							</div>
						</div>
						<div class="row foundation__item">
							<div class="col-md-6">
								<div class="form-block">
									<label <?if ($arResult['LIST_WORKS'][13]['STEPS'][0]) echo 'class="label-up"'?>>
										<textarea class="form-textarea" name="project_block_steps_1[]" cols="30" rows="3" placeholder=""><?=$arResult['LIST_WORKS'][13]['STEPS'][0]?></textarea>
										<span class="placeholder">Шаг <span>1</span>. Например, Удаление поверхностного слоя грунта*</span>
									</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-block">
									<label <?if ($arResult['LIST_WORKS'][13]['STEPS'][1]) echo 'class="label-up"'?>>
										<textarea class="form-textarea" name="project_block_steps_1[]" cols="30" rows="3" placeholder=""><?=$arResult['LIST_WORKS'][13]['STEPS'][1]?></textarea>
										<span class="placeholder">Шаг <span>2</span>. Например, Удаление поверхностного слоя грунта*</span>
									</label>
								</div>
							</div>
						</div>
						<div class="row foundation__item">
							<div class="col-md-6">
								<div class="form-block">
									<label <?if ($arResult['LIST_WORKS'][13]['STEPS'][2]) echo 'class="label-up"'?>>
										<textarea class="form-textarea" name="project_block_steps_1[]" cols="30" rows="3" placeholder=""><?=$arResult['LIST_WORKS'][13]['STEPS'][2]?></textarea>
										<span class="placeholder">Шаг <span>3</span>. Например, Удаление поверхностного слоя грунта*</span>
									</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-block">
									<label <?if ($arResult['LIST_WORKS'][13]['STEPS'][3]) echo 'class="label-up"'?>>
										<textarea class="form-textarea" name="project_block_steps_1[]" cols="30" rows="3" placeholder=""><?=$arResult['LIST_WORKS'][13]['STEPS'][3]?></textarea>
										<span class="placeholder">Шаг <span>4</span>. Например, Удаление поверхностного слоя грунта*</span>
									</label>
								</div>
							</div>
						</div>
						<div class="row foundation__item">
							<div class="col-md-6">
								<div class="form-block">
									<label <?if ($arResult['LIST_WORKS'][13]['STEPS'][4]) echo 'class="label-up"'?>>
										<textarea class="form-textarea" name="project_block_steps_1[]" cols="30" rows="3" placeholder=""><?=$arResult['LIST_WORKS'][13]['STEPS'][4]?></textarea>
										<span class="placeholder">Шаг <span>5</span>. Например, Удаление поверхностного слоя грунта*</span>
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="form-main-block">
						<div class="row">
							<div class="col-md-12">
								<div class="form-block">
									<div class="form-block__name">Коробка дома</div>
									<div class="form-block__desc">Опишите пошагово, какой перечень работ входит в стоимость. Каждый шаг должен содержать не менее 10 символов</div>
									<input type="hidden" name="project_block_id_2" value="<?=$arResult['LIST_WORKS'][14]['ID']?>">
									<input type="hidden" name="project_block_type_2" value="14">
								</div>
							</div>
						</div>
						<div class="row foundation__item">
							<div class="col-md-6">
								<div class="form-block">
									<label <?if ($arResult['LIST_WORKS'][14]['STEPS'][0]) echo 'class="label-up"'?>>
										<textarea class="form-textarea" name="project_block_steps_2[]" cols="30" rows="3" placeholder=""><?=$arResult['LIST_WORKS'][14]['STEPS'][0]?></textarea>
										<span class="placeholder">Шаг <span>1</span>. Например, Удаление поверхностного слоя грунта*</span>
									</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-block">
									<label <?if ($arResult['LIST_WORKS'][14]['STEPS'][1]) echo 'class="label-up"'?>>
										<textarea class="form-textarea" name="project_block_steps_2[]" cols="30" rows="3" placeholder=""><?=$arResult['LIST_WORKS'][14]['STEPS'][1]?></textarea>
										<span class="placeholder">Шаг <span>2</span>. Например, Удаление поверхностного слоя грунта*</span>
									</label>
								</div>
							</div>
						</div>
						<div class="row foundation__item">
							<div class="col-md-6">
								<div class="form-block">
									<label <?if ($arResult['LIST_WORKS'][14]['STEPS'][2]) echo 'class="label-up"'?>>
										<textarea class="form-textarea" name="project_block_steps_2[]" cols="30" rows="3" placeholder=""><?=$arResult['LIST_WORKS'][14]['STEPS'][2]?></textarea>
										<span class="placeholder">Шаг <span>3</span>. Например, Удаление поверхностного слоя грунта*</span>
									</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-block">
									<label <?if ($arResult['LIST_WORKS'][14]['STEPS'][3]) echo 'class="label-up"'?>>
										<textarea class="form-textarea" name="project_block_steps_2[]" cols="30" rows="3" placeholder=""><?=$arResult['LIST_WORKS'][14]['STEPS'][3]?></textarea>
										<span class="placeholder">Шаг <span>4</span>. Например, Удаление поверхностного слоя грунта*</span>
									</label>
								</div>
							</div>
						</div>
						<div class="row foundation__item">
							<div class="col-md-6">
								<div class="form-block">
									<label <?if ($arResult['LIST_WORKS'][14]['STEPS'][4]) echo 'class="label-up"'?>>
										<textarea class="form-textarea" name="project_block_steps_2[]" cols="30" rows="3" placeholder=""><?=$arResult['LIST_WORKS'][14]['STEPS'][4]?></textarea>
										<span class="placeholder">Шаг <span>5</span>. Например, Удаление поверхностного слоя грунта*</span>
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="form-main-block">
						<div class="row">
							<div class="col-md-12">
								<div class="form-block">
									<div class="form-block__name">Кровля</div>
									<div class="form-block__desc">Опишите пошагово, какой перечень работ входит в стоимость. Каждый шаг должен содержать не менее 10 символов</div>
									<input type="hidden" name="project_block_id_3" value="<?=$arResult['LIST_WORKS'][15]['ID']?>">
									<input type="hidden" name="project_block_type_3" value="15">
								</div>
							</div>
						</div>
						<div class="row foundation__item">
							<div class="col-md-6">
								<div class="form-block">
									<label <?if ($arResult['LIST_WORKS'][15]['STEPS'][0]) echo 'class="label-up"'?>>
										<textarea class="form-textarea" name="project_block_steps_3[]" cols="30" rows="3" placeholder=""><?=$arResult['LIST_WORKS'][15]['STEPS'][0]?></textarea>
										<span class="placeholder">Шаг <span>1</span>. Например, Удаление поверхностного слоя грунта*</span>
									</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-block">
									<label <?if ($arResult['LIST_WORKS'][15]['STEPS'][1]) echo 'class="label-up"'?>>
										<textarea class="form-textarea" name="project_block_steps_3[]" cols="30" rows="3" placeholder=""><?=$arResult['LIST_WORKS'][15]['STEPS'][1]?></textarea>
										<span class="placeholder">Шаг <span>2</span>. Например, Удаление поверхностного слоя грунта*</span>
									</label>
								</div>
							</div>
						</div>
						<div class="row foundation__item">
							<div class="col-md-6">
								<div class="form-block">
									<label <?if ($arResult['LIST_WORKS'][15]['STEPS'][2]) echo 'class="label-up"'?>>
										<textarea class="form-textarea" name="project_block_steps_3[]" cols="30" rows="3" placeholder=""><?=$arResult['LIST_WORKS'][15]['STEPS'][2]?></textarea>
										<span class="placeholder">Шаг <span>3</span>. Например, Удаление поверхностного слоя грунта*</span>
									</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-block">
									<label <?if ($arResult['LIST_WORKS'][15]['STEPS'][3]) echo 'class="label-up"'?>>
										<textarea class="form-textarea" name="project_block_steps_3[]" cols="30" rows="3" placeholder=""><?=$arResult['LIST_WORKS'][15]['STEPS'][3]?></textarea>
										<span class="placeholder">Шаг <span>4</span>. Например, Удаление поверхностного слоя грунта*</span>
									</label>
								</div>
							</div>
						</div>
						<div class="row foundation__item">
							<div class="col-md-6">
								<div class="form-block">
									<label <?if ($arResult['LIST_WORKS'][15]['STEPS'][4]) echo 'class="label-up"'?>>
										<textarea class="form-textarea" name="project_block_steps_3[]" cols="30" rows="3" placeholder=""><?=$arResult['LIST_WORKS'][15]['STEPS'][4]?></textarea>
										<span class="placeholder">Шаг <span>5</span>. Например, Удаление поверхностного слоя грунта*</span>
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="form-main-block">
						<div class="row">
							<div class="col-md-12">
								<div class="form-block">
									<div class="form-block__name">Окна и двери</div>
									<div class="form-block__desc">Опишите пошагово, какой перечень работ входит в стоимость. Каждый шаг должен содержать не менее 10 символов</div>
									<input type="hidden" name="project_block_id_4" value="<?=$arResult['LIST_WORKS'][16]['ID']?>">
									<input type="hidden" name="project_block_type_4" value="16">
								</div>
							</div>
						</div>
						<div class="row foundation__item">
							<div class="col-md-6">
								<div class="form-block">
									<label <?if ($arResult['LIST_WORKS'][16]['STEPS'][0]) echo 'class="label-up"'?>>
										<textarea class="form-textarea" name="project_block_steps_4[]" cols="30" rows="3" placeholder=""><?=$arResult['LIST_WORKS'][16]['STEPS'][0]?></textarea>
										<span class="placeholder">Шаг <span>1</span>. Например, Удаление поверхностного слоя грунта*</span>
									</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-block">
									<label <?if ($arResult['LIST_WORKS'][16]['STEPS'][1]) echo 'class="label-up"'?>>
										<textarea class="form-textarea" name="project_block_steps_4[]" cols="30" rows="3" placeholder=""><?=$arResult['LIST_WORKS'][16]['STEPS'][1]?></textarea>
										<span class="placeholder">Шаг <span>2</span>. Например, Удаление поверхностного слоя грунта*</span>
									</label>
								</div>
							</div>
						</div>
						<div class="row foundation__item">
							<div class="col-md-6">
								<div class="form-block">
									<label <?if ($arResult['LIST_WORKS'][16]['STEPS'][2]) echo 'class="label-up"'?>>
										<textarea class="form-textarea" name="project_block_steps_4[]" cols="30" rows="3" placeholder=""><?=$arResult['LIST_WORKS'][16]['STEPS'][2]?></textarea>
										<span class="placeholder">Шаг <span>3</span>. Например, Удаление поверхностного слоя грунта*</span>
									</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-block">
									<label <?if ($arResult['LIST_WORKS'][16]['STEPS'][3]) echo 'class="label-up"'?>>
										<textarea class="form-textarea" name="project_block_steps_4[]" cols="30" rows="3" placeholder=""><?=$arResult['LIST_WORKS'][16]['STEPS'][3]?></textarea>
										<span class="placeholder">Шаг <span>4</span>. Например, Удаление поверхностного слоя грунта*</span>
									</label>
								</div>
							</div>
						</div>
						<div class="row foundation__item">
							<div class="col-md-6">
								<div class="form-block">
									<label <?if ($arResult['LIST_WORKS'][16]['STEPS'][4]) echo 'class="label-up"'?>>
										<textarea class="form-textarea" name="project_block_steps_4[]" cols="30" rows="3" placeholder=""><?=$arResult['LIST_WORKS'][16]['STEPS'][4]?></textarea>
										<span class="placeholder">Шаг <span>5</span>. Например, Удаление поверхностного слоя грунта*</span>
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="form-main-block">
						<div class="row">
							<div class="col-md-12">
								<div class="form-block">
									<div class="form-block__name">Инженерия</div>
									<div class="form-block__desc">Опишите пошагово, какой перечень работ входит в стоимость. Каждый шаг должен содержать не менее 10 символов</div>
									<input type="hidden" name="project_block_id_5" value="<?=$arResult['LIST_WORKS'][17]['ID']?>">
									<input type="hidden" name="project_block_type_5" value="17">
								</div>
							</div>
						</div>
						<div class="row foundation__item">
							<div class="col-md-6">
								<div class="form-block">
									<label <?if ($arResult['LIST_WORKS'][17]['STEPS'][0]) echo 'class="label-up"'?>>
										<textarea class="form-textarea" name="project_block_steps_5[]" cols="30" rows="3" placeholder=""><?=$arResult['LIST_WORKS'][17]['STEPS'][0]?></textarea>
										<span class="placeholder">Шаг <span>1</span>. Например, Удаление поверхностного слоя грунта*</span>
									</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-block">
									<label <?if ($arResult['LIST_WORKS'][17]['STEPS'][1]) echo 'class="label-up"'?>>
										<textarea class="form-textarea" name="project_block_steps_5[]" cols="30" rows="3" placeholder=""><?=$arResult['LIST_WORKS'][17]['STEPS'][1]?></textarea>
										<span class="placeholder">Шаг <span>2</span>. Например, Удаление поверхностного слоя грунта*</span>
									</label>
								</div>
							</div>
						</div>
						<div class="row foundation__item">
							<div class="col-md-6">
								<div class="form-block">
									<label <?if ($arResult['LIST_WORKS'][17]['STEPS'][2]) echo 'class="label-up"'?>>
										<textarea class="form-textarea" name="project_block_steps_5[]" cols="30" rows="3" placeholder=""><?=$arResult['LIST_WORKS'][17]['STEPS'][2]?></textarea>
										<span class="placeholder">Шаг <span>3</span>. Например, Удаление поверхностного слоя грунта*</span>
									</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-block">
									<label <?if ($arResult['LIST_WORKS'][17]['STEPS'][3]) echo 'class="label-up"'?>>
										<textarea class="form-textarea" name="project_block_steps_5[]" cols="30" rows="3" placeholder=""><?=$arResult['LIST_WORKS'][17]['STEPS'][3]?></textarea>
										<span class="placeholder">Шаг <span>4</span>. Например, Удаление поверхностного слоя грунта*</span>
									</label>
								</div>
							</div>
						</div>
						<div class="row foundation__item">
							<div class="col-md-6">
								<div class="form-block">
									<label <?if ($arResult['LIST_WORKS'][17]['STEPS'][4]) echo 'class="label-up"'?>>
										<textarea class="form-textarea" name="project_block_steps_5[]" cols="30" rows="3" placeholder=""><?=$arResult['LIST_WORKS'][17]['STEPS'][4]?></textarea>
										<span class="placeholder">Шаг <span>5</span>. Например, Удаление поверхностного слоя грунта*</span>
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="form-main-block">
						<div class="row">
							<div class="col-md-12">
								<div class="form-block">
									<div class="form-block__name">Фасад и отделка</div>
									<div class="form-block__desc">Опишите пошагово, какой перечень работ входит в стоимость. Каждый шаг должен содержать не менее 10 символов</div>
									<input type="hidden" name="project_block_id_6" value="<?=$arResult['LIST_WORKS'][18]['ID']?>">
									<input type="hidden" name="project_block_type_6" value="18">
								</div>
							</div>
						</div>
						<div class="row foundation__item">
							<div class="col-md-6">
								<div class="form-block">
									<label <?if ($arResult['LIST_WORKS'][18]['STEPS'][0]) echo 'class="label-up"'?>>
										<textarea class="form-textarea" name="project_block_steps_6[]" cols="30" rows="3" placeholder=""><?=$arResult['LIST_WORKS'][18]['STEPS'][0]?></textarea>
										<span class="placeholder">Шаг <span>1</span>. Например, Удаление поверхностного слоя грунта*</span>
									</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-block">
									<label <?if ($arResult['LIST_WORKS'][18]['STEPS'][1]) echo 'class="label-up"'?>>
										<textarea class="form-textarea" name="project_block_steps_6[]" cols="30" rows="3" placeholder=""><?=$arResult['LIST_WORKS'][18]['STEPS'][1]?></textarea>
										<span class="placeholder">Шаг <span>2</span>. Например, Удаление поверхностного слоя грунта*</span>
									</label>
								</div>
							</div>
						</div>
						<div class="row foundation__item">
							<div class="col-md-6">
								<div class="form-block">
									<label <?if ($arResult['LIST_WORKS'][18]['STEPS'][2]) echo 'class="label-up"'?>>
										<textarea class="form-textarea" name="project_block_steps_6[]" cols="30" rows="3" placeholder=""><?=$arResult['LIST_WORKS'][18]['STEPS'][2]?></textarea>
										<span class="placeholder">Шаг <span>3</span>. Например, Удаление поверхностного слоя грунта*</span>
									</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-block">
									<label <?if ($arResult['LIST_WORKS'][18]['STEPS'][3]) echo 'class="label-up"'?>>
										<textarea class="form-textarea" name="project_block_steps_6[]" cols="30" rows="3" placeholder=""><?=$arResult['LIST_WORKS'][18]['STEPS'][3]?></textarea>
										<span class="placeholder">Шаг <span>4</span>. Например, Удаление поверхностного слоя грунта*</span>
									</label>
								</div>
							</div>
						</div>
						<div class="row foundation__item">
							<div class="col-md-6">
								<div class="form-block">
									<label <?if ($arResult['LIST_WORKS'][18]['STEPS'][4]) echo 'class="label-up"'?>>
										<textarea class="form-textarea" name="project_block_steps_6[]" cols="30" rows="3" placeholder=""><?=$arResult['LIST_WORKS'][18]['STEPS'][4]?></textarea>
										<span class="placeholder">Шаг <span>5</span>. Например, Удаление поверхностного слоя грунта*</span>
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-xl-3 d-none d-lg-block">
				<div class="completed__block-name">
					<div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_03.svg" alt=""></div><strong>Материалы и перечень работ</strong>
					<div class="completed__block-desc">
						Опишите материалы, используемые вами в работе, и перечень работ
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="completed__item">
		<div class="row">
			<div class="col-lg-8">
				<div class="completed__block-name d-none d-xs-block d-sm-block d-md-hide">
					<div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_kp_08.svg" alt=""></div><strong>Бонусы и подарки</strong>
					<div class="completed__block-desc">
						Выберите, что еще входит в стоимость проекта
					</div>
				</div>
				<div class="technologies completed__container">
					<div class="form-main-block">
						<div class="row">
							<div class="col-md-12">
								<div class="form-block">
									<div class="form-block__name">Что еще входит в стоимость:</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<?foreach ($arResult['INCLUDED_PRICE'] as $key => $value) {
							$checked = (array_key_exists($key,$arResult['PROJECT']['PROPERTY_INCLUDED_PRICE_VALUE'])) ? 'checked' : '';
							switch ($key) {
								case 8: $class = ''; $img = 'advan_01'; break;
								case 9: $class = 'else--green'; $img = 'advan_02'; break;
								case 10: $class = 'else--purple'; $img = 'advan_03'; break;
								case 11: $class = 'else--green'; $img = 'advan_04'; break;
								default: $class = ''; $img = 'advan_01'; break;
							}?>
							<div class="col-md-4">
								<div class="technologies__item <?=$class?>">
									<input id="technologies__item-<?=$value['XML_ID']?>" class="checkbox" name="project_included_price[]" value="<?=$key?>" type="checkbox" <?=$checked?>/>
									<label for="technologies__item-<?=$value['XML_ID']?>">
										<span class="technologies__ico"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/<?=$img?>.svg" alt=""></span>
										<span class="technologies__name"><?=$value['NAME']?></span>
									</label>
								</div>
							</div>
						<?}?>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-xl-3 d-none d-lg-block">
				<div class="completed__block-name">
					<div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_kp_08.svg" alt=""></div><strong>Бонусы и подарки</strong>
					<div class="completed__block-desc">
						Выберите, что еще входит в стоимость проекта
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-8 text-right btn__send">
			<!-- <a href="#" class="btn">Сохранить</a> -->
			<!-- <a href="#" class="btn off">Сохранить</a> -->
			<input type="hidden" name="COMPANY" value="<?=$arResult['PROJECT']['PROPERTY_COMPANY_VALUE']?>">
			<input type="submit" name="sendFrom" value="Сохранить" class="btn">
		</div>
	</div>
</form>

<div class="hide" style="display: none;">
	<div class="row house_plan-item clone_block_plan">
		<div class="col-md-6">
			<div class="form-block">
				<label>
					<input class="form-input" type="text" name="layout_area[]" placeholder="Площадь этажа, м2*">
					<span class="placeholder">Площадь этажа, м2*</span>
					<a href="#" class="edit-input">
						<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
					</a>
				</label>
			</div>
		</div>
		<div class="col-md-6">
			<div class="inline form-block">
				<div class="example-2">
					<div class="form-group download-photo">
						<label class="btn-tertiary js-labelFile">
							<input type="file" name="layout_file[]" class="input-file">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px">
								<image x="0px" y="0px" width="12px" height="12px" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAQAAAD8fJRsAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfkCgUXGhsfTp1lAAAA50lEQVQY00WQu0oDUQBEz1zXIibFtmJjYSEq+BemMYiF4AOsbGS/YH1gZSCQIIsg2oRooRAVwd4PsLUTq5gUYiNJ5wPGwsdOeYrhzCgDQBhghD2tM+xjqhF5ihyxwK6/2OE+5FgHVLTpQ871qVL0jxssK/EFBdXd10P4Lamz4o7HiKi6oneXA1BSw6t0GdIL+yzy5lF6gQI1r/HEBzVmNU9PMdtcBpYoc0rsK6aZ8ysTbHGGlV3rkRNuKTKg7ymlNA0EAjEdNmi560lSN380I26c6dl3FDTjlNbfrIg24yQkDEhp5zd8A7H2TNMiTyyJAAAAAElFTkSuQmCC" />
							</svg>
							<img src="" alt="">
							<span class="js-fileName">Выберите фото</span>
						</label>
						<span class="delete-file">х</span>
					</div>
				</div>
				<a href="" class="block_remove">
					<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/remove.svg">
				</a>
			</div>
		</div>
	</div>
</div>
