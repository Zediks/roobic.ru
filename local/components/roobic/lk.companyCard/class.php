<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
	Loader::includeModule('iblock');
	Loader::includeModule('highloadblock');
use Bitrix\Main\Application;
use Bitrix\Highloadblock as HL, Bitrix\Main\Entity;
use Bitrix\Main\Page\Asset;

// yandex_map
Asset::getInstance()->addJs("https://api-maps.yandex.ru/2.1/?lang=ru_RU&apikey=40336fa1-cf4d-44d7-a4fa-8c7ac8e9f5a5
");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/yandex_map.js");

class company extends CBitrixComponent
{
	function editCompany()
	{
		global $USER;
		$userID = $USER->GetID();
		$iblockID = 2;
		$contentManager = CSite::InGroup([8]);
		$arResult['CONTENT_MANAGER'] = $contentManager;

		$request = Application::getInstance()->getContext()->getRequest();
		$company_id = $request->getQuery("company_id");

		$by = 'id'; $sort = 'asc'; // поле сортировки
	  $filter = ['ID' => $userID];
	  $arParams = [ // наши поля
			'FIELDS' => ['ID','NAME','EMAIL','PERSONAL_PHONE'], 'SELECT' => ['UF_COMPANIES']
	  ];
	  $rsUsers = CUser::GetList($by, $sort, $filter, $arParams);
	  $arUser = $rsUsers->Fetch(); // dump($arUser);
		$arResult['USER'] = $arUser;
		$companyID = $arUser['UF_COMPANIES'][0];

		if ($contentManager && $company_id)
			$companyID = $company_id;

		if ($companyID)
		{
			$arOrder = ['SORT'=>'ASC'];
			$arFilter = ['IBLOCK_ID'=>$iblockID,'ID'=>$companyID];
			$arSelect = ['ID','NAME'];
			$rsElements = CIBlockElement::GetList($arOrder,$arFilter,false,false,$arSelect);
			if (!$arElement = $rsElements->Fetch())
				unset($companyID);
		}

		$params = [
			"max_len" => "100", // обрезает символьный код до 100 символов
			"change_case" => "L", // буквы преобразуются к нижнему регистру
			"replace_space" => "-", // меняем пробелы на тире
			"replace_other" => "-", // меняем левые символы на тире
			"delete_repeat_replace" => "true", // удаляем повторяющиеся символы
			"use_google" => "false", // отключаем использование google
		];

		$rsEnums = CIBlockPropertyEnum::GetList([], Array("IBLOCK_ID"=>$iblockID, "CODE"=>"PRICE"));
		while($arEnums = $rsEnums->GetNext()) // dump($arEnums);
			$arResult['PRICE'][$arEnums['ID']] = $arEnums['VALUE'];

		$arResult['REGIONS'] = getElHL(2,[],[],['ID','UF_NAME','UF_XML_ID']);
		$arService = getElHL(3,[],[],['ID','UF_MAIN_SECTION','UF_SUBSECTION','UF_NAME','UF_XML_ID']);
		$arServicesMain = getListProperty([],["USER_FIELD_ID" => 72]); // Главный раздел
		$arServicesSub = getListProperty([],["USER_FIELD_ID" => 73]); // Подраздел
		$arResult['MATERIALS'] = getElHL(7,[],[],['ID','UF_NAME']);
		$arResult['TECH'] = getElHL(5,[],[],['*']);
		$arResult['SOCIAL'] = ['VK','OK','Telegram','Twitter'];

		// сформируем услуги
		foreach ($arService as $value) {
			// dump($value);
			$mainSectionID = $value['UF_MAIN_SECTION'];
			$subSectionID = $value['UF_SUBSECTION'];
			$mainSection = $arServicesMain[$mainSectionID];
			$subSection = $arServicesSub[$subSectionID];
			$arResult['SERVICES'][$mainSectionID]['NAME'] = $mainSection;
			$arResult['SERVICES'][$mainSectionID]['ITEMS'][$subSectionID]['NAME'] = $subSection;
			$arResult['SERVICES'][$mainSectionID]['ITEMS'][$subSectionID]['ITEMS'][$value['ID']] = [
				'NAME' => $value['UF_NAME'],
				'XML_ID' => $value['UF_XML_ID']
			];
			$arSectionShow[$value['ID']] = [
				'MAIN_SECTION' => $mainSectionID,
				'SUBSECTION' => $subSectionID,
			];
		}
		// dump($arSectionShow);

		$request = Application::getInstance()->getContext()->getRequest();
		$sendFrom = $request->getPost("sendFrom");

		if($sendFrom){ // если форма отправлена

			$company_name = trim($request->getPost("company_name"));
			$company_code = CUtil::translit($company_name, "ru", $params);
			// $company_code = substr($company_code, 0, strrpos($company_code, '-'));

			// фото сертификатов
			$filesPhotosSertif = $request->getFile("sertif_photos");
			foreach ($filesPhotosSertif as $key => $arPhoto) {
				foreach($arPhoto as $key2 => $val2){
					$arPhotosSertif[$key2][$key] = $val2;
				}
			} // dump($arPhotosSertif);
			$arFields['SERTIFICATES'] = $arPhotosSertif;

			$companyElement = new CIBlockElement;
			$elementFields = ['REGION','SERVICES','YEARS','CNT_STAFF','PRICE','ADDRESS','VIDEO','TECH','SOCIAL','FULL_NAME','INN','DIRECTOR'];

			foreach ($elementFields as $field) { // заполним с формы
				if($field == 'SOCIAL'){ // соц. сети
					$social = $request->getPost('company_'.strtolower($field));
					$social_url = $request->getPost('social_link');
					foreach ($social as $key => $value) {
						$arFields[$field][] = ['VALUE'=>$social_url[$key],'DESCRIPTION'=>$social[$key]];
					}
				}else
					$arFields[$field] = $request->getPost('company_'.strtolower($field));
			} // dump($arFields);

			// удаление файлов
			$delFile = $request->getPost("del_file");
			if ($delFile) $arDelFile = explode(',',$delFile);

			$arLoadProductArray = Array(
		    "IBLOCK_ID"					=> $iblockID,
				"IBLOCK_SECTION_ID"	=> false, // элемент лежит в корне раздела
				"MODIFIED_BY"				=> $userID, // элемент изменен текущим пользователем
				"ACTIVE"         		=> "Y", // активен
		    "NAME"           		=> $company_name,
		    "CODE"           		=> $company_code,
		    "DETAIL_TEXT"   		=> $request->getPost("about"),
				"PREVIEW_PICTURE"   => $request->getFile("DETAIL_PICTURE"),
				"DETAIL_PICTURE"   	=> $request->getFile("DETAIL_PICTURE"),
				"PROPERTY_VALUES"		=> $arFields,
		  );

			if ($companyID) {

				// удаление файлов
				if($delFile){
					foreach ($arDelFile as $key => $value)
						$arDeleteImages[$value] = ['VALUE' => ['del' => 'Y']];

					\CIBlockElement::SetPropertyValueCode($companyID, 'SERTIFICATES', $arDeleteImages);
				}

				if ($companyElement->Update($companyID,$arLoadProductArray))
					echo 'Успешно ID: '.$companyID.'<br>';
				else
					echo 'Ошибка: '.$projectElement->LAST_ERROR.'<br>';
			} else
				if ($companyID = $companyElement->Add($arLoadProductArray))
					echo 'Успешно ID: '.$companyID.'<br>';
				else
					echo 'Ошибка: '.$projectElement->LAST_ERROR.'<br>';

			if ($companyID)
			{
				// добавим услуги
				$service = $request->getPost("service");
				if($service)
				{
					$hlblock_id = 4; // id HL
					$hlblock = HL\HighloadBlockTable::getById($hlblock_id)->fetch();
					$entity = HL\HighloadBlockTable::compileEntity($hlblock);
					$entity_data_class = $entity->getDataClass();

					// получим Услуги и Цены
					$arElHL = array_values(getElHL(4,[],['UF_COMPANY' => $companyID],['ID']));
					foreach ($arElHL as $value)
						$priceListAvial[] = $value['ID'];

					foreach ($service as $key => $val)
					{
						$service_id = $request->getPost("service_id_".$key);
						$material = $request->getPost("material_".$key);
						$material_price = $request->getPost("material_price_".$key);

						foreach ($material as $keyM => $value)
						{
							$serviceId = $service_id[$keyM];
							$priceListNew[] = $serviceId;
							// $serviceCode = $val.'-'.$material[$keyM].'-'.$material_price[$keyM];

							$data = [
						    // "UF_SERVICE" => $val,
								"UF_SERVICE" => $material[$keyM],
						    // "UF_MATERIAL" => $material[$keyM],
						    "UF_PRICE" => $material_price[$keyM],
								// "UF_XML_ID" => $serviceCode,
								"UF_COMPANY" => $companyID
						  ];

							if ($serviceId)
								$entity_data_class::update($serviceId,$data);
							else
								$result = $entity_data_class::add($data);

							$arFields['PRICE_LIST'][] = $serviceCode;
						}
					}

					// удалим услуги
					foreach ($priceListAvial as $value) {
						if(!in_array($value,$priceListNew))
							$entity_data_class::delete($value);
					}
				}

				// добавим сотрудников
				$staff_id = $request->getPost("staff_id");
				$staff_name = $request->getPost("staff_name");
				$staff_position = $request->getPost("staff_position");
				$staff_exp = $request->getPost("staff_exp");

				$staff_photo = $request->getFile("staff_photo");
				foreach ($staff_photo as $key => $arStaffPhoto) // сформируем для добавления
					foreach ($arStaffPhoto as $key2 => $val2)
						$arStaffPhotos[$key2][$key] = $val2;

				$ourFile = $request->getPost("our_file");

				if($ourFile || $delFile)
				{
					$arOurFile = explode(',',$ourFile);
					foreach ($arOurFile as $key => $value) {
						if (!in_array($value,$arDelFile)) $arStaffPhotos[$key] = CFile::MakeFileArray($value);
					}
				}

				if($staff_name)
				{
					$hlblock_id = 6; // id HL
					$hlblock = HL\HighloadBlockTable::getById($hlblock_id)->fetch();
					$entity = HL\HighloadBlockTable::compileEntity($hlblock);
					$entity_data_class = $entity->getDataClass();

					foreach ($staff_name as $key => $val)
					{
						$staffId = $staff_id[$key];
						// $staff_code = CUtil::translit($staff_name[$key], "ru", $params). '_' . time();
					  $data = [
					    "UF_NAME" => $staff_name[$key],
							// "UF_XML_ID" => $staff_code,
					    "UF_POSITION" => $staff_position[$key],
					    "UF_EXP" => $staff_exp[$key],
							"UF_FILE" => $arStaffPhotos[$key],
							"UF_COMPANY" => $companyID
					  ];

						if ($staffId)
							$entity_data_class::update($staffId,$data);
						else
							$result = $entity_data_class::add($data);

						$arFields['STAFF'][] = $staff_code;
					}
				}
			}

			// обновление пользователя
			if (!$contentManager)
			{
				$userCompany = new CUser;
				$arFieldsUser = Array(
					'NAME' => $request->getPost("user_name"),
					'PERSONAL_PHONE' => $request->getPost("user_phone"),
					'EMAIL' => $request->getPost("user_email"),
					'UF_COMPANIES' => [$companyID],
				);

				if($userCompany->Update($userID,$arFieldsUser))
					$arResult['USER'] = $arFieldsUser;
				else
					echo $userCompany->LAST_ERROR;
			}

			$arResult['SEND_FORM'] = $sendFrom;
		}

		if($companyID){ // если есть компания
			// получим саму компанию
			$arOrder = Array('SORT'=>'ASC');
			$arFilter = Array('IBLOCK_ID'=>$iblockID,'ID'=>$companyID);
			$arSelect = Array('ID','NAME','DETAIL_TEXT','DETAIL_PICTURE','PROPERTY_YEARS','PROPERTY_CNT_STAFF','PROPERTY_PRICE','PROPERTY_ADDRESS','PROPERTY_VIDEO','PROPERTY_PRICE_LIST','PROPERTY_TECH','PROPERTY_STAFF','PROPERTY_SOCIAL','PROPERTY_FULL_NAME','PROPERTY_INN','PROPERTY_DIRECTOR','PROPERTY_SERVICES','PROPERTY_REGION','PROPERTY_SERTIFICATES'); // PROPERTY_
			$rsElements = CIBlockElement::GetList($arOrder,$arFilter,false,false,$arSelect);
			if($arElement = $rsElements->GetNext()){ // dump($arElement);

				$arResult['COMPANY'] = [
					'NAME' => $arElement['NAME'],
					'DETAIL_TEXT' => $arElement['DETAIL_TEXT'],
					'DETAIL_PICTURE' => $arElement['DETAIL_PICTURE'],
					'SERVICES' => $arElement['PROPERTY_SERVICES_VALUE'],
					'YEARS' => $arElement['PROPERTY_YEARS_VALUE'],
					'CNT_STAFF' => $arElement['PROPERTY_CNT_STAFF_VALUE'],
					'PRICE' => $arElement['PROPERTY_PRICE_ENUM_ID'],
					'REGION' => $arElement['PROPERTY_REGION_VALUE'],
					'ADDRESS' => $arElement['PROPERTY_ADDRESS_VALUE'],
					'VIDEO' => $arElement['PROPERTY_VIDEO_VALUE'],
					'TECH' => $arElement['PROPERTY_TECH_VALUE'],
					'SOCIAL' => $arElement['PROPERTY_SOCIAL_VALUE'],
					'SOCIAL_DESC' => $arElement['PROPERTY_SOCIAL_DESCRIPTION'],
					'FULL_NAME' => $arElement['PROPERTY_FULL_NAME_VALUE'],
					'INN' => $arElement['PROPERTY_INN_VALUE'],
					'DIRECTOR' => $arElement['PROPERTY_DIRECTOR_VALUE'],
					'SERTIFICATES_ID' => $arElement['PROPERTY_SERTIFICATES_PROPERTY_VALUE_ID']
				];

				// получим сертификаты
				foreach ($arElement['PROPERTY_SERTIFICATES_VALUE'] as $key => $value) {
						$arResult['COMPANY']['SERTIFICATES'][$key] = CFile::ResizeImageGet($value, array('width'=>488, 'height'=>360), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
				}
			} // dump($arResult['COMPANY']);

			// получим Услуги и Цены
			$arResult['PRICE_LIST'] = array_values(getElHL(4,[],['UF_COMPANY' => $companyID],['*']));
			foreach ($arResult['PRICE_LIST'] as $value) {
				$mainSectionID = $arSectionShow[$value['UF_SERVICE']]['MAIN_SECTION'];
				$subSectionID = $arSectionShow[$value['UF_SERVICE']]['SUBSECTION'];
				$value['MAIN_SECTION'] = $mainSectionID;
				$value['SUBSECTION'] = $subSectionID;
				$arResult['PRICE_LIST_SHOW'][$mainSectionID][] = $value;
				// $arResult['PRICE_LIST_SHOW'][$value['UF_SERVICE']][] = $value;
			}
			$arResult['PRICE_LIST_SHOW'] = array_values($arResult['PRICE_LIST_SHOW']);

			// получим персонал
			$arResult['COMPANY_STAFF'] = array_values(getElHL(6,[],['UF_COMPANY' => $companyID],['*']));
			foreach ($arResult['COMPANY_STAFF'] as $key => $value) { // dump($value);
				if($value['UF_FILE']){
					$arResult['COMPANY_STAFF'][$key]['UF_FILE_AR'] = CFile::ResizeImageGet($value['UF_FILE'], array('width'=>488, 'height'=>360), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
					$arResult['COMPANY_STAFF_IMG'][] = $value['UF_FILE'];
				}
			}
		}

    return $arResult;
	}

	public function executeComponent()
	{
		$this->arResult = array_merge($this->arResult,$this->editCompany());
		$this->includeComponentTemplate();
	}
}?>
