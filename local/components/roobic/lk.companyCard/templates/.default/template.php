<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// dump($arResult);?>
<?$this->SetViewTarget('body-class');?>
	class="lk-page"
<?$this->EndViewTarget();?>
<?if(!$arResult['SEND_FORM']):?>
	<form action="" method="post" enctype="multipart/form-data">
	  <div class="completed__item">
	    <div class="row">
	      <div class="col-lg-8 col-md-12">
	        <div class="completed__block-name d-none d-xs-block d-sm-block d-md-hide">
	          <div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_01.svg" alt=""></div><strong>Информация о компании </strong>
	          <div class="completed__block-desc">
	            Заполните основновную информацию о вашей компании
	          </div>
	        </div>
	        <div class="completed__container">
	          <div class="form-main-block hide-xs">
	            <div class="form-block">
	              <div class="form-block__name">Основная информация</div>
	            </div>
	          </div>
	          <div class="row">
	            <div class="col-md-6">
	              <div class="form-block">
	                <label for="company_name">
	                  <input class="form-input" type="text" id="company_name" name="company_name" value="<?=$arResult['COMPANY']['NAME']?>" placeholder="Название компании*" required>
	                  <span class="placeholder">Название компании*</span>
	                  <a href="#" class="edit-input">
	                    <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
	                  </a>
	                </label>
	              </div>
	            </div>
	            <div class="col-md-6">
	              <div class="form-block">
	                <label class="select" for="company_service">
	                  <select class="select__select select__styler" name="company_services[]" id="company_service" title="Выберите категорию деятельности*" multiple data-live-search="true">
	                    <option value="">Выберите категорию деятельности*</option>
											<?foreach ($arResult['SERVICES'] as $mainSectionID => $mainSection) {
													foreach ($mainSection['ITEMS'] as $subSectionID => $subSection)
													{
														if ($subSectionID) echo '<optgroup data-main="'.$mainSectionID.'" label="'.$subSection['NAME'].'">';
															foreach ($subSection['ITEMS'] as $key => $value) {?>
																<option value="<?=$value['XML_ID']?>" data-main="<?=$mainSectionID?>" <?if(in_array($value['XML_ID'],$arResult['COMPANY']['SERVICES'])) echo 'selected';?>><?=$value['NAME']?></option>
															<?}
														if ($subSectionID) echo '</optgroup>';
													}
												}?>
	                  </select>
	                </label>
	              </div>
	            </div>
	          </div>
	          <div class="row">
	            <div class="col-md-6">
	              <div class="form-block">
	                <label for="company_years">
	                  <input class="form-input" type="text" id="company_years" name="company_years" value="<?=$arResult['COMPANY']['YEARS']?>" placeholder="Опыт работы*" required>
	                  <span class="placeholder">Опыт работы*</span>
	                  <a href="#" class="edit-input">
	                    <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
	                  </a>
	                </label>
	              </div>
	            </div>
	            <div class="col-md-6">
	              <div class="form-block">
	                <label for="company_cnt_staff">
	                  <input class="form-input" type="text" id="company_cnt_staff" name="company_cnt_staff" value="<?=$arResult['COMPANY']['CNT_STAFF']?>" placeholder="Сколько сотрудников в компании*">
	                  <span class="placeholder">Сколько сотрудников в компании*</span>
	                  <a href="#" class="edit-input">
	                    <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
	                  </a>
	                </label>
	              </div>
	            </div>
	          </div>
	          <div class="row">
	            <div class="col-md-6">
	              <div class="form-block">
	                <label class="select" for="company_price">
	                  <select class="select__select select__styler" name="company_price" id="company_price" title="В каком сегменте цен вы работаете*" required>
	                    <option value="">В каком сегменте цен вы работаете*</option>
	                    <?foreach ($arResult['PRICE'] as $key => $value) {?>
	                      <option value="<?=$key?>" <?if ($key == $arResult['COMPANY']['PRICE']) echo 'selected';?>><?=$value?></option>
	                    <?}?>
	                  </select>
	                </label>
	              </div>
	            </div>
							<div class="col-md-6">
	              <div class="form-block">
	                <label class="select" for="company_region">
	                  <select class="select__select select__styler" name="company_region[]" id="company_region" title="Выберите регион*" multiple>
	                    <option value="">Выберите регион*</option>
											<?foreach ($arResult['REGIONS'] as $value) {?>
												<option value="<?=$value['UF_XML_ID']?>" <?if(in_array($value['UF_XML_ID'],$arResult['COMPANY']['REGION'])) echo 'selected';?>><?=$value['UF_NAME']?></option>
											<?}?>
	                  </select>
	                </label>
	              </div>
	            </div>
	          </div>
						<div class="form-main-block">
							<div class="row">
	              <div class="col-md-12">
	                <div class="form-block">
	                  <div class="form-block__name">Главное фото</div>
										<div class="form-block__desc">
											Загрузите фото компании, которое будет размещено в вашей карточке. Это может быть фото или проект вашего дома, фото офиса или команды. Фотография должна быть в хорошем качестве и весить не более 1 мб, рекомендованное соотношение сторон 4:2
										</div>
	                </div>
	              </div>
	              <div class="col-md-12">
									<div class="download-photo">
										<div class="list">
											<label class="label-file <?if($arResult['COMPANY']['DETAIL_PICTURE'])echo'has-img'?>">
												<input type="file" accept=".png, .jpg, .jpeg" name="DETAIL_PICTURE">
												<span>Добавьте файл для загрузки</span>
												<span>в это поле</span>
												<img src="<?=\CFile::GetPath($arResult['COMPANY']['DETAIL_PICTURE'])?>" alt="">
											</label>
											<span class="delete-file" data-id="<?=$arResult['COMPANY']['DETAIL_PICTURE']?>">х</span>
										</div>
									</div>
	              </div>
							</div>
						</div>
	          <div class="form-main-block">
	            <div class="row">
	              <div class="col-md-12">
	                <div class="form-block">
	                  <div class="form-block__name">Адрес</div>
	                </div>
	              </div>
	              <div class="col-md-12">
	                <div class="form-block">
	                  <label for="input_address">
	                    <input class="form-input" type="text" id="input_address" name="company_address" value="<?=$arResult['COMPANY']['ADDRESS']?>" placeholder="Адрес компании*">
	                    <span class="placeholder">Адрес компании*</span>
	                    <a href="#" class="edit-input">
	                      <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
	                    </a>
	                  </label>
	                </div>
	                <div class="form-map" id="yandex_map"></div>
	              </div>
	            </div>
	          </div>
	          <div class="form-main-block">
	            <div class="row">
	              <div class="col-md-12">
	                <div class="form-block">
	                  <div class="form-block__name">Видео с YouTube <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/youtube_icon.svg" alt="" class="form-block__ico"></div>
	                </div>
	                <div class="form-block">
	                  <label for="company_video">
	                    <input class="form-input" type="text" id="company_video" name="company_video" value="<?=$arResult['COMPANY']['VIDEO']?>" placeholder="Ссылка на видео с YouTube">
	                    <span class="placeholder">Ссылка на видео с YouTube</span>
	                    <a href="#" class="edit-input">
	                      <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
	                    </a>
	                  </label>
	                </div>
	              </div>
	            </div>
	          </div>
	        </div>
	      </div>
	      <div class="col-lg-4 col-xl-3 d-none d-lg-block">
	        <div class="completed__block-name">
	          <div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_01.svg" alt=""></div><strong>Информация о компании </strong>
	          <div class="completed__block-desc">
	            Заполните основновную информацию о вашей компании
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	  <div class="completed__item">
	    <div class="row">
	      <div class="col-lg-8 col-md-12">
	        <div class="completed__block-name d-none d-xs-block d-sm-block d-md-hide">
	          <div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_02.svg" alt=""></div><strong>Услуги и цены</strong>
	          <div class="completed__block-desc">
	            Заполните подробно какие услуги вы оказываете, с какими материалами работаете, и ценами на них.
	          </div>
	        </div>
	        <div class="completed__container">
	          <div class="form-main-block block_service">
							<div class="clone_block_service">
	            <div class="row">
	              <div class="col-md-12 hide-xs">
	                <div class="form-block">
	                  <div class="form-block__name">Услуги и цены</div>
	                </div>
	              </div>
	              <div class="col-md-12">
	                <div class="form-block">
										<?//dump($arResult['SERVICES'])?>
	                  <label class="select select-up" for="service">
	                    <select class="select__select select__styler select_service" name="service[]" id="service" title="Услуги*" >
	                      <option value="">Услуги*</option>
	                      <!-- <?foreach ($arResult['SERVICES'] as $key => $value) {?>
	                        <option value="<?=$key?>" <?if($key == $arResult['PRICE_LIST_SHOW'][0][0]['UF_SERVICE']) echo 'selected';?>><?=$value['UF_NAME']?></option>
	                      <?}?> -->
												<?foreach ($arResult['SERVICES'] as $key => $value) {?>
	                        <option value="<?=$key?>" <?if($key == $arResult['PRICE_LIST_SHOW'][0][0]['MAIN_SECTION']) echo 'selected';?>><?=$value['NAME']?></option>
	                      <?}?>
	                    </select>
	                  </label>
	                </div>
	              </div>
	            </div>
							<div class="block_material">
								<? $i_material = 0;
								do { ?>
			            <div class="row">
			              <div class="col-md-6">
			                <div class="form-block">
			                  <label class="select" for="material">
			                    <select class="select__select select__styler" name="material_0[]" id="material" title="Материал, тип услуг*" data-hide-disabled='true'>
			                      <option value="">Материал, тип услуг*</option>
			                      <!-- <?foreach ($arResult['MATERIALS'] as $key => $value) {?>
			                        <option value="<?=$key?>" <?if($key == $arResult['PRICE_LIST_SHOW'][0][$i_material]['UF_MATERIAL']) echo 'selected';?>><?=$value['UF_NAME']?></option>
			                      <?}?> -->
														<?foreach ($arResult['SERVICES'] as $mainSectionID => $mainSection) {
																foreach ($mainSection['ITEMS'] as $subSectionID => $subSection)
																{
																	if ($subSectionID) echo '<optgroup data-main="'.$mainSectionID.'" label="'.$subSection['NAME'].'">';
																		foreach ($subSection['ITEMS'] as $key => $value) {?>
																			<option value="<?=$key?>" <?if($key == $arResult['PRICE_LIST_SHOW'][0][$i_material]['UF_SERVICE']) echo 'selected';?> data-main="<?=$mainSectionID?>"><?=$value['NAME']?></option>
																		<?}
																	if ($subSectionID) echo '</optgroup>';
																}
															}?>
			                    </select>
			                  </label>
												<input type="hidden" name="service_id_0[]" value="<?=$arResult['PRICE_LIST_SHOW'][0][$i_material]['ID']?>">
			                </div>
			              </div>
			              <div class="col-md-6">
			                <div class="form-block">
			                  <label>
			                    <input class="form-input" type="text" name="material_price_0[]" placeholder="Цена, от*" value="<?=$arResult['PRICE_LIST_SHOW'][0][$i_material]['UF_PRICE']?>">
			                    <span class="placeholder">Цена, от*</span>
			                    <a href="#" class="edit-input">
			                      <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
			                    </a>
			                  </label>
			                </div>
			              </div>
										<?if($i_material != 0):?>
											<a href="" class="block_remove">
								        <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/remove.svg">
								      </a>
										<?endif;?>
			            </div>
								<? $i_material++;
								} while ($i_material < count($arResult['PRICE_LIST_SHOW'][0]));?>
								<div class="row insert_before">
									<div class="col-md-12">
										<div class="form-block">
		                  <a href="#" class="add-item-material" data-block="block_material" data-sid="0"><span class="add-item_ico">+</span> <span class="add-item_link">Добавить материал, тип услуг</span></a>
		                </div>
									</div>
								</div>
							</div></div>
							<?$i_service = 1;
							while ($i_service < count($arResult['PRICE_LIST_SHOW'])) {?>
								<div class="clone_block_service">
								  <div class="row block_service">
								    <div class="col-md-12">
								      <div class="form-block">
												<a href="#" class="service_remove">
						              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30px" height="30px">
						                <image x="0px" y="0px" width="30px" height="30px" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAMAAAAM7l6QAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAaVBMVEXk5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7j5e3Bwsa7vL6np6f///98V12LAAAAHXRSTlMABUyRxuj4EIDq61gDlq2TlFfpBpLExflN7IHH+vrIk+sAAAABYktHRCJdZVysAAAAB3RJTUUH5AkLDiEgJn8OZQAAAMdJREFUKM+Nk1sSgyAMACNKlfoGq/URpfe/ZKtVBKvQ/WCG7AABEoANj/gBvd1o4BMPjoQRQ8U9Ck0bUzSgsSaTFH9IE2UzPCHbfI6nFF9b4gXlkjO/0nzOP8JLIgDBrjUTQLTpqIYVAtU+meRHjXLaIzk89rXyJcdlUKEatKNnZVhk0KDhDYuNSzs2r+2pOS5mfZYniNb6qI4vsXwoC63lsJZjcW6L/0rRUchzGxzy43obzPl32v3b7tBESwtWPR8G3ldEqOAbo1NPex7+7UEAAAAASUVORK5CYII=" />
						              </svg>
						              <span>Удалить услугу</span></a>
						            <div class="clr"></div>
								        <label class="select select-up" for="service_m">
								          <select class="select__select select__styler select_service" name="service[]" id="service_M" title="Услуги*" >
								            <option value="">Услуги*</option>
								            <!-- <?foreach ($arResult['SERVICES'] as $key => $value) {?>
								              <option value="<?=$key?>" <?if($key == $arResult['PRICE_LIST_SHOW'][$i_service][0]['UF_SERVICE']) echo 'selected';?>><?=$value['UF_NAME']?></option>
								            <?}?> -->
														<?foreach ($arResult['SERVICES'] as $key => $value) {?>
			                        <option value="<?=$key?>" <?if($key == $arResult['PRICE_LIST_SHOW'][$i_service][0]['MAIN_SECTION']) echo 'selected';?>><?=$value['NAME']?></option>
			                      <?}?>
								          </select>
								        </label>
								      </div>
								    </div>
								  </div>
								  <div class="block_material">
										<? $i_material = 0;
										do { ?>
											<div class="row">
										    <div class="col-md-6">
										      <div class="form-block">
										        <label class="select" for="material_m">
										          <select class="select__select select__styler" name="material_<?=$i_service?>[]" id="material_m" title="Материал, тип услуг*" data-hide-disabled='true'>
										            <option value="">Материал, тип услуг*</option>
										            <!-- <?foreach ($arResult['MATERIALS'] as $key => $value) {?>
										              <option value="<?=$key?>" <?if($key == $arResult['PRICE_LIST_SHOW'][$i_service][$i_material]['UF_MATERIAL']) echo 'selected';?>><?=$value['UF_NAME']?></option>
										            <?}?> -->
																<?foreach ($arResult['SERVICES'] as $mainSectionID => $mainSection) {
																		foreach ($mainSection['ITEMS'] as $subSectionID => $subSection)
																		{
																			if ($subSectionID) echo '<optgroup data-main="'.$mainSectionID.'" label="'.$subSection['NAME'].'">';
																				foreach ($subSection['ITEMS'] as $key => $value) {?>
																					<option value="<?=$key?>" <?if($key == $arResult['PRICE_LIST_SHOW'][$i_service][$i_material]['UF_SERVICE']) echo 'selected';?> data-main="<?=$mainSectionID?>"><?=$value['NAME']?></option>
																				<?}
																			if ($subSectionID) echo '</optgroup>';
																		}
																	}?>
										          </select>
										        </label>
														<input type="hidden" name="service_id_<?=$i_service?>[]" value="<?=$arResult['PRICE_LIST_SHOW'][$i_service][$i_material]['ID']?>">
										      </div>
										    </div>
										    <div class="col-md-6">
										      <div class="form-block">
										        <label>
										          <input class="form-input" type="text" name="material_price_<?=$i_service?>[]" placeholder="Цена, от*" value="<?=$arResult['PRICE_LIST_SHOW'][$i_service][$i_material]['UF_PRICE']?>">
										          <span class="placeholder">Цена, от*</span>
										          <a href="#" class="edit-input">
										            <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
										          </a>
										        </label>
										      </div>
										    </div>
												<?if($i_material != 0):?>
													<a href="" class="block_remove">
										        <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/remove.svg">
										      </a>
												<?endif;?>
											</div>
										<? $i_material++;
										} while ($i_material < count($arResult['PRICE_LIST_SHOW'][$i_service]));?>
										<div class="row insert_before">
							        <div class="col-md-12">
							          <div class="form-block">
							            <a href="#" class="add-item-material" data-block="block_material" data-sid="<?=$i_service?>"><span class="add-item_ico">+</span> <span class="add-item_link">Добавить материал, тип услуг</span></a>
							          </div>
							        </div>
							      </div>
								  </div>
								</div>
							<? $i_service++; } ?>
							<div class="form-bottom">
								<a href="#" class="add-service btn" data-block="block_service">Добавить услугу</a>
							</div>
	          </div>
	        </div>
	      </div>
	      <div class="col-lg-4 col-xl-3 d-none d-lg-block">
	        <div class="completed__block-name">
	          <div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_02.svg" alt=""></div><strong>Услуги и цены</strong>
	          <div class="completed__block-desc">
	            Заполните подробно какие услуги вы оказываете, с какими материалами работаете, и ценами на них.
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	  <div class="completed__item">
	    <div class="row">
	      <div class="col-lg-8">
	        <div class="completed__block-name d-none d-xs-block d-sm-block d-md-hide">
	          <div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_03.svg" alt=""></div><strong>Технологии и материалы</strong>
	          <div class="completed__block-desc">
	            Выберите технологии и материалы, используемые вами в работе
	          </div>
	        </div>
	        <div class="technologies completed__container">
	          <div class="form-main-block hide-xs">
	            <div class="row">
	              <div class="col-md-12">
	                <div class="form-block">
	                  <div class="form-block__name">Технологии и материалы</div>
	                </div>
	              </div>
	            </div>
	          </div>
	          <div class="row">
	            <?foreach ($arResult['TECH'] as $key => $tech) { // dump($tech);
	              switch ($key) {
	  							case 2: $classColor = 'else--green'; break;
	  							case 3: $classColor = 'else--purple'; break;
	  							case 4: $classColor = 'else--green'; break;
	  							case 6: $classColor = 'else--purple'; break;
	  							default: $classColor = ''; break;
	  						}?>
	              <div class="col-md-4">
	                <div class="technologies__item <?=$classColor?>">
	                  <input id="<?=$tech['UF_XML_ID']?>" class="checkbox" type="checkbox" value="<?=$tech['UF_XML_ID']?>" name="company_tech[]" <?if(in_array($tech['UF_XML_ID'],$arResult['COMPANY']['TECH']))echo'checked';?>/>
	                  <label for="<?=$tech['UF_XML_ID']?>">
	                    <span class="technologies__ico"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_01.svg" alt=""></span>
	                    <span class="technologies__name"><?=$tech['UF_NAME']?></span>
	                  </label>
	                </div>
	              </div>
	            <?}?>
	          </div>
	          <div class="row">
	            <div class="col-md-12 text-right">
	              <a href="#" class="align-self-end order__new-btn btn">Показать еще</a>
	            </div>
	          </div>
	        </div>
	      </div>
	      <div class="col-lg-4 col-xl-3 d-none d-lg-block">
	        <div class="completed__block-name">
	          <div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_03.svg" alt=""></div><strong>Технологии и материалы</strong>
	          <div class="completed__block-desc">
	            Выберите технологии и материалы, используемые вами в работе
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	  <div class="completed__item">
	    <div class="row">
	      <div class="col-lg-8 col-md-12">
	        <div class="completed__block-name d-none d-xs-block d-sm-block d-md-hide">
	          <div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_04.svg" alt=""></div><strong>Сотрудники компании</strong>
	          <div class="completed__block-desc">
	            Заполните информацию о ваших сотрудниках
	          </div>
	        </div>
	        <div class="completed__container block_staff">
						<? $i_staff = 0;
							 $cntStaff = count($arResult['COMPANY_STAFF']);
						do { ?>
							<div class="staff__container <?if ($cntStaff == 0 || $i_staff == $cntStaff-1) echo 'insert_after'?>">
								<div class="form-main-block">
									<?if ($i_staff == 0) {?>
										<div class="row hide-xs">
				              <div class="col-md-12">
				                <div class="form-block">
				                  <div class="form-block__name">Сотрудники</div>
				                </div>
				              </div>
				            </div>
									<?}?>
			            <div class="row">
			              <div class="col-md-6">
			                <div class="form-block">
			                  <label for="staff_name">
			                    <input class="form-input" type="text" id="staff_name" name="staff_name[]" placeholder="ФИО" value="<?=$arResult['COMPANY_STAFF'][$i_staff]['UF_NAME']?>">
			                    <span class="placeholder">ФИО</span>
			                    <a href="#" class="edit-input">
			                      <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
			                    </a>
			                  </label>
			                  <input type="hidden" name="staff_id[]" value="<?=$arResult['COMPANY_STAFF'][$i_staff]['ID']?>">
			                </div>
			              </div>
			              <div class="col-md-6">
			                <div class="form-block">
			                  <label for="staff_position">
			                    <input class="form-input" type="text" id="staff_position" name="staff_position[]" placeholder="Должность" value="<?=$arResult['COMPANY_STAFF'][$i_staff]['UF_POSITION']?>">
			                    <span class="placeholder">Должность</span>
			                    <a href="#" class="edit-input">
			                      <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
			                    </a>
			                  </label>
			                </div>
			              </div>
			            </div>
			            <div class="row">
			              <div class="col-md-6">
			                <div class="form-block">
			                  <label for="staff_exp">
			                    <input class="form-input" type="text" id="staff_exp" name="staff_exp[]" placeholder="Опыт работы*" value="<?=$arResult['COMPANY_STAFF'][$i_staff]['UF_EXP']?>">
			                    <span class="placeholder">Опыт работы*</span>
			                    <a href="#" class="edit-input">
			                      <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
			                    </a>
			                  </label>
			                </div>
			              </div>
			            </div>
			          </div>
		          	<div class="form-main-block">
			            <div class="row">
			              <div class="col-md-12">
			                <div class="form-block">
			                  <div class="form-block__name">Фото</div>
			                  <div class="form-block__desc">Загрузите фото сотрудника</div>
												<div class="download-photo">
													<div class="list">
														<label class="label-file <?if($arResult['COMPANY_STAFF'][$i_staff]['UF_FILE'])echo'has-img'?>">
															<input type="file" accept=".png, .jpg, .jpeg" name="staff_photo[]">
															<span>Добавьте файл для загрузки</span>
															<span>в это поле</span>
															<img src="<?=$arResult['COMPANY_STAFF'][$i_staff]['UF_FILE_AR']['src']?>" alt="">
														</label>
														<span class="delete-file" data-id="<?=$arResult['COMPANY_STAFF'][$i_staff]['UF_FILE']?>">х</span>
													</div>
												</div>
			                </div>
			              </div>
			            </div>
									<?if($i_staff != 0){?>
										<a href="#" class="staff_remove">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30px" height="30px">
												<image x="0px" y="0px" width="30px" height="30px" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAMAAAAM7l6QAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAaVBMVEXk5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7j5e3Bwsa7vL6np6f///98V12LAAAAHXRSTlMABUyRxuj4EIDq61gDlq2TlFfpBpLExflN7IHH+vrIk+sAAAABYktHRCJdZVysAAAAB3RJTUUH5AkLDiEgJn8OZQAAAMdJREFUKM+Nk1sSgyAMACNKlfoGq/URpfe/ZKtVBKvQ/WCG7AABEoANj/gBvd1o4BMPjoQRQ8U9Ck0bUzSgsSaTFH9IE2UzPCHbfI6nFF9b4gXlkjO/0nzOP8JLIgDBrjUTQLTpqIYVAtU+meRHjXLaIzk89rXyJcdlUKEatKNnZVhk0KDhDYuNSzs2r+2pOS5mfZYniNb6qI4vsXwoC63lsJZjcW6L/0rRUchzGxzy43obzPl32v3b7tBESwtWPR8G3ldEqOAbo1NPex7+7UEAAAAASUVORK5CYII=" />
											</svg>
											<span> Удалить сотрудника</span></a>
										<div class="clr"></div>
									<?}?>
									<?if ($cntStaff == 0 || $i_staff == $cntStaff-1):?>
				            <div class="row">
				              <div class="col-md-12">
				                <div class="form-block">
				                  <a href="#" class="add-item" data-block="block_staff"><span class="add-item_ico">+</span> <span class="add-item_link">Добавить сотрудника</span></a>
				                </div>
				              </div>
				            </div>
									<?endif;?>
			          </div>
							</div>
						<? $i_staff++;
						} while ($i_staff < $cntStaff);?>
	        </div>
	      </div>
	      <div class="col-lg-4 col-xl-3 d-none d-lg-block">
	        <div class="completed__block-name">
	          <div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_04.svg" alt=""></div><strong>Сотрудники компании</strong>
	          <div class="completed__block-desc">
	            Заполните информацию о ваших сотрудниках
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	  <div class="completed__item">
	    <div class="row">
	      <div class="col-lg-8 col-md-12">
	        <div class="completed__block-name d-none d-xs-block d-sm-block d-md-hide">
	          <div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_05.svg" alt=""></div><strong>Вы в социальных сетях</strong>
	          <div class="completed__block-desc">
	            Поделитесь своим профилем в социальных сетях
	          </div>
	        </div>
	        <div class="completed__container">
	          <div class="form-main-block">
	            <div class="block_social">
	              <div class="hide-xs">
	                <div class="form-block">
	                  <div class="form-block__name">Компания в социальных сетях</div>
	                </div>
	              </div>
								<? $i_social = 0;
									 $cntCocial = count($arResult['COMPANY']['SOCIAL']);
								do {?>
									<div class="row <?if ($cntCocial == 0 || $i_social == $cntCocial-1) echo 'insert_after'?>">
			              <div class="col-md-6">
			                <div class="form-block">
			                  <label class="select" for="company_social">
			                    <select class="select__select select__styler" name="company_social[]" id="company_social" title="Выберите соц.сеть">
			                      <option value="">Выберите соц.сеть</option>
			                      <?foreach ($arResult['SOCIAL'] as $social) {?>
			                        <option value="<?=$social?>" <?if($social == $arResult['COMPANY']['SOCIAL_DESC'][$i_social])echo'selected';?>><?=$social?></option>
			                      <?}?>
			                    </select>
			                  </label>
			                </div>
			              </div>
			              <div class="col-md-6">
			                <div class="form-block">
			                  <label for="social_link">
			                    <input class="form-input" type="text" id="social_link" name="social_link[]" placeholder="Ссылка на профиль компании" value="<?=$arResult['COMPANY']['SOCIAL'][$i_social]?>">
			                    <span class="placeholder">Ссылка на профиль компании</span>
			                    <a href="#" class="edit-input">
			                      <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
			                    </a>
			                  </label>
			                </div>
			              </div>
										<?if($i_social != 0):?>
											<a href="" class="block_remove">
								        <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/remove.svg">
								      </a>
										<?endif;?>
									</div>
								<? $i_social++;
								} while ($i_social < $cntCocial);?>
	            </div>
	            <div class="row">
	              <div class="col-md-12">
	                <div class="form-block">
	                  <a href="#" class="add-item" data-block="block_social"><span class="add-item_ico">+</span> <span class="add-item_link">Добавить соц.сеть</span></a>
	                </div>
	              </div>
	            </div>
	          </div>
	        </div>
	      </div>
	      <div class="col-lg-4 col-xl-3 d-none d-lg-block">
	        <div class="completed__block-name">
	          <div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_05.svg" alt=""></div><strong>Вы в социальх сетях</strong>
	          <div class="completed__block-desc">
	            Поделитесь своим профилем в социальных сетях
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	  <div class="completed__item">
	    <div class="row">
	      <div class="col-lg-8 col-md-12">
	        <div class="completed__block-name d-none d-xs-block d-sm-block d-md-hide">
	          <div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_06.svg" alt=""></div><strong>О компании</strong>
	          <div class="completed__block-desc">
	            Заполните наименование и ИНН. Опишите преимущества работы вашей компании
	          </div>
	        </div>
	        <div class="completed__container">
	          <div class="form-main-block">
	            <div class="row">
	              <div class="col-md-12">
	                <div class="form-block">
	                  <div class="form-block__name">Информация о компании</div>
	                </div>
	              </div>
	              <div class="col-md-6">
	                <div class="form-block">
	                  <label for="company_full_name">
	                    <input class="form-input" type="text" id="company_full_name" name="company_full_name" placeholder="Полное наименование*" value="<?=$arResult['COMPANY']['FULL_NAME']?>">
	                    <span class="placeholder">Полное наименование*</span>
	                    <a href="#" class="edit-input">
	                      <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
	                    </a>
	                  </label>
	                </div>
	              </div>
								<div class="col-md-6">
	                <div class="form-block">
	                  <label for="company_inn">
	                    <input class="form-input" type="text" id="company_inn" name="company_inn" placeholder="ИНН*" value="<?=$arResult['COMPANY']['INN']?>">
	                    <span class="placeholder">ИНН*</span>
	                    <a href="#" class="edit-input">
	                      <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
	                    </a>
	                  </label>
	                </div>
	              </div>
	              <div class="col-md-6">
	                <div class="form-block">
	                  <label for="company_director">
	                    <input class="form-input" type="text" id="company_director" name="company_director" placeholder="Директор*" value="<?=$arResult['COMPANY']['DIRECTOR']?>">
	                    <span class="placeholder">Директор*</span>
	                    <a href="#" class="edit-input">
	                      <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
	                    </a>
	                  </label>
	                </div>
	              </div>
	            </div>
	            <div class="row">
	              <div class="col-md-12">
	                <div class="form-block">
	                  <label for="about">
	                    <textarea class="form-textarea" name="about" id="about" cols="30" rows="10" placeholder="Описание деятельности"><?=$arResult['COMPANY']['DETAIL_TEXT']?></textarea>
	                  </label>
	                  <p>Описание должно содержать не менее 200, и не более 1000 символов</p>
	                </div>
	              </div>
	            </div>
	          </div>
	        </div>
	      </div>
	      <div class="col-lg-4 col-xl-3 d-none d-lg-block">
	        <div class="completed__block-name">
	          <div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_06.svg" alt=""></div><strong>О компании</strong>
	          <div class="completed__block-desc">
	            Заполните наименование и ИНН. Опишите преимущества работы вашей компании
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	  <div class="completed__item">
	    <div class="row">
	      <div class="col-lg-8 col-md-12">
	        <div class="completed__block-name d-none d-xs-block d-sm-block d-md-hide">
	          <div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_07.svg" alt=""></div><strong>Загрузите сертификаты</strong>
	          <div class="completed__block-desc">
	            Подтвердите свой профессионализм, покажите сертификаты, благодарности заказчику
	          </div>
	        </div>
	        <div class="completed__container">
	          <div class="form-main-block">
	            <div class="row">
	              <div class="col-md-12">
	                <div class="form-block">
	                  <div class="form-block__name hide-xs">Сертификаты компании</div>
										<div class="download-photo">
											<?for ($i=0; $i < 3; $i++) {?>
												<div class="list">
													<label class="label-file <?if($arResult['COMPANY']['SERTIFICATES'][$i])echo'has-img'?>">
														<input type="file" accept=".png, .jpg, .jpeg" name="sertif_photos[]">
														<span>Добавьте файл для загрузки</span>
														<span>в это поле</span>
														<img src="<?=$arResult['COMPANY']['SERTIFICATES'][$i]['src']?>" alt="">
													</label>
													<span class="delete-file" data-id="<?=$arResult['COMPANY']['SERTIFICATES_ID'][$i]?>">х</span>
												</div>
											<?}?>
											<input type="hidden" name="del_file" value="">
											<input type="hidden" name="our_file" value="<?=implode(',',$arResult['COMPANY_STAFF_IMG'])?>">
										</div>
	                </div>
	              </div>
	            </div>
	          </div>
	        </div>
	      </div>
	      <div class="col-lg-4 col-xl-3 d-none d-lg-block">
	        <div class="completed__block-name">
	          <div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_07.svg" alt=""></div><strong>Загрузите сертификаты</strong>
	          <div class="completed__block-desc">
	            Подтвердите свой профессионализм, покажите сертификаты, благодарности заказчику
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	  <!-- <div class="completed__item">
	    <div class="row">
	      <div class="col-lg-8 col-md-12">
	        <div class="completed__container">
	          <div class="form-main-block">
	            <div class="row">
	              <div class="col-md-12">
	                <div class="form-block">
	                  <div class="form-block__name">Отзывы о компании</div>
	                  <div class="form-block__desc">
	                    Отзывы повышают доверие со стороны клиентов, поэтому если у вас еще нет отзывов, скопируйте эту ссылку, и отправьте вашим клиентам, чтобы они оставили о вас первый рекомендательный отзыв.
	                  </div>
	                </div>
	                <div class="form-block form-inline">
	                  <label for="reviews">
	                    <input class="form-input" type="text" id="reviews" name="reviews" placeholder="https://www.youtube.com/watch?">
	                    <a href="#" class="btn btn--red">Копировать</a>
	                  </label>
	                </div>
	              </div>
	            </div>
	          </div>
	        </div>
	      </div>
	      <div class="col-lg-4 col-xl-3 d-none d-lg-block">

	      </div>
	    </div>
	  </div> -->
		<?if(!$arResult['CONTENT_MANAGER']):?>
	  <div class="completed__item">
	    <div class="row">
	      <div class="col-lg-8 col-md-12">
	        <div class="completed__block-name d-none d-xs-block d-sm-block d-md-hide">
	          <div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_08.svg" alt=""></div><strong>Обратная связь</strong>
	          <div class="completed__block-desc">
	            Ваши контакты необходимы, исключительно, для обратной связи, эти данные не публикуются на сайте
	          </div>
	        </div>
	        <div class="completed__container">
	          <div class="form-main-block">
	            <div class="row">
	              <div class="col-md-12 hide-xs">
	                <div class="form-block">
	                  <div class="form-block__name">Контакты</div>
	                </div>
	              </div>
	              <div class="col-md-6">
	                <div class="form-block">
	                  <label for="user_name">
	                    <input class="form-input" type="text" id="user_name" name="user_name" placeholder="Как к вам обращаться*" value="<?=$arResult['USER']['NAME']?>">
	                    <span class="placeholder">Как к вам обращаться*</span>
	                    <a href="#" class="edit-input">
	                      <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
	                    </a>
	                  </label>
	                </div>
	              </div>
	              <div class="col-md-6">
	                <div class="form-block">
	                  <label for="user_phone">
	                    <input class="form-input" type="tel" id="user_phone" name="user_phone" placeholder="Номер телефона*" value="<?=$arResult['USER']['PERSONAL_PHONE']?>">
	                    <span class="placeholder">Номер телефона*</span>
	                    <a href="#" class="edit-input">
	                      <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
	                    </a>
	                  </label>
	                </div>
	              </div>
	            </div>
	            <div class="row">
	              <div class="col-md-6">
	                <div class="form-block">
	                  <label for="user_email">
	                    <input class="form-input" type="email" id="user_email" name="user_email" placeholder="Адрес электронной почты*" value="<?=$arResult['USER']['EMAIL']?>">
	                    <span class="placeholder">Адрес электронной почты*</span>
	                    <a href="#" class="edit-input">
	                      <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
	                    </a>
	                  </label>
	                </div>
	              </div>
	            </div>
	          </div>
	        </div>
	      </div>
	      <div class="col-lg-4 col-xl-3 d-none d-lg-block">
	        <div class="completed__block-name">
	          <div class="completed__block__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/svg/form_08.svg" alt=""></div><strong>Обратная связь</strong>
	          <div class="completed__block-desc">
	            Ваши контакты необходимы, исключительно, для обратной связи, эти данные не публикуются на сайте
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
		<?endif;?>
	  <div class="row">
	    <div class="col-md-8 text-right btn__send">
	      <input type="submit" name="sendFrom" value="Сохранить" class="btn">
	      <!-- <a href="#" class="btn">Сохранить</a> -->
	      <!-- <a href="#" class="btn off">Сохранить</a> -->
	    </div>
	  </div>
	</form>

	<div class="hide">

	  <!-- услуги -->
		<div class="clone_block_service">
		  <div class="row block_service">
		    <div class="col-md-12">
		      <div class="form-block-service">
						<a href="#" class="service_remove">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30px" height="30px">
                <image x="0px" y="0px" width="30px" height="30px" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAMAAAAM7l6QAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAaVBMVEXk5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7j5e3Bwsa7vL6np6f///98V12LAAAAHXRSTlMABUyRxuj4EIDq61gDlq2TlFfpBpLExflN7IHH+vrIk+sAAAABYktHRCJdZVysAAAAB3RJTUUH5AkLDiEgJn8OZQAAAMdJREFUKM+Nk1sSgyAMACNKlfoGq/URpfe/ZKtVBKvQ/WCG7AABEoANj/gBvd1o4BMPjoQRQ8U9Ck0bUzSgsSaTFH9IE2UzPCHbfI6nFF9b4gXlkjO/0nzOP8JLIgDBrjUTQLTpqIYVAtU+meRHjXLaIzk89rXyJcdlUKEatKNnZVhk0KDhDYuNSzs2r+2pOS5mfZYniNb6qI4vsXwoC63lsJZjcW6L/0rRUchzGxzy43obzPl32v3b7tBESwtWPR8G3ldEqOAbo1NPex7+7UEAAAAASUVORK5CYII=" />
              </svg>
              <span>Удалить услугу</span></a>
            <div class="clr"></div>
		        <label class="select select-up" for="service_m">
		          <select class="select__select select__styler select_service" name="service[]" id="service_M" title="Услуги*" >
		            <option value="">Услуги*</option>
		            <?foreach ($arResult['SERVICES'] as $key => $value) {?>
		              <option value="<?=$key?>"><?=$value['NAME']?></option>
		            <?}?>
		          </select>
		        </label>
		      </div>
		    </div>
		  </div>
		  <div class="block_material">
				<div class="row">
			    <div class="col-md-6">
			      <div class="form-block-service">
			        <label class="select" for="material_m">
			          <select class="select__select select__styler select_material" name="material[]" id="material_m" title="Материал, тип услуг*" data-hide-disabled='true'>
			            <option value="">Материал, тип услуг*</option>
			            <!-- <?foreach ($arResult['MATERIALS'] as $key => $value) {?>
			              <option value="<?=$key?>"><?=$value['UF_NAME']?></option>
			            <?}?> -->
									<?foreach ($arResult['SERVICES'] as $mainSectionID => $mainSection) {
											foreach ($mainSection['ITEMS'] as $subSectionID => $subSection)
											{
												if ($subSectionID) echo '<optgroup data-main="'.$mainSectionID.'" label="'.$subSection['NAME'].'">';
													foreach ($subSection['ITEMS'] as $key => $value) {?>
														<option value="<?=$key?>" data-main="<?=$mainSectionID?>"><?=$value['NAME']?></option>
													<?}
												if ($subSectionID) echo '</optgroup>';
											}
										}?>
			          </select>
			        </label>
			      </div>
			    </div>
			    <div class="col-md-6">
			      <div class="form-block">
			        <label>
			          <input class="form-input input_price" type="text" name="material_price[]" placeholder="Цена, от*">
			          <span class="placeholder">Цена, от*</span>
			          <a href="#" class="edit-input">
			            <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
			          </a>
			        </label>
			      </div>
			    </div>
				</div>
				<div class="row insert_before">
	        <div class="col-md-12">
	          <div class="form-block">
	            <a href="#" class="add-item-material a_material" data-block="block_material"><span class="add-item_ico">+</span> <span class="add-item_link">Добавить материал, тип услуг</span></a>
	          </div>
	        </div>
	      </div>
		  </div>
		</div>

	  <!-- материалы -->
		<div class="row clone_block_material">
		  <div class="col-md-6">
		    <div class="form-block">
		      <label class="select" for="material_m">
		        <select class="select__select select__styler not_selectpicker select_material" name="material[]" id="material_m" title="Материал, тип услуг*" data-hide-disabled="true">
		          <option value="">Материал, тип услуг*</option>
		          <!-- <?foreach ($arResult['MATERIALS'] as $key => $value) {?>
		            <option value="<?=$key?>"><?=$value['UF_NAME']?></option>
		          <?}?> -->
							<?foreach ($arResult['SERVICES'] as $mainSectionID => $mainSection) {
									foreach ($mainSection['ITEMS'] as $subSectionID => $subSection)
									{
										if ($subSectionID) echo '<optgroup data-main="'.$mainSectionID.'" label="'.$subSection['NAME'].'">';
											foreach ($subSection['ITEMS'] as $key => $value) {?>
												<option value="<?=$key?>" data-main="<?=$mainSectionID?>"><?=$value['NAME']?></option>
											<?}
										if ($subSectionID) echo '</optgroup>';
									}
								}?>
		        </select>
		      </label>
		    </div>
		  </div>
		  <div class="col-md-6">
		    <div class="form-block">
		      <label>
		        <input class="form-input input_price" type="text" name="material_price[]" placeholder="Цена, от*">
		        <span class="placeholder">Цена, от*</span>
		        <a href="#" class="edit-input">
		          <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
		        </a>
		      </label>
		    </div>
		  </div>
			<a href="" class="block_remove">
        <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/remove.svg">
      </a>
		</div>

	  <!-- сотрудники -->
		<div class="staff__container clone_block_staff">
		  <div class="form-main-block">
		    <div class="row">
		      <div class="col-md-6">
		        <div class="form-block">
		          <label for="staff_name_m">
		            <input class="form-input" type="text" id="staff_name_m" name="staff_name[]" placeholder="ФИО">
		            <span class="placeholder">ФИО</span>
		            <a href="#" class="edit-input">
		              <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
		            </a>
		          </label>
		        </div>
		      </div>
		      <div class="col-md-6">
		        <div class="form-block">
		          <label for="staff_position_m">
		            <input class="form-input" type="text" id="staff_position_m" name="staff_position[]" placeholder="Должность">
		            <span class="placeholder">Должность</span>
		            <a href="#" class="edit-input">
		              <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
		            </a>
		          </label>
		        </div>
		      </div>
		    </div>
		    <div class="row">
		      <div class="col-md-6">
		        <div class="form-block">
		          <label for="staff_exp_m">
		            <input class="form-input" type="text" id="staff_exp_m" name="staff_exp[]" placeholder="Опыт работы*">
		            <span class="placeholder">Опыт работы*</span>
		            <a href="#" class="edit-input">
		              <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
		            </a>
		          </label>
		        </div>
		      </div>
		    </div>
		  </div>
		  <div class="form-main-block">
		    <div class="row">
		      <div class="col-md-12">
		        <div class="form-block">
		          <div class="form-block__name">Фото</div>
		          <div class="form-block__desc">Загрузите фото сотрудника</div>
							<div class="download-photo">
								<div class="list">
									<label class="label-file">
										<input type="file" accept=".png, .jpg, .jpeg" name="staff_photo[]">
										<span>Добавьте файл для загрузки</span>
										<span>в это поле</span>
										<img src="" alt="">
									</label>
									<span class="delete-file" data-id="">х</span>
								</div>
							</div>
		        </div>
		      </div>
		    </div>
				<a href="#" class="staff_remove">
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30px" height="30px">
            <image x="0px" y="0px" width="30px" height="30px" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAMAAAAM7l6QAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAaVBMVEXk5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7k5u7j5e3Bwsa7vL6np6f///98V12LAAAAHXRSTlMABUyRxuj4EIDq61gDlq2TlFfpBpLExflN7IHH+vrIk+sAAAABYktHRCJdZVysAAAAB3RJTUUH5AkLDiEgJn8OZQAAAMdJREFUKM+Nk1sSgyAMACNKlfoGq/URpfe/ZKtVBKvQ/WCG7AABEoANj/gBvd1o4BMPjoQRQ8U9Ck0bUzSgsSaTFH9IE2UzPCHbfI6nFF9b4gXlkjO/0nzOP8JLIgDBrjUTQLTpqIYVAtU+meRHjXLaIzk89rXyJcdlUKEatKNnZVhk0KDhDYuNSzs2r+2pOS5mfZYniNb6qI4vsXwoC63lsJZjcW6L/0rRUchzGxzy43obzPl32v3b7tBESwtWPR8G3ldEqOAbo1NPex7+7UEAAAAASUVORK5CYII=" />
          </svg>
          <span> Удалить сотрудника</span></a>
				<div class="clr"></div>
		  </div>
		</div>

		<!-- соц. сети -->
		<div class="row clone_block_social">
		  <div class="col-md-6">
		    <div class="form-block">
		      <label class="select" for="company_social_m">
		        <select class="select__select select__styler not_selectpicker" name="company_social[]" id="company_social_m" title="Выберите соц.сеть">
		          <option value="">Выберите соц.сеть</option>
		          <?foreach ($arResult['SOCIAL'] as $social) {?>
		            <option value="<?=$social?>"><?=$social?></option>
		          <?}?>
		        </select>
		      </label>
		    </div>
		  </div>
		  <div class="col-md-6">
		    <div class="form-block">
		      <label for="social_link_m">
		        <input class="form-input" type="text" id="social_link_m" name="social_link[]" placeholder="Ссылка на профиль компании">
		        <span class="placeholder">Ссылка на профиль компании</span>
		        <a href="#" class="edit-input">
		          <img src="<?=SITE_TEMPLATE_PATH?>/images/svg/pencil.svg" class="icon">
		        </a>
		      </label>
		    </div>
		  </div>
			<a href="" class="block_remove">
				<img src="<?=SITE_TEMPLATE_PATH?>/images/svg/remove.svg">
			</a>
		</div>
	</div>
<?else:?>
	<div class="leave-esponse">
		<h6>Отлично, осталось совсем немного!</h6>
		<p>Осталось заполнить примеры выполненных объектов</p>
		<a href="/lk/company_work" class="btn btn--red">Перейти к заполнению</a>
	</div>
<?endif;?>
