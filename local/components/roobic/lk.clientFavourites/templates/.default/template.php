<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// dump($arResult);
use Bitrix\Main\Grid\Declension;
// выводим правильное окончание
$reviewsDeclension = new Declension('отзыв', 'отзыва', 'отзывов');
?>
<?$this->SetViewTarget('body-class');?>
	class="clients_my-favourites"
<?$this->EndViewTarget();?>
<div class="leads">
		<div class="leads__top">
			<div class="leads__top-title">Избранное</div>
		</div>
		<div class="leads__container">
			<div class="leads_tabs">
				<a href="#" class="btn active" data-id="all">Все</a>
				<a href="#" class="btn" data-id="favorites_bild">Компании</a>
				<a href="#" class="btn" data-id="favorites_house">Проекты</a>
			</div>
			<div class="row object-list object-list--catalog">
				<?foreach ($arResult['FAVORITES_BILD'] as $key => $arItem) {
					$comp_active = ($arItem['COMPARISON'] == 'Y') ? 'control--favorites--active' : '';
					$fav_active = ($arItem['FAVORITES'] == 'Y') ? 'control--favorites--active' : '';
					$comp_text = ($arItem['COMPARISON'] == 'Y') ? 'Удалить из сравнения' : 'Добавить к сравнению';
					$fav_text = ($arItem['FAVORITES'] == 'Y') ? 'Удалить из избранного' : 'Добавить в избранное';

					$rating = round($arItem['PROPERTY_RATING_VALUE']); // рейтинг
					$services = implode(', ',$arItem['SERVICES']); // услуги
					// отзывы
					$cntReviews = ($arItem['PROPERTY_REVIEWS_VALUE']) ? count($arItem['PROPERTY_REVIEWS_VALUE']) : 0;
					$reviewsText = $reviewsDeclension->get($cntReviews);
					$reviewsText = ($cntReviews > 0) ? $cntReviews.' '.$reviewsText : 'Нет отзывов';?>
					<div class="col-md-6 tab-block favorites_bild">
						<div class="object">
							<div class="row">
								<div class="col-md-12">
									<a class="object__images" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
											<div class="object-slider swiper-container">
												<div class="swiper-wrapper">
													<?foreach ($arItem['PROPERTY_PHOTO_VALUE'] as $photo) {
														$photoRes = CFile::ResizeImageGet($photo, array('width' => 692, 'height' => 484), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);?>
														<div class="swiper-slide"><img class="lazyload" src="" data-src="<?=$photoRes['src']?>" alt="Изображение"></div>
													<?}?>
												</div>
												<div class="swiper-pagination"></div>
												<div class="js-slider-img swiper-button-next" data-icon="5"></div>
												<div class="js-slider-img swiper-button-prev" data-icon="4"></div>
											</div>
										</a>
								</div>
								<div class="col-md-12">
									<div class="object-body"><a class="object__name" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
										<div class="reviews">
											<div class="reviews-stars">
												<?for ($x=0; $x<5; $x++) {?>
													<div class="reviews__star <?if ($rating > $x) echo 'reviews__star--active'?>">
														<svg class="icon">
															<use xlink:href="#icon_star"></use>
														</svg>
													</div>
												<?}?>
												<div class="reviews__count"><?=$arItem['PROPERTY_RATING_VALUE']?></div>
											</div>
											<a class="reviews__link" href="<?=$arItem["DETAIL_PAGE_URL"]?>#reviews-block"><?=$reviewsText?></a>
										</div>
										<div class="object-count-list">
											<div class="object-count"> <span>Количество работ на сайте:</span><strong><?=count($arItem['PROPERTY_COMPLETED_WORK_VALUE'])?></strong></div>
											<div class="object-count"> <span>Уровень цен на услуги:</span><strong><?=$arItem['PROPERTY_PRICE_VALUE']?></strong></div>
										</div>
										<p><?=$services?></p>
										<a class="btn" href="<?=$arItem["DETAIL_PAGE_URL"]?>">Подробнее</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?unset($services);
				}?>
				<?foreach ($arResult['FAVORITES_HOUSE'] as $key => $arItem) {
					$comp_active = ($arItem['COMPARISON'] == 'Y') ? 'control--favorites--active' : '';
					$fav_active = ($arItem['FAVORITES'] == 'Y') ? 'control--favorites--active' : '';
					$comp_text = ($arItem['COMPARISON'] == 'Y') ? 'Удалить из сравнения' : 'Добавить к сравнению';
					$fav_text = ($arItem['FAVORITES'] == 'Y') ? 'Удалить из избранного' : 'Добавить в избранное';

					$companyRating = $arResult['COMPANIES'][$arItem['PROPERTY_COMPANY_VALUE']]['RATING'];
					$rating = round($companyRating); // рейтинг
					$services = implode(', ',$arItem['SERVICES']); // услуги
					// отзывы
					$companyReviews = $arResult['COMPANIES'][$arItem['PROPERTY_COMPANY_VALUE']]['REVIEWS'];
					$cntReviews = ($companyReviews) ? count($companyReviews) : 0;
					$reviewsText = $reviewsDeclension->get($cntReviews);
					$reviewsText = ($cntReviews > 0) ? $cntReviews.' '.$reviewsText : 'Нет отзывов';?>
					<div class="col-md-6 tab-block favorites_house">
						<div class="object">
							<div class="row">
								<div class="col-md-12">
									<a class="object__images" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
											<div class="object-slider swiper-container">
												<div class="swiper-wrapper">
													<?foreach ($arItem['PROPERTY_PHOTO_VALUE'] as $photo) {
														$photoRes = CFile::ResizeImageGet($photo, array('width' => 692, 'height' => 484), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);?>
														<div class="swiper-slide"><img class="lazyload" src="" data-src="<?=$photoRes['src']?>" alt="Изображение"></div>
													<?}?>
												</div>
												<div class="swiper-pagination"></div>
												<div class="js-slider-img swiper-button-next" data-icon="5"></div>
												<div class="js-slider-img swiper-button-prev" data-icon="4"></div>
											</div>
										</a>
								</div>
								<div class="col-md-12">
									<div class="object-body"><a class="object__name" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
										<div class="reviews">
											<div class="reviews-stars">
												<?for ($x=0; $x<5; $x++) {?>
													<div class="reviews__star <?if ($rating > $x) echo 'reviews__star--active'?>">
														<svg class="icon">
															<use xlink:href="#icon_star"></use>
														</svg>
													</div>
												<?}?>
												<div class="reviews__count"><?=$companyRating?></div>
											</div>
											<a class="reviews__link" href="<?=$arItem["DETAIL_PAGE_URL"]?>#reviews-block"><?=$reviewsText?></a>
										</div>
										<div class="object-count-list">
											<div class="object-count"> <span>Площадь:</span><strong><?=$arItem['PROPERTY_AREA_VALUE']?></strong></div>
											<div class="object-count"> <span>Размер:</span><strong><?=$arItem['PROPERTY_SIZE_VALUE']?></strong></div>
											<div class="object-count"> <span>Стоимость:</span><strong><?=formatPrice($arItem['PROPERTY_PRICE_VALUE'])?></strong></div>
										</div>
										<p><?=$services?></p>
										<a class="btn" href="<?=$arItem["DETAIL_PAGE_URL"]?>">Подробнее</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?unset($services);
				}?>
			</div>
		</div>
	</div>
