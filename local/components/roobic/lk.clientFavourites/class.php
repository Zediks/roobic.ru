<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Application;
use Bitrix\Main\Grid\Declension;

class client extends CBitrixComponent
{
	function favourites()
	{

		if(isset($_COOKIE['favorites_bild'])){

			$arFavorites = explode('-',$_COOKIE['favorites_bild']);

			$arOrder = Array("SORT"=>"ASC");
			$arFilter = Array("IBLOCK_ID"=>2,"ID"=>$arFavorites);
			$arSelect = Array("ID","NAME","ACTIVE_FROM","PREVIEW_TEXT","DETAIL_PAGE_URL","PROPERTY_PHOTO","PROPERTY_RATING","PROPERTY_COMPLETED_WORK","PROPERTY_PRICE","PROPERTY_SERVICES");
			$rsElements = CIBlockElement::GetList($arOrder,$arFilter,false,false,$arSelect);
			while($arElement = $rsElements->GetNext()){ // dump($arElement);

				$arElement['COMPARISON'] = (in_array($arElement['ID'],$arComparison)) ? 'Y' : 'N';
			  $arElement['FAVORITES'] = (in_array($arElement['ID'],$arFavorites)) ? 'Y' : 'N';

				// получим услуги
				$arServicesXML = $arElement['PROPERTY_SERVICES_VALUE'];
				$arServices = getElHL(3,[],['UF_XML_ID' => $arServicesXML],['ID','UF_NAME','UF_XML_ID']);
				foreach ($arServices as $key => $value) {
					$arElement['SERVICES'][] = $value['UF_NAME'];
				} // dump($arResult['SERVICES']);

				$arResult['FAVORITES_BILD'][] = $arElement;
			}
		}

		if(isset($_COOKIE['favorites_house'])){

			$arFavorites = explode('-',$_COOKIE['favorites_house']);

			$arOrder = Array("SORT"=>"ASC");
			$arFilter = Array("IBLOCK_ID"=>3,"ID"=>$arFavorites);
			$arSelect = Array("ID","NAME","ACTIVE_FROM","PREVIEW_TEXT","DETAIL_PAGE_URL","PROPERTY_PHOTO","PROPERTY_COMPANY","PROPERTY_AREA","PROPERTY_SIZE","PROPERTY_PRICE","PROPERTY_SERVICES");
			$rsElements = CIBlockElement::GetList($arOrder,$arFilter,false,false,$arSelect);
			while($arElement = $rsElements->GetNext()){ // dump($arElement);

				$arElement['COMPARISON'] = (in_array($arElement['ID'],$arComparison)) ? 'Y' : 'N';
			  $arElement['FAVORITES'] = (in_array($arElement['ID'],$arFavorites)) ? 'Y' : 'N';

				// получим услуги
				$arServicesXML = $arElement['PROPERTY_SERVICES_VALUE'];
				$arServices = getElHL(3,[],['UF_XML_ID' => $arServicesXML],['ID','UF_NAME','UF_XML_ID']);
				foreach ($arServices as $key => $value) {
					$arElement['SERVICES'][] = $value['UF_NAME'];
				} // dump($arResult['SERVICES']);

				$idCompany = $arItem['PROPERTY_COMPANY_VALUE'];
			  $arIdsCompany[] = $idCompany;

				$arResult['FAVORITES_HOUSE'][] = $arElement;
			}

			// получаем компании
			$arOrder = Array('SORT'=>'ASC');
			$arFilter = Array('IBLOCK_ID'=>2,'ID'=>$arIdsCompany);
			$arSelect = Array('ID','NAME','PROPERTY_RATING','PROPERTY_REVIEWS');
			$rsElements = CIBlockElement::GetList($arOrder,$arFilter,false,false,$arSelect);
			while($arElement = $rsElements->GetNext()){ // dump($arElement);
			  $arResult['COMPANIES'][$arElement['ID']]['NAME'] = $arElement['NAME'];
			  $arResult['COMPANIES'][$arElement['ID']]['RATING'] = $arElement['PROPERTY_RATING_VALUE'];
				$arResult['COMPANIES'][$arElement['ID']]['REVIEWS'] = $arElement['PROPERTY_REVIEWS_VALUE'];
			}
		}

		return $arResult;
	}

	public function executeComponent()
	{
		$this->arResult = array_merge($this->arResult,$this->favourites());
		$this->includeComponentTemplate();
	}
}?>
