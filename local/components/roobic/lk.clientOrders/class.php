<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Application;

class company extends CBitrixComponent
{
	function profile()
	{
		global $USER;
		$userID = $USER->GetID();
		$iblockID = 4;
		$contentManager = CSite::InGroup([8]);

		// получим услуги
		$arElHL = getElHL(3,[],[],['ID','UF_NAME','UF_XML_ID','UF_MAIN_SECTION']);
		foreach ($arElHL as $key => $value)
			$arResult['SERVICES'][$value['UF_XML_ID']] = [
				'NAME' => $value['UF_NAME'],
				'MAIN_SECTION' => $value['UF_MAIN_SECTION'],
			];

		// изменение статуса
		$request = Application::getInstance()->getContext()->getRequest();
		$orderID = $request->getQuery("order_id");
		$orderStatus = $request->getQuery("status");
		if($orderID && $orderStatus)
			CIBlockElement::SetPropertyValues($orderID, $iblockID, $orderStatus, "STATUS");

		// удаление
		$orderDelete = $request->getQuery("del");
		if ($orderID && $orderDelete == 'y')
			CIBlockElement::Delete($orderID);

		// получим сами заказы
		$arOrder = Array('SORT'=>'ASC');

		// if ($contentManager)
		// 	$arFilter = Array('IBLOCK_ID'=>$iblockID);
		// else
			$arFilter = Array('IBLOCK_ID'=>$iblockID,'PROPERTY_USER'=>$userID);

		$arSelect = Array('ID','NAME','CODE','DETAIL_TEXT','DATE_CREATE','PROPERTY_ADDRESS','PROPERTY_SERVICE','PROPERTY_BUDGET','PROPERTY_STATUS','PROPERTY_CONTRACT'); // ,'PROPERTY_'
		$rsElements = CIBlockElement::GetList($arOrder,$arFilter,false,false,$arSelect);
		while($arElement = $rsElements->GetNext()) // dump($arElement);
		{
			$arOrderId[] = $arElement['ID'];
			$arResult['ORDERS'][] = $arElement;
		}

		if ($arOrderId)
		{
			// получим отклики на заказы
			$arHL = getElHL(12,[],['UF_ORDER'=>$arOrderId],['*']); // dump($arHL);
			foreach ($arHL as $value)
				$arResult['RESPONSES'][$value['UF_ORDER']][] = $value['UF_COMPANY'];
		}

		return $arResult;
	}

	public function executeComponent()
	{
		$this->arResult = array_merge($this->arResult,$this->profile());
		$this->includeComponentTemplate();
	}
}?>
