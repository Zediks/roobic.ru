<?php
require 'phpQuery.php';

	

	$link = array();
	
	
	//Собираем ссылки на проекты из списка проектов	
	for($i=0;$i<800;$i++){ //800
		$url = "https://akademik-stroy.ru/category/projects/page/".$i."/?orderby=date";
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_COOKIEFILE, __DIR__ . '/cookie.txt');
		curl_setopt($ch, CURLOPT_COOKIEJAR, __DIR__ . '/cookie.txt');
		curl_setopt($ch, CURLOPT_REFERER, 'http://yandex.ru');
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HEADER, false);
		$html = curl_exec($ch);
		curl_close($ch);
		
		$document = phpQuery::newDocument($html);
		$pq = pq($document);
		
		$elements = $pq->find('.products.columns-4:eq(0) .woocommerce-LoopProduct-link.woocommerce-loop-product__link');
		foreach ($elements as $el) {
			$pqEl = pq($el);
			$link[] = trim($pqEl->attr('href'));
			echo trim($pqEl->attr('href'))."\n";
		}	

		phpQuery::unloadDocuments();
	}
	
	
	//Чистим от дублей
	$link = array_unique($link);
	$link = array_values($link);
	
	$txt = "ID;Название;Описание;Строительная компания;Тип дома;Цена;Старая цена;Материал стен;Материал кровли;Площадь;Жилая площадь;Количество этажей;Кол-во спален;Кол-во санузлов;Назначение дома;Видео;Входит в стоимость;Площадь террасы/балкона;Гараж;Гарантии;Класс;Многоэтажный;Отделка;Отделка фасада;Площадь 1 этажа;Площадь 1 этажа (Изображение);Площадь 2 этажа;Площадь 2 этажа (Изображение);Площадь кровли;Пристрой;Размер;Размер бруса;Стиль, разновидность;Тип кровли;Утеплитель;Форма;Фундамент;Фасады (Изображения);Фотографии \n"; //Строка для заполнения данными
	
	for($i=0;$i<count($link);$i++){//count($link)
		
		$url = trim($link[$i]);
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_COOKIEFILE, __DIR__ . '/cookie.txt');
		curl_setopt($ch, CURLOPT_COOKIEJAR, __DIR__ . '/cookie.txt');
		curl_setopt($ch, CURLOPT_REFERER, 'http://yandex.ru');
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HEADER, false);
		$html = curl_exec($ch);
		curl_close($ch);
			
		$document = phpQuery::newDocument($html);
		$pq = pq($document);
		
		
		$id = "";             //id
		$title = "";          //Название
		$description = "";    //Описание		
		$company = "";        //Строительная компания		
		$type_house = "";     //Тип дома
		$price = "";          //Цена
		$oldprice = "";       //Старая Цена
		$material = "";       //Материал стен 
		$material_2 = "";     //Материал кровли
		$sqr = "";            //Площадь
		$floors = "";         //Количество этажей
		$beds = "";           //Кол-во спален
		$sqr_l = "";          //Жилая площадь
		$bath = "";           //Кол-во санузлов
		$house = "";          //Назначение дома
		$video = "";          //Видео
		$all_in = "";         //Входит в стоимость
		$sqr_ter = "";        //Площадь террасы/балкона
		$garage = "";         //Гараж 
		$garant = "";         //Гарантии
        $class = "";		  //Класс
		$floors_1 = "";       //Многоэтажный
		$material_3 = "";     //Отделка
		$material_4 = "";     //Отделка фасада
		$sqr_1 = "";          //Площадь 1 этажа
		$sqr_1_img = "";      //Площадь 1 этажа (Изображение)
		$sqr_2 = "";          //Площадь 2 этажа
		$sqr_2_img = "";      //Площадь 2 этажа (Изображение)
		$sqr_rf = "";         //Площадь кровли
		$build = "";          //Пристрой
		$size = "";           //Размер
		$size_b = "";         //Размер бруса
		$style = "";          //Стиль, разновидность
		$type_rf = "";        //Тип кровли
		$hots = "";           //Утеплитель
		$form = "";           //Форма
		$fund = "";           //Фундамент
		$fasade_img = "";     //Фасады (Изображения)
		$photos = "";         //Фотографии
		
		
		
		
		$id = preg_replace("/('|\"|\r?\n)/",'', trim($pq->find('#button-heart')->attr('data-postid')));		
		$title = preg_replace("/('|\"|\r?\n)/",'', trim($pq->find('h1:eq(0)')->text()));			
		$title = preg_replace("/ за.*/m",'', $title);		
		$price  = preg_replace("/('|\"|\r?\n)/",'', trim($pq->find('.pr-item__main-price .full-price:eq(0)')->text()));
		$price = str_ireplace(array("<sup>2</sup>","м","от "," руб."," "),"", $price);
		
		
		
		
		
		$description = preg_replace("/('|\"|\r?\n)/",'', trim($pq->find('#descriptionAll')->html()));			
		$company  = preg_replace("/('|\"|\r?\n)/",'', trim($pq->find('.constructor-name:eq(0)')->text()));
		$type_house  = preg_replace("/('|\"|\r?\n)/",'', trim($pq->find('.search-params__i .item__i:contains("Тип постройки:") p:last')->text()));
				
		$sqr_ter = preg_replace("/('|\"|\r?\n)/",'', trim($pq->find('.stat-block-item:contains("Террасы и балконы") .b')->text()));		
		$garage = preg_replace("/('|\"|\r?\n)/",'', trim($pq->find('.stat-block-item:contains("араж")')->text()));		
		$floors = preg_replace("/('|\"|\r?\n)/",'', trim($pq->find('.stat-block-item:contains("Этажей") .b')->text()));		
		$beds = preg_replace("/('|\"|\r?\n)/",'', trim($pq->find('.stat-block-item:contains("Количество спален") .b')->text()));		
		$size = preg_replace("/('|\"|\r?\n)/",'', trim($pq->find('.stat-block-item:contains("Размеры") .b')->text()));		
		$size = str_ireplace(array("м","м2"," "),"", $size);		
		$bath = preg_replace("/('|\"|\r?\n)/",'', trim($pq->find('.stat-block-item:contains("Количество санузлов") .b')->text()));	
		
		$sqr_1_img = preg_replace("/('|\"|\r?\n)/",'', trim($pq->find('img[data-plan-name="План первого этажа"]')->attr('data-plan')));
		$sqr_2_img = preg_replace("/('|\"|\r?\n)/",'', trim($pq->find('img[data-plan-name="План второго этажа"]')->attr('data-plan')));
		
		$sqr = preg_replace("/('|\"|\r?\n)/",'', trim($pq->find('.accordion__heading.accordion__heading--promo:contains("Общая площадь") .accordion__value')->text()));		
		$sqr = str_ireplace(array("<sup>2</sup>","м2","m²"),"", $sqr);
		
		
		
		$fund = preg_replace("/('|\"|\r?\n)/",'', trim($pq->find('.builds_calc__item-title:contains("Фундамент") + div.builds_calc__item .carousel.slide .nav-link.active')->text()));
		$material  = preg_replace("/('|\"|\r?\n)/",'', trim($pq->find('.builds_calc__item-title:contains("Стены 1 этаж (несущие)") + div.builds_calc__item .carousel.slide .nav-link.active')->text()));
		$material_2  = preg_replace("/('|\"|\r?\n)/",'', trim($pq->find('.builds_calc__item-title:contains("Крыша (Скатная)") + div.builds_calc__item .carousel.slide .nav-link.active')->text()));
		$hots  = preg_replace("/('|\"|\r?\n)/",'', trim($pq->find('.builds_calc__item-title:contains("Дополнительное утепление наружных стен") + div.builds_calc__item .carousel.slide .nav-link.active')->text()));
		$material_4  = preg_replace("/('|\"|\r?\n)/",'', trim($pq->find('.builds_calc__item-title:contains("Наружная отделка фасада") + div.builds_calc__item .carousel.slide .nav-link.active')->text()));
		$garant = preg_replace("/('|\"|\r?\n)/",'', trim($pq->find('.advantages-desc:contains("гарант")')->text()));
		
		$type_rf = preg_replace("/('|\"|\r?\n)/",'', trim($pq->find('.search-params__i .item__i:contains("Крыша:") p:last')->text()));
		$style = preg_replace("/('|\"|\r?\n)/",'', trim($pq->find('.search-params__i .item__i:contains("Стиль:") p:last')->text()));		
		$form = preg_replace("/('|\"|\r?\n)/",'', trim($pq->find('.search-params__i .item__i:contains("Форма:") p:last')->text()));		
		
		
		
		$sqr_l = preg_replace("/('|\"|\r?\n)/",'', trim($pq->find('.accordion__heading.accordion__heading--promo:contains("Жилая площадь") .accordion__value')->text()));
		$sqr_l = str_ireplace(array("<sup>2</sup>","м2","m²"),"", $sqr_l);
		$sqr_rf = preg_replace("/('|\"|\r?\n)/",'', trim($pq->find('.accordion__heading.accordion__heading--promo:contains("Площадь крыши") .accordion__value')->text()));
		$sqr_rf = str_ireplace(array("<sup>2</sup>","м2","m²"),"", $sqr_rf);
		
		
		$sqr_1 = preg_replace("/('|\"|\r?\n)/",'', trim($pq->find('.additional-info.table-responsive table tr:contains("1 этаж") td:eq(4)')->text()));
		$sqr_1 = str_ireplace(array("<sup>2</sup>","м2","m²"),"", $sqr_1);
		$sqr_2 = preg_replace("/('|\"|\r?\n)/",'', trim($pq->find('.additional-info.table-responsive table tr:contains("2 этаж") td:eq(4)')->text()));
		$sqr_2 = str_ireplace(array("<sup>2</sup>","м2","m²"),"", $sqr_2);
		
		

		
		$material_3 = preg_replace("/('|\"|\r?\n)/",'', trim($pq->find('.price:eq(0) p:contains("Комплектация:") a')->text()));
		
		$elements = $pq->find('.woocommerce-product-gallery__wrapper a');
		foreach ($elements as $el) {
			$pqEl = pq($el);
			$photos .= preg_replace("/('|\"|\r?\n)/",'', trim($pqEl->attr('href')))."|";			
		}
		
		$elements = $pq->find('#elevations-section .elevations__item img');
		foreach ($elements as $el) {
			$pqEl = pq($el);
			$fasade_img .= "https://akademik-stroy.ru".preg_replace("/('|\"|\r?\n)/",'', trim($pqEl->attr('src')))."|";			
		}
		
		
		
		
		$txt .= $id.";\"".$title."\";\"".$description."\";".$company.";".$type_house.";".$price.";".$oldprice.";".$material.";".$material_2.";".$sqr.";".$sqr_l.";".$floors.";".$beds.";".$bath.";".$house.";".$video.";".$all_in.";".$sqr_ter.";".$garage.";".$garant.";".$class.";".$floors_1.";".$material_3.";".$material_4.";".$sqr_1.";".$sqr_1_img.";".$sqr_2.";".$sqr_2_img.";".$sqr_rf.";".$build.";".$size.";".$size_b.";".$style.";".$type_rf.";".$hots.";".$form.";".$fund.";".$fasade_img.";".$photos."\n";
		
				
		phpQuery::unloadDocuments();	
	}
	
	
	$fp = fopen(__DIR__ .'/akademik.csv', 'w+');//	
	fwrite($fp, $txt);
	fclose($fp);
?>