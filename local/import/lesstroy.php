<?php set_time_limit(0);
header('Content-Type: text/html; charset=utf-8');
// $_SERVER["DOCUMENT_ROOT"] = '/var/www/u0428181/data/www/olne.ru';
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Loader;
	Loader::includeModule('iblock');
	Loader::includeModule('highloadblock');
use Bitrix\Highloadblock as HL, Bitrix\Main\Entity;

$arParams = [
  "max_len" => "100", // обрезает символьный код до 100 символов
  "change_case" => "L", // буквы преобразуются к нижнему регистру
  "replace_space" => "-", // меняем пробелы на тире
  "replace_other" => "-", // меняем левые символы на тире
  "delete_repeat_replace" => "true", // удаляем повторяющиеся символы
  "use_google" => "false", // отключаем использование google
];

function trim_and_del($val){
  $val = trim($val); // убираем пробелы
  $val = str_replace('"','',$val); // убираем кавычки
  return $val;
}

$fileName = 'lesstroy.csv';
$urlFeed = $_SERVER["DOCUMENT_ROOT"].'/local/import/'.$fileName;
$feedFile = file_get_contents($urlFeed);
$arFile = explode("\n",$feedFile);

$test = true; // режим теста

// получим материалы
$arMaterials = getElHL(11,[],['UF_ACTIVE'=>true],['*']);
foreach ($arMaterials as $value)
  $arTypeHouse[$value['UF_NAME']] = $value['UF_XML_ID'];
$arTypeHouse['Дом из обрезного бруса'] = 'doma-iz-brusa';
// dump($arTypeHouse);

$arElHL = getElHL(7,[],[],['ID','UF_NAME','UF_XML_ID','UF_TYPE']);
foreach ($arElHL as $value) {
  if ($value['UF_TYPE'] == 46) $arResult['MATERIALS_WALL'][] = $value; // Материал стен
  elseif ($value['UF_TYPE'] == 47) $arMaterialRoof[$value['UF_NAME']] = $value['UF_XML_ID']; // Материал кровли
}

// Размер (список)
$propCode = 'SIZE_LIST';
$rsPropertyEnum = \CIBlockPropertyEnum::GetList([],["IBLOCK_ID"=>3, "CODE"=>$propCode]);
while($arPropertyEnum = $rsPropertyEnum->Fetch())
	$arSize[$arPropertyEnum['VALUE']] = $arPropertyEnum['ID'];

// Стиль, разновидность
$propCode = 'STYLE';
$rsPropertyEnum = \CIBlockPropertyEnum::GetList([],["IBLOCK_ID"=>3, "CODE"=>$propCode]);
while($arPropertyEnum = $rsPropertyEnum->Fetch())
	$arStyle[$arPropertyEnum['VALUE']] = $arPropertyEnum['ID'];

foreach ($arFile as $key => $value)
{
  if ($key == 0) continue; // пропустим заголовок
  if ($value) {
    $arHouseInfo = array_map("trim_and_del",explode(";",$value)); // dump($arHouseInfo);

    $houseName = $arHouseInfo[1];
    $houseCode = CUtil::translit($houseName, "ru", $arParams);

    $el = new CIBlockElement;
    $PROP = array();

    $houseType = $arHouseInfo[4];
    if (array_key_exists($houseType,$arTypeHouse))
      $PROP['TYPE'] = $arTypeHouse[$houseType];
    else
      echo 'Не задан Tип дома: <b srtyle="color:red">'.$houseType.'</b><br>';

    $PROP['PRICE'] = $arHouseInfo[5];

    if(in_array($arHouseInfo[7],['Дом из профилированного бруса','Дом из строганного бруса','Дом из обрезного бруса']))
      $PROP['MATERIAL_WALL'] = 'brus';
    else
      echo 'Не задан Материал стен: <b srtyle="color:red">'.$arHouseInfo[7].'</b><br>';

    $materialRoof = $arHouseInfo[8];
    if (array_key_exists($materialRoof,$arMaterialRoof))
      $PROP['MATERIAL_ROOF'] = $arMaterialRoof[$materialRoof];
    else
      echo 'Не задан Материал кровли: <b srtyle="color:red">'.$materialRoof.'</b><br>';

    $PROP['AREA'] = $arHouseInfo[9];
    $PROP['CNT_FLOORS'] = $arHouseInfo[11];

		$arBedroom = explode(' ',$arHouseInfo[12]);
		$PROP['CNT_BEDROOM'] = $arBedroom[0];

		if(in_array($arHouseInfo[22],['Под ключ','Под усадку','Под крышу']))
      $PROP['FINISHING'] = 15; // Без отделки
    else
      echo 'Не задана Отделка: <b srtyle="color:red">'.$arHouseInfo[22].'</b><br>';

		// Размер
		$size = str_replace(' ','',$arHouseInfo[30]);
		if (array_key_exists($size,$arSize))
      $PROP['SIZE_LIST'] = $arSize[$size];
    else
      echo 'Не задан Размер: <b srtyle="color:red">'.$size.'</b><br>';

		switch ($arHouseInfo[32]) { // Стиль, разновидность
			case 'Кантри':
				$PROP['STYLE'] = 170;
				break;
			case 'Русский':
				$PROP['STYLE'] = 171;
				break;
			case 'Иное':
				$PROP['STYLE'] = 172;
				break;
		}
		if (!$PROP['STYLE'])
			echo 'Не задан Стиль: <b srtyle="color:red">'.$arHouseInfo[32].'</b><br>';

		switch ($arHouseInfo[33]) { // Тип кровли
			case 'Мансардная':
				$PROP['TYPE_ROOF'] = 173;
				break;
			case 'Двускатная (щипцовая)':
				$PROP['TYPE_ROOF'] = 62;
				break;
			case 'Четырехскатная (вальмовая)':
				$PROP['TYPE_ROOF'] = 64;
				break;
			case 'Иное':
				$PROP['TYPE_ROOF'] = 174;
				break;
		}

		if (!$PROP['TYPE_ROOF'])
			echo 'Не задан Тип кровли: <b srtyle="color:red">'.$arHouseInfo[33].'</b><br>';

		switch ($arHouseInfo[35]) { // Форма
			case 'Прямоугольные':
				$PROP['FORM'] = 175;
				break;
			case 'П-образные':
				$PROP['FORM'] = 59;
				break;
			case 'Квадратные':
				$PROP['FORM'] = 60;
				break;
		}

		if (!$PROP['FORM'])
			echo 'Не задана Форма: <b srtyle="color:red">'.$arHouseInfo[35].'</b><br>';

		// Фото
		// $arPhoto = explode('|',$arHouseInfo[38]);
		// for ($i=0; $i < 5; $i++)
		// 	if ($arPhoto[$i])
		// 		$PROP['PHOTO'][] = CFile::MakeFileArray($arPhoto[$i]);

		$houseText = preg_replace('#<a.*?>.*?</a>#i', '', $arHouseInfo[2]);

    $arLoadProductArray = Array(
      "IBLOCK_ID"      		=> 3,
      "PROPERTY_VALUES"		=> $PROP,
      "NAME"           		=> $houseName,
      "CODE"					 		=> $houseCode,
      "DETAIL_TEXT"       => $houseText,
      "DETAIL_TEXT_TYPE" 	=> 'html',
    ); // dump($arLoadProductArray);

    if (!$test) {
      if (!$el->Add($arLoadProductArray))
				echo 'Ошибка добавления: <b srtyle="color:red">'.$el->LAST_ERROR.'</b><br>';
			else
				echo '<a href="https://stroiman.ru/doma/'.$houseCode.'/">https://stroiman.ru/doma/'.$houseCode.'/</a><br>';
    }
  }
}
