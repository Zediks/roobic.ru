<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$request = $context->getRequest();
$action = $request->getPost('action');

if ($action == 'profile_field') { // изменение полей в профиле

  $nameEl = $request->getPost('nameEl');
  $valEl = $request->getPost('valEl');

  switch ($nameEl) {
    case 'user_name': $field = 'NAME'; break;
    case 'user_email': $field = 'EMAIL'; break;
    case 'user_phone': $field = 'PHONE_NUMBER'; break;
  }

  global $USER;
  $userID = $USER->GetID();

  // обновление пользователя
  $userCompany = new CUser;
  $arFieldsUser[$field] = $valEl;

  if($userCompany->Update($userID,$arFieldsUser))
    echo 'Успешно!';
  else
    echo $userCompany->LAST_ERROR;

} elseif ($action == 'notif_read') { // прочитано уведомление

  $hlID = 13;
  $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($hlID)->fetch();
  $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
  $entity_data_class = $entity->getDataClass();

  $dataID = $request->getPost('dataID');
  $data = [
    "UF_READ" => 1
  ];
  $result = $entity_data_class::update($dataID,$data);

  if ($result->isSuccess()) echo 'Успешно!';
  else echo 'Ошибка: '.$result->getErrorMessages();

}
