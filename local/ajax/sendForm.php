<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Loader;
	Loader::includeModule('highloadblock');
use Bitrix\Highloadblock as HL, Bitrix\Main\Entity;

$request = $context->getRequest();

$act = $request->getPost('act');

if($act == 'sendForm'){ // отправка заявки

  $mailFields = array(
    "NAME" => $request->getPost('name'),
    "PHONE" => $request->getPost('phone'),
    "EMAIL" => $request->getPost('email'),
    "PAGE" => $_SERVER['HTTP_REFERER'],
  );
  if (CEvent::Send("SEND_ORDER", "s1", $mailFields)) echo '<p>Спасибо! Данные успешно отправлены!</p>';
  else echo '<p>Ошибка! Данные не отправлены!</p>';

}elseif($act == 'sendWriteToUs'){ // Написать нам

  $mailFields = array(
    "NAME" => $request->getPost('name'),
    "PHONE" => $request->getPost('phone'),
    "TEXT" => $request->getPost('text'),
    "PAGE" => $_SERVER['HTTP_REFERER'],
  );
  if (CEvent::Send("SEND_WRITE_TO_US", "s1", $mailFields)) echo '<p>Спасибо! Данные успешно отправлены!</p>';
  else echo '<p>Ошибка! Данные не отправлены!</p>';

}elseif ($act == 'sendCalculate') { // калькулятор

  $mailFields = array(
    "SERVICE" => $request->getPost('option-1'),
    "MATERIAL" => $request->getPost('option-2'),
    "NUMBER" => $request->getPost('number'),
    "PAGE" => $_SERVER['HTTP_REFERER'],
  );
  if (CEvent::Send("SEND_CALCULATE", "s1", $mailFields)) echo '<p>Спасибо! Данные успешно отправлены!</p>';
  else echo '<p>Ошибка! Данные не отправлены!</p>';

}elseif ($act == 'sendEstimate') { // смета

  $mailFields = array(
    "NAME" => $request->getPost('name'),
    "PHONE" => $request->getPost('phone'),
    "COMMENT" => $request->getPost('comment'),
    "TENDER" => $request->getPost('konkurs'),
    "PAGE" => $_SERVER['HTTP_REFERER'],
  );
  if (CEvent::Send("SEND_ESTIMATE", "s1", $mailFields)) echo '<p>Спасибо! Данные успешно отправлены!</p>';
  else echo '<p>Ошибка! Данные не отправлены!</p>';

}elseif ($act == 'sendReview') { // отзыв

  $hlblock_id = 9; // id HL
  $hlblock = HL\HighloadBlockTable::getById($hlblock_id)->fetch();
  $entity = HL\HighloadBlockTable::compileEntity($hlblock);
  $entity_data_class = $entity->getDataClass();

  $data = [
    "UF_NAME" => $request->getPost('name'),
    "UF_LAST_NAME" => $request->getPost('fname'),
    "UF_EXPERTISE" => $request->getPost('expertise'),
    "UF_DEADLINE" => $request->getPost('deadline'),
    "UF_SERVICE" => $request->getPost('service'),
    "UF_PRICE" => $request->getPost('price'),
    "UF_QUALITY" => $request->getPost('quality'),
    "UF_DESCRIPTION" => $request->getPost('text'),
    "UF_EMAIL" => $request->getPost('email'),
    "UF_DATE_TIME" => date('d.m.Y H:i:s'),
    "UF_COMPANY" => $request->getPost('company'),
		"UF_ACTIVE" => false
  ]; // dump($data);

  $result = $entity_data_class::add($data);

  if ($result->isSuccess()) {
    echo '<p>Спасибо! Отзыв успешно отправлен!</p>';
  } else {
    echo '<p>Ошибка: '.$result->getErrorMessages().'</p>';
  }
}elseif ($act == 'sendResponse') { // заявка на заказ

	// получим компанию
	$by = 'id'; $sort = 'asc'; // поле сортировки
	$filter = ['ID' => $request->getPost('userID')];
	$arParams = [ // наши поля
		'FIELDS' => ['ID'], 'SELECT' => ['UF_COMPANIES']
	];
	$rsUsers = CUser::GetList($by, $sort, $filter, $arParams);
	$arUser = $rsUsers->GetNext(); // dump($arUser);
	$arResult['USER'] = $arUser;
	$companyID = $arUser['UF_COMPANIES'][0];

	// создадим отклик
  $hlblock_id = 12; // id HL
  $hlblock = HL\HighloadBlockTable::getById($hlblock_id)->fetch();
  $entity = HL\HighloadBlockTable::compileEntity($hlblock);
  $entity_data_class = $entity->getDataClass();

  $data = [
		'UF_ORDER' => $request->getPost('orderID'),
		'UF_COMPANY' => $companyID,
	];

	$result = $entity_data_class::add($data);

  if ($result->isSuccess())
    echo '<p>Ваш отклик успешно отправлен!</p>';
  else
    echo '<p>Ошибка: '.$result->getErrorMessages().'</p>';
}
?>
