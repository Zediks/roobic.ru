<?
// тест вывод
function dump($el){
	global $USER;
	if($USER->IsAdmin()){
		echo "<pre>";print_r($el);echo "</pre>";
	}
}

// формат цены
function getBath($code){
	$arBath = [
		'bani-iz-dereva',
		'bani-iz-brusa',
		'bani-iz-strogannogo-brusa',
		'bani-iz-profilirovannogo-brusa',
		'bani-iz-kleenogo-brusa',
		'karkasnye-bani',
		'bani-iz-kirpicha',
		'bani-iz-penoblokov',
		'bani-iz-paneley',
	];

	$nameHouse = (in_array($code,$arBath)) ? 'бани' : 'дома';
	return $nameHouse;
}

// получение иконки главного раздела
function getIconMainSection($mainSectionID){
	switch ($mainSectionID) {
    case 4: $icon = 's_01.svg'; break; // Строительство дома
    case 5: $icon = 's_02.svg'; break; // Строительные работы
    case 6: $icon = 's_05.svg'; break; // Отделочные работы
    case 7: $icon = 's_03.svg'; break; // Коммуникации
    case 8: $icon = 's_04.svg'; break; // Проектные работы
    case 19: $icon = 's_01.svg'; break; // Жилые дома
    case 23: $icon = 's_06.svg'; break; // Бани
    case 24: $icon = 's_07.svg'; break; // Беседки
    case 25: $icon = 's_08.svg'; break; // Бытовки
    case 26: $icon = 's_09.svg'; break; // Прочие строения
		case 48: $icon = 's_10.svg'; break; // Стойматериалы

		case 51: $icon = 's_01.svg'; break; // Общестрой
		case 52: $icon = 's_05.svg'; break; // Монтаж
		case 53: $icon = 's_02.svg'; break; // Отделка
		case 54: $icon = 's_10.svg'; break; // Разное
		case 55: $icon = 's_03.svg'; break; // Товары

		default: $icon = 's_01.svg'; break;
  }
	return $icon;
}

// формат цены
function formatPrice($price){
	$newPrice = number_format($price, 0, ',', ' ');
	return $newPrice;
}

// получение списка свойства
function getListProperty($arSort,$arFilter){ // USER_FIELD_ID
	$rsField = CUserFieldEnum::GetList($arSort,$arFilter);
	while($arField = $rsField->GetNext())
	  $arElements[$arField["ID"]]=$arField["VALUE"];

	return $arElements;
}

function getPropertyEnum($arSort,$arFilter){
	$rsPropertyEnum = CIBlockPropertyEnum::GetList($arSort,$arFilter);
	while ($arPropertyEnum = $rsPropertyEnum->Fetch())
	  $arElements[$arPropertyEnum['ID']] = $arPropertyEnum;

	return $arElements;
}

function getRegionURL(){
	switch ($_SESSION["SOTBIT_REGIONS"]["CODE"]) {
		case 'https://spb.stroiman.ru/':
			$regionURL = 'sankt-peterburg/';
			break;
		case 'https://mo.stroiman.ru/':
			$regionURL = 'moskovskaya-obl/';
			break;
		case 'https://lo.stroiman.ru/':
			$regionURL = 'leningradskaya-obl/';
			break;

		default:
			$regionURL = 'moskva/';
			break;
	}

	return $regionURL;
}

// получение кол-ва
function getMetaInfo($arrFilter){
	// получим кол-во домов
	$cntPos = 0;
	$arOrder = Array("SORT"=>"ASC");
	$arFilter = Array("IBLOCK_ID"=>3,"ACTIVE"=>"Y");
	if($arrFilter)array_push($arFilter,$arrFilter); // dump($arrFilter);
	$arSelect = Array("ID");
	$rsElements = CIBlockElement::GetList($arOrder,$arFilter,false,false,$arSelect);
	while($arElement = $rsElements->Fetch()){ // dump($arElement);
		$cntPos++;
	} // echo '$minPrice: '.$minPrice.'$cntPos: '.$cntPos;

	return $metaInfo[] = [ 'cntPos' => $cntPos ];
}

// получение элементов HL
use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL, Bitrix\Main\Entity;
	Loader::includeModule('highloadblock');

// получение элементов HL-блока
function getElHL($idHL,$order,$filter,$select){

	$hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($idHL)->fetch();
	$entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
	$entity_data_class = $entity->getDataClass();
	$entity_table_name = $hlblock['TABLE_NAME'];
	$sTableID = 'tbl_'.$entity_table_name;

	$rsData = $entity_data_class::getList([
		'order' => $order,
	  'filter' => $filter,
		'select' => $select
	]);
	$rsData = new CDBResult($rsData, $sTableID);

	while($arRes = $rsData->Fetch()){
		$arElements[$arRes['ID']] = $arRes;
	}
	return $arElements;
}

// при регистрации в нужную группу
\Bitrix\Main\EventManager::getInstance()->addEventHandlerCompatible(
	'main',
	'OnBeforeUserRegister',
	'OnBeforeUserRegisterAddHandler'
);
// создаем обработчик события "OnBeforeUserRegister"
function OnBeforeUserRegisterAddHandler(&$arFields)
{ // echo "<pre>";print_r($arFields);echo "</pre>"; die();
	if($arFields['PERSONAL_ICQ'])
		$arFields['GROUP_ID'][] = (int)$arFields['PERSONAL_ICQ'];

	$arFields['LOGIN'] = $arFields['EMAIL'];
	$arFields['UF_PASSWORD'] = $arFields['PASSWORD'];
}

\Bitrix\Main\EventManager::getInstance()->addEventHandlerCompatible(
	'main',
	'OnBeforeEventAdd',
	'SendPassword::onBeforeEventAdd'
);

class SendPassword
{
	function onBeforeEventAdd(&$event, &$lid, &$arFields, &$message_id, &$files)
	{
		if($event == "NEW_USER_CONFIRM")
			$arFields["PASSWORD"] = $arFields['UF_PASSWORD'];

		// echo "<pre>";print_r($arFields);echo "</pre>"; die();
	}
}
?>
