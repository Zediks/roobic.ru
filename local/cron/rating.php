<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Loader;
	Loader::includeModule('iblock');
	Loader::includeModule('highloadblock');
use Bitrix\Highloadblock as HL, Bitrix\Main\Entity;

$iblockID = 2;

// получим отзывы
$arElHL = getElHL(9,[],[],['ID','UF_COMPANY','UF_EXPERTISE','UF_DEADLINE','UF_SERVICE','UF_PRICE','UF_QUALITY']);
foreach ($arElHL as $value)
{
	$sumScore = (int)$value['UF_EXPERTISE'] + (int)$value['UF_DEADLINE'] + (int)$value['UF_SERVICE'] + (int)$value['UF_PRICE'] + (int)$value['UF_QUALITY'];
  $rating = $sumScore / 5;
  $value['RATING'] = round($rating,2);

	$arReviews[$value['UF_COMPANY']][] = $value;
}

foreach ($arReviews as $companyID => $reviews)
{
  $cntReviews = count($reviews);
  if ($cntReviews) // рейтинг
  {
    $ratingSum = 0;
    foreach ($reviews as $value)
      $ratingSum += $value['RATING'];

    $rating = $ratingSum / $cntReviews;
  }
  else $rating = 0;

  \CIBlockElement::SetPropertyValues($companyID, $iblockID, $rating, "RATING");
}
