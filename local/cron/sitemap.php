<?php //$_SERVER["DOCUMENT_ROOT"] = '/var/www/u0428181/data/www/olne.ru';
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Loader;
	Loader::includeModule('iblock');
	Loader::includeModule('highloadblock');
use Bitrix\Seo\SitemapTable;
use Bitrix\Highloadblock as HL, Bitrix\Main\Entity;

echo 'sitemap<br>';

// получим материалы
$arMaterials = getElHL(11,[],['UF_ACTIVE'=>true],['*']);
foreach ($arMaterials as $key => $value) // распределим для формирования
  $materials[$value['UF_MAIN_SECTION']][$value['ID']] = $value;

foreach ($materials as $idMain => $mainSection) {
  foreach ($mainSection as $section) {
    $xml_content .= '<url><loc>https://stroiman.ru/'.$section['UF_XML_ID'].'/</loc><lastmod>'.date('c').'</lastmod><priority>1</priority></url>';
  }
}

// Кол-во спален
for ($i=2; $i <= 4; $i++) {
	$xml_content .= '<url><loc>https://stroiman.ru/dom-'.$i.'-spalni/</loc><lastmod>'.date('c').'</lastmod><priority>1</priority></url>';
}
for ($i=5; $i <= 10; $i++) {
	$xml_content .= '<url><loc>https://stroiman.ru/dom-'.$i.'-spalen/</loc><lastmod>'.date('c').'</lastmod><priority>1</priority></url>';
}

// Кол-во санузлов
$xml_content .= '<url><loc>https://stroiman.ru/doma-1sanuzel/</loc><lastmod>'.date('c').'</lastmod><priority>1</priority></url>';
for ($i=2; $i <= 4; $i++) {
	$xml_content .= '<url><loc>https://stroiman.ru/doma-'.$i.'sanuzla/</loc><lastmod>'.date('c').'</lastmod><priority>1</priority></url>';
}

// Площадь
for ($i=70; $i < 200; $i+=10) {
  $xml_content .= '<url><loc>https://stroiman.ru/doma-'.$i.'m/</loc><lastmod>'.date('c').'</lastmod><priority>1</priority></url>';
}
for ($i=200; $i <= 500; $i+=50) {
  $xml_content .= '<url><loc>https://stroiman.ru/doma-'.$i.'m/</loc><lastmod>'.date('c').'</lastmod><priority>1</priority></url>';
}

// Этажность
$xml_content .= '<url><loc>https://stroiman.ru/doma-1-etazh/</loc><lastmod>'.date('c').'</lastmod><priority>1</priority></url>';
$xml_content .= '<url><loc>https://stroiman.ru/doma-2-etazh/</loc><lastmod>'.date('c').'</lastmod><priority>1</priority></url>';

// Стоимость
$xml_content .= '<url><loc>https://stroiman.ru/doma-do1mln/</loc><lastmod>'.date('c').'</lastmod><priority>1</priority></url>';
for ($i=2; $i <= 10; $i++) {
  $xml_content .= '<url><loc>https://stroiman.ru/doma-'.$i.'mln/</loc><lastmod>'.date('c').'</lastmod><priority>1</priority></url>';
}
$xml_content .= '<url><loc>https://stroiman.ru/doma-15mln/</loc><lastmod>'.date('c').'</lastmod><priority>1</priority></url>';

$arPages = ['doma-iz-dereva','doma-iz-brusa','dom-iz-kleennogo-brusa','dom-iz-profilirovannnogo-brusa','dom-iz-brevna','karkasnye-doma','karkasno-shchitovye-doma','doma-iz-sip-paneley','kamennye-doma','doma-s-kryshei-iz-metallocherepici','doma-s-kryshei-iz-profnastila','doma-s-kryshei-iz-ondulina','doma-s-kryshei-iz-shifera','doma-s-falcevoi-kryshei','doma-s-kryshei-iz-cherepici','doma-s-kryshei-iz-keramicheskoy-cherepici','doma-s-kryshei-iz-cem-pesch-cherepici','doma-s-kryshei-iz-gibkoy-cherepici'];

foreach ($arPages as $value) {
	$xml_content .= '<url><loc>https://stroiman.ru/'.$value.'/</loc><lastmod>'.date('c').'</lastmod><priority>1</priority></url>';
}

$arPropNew = ['STYLE','FORM','TYPE_ROOF','ANNEX','FINISHING_FACADE','INSULATION','FOUNDATION_LIST','CLASS','SIZE_BEAM','FINISHING'];
foreach ($arPropNew as $propCode)
{
	$rsPropertyEnum = \CIBlockPropertyEnum::GetList([],["IBLOCK_ID"=>3, "CODE"=>$propCode]);
	while($arPropertyEnum = $rsPropertyEnum->Fetch())
		$xml_content .= '<url><loc>https://stroiman.ru/'.$arPropertyEnum['XML_ID'].'/</loc><lastmod>'.date('c').'</lastmod><priority>1</priority></url>';

}

// запись в файл sitemap-tegi-pages.xml
$fp = fopen($_SERVER["DOCUMENT_ROOT"].'/sitemap-seo-pages.xml', 'w+');
$xml = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'.$xml_content.'</urlset>';
fwrite($fp,$xml);
fclose($fp);
unset($xml_content);
