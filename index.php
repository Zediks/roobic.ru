<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Строители");
$APPLICATION->SetPageProperty("title", "СТРОЙМАН - строительство домов и бань от проверенных застройщиков в Москве и Московской области");
$APPLICATION->SetPageProperty("description", "Выбор лучших компаний по строительству и отделке домов и бань, обустройству участков в Москве и Московской области ★ Только проверенные застройщики ★ Проекты, цены, отзывы о подрядчиках ★★★ Stroiman.ru");?>
<div class="index-screen-block">
  <div class="container-fluid">
    <div class="index-screen">
      <div class="container-fluid">
        <h1 class="title">Подобрать проект дома</h1>
        <div class="index-screen-filter">
          <?require_once $_SERVER["DOCUMENT_ROOT"].'/local/inc/filterHouses.php';?>
        </div>
        <div class="desc">Выбрать дом для строительства ИЖС теперь просто. На Стройман.ру укажите важные для себя параметры и найдите дом мечты!</div>
      </div>
    </div>
  </div>
</div>
<section class="company-list-block pb-0">
  <div class="container-fluid">
    <h2 class="title title--black title--small">Проекты домов</h2>
    <?$APPLICATION->IncludeComponent(
      "bitrix:news.list",
      "doma",
      Array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "DISPLAY_DATE" => "N",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "FIELD_CODE" => array("",""),
        "FILTER_NAME" => "arFilterHouse",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => "3",
        "IBLOCK_TYPE" => "content",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "INCLUDE_SUBSECTIONS" => "Y",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "3",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "roobic",
        "PAGER_TITLE" => "Новости",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "PROPERTY_CODE" => array("TYPE","COMPANY",""),
        "SET_BROWSER_TITLE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_BY2" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_ORDER2" => "ASC",
        "STRICT_SECTION_CHECK" => "N"
      )
     );?>
     <div class="company-list-bottom">
		   <a class="show-all" href="/doma/">Все проекты</a>
	   </div>
  </div>
</section>
<section class="company-list-block">
  <div class="container-fluid">
    <?$APPLICATION->IncludeComponent("bitrix:news.list", "indexBuilders", Array(
    	"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
    		"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
    		"AJAX_MODE" => "N",	// Включить режим AJAX
    		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
    		"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
    		"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
    		"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
    		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
    		"CACHE_GROUPS" => "Y",	// Учитывать права доступа
    		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
    		"CACHE_TYPE" => "A",	// Тип кеширования
    		"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
    		"DETAIL_URL" => "/stroiteli/#ELEMENT_CODE#/",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
    		"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
    		"DISPLAY_DATE" => "N",	// Выводить дату элемента
    		"DISPLAY_NAME" => "Y",	// Выводить название элемента
    		"DISPLAY_PICTURE" => "N",	// Выводить изображение для анонса
    		"DISPLAY_PREVIEW_TEXT" => "N",	// Выводить текст анонса
    		"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
    		"FIELD_CODE" => array(	// Поля
    			0 => "PREVIEW_PICTURE",
    			1 => "",
    		),
    		"FILTER_NAME" => "",	// Фильтр
    		"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
    		"IBLOCK_ID" => "2",	// Код информационного блока
    		"IBLOCK_TYPE" => "content",	// Тип информационного блока (используется только для проверки)
    		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
    		"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
    		"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
    		"NEWS_COUNT" => "9",	// Количество новостей на странице
    		"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
    		"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
    		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
    		"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
    		"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
    		"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
    		"PAGER_TITLE" => "Новости",	// Название категорий
    		"PARENT_SECTION" => "",	// ID раздела
    		"PARENT_SECTION_CODE" => "",	// Код раздела
    		"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
    		"PROPERTY_CODE" => array(	// Свойства
    			0 => "PRICE",
    			1 => "SERVICES",
    			2 => "",
    		),
    		"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
    		"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
    		"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
    		"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
    		"SET_STATUS_404" => "N",	// Устанавливать статус 404
    		"SET_TITLE" => "N",	// Устанавливать заголовок страницы
    		"SHOW_404" => "N",	// Показ специальной страницы
    		"SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
    		"SORT_BY2" => "ID",	// Поле для второй сортировки новостей
    		"SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
    		"SORT_ORDER2" => "DESC",	// Направление для второй сортировки новостей
    		"STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа списка
    	),
    	false
    );?>
  </div>
</section>
<section class="about-project">
  <div class="container-fluid">
    <h5 class="title title--black title--small">О проекте</h5>
    <div class="row">
      <div class="col-xl-9">
        <p>На&nbsp;сайте stroiman.ru собраны проверенные подрядчики по&nbsp;строительству и&nbsp;отделке домов, обустройству участков. Каждая компания проходит проверку на&nbsp;юридическую чистоту и&nbsp;выполненные заказы. Мы&nbsp;связываемся с&nbsp;клиентами и&nbsp;узнаем их&nbsp;мнение по&nbsp;качеству работ, точности сметы и&nbsp;выполнению сроков. Вы&nbsp;можете выбрать любую компанию, которая вам понравилась и&nbsp;заказать необходимые работы. Обратите внимание на&nbsp;выполненные работы компании и&nbsp;возможность выезда на&nbsp;объект, чтобы посмотреть вживую. Не&nbsp;маловажным фактором являются отзывы о&nbsp;компании и&nbsp;юридическая информация.</p>
      </div>
    </div>
    <div class="row about-list">
      <div class="col-md-6 col-xl-3 order-xl-1">
        <div class="about-item">
          <div class="about-item__icon">
            <svg class="icon">
              <use xlink:href="#icon-about_01"> </use>
            </svg>
          </div>
          <div class="about-item__number">01 </div>
          <p>Выбираете одного или нескольких подрядчиков</p>
        </div>
      </div>
      <div class="col-md-6 col-xl-3 order-xl-2">
        <div class="about-item">
          <div class="about-item__icon">
            <svg class="icon">
              <use xlink:href="#icon-about_02"> </use>
            </svg>
          </div>
          <div class="about-item__number">02 </div>
          <p>Создаете задание для выбранных исполнителей </p>
        </div>
      </div>
      <div class="col-md-6 col-xl-3 order-md-last order-xl-3">
        <div class="about-item">
          <div class="about-item__icon">
            <svg class="icon">
              <use xlink:href="#icon-about_03"> </use>
            </svg>
          </div>
          <div class="about-item__number">03 </div>
          <p>Рассматриваете предложения и выбираете исполнителя </p>
        </div>
      </div>
      <div class="col-md-6 col-xl-3 order-xl-4">
        <div class="about-item">
          <div class="about-item__icon">
            <svg class="icon">
              <use xlink:href="#icon-about_04"> </use>
            </svg>
          </div>
          <div class="about-item__number">04</div>
          <p>Строители выполняют работу, а вы принимаете заказ</p>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- <section class="partners-block">
  <div class="container-fluid">
    <h5 class="title title--black title--small">Строители, которым доверяют наши клиенты</h5>
    <div class="partners-list swiper-container">
      <div class="swiper-wrapper">
        <div class="swiper-slide">
          <div class="partners-item"><img class="lazyload" data-src="<?=SITE_TEMPLATE_PATH?>/images/partners/l_01.png" alt="" /></div>
          <div class="partners-item"><img class="lazyload" data-src="<?=SITE_TEMPLATE_PATH?>/images/partners/l_06.png" alt="" /></div>
        </div>
        <div class="swiper-slide">
          <div class="partners-item"><img class="lazyload" data-src="<?=SITE_TEMPLATE_PATH?>/images/partners/l_02.png" alt="" /></div>
          <div class="partners-item"><img class="lazyload" data-src="<?=SITE_TEMPLATE_PATH?>/images/partners/l_05.png" alt="" /></div>
        </div>
        <div class="swiper-slide">
          <div class="partners-item"><img class="lazyload" data-src="<?=SITE_TEMPLATE_PATH?>/images/partners/l_03.png" alt="" /></div>
          <div class="partners-item"><img class="lazyload" data-src="<?=SITE_TEMPLATE_PATH?>/images/partners/l_04.png" alt="" /></div>
        </div>
        <div class="swiper-slide">
          <div class="partners-item"><img class="lazyload" data-src="<?=SITE_TEMPLATE_PATH?>/images/partners/l_04.png" alt="" /></div>
          <div class="partners-item"><img class="lazyload" data-src="<?=SITE_TEMPLATE_PATH?>/images/partners/l_03.png" alt="" /></div>
        </div>
        <div class="swiper-slide">
          <div class="partners-item"><img class="lazyload" data-src="<?=SITE_TEMPLATE_PATH?>/images/partners/l_05.png" alt="" /></div>
          <div class="partners-item"><img class="lazyload" data-src="<?=SITE_TEMPLATE_PATH?>/images/partners/l_02.png" alt="" /></div>
        </div>
        <div class="swiper-slide">
          <div class="partners-item"><img class="lazyload" data-src="<?=SITE_TEMPLATE_PATH?>/images/partners/l_06.png" alt="" /></div>
          <div class="partners-item"><img class="lazyload" data-src="<?=SITE_TEMPLATE_PATH?>/images/partners/l_01.png" alt="" /></div>
        </div>
        <div class="swiper-slide">
          <div class="partners-item"><img class="lazyload" data-src="<?=SITE_TEMPLATE_PATH?>/images/partners/l_01.png" alt="" /></div>
          <div class="partners-item"><img class="lazyload" data-src="<?=SITE_TEMPLATE_PATH?>/images/partners/l_06.png" alt="" /></div>
        </div>
        <div class="swiper-slide">
          <div class="partners-item"><img class="lazyload" data-src="<?=SITE_TEMPLATE_PATH?>/images/partners/l_02.png" alt="" /></div>
          <div class="partners-item"><img class="lazyload" data-src="<?=SITE_TEMPLATE_PATH?>/images/partners/l_05.png" alt="" /></div>
        </div>
        <div class="swiper-slide">
          <div class="partners-item"><img class="lazyload" data-src="<?=SITE_TEMPLATE_PATH?>/images/partners/l_03.png" alt="" /></div>
          <div class="partners-item"><img class="lazyload" data-src="<?=SITE_TEMPLATE_PATH?>/images/partners/l_04.png" alt="" /></div>
        </div>
        <div class="swiper-slide">
          <div class="partners-item"><img class="lazyload" data-src="<?=SITE_TEMPLATE_PATH?>/images/partners/l_04.png" alt="" /></div>
          <div class="partners-item"><img class="lazyload" data-src="<?=SITE_TEMPLATE_PATH?>/images/partners/l_03.png" alt="" /></div>
        </div>
        <div class="swiper-slide">
          <div class="partners-item"><img class="lazyload" data-src="<?=SITE_TEMPLATE_PATH?>/images/partners/l_05.png" alt="" /></div>
          <div class="partners-item"><img class="lazyload" data-src="<?=SITE_TEMPLATE_PATH?>/images/partners/l_02.png" alt="" /></div>
        </div>
        <div class="swiper-slide">
          <div class="partners-item"><img class="lazyload" data-src="<?=SITE_TEMPLATE_PATH?>/images/partners/l_06.png" alt="" /></div>
          <div class="partners-item"><img class="lazyload" data-src="<?=SITE_TEMPLATE_PATH?>/images/partners/l_01.png" alt="" /></div>
        </div>
      </div>
    </div>
    <div class="company-list-bottom">
      <div class="swiper-btn">
        <div class="swiper-button-prev js-btns">
          <svg class="icon">
            <use xlink:href="#icon-arrow_work"></use>
          </svg>
        </div>
        <div class="swiper-button-next js-btns">
          <svg class="icon">
            <use xlink:href="#icon-arrow_work"></use>
          </svg>
        </div>
      </div>
    </div>
  </div>
</section> -->
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
